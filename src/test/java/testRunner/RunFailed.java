package testRunner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
    monochrome = true,
   features = "@target/rerun.txt",
   glue = {"stepDefinition","hooks" }, 
   plugin  = {"pretty",
            "json:target/cucumber.json", "html:target/MatrixQAReport.html", "rerun:target/rerun.txt"}
  )
public class RunFailed {

}
