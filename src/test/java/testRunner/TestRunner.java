package testRunner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/Features/", 
glue = {"stepDefinition","hooks" },
monochrome = true, dryRun = false,
plugin = { "pretty", "json:target/cucumber.json",
				"junit:target/cucumber.xml", 
				"html:target/MatrixQAReport.html", "rerun:target/rerun.txt"}, 
publish = false, tags = " ")
public class TestRunner {
}

/*
 *  @FreeQty or @mainLine or @WithAccessory or @WithUpsell or @PriceAndCSRHold or @QuoteOrder
 * 
 * 
 * 
 */