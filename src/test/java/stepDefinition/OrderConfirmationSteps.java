package stepDefinition;

import static base.BaseClass.CustProd;
import static base.BaseClass.manageOrder;
import static base.BaseClass.orderconf;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import base.BaseClass;
import hooks.Hooks;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class OrderConfirmationSteps {
	Map<String, String> map = new HashMap<String, String>();	
	int rownumber  = BaseClass.excel.e.getRowCount("MatrixOrder") + 1;
	int rownumber2 = BaseClass.excel.e.getRowCount("OrdersForModification") + 1;
	int page = 0;
	String addresLine1, shippingCharge;
	
	
	@Then("User click on select different BillTo button")
	public void user_clicks_on_select_different_bill_to_button() throws InterruptedException {
		 BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.orderconf.salesRepHeader));
		 addresLine1 = orderconf.billToAddLine1.getAttribute("textContent");
		 Thread.sleep(5000);
		orderconf.differentBillTo.click();
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(orderconf.frameBillAndShipTo));

	}

	@Then("User click on select different ShipTo button")
	public void user_clicks_on_select_different_ship_to_button() {
		addresLine1 = orderconf.shipToAddLine1.getAttribute("textContent");
		orderconf.differentShipTo.click();
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(orderconf.frameBillAndShipTo));
		
	}
	@Then("User click on Add New BillTo/ShipTo button")
	public void user_clicks_on_add_new_bill_to_button() {
		if(Hooks.sceanrioName.contains("Create New BillTo Address")) {
			orderconf.newBillToAddress.click();
		} else if (Hooks.sceanrioName.contains("Create New ShipTo Address")) {
			orderconf.newShipToAddress.click();
		}

	}


	@Then("User enters the address details {string}")
	public void user_enters_the_address_details(String primary, List<Map<String, String>> address) {
		if (Hooks.sceanrioName.contains("Edit") || Hooks.sceanrioName.contains("Update")) {
			orderconf.addLine1.clear();
			orderconf.addLine2.clear();
			orderconf.city.clear();
			orderconf.postalCode.clear();
			orderconf.county.clear();
			orderconf.province.clear();
			orderconf.state.clear();
			if (primary.equalsIgnoreCase("YES") && !BaseClass.getDriver().findElement(By.xpath("//label[contains(@for,'_PRIMARY_FLG_0')]//preceding-sibling::input")).isSelected()) {
				orderconf.markPrimary.click();
			}
		}
		BaseClass.select.selectByVisibleText(orderconf.selectCountry,address.get(0).get("country") );
		orderconf.addLine1.sendKeys(address.get(0).get("addline1") + Hooks.randomInt);
		orderconf.addLine2.sendKeys(address.get(0).get("addline2"));
		orderconf.city.sendKeys(address.get(0).get("city"));
		orderconf.postalCode.sendKeys(address.get(0).get("postal") + Hooks.randomInt);
		orderconf.county.sendKeys(address.get(0).get("county"));
		orderconf.province.sendKeys(address.get(0).get("prov"));
		orderconf.state.sendKeys(address.get(0).get("state"));
		if (Hooks.sceanrioName.contains("Create")) {
			if (primary.equalsIgnoreCase("YES")) {
				orderconf.markPrimary.click();

			}
		}
	}

	@Then("User enters the contact information")
	public void user_enters_the_contact_information(List<Map<String, String>> contact) {
		
		if (Hooks.sceanrioName.contains("Edit") || Hooks.sceanrioName.contains("Update")) {
			orderconf.firstName.clear();
			orderconf.lastName.clear();
			orderconf.email.clear();
			orderconf.phone.clear();
			orderconf.mobile.clear();
			orderconf.fax.clear();
		}
		orderconf.firstName.sendKeys(contact.get(0).get("firstname")+Hooks.randomInt);
		orderconf.lastName.sendKeys(contact.get(0).get("lastname"));
		orderconf.email.sendKeys(Hooks.randomInt+contact.get(0).get("email"));
		orderconf.phone.sendKeys(contact.get(0).get("phone"));
		orderconf.mobile.sendKeys(contact.get(0).get("mobile")+Hooks.randomInt);
		orderconf.fax.sendKeys(contact.get(0).get("fax")+Hooks.randomInt);

	}
	 @Then ("User click on create button to create new address with primary {string}")
	 public void User_clicks_on_create_button_to_create_new_address(String primary) throws InterruptedException {
	        
			orderconf.createBtn.click();
			Thread.sleep(5000);
			orderconf.verifyAddress(primary, Hooks.randomInt);
		}
	 
	 @Then ("User click on Add BillTo/ShipTo conatct")
	 public void user_click_on_add_shipTo_conatct() {
		 orderconf.addContact.click();
	 }
	 
	 
	 @Then ("User click on create to add new contact")
	 public void user_click_on_create_to_add_new_contact() throws InterruptedException {
		 orderconf.createBtn.click();
		 BaseClass.getDriver().switchTo().defaultContent();
		 Thread.sleep(10000);
		 if(Hooks.sceanrioName.contains("BillTo")) {
			 orderconf.differentBillTo.click();
		 }
		 else if(Hooks.sceanrioName.contains("ShipTo")){
			 orderconf.differentShipTo.click();
		 }
		 
		 BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(orderconf.frameBillAndShipTo));
	    orderconf.verifyContact(Hooks.randomInt);
		if(Hooks.sceanrioName.contains("Without Email")) {
			orderconf.verifyNoEmail(Hooks.randomInt);
			
		}
	 }
	 @Then ("User click on edit ShipTo/BillTo conatct")
	 public void user_click_on_edit_shipTo_conatct() {
		 orderconf.editContact.click();
		 BaseClass.getDriver().switchTo().defaultContent();
		 BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(orderconf.frameUpdateContact));

	 } 
	 @Then ("User click on apply changes button to update contact")
	 public void user_click_on_apply_changes_button_to_update_contact() throws InterruptedException {
		 orderconf.applychg.click();	
	     BaseClass.getDriver().switchTo().defaultContent();     
		 BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(orderconf.frameBillAndShipTo));
		 Thread.sleep(5000);
		 orderconf.verifyContact(Hooks.randomInt);
	 }
	 
	 	 
	 @Then("User select BillTo/ShipTo Address and click on save")
	 public void user_select_BillTo_address_and_click_on_save() throws InterruptedException {
			Iterator<WebElement> address = orderconf.newAddressLine1.iterator();
			int count = 0;
			int pagecount=0;
			String temp ="";
			while (address.hasNext()) {
				WebElement tempElement = address.next();
				BaseClass.wait.until(ExpectedConditions.visibilityOf(tempElement));
				if (!tempElement.getText().contains(addresLine1)) {
					 temp = tempElement.getText();
					 tempElement.findElement(By.xpath("//td[text()='"+temp+"']//preceding-sibling::td/input[@type='radio']")).click();
					 orderconf.saveBtn.click();
			         BaseClass.getDriver().switchTo().defaultContent();
					break;
				}
				count++;
				if (count == orderconf.newAddressLine1.size()) {
					orderconf.nextButton.click();
					pagecount += count;
					count = 0;
					Thread.sleep(4000);
					address = orderconf.newAddressLine1.iterator();
					if (pagecount == BaseClass.manageOrder.getTotalLine()) {
						Assert.fail("Unique addressLine1 is not found");
					}
					
				}
			}
			if (Hooks.sceanrioName.contains("BillTo")) {
				BaseClass.wait.until(ExpectedConditions.textToBePresentInElement(
						BaseClass.wait.until(ExpectedConditions.visibilityOf(orderconf.billToAddLine1)), temp));
				Assert.assertNotEquals("Changed AddressLine1 not matching with selected", addresLine1,
						orderconf.billToAddLine1.getAttribute("textContent"));
			} else if (Hooks.sceanrioName.contains("ShipTo")) {
				BaseClass.wait.until(ExpectedConditions.textToBePresentInElement(
						BaseClass.wait.until(ExpectedConditions.visibilityOf(orderconf.shipToAddLine1)), temp));
				Assert.assertNotEquals("Changed AddressLine1 not matching with selected", addresLine1,
						orderconf.shipToAddLine1.getAttribute("textContent"));
			}
		}

	@Then("User click on edit address button")
	public void user_clicks_on_edit_address_button_in_order_summary_frame() {		
 	   orderconf.editAddress.click();
		BaseClass.wait.until(ExpectedConditions.visibilityOf(orderconf.applychg));
	}

	@Then("User click on apply changes button to update address with primary {string}")
	public void user_clicks_on_apply_changes_button_of_update_bill_to_frame(String primary) throws InterruptedException {
		orderconf.applychg.click();
	//	Thread.sleep(5000);
         BaseClass.getDriver().switchTo().defaultContent();
		if (Hooks.sceanrioName.contains("BillTo")) {
			user_clicks_on_select_different_bill_to_button();
		}
		else if (Hooks.sceanrioName.contains("ShipTo")) {
			user_clicks_on_select_different_ship_to_button();
		}
		orderconf.verifyAddress(primary, Hooks.randomInt);
	}


	 @Then ("User Edit BillTo email")
	 public void user_edit_billTo_email() {
		 orderconf.editEmailBtn.click();
		 BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
		 orderconf.editEmail.clear();
		 orderconf.editEmail.sendKeys(Hooks.randomInt+"@email.com");
		 orderconf.updateBtn.click();
		 BaseClass.getDriver().switchTo().defaultContent();
		 Assert.assertTrue("Bill To Email not updated",
				 BaseClass.wait.until(ExpectedConditions.textToBePresentInElement(orderconf.billToEmail, 
						 Hooks.randomInt+"@email.com")));
		 
	 }
	
	@Then("User click on the specification button of the product")
	public void user_click_on_the_specification_button_of_the_product() throws InterruptedException {
		shippingCharge = orderconf.shippingCharge.getAttribute("textContent");
		Thread.sleep(5000);
		BaseClass.js.click(BaseClass.wait.until(ExpectedConditions.visibilityOf(orderconf.specificationsBtn)));
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(orderconf.frameProductSpecifications));
		Thread.sleep(5000);
	}


	@Then("User select the shipment priority {string}")
	public void user_select_the_shipment_priority(String shipment) throws InterruptedException {
		if(shipment.equalsIgnoreCase("EXPRESS") && !orderconf.expressShipment.isSelected()) {
			orderconf.expressShipment.click();
		}else if(shipment.equalsIgnoreCase("STANDARD") && !orderconf.standardShipment.isSelected()) {
			orderconf.standardShipment.click();
		}else {
		   Assert.fail("SHIPMENT PRIORTY ALREADY SELECTED OR SOMETHING WRONG WITH TEXT!");
		}
	}


	@Then("User click on the save button")
	public void user_clicks_on_the_save_button() throws InterruptedException {
		orderconf.saveBtn.click();
	}


	@Then("verify the current hold is released/applied {string}")
	public void verify_the_current_hold_is_applied_or_not(String holdname) throws InterruptedException {
		Thread.sleep(5000);
		user_click_on_the_specification_button_of_the_product();
		String[] hold = holdname.split(",");
		for (String h : hold) {	
			if(Hooks.sceanrioName.toUpperCase().contains("Remove hold".toUpperCase())) {
				Assert.assertTrue(h + " Hold is not Removed in current hold list",
						!manageOrder.currentAppliedHold.getAttribute("textContent").contains(h.toUpperCase()));
				Assert.assertTrue("No Holds not present in current hold list",
						manageOrder.currentAppliedHold.getAttribute("textContent").contains("No Holds"));
			}	
			else{
				assertTrue("Holds not applied or matching. ",
						orderconf.currentHolds.getAttribute("textContent").contains(h.toUpperCase()));
			}
		}
		BaseClass.orderconf.saveBtn.click();
		BaseClass.getDriver().switchTo().defaultContent();
		try {
			CustProd.okBtn.click();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Then("Changes should be added {string} and {string} and {string}")
	public void changes_should_be_added(String priority, String holdname, String upsell) throws InterruptedException {
		BaseClass.getDriver().switchTo().defaultContent();
		Thread.sleep(5000);
		user_click_on_the_specification_button_of_the_product();
		String[] hold = holdname.split(",");
		for (String h : hold) {
			assertTrue("Holds not applied or matching. ",
					orderconf.currentHolds.getAttribute("textContent").contains(h.toUpperCase()));
		}
		user_select_the_shipment_priority(priority);
		Assert.assertFalse(BaseClass.CustProd.internalNotes.getText().isEmpty());
		BaseClass.orderconf.saveBtn.click();
		BaseClass.getDriver().switchTo().defaultContent();
		Thread.sleep(5000);
		if(priority.equalsIgnoreCase("EXPRESS")) {
			Assert.assertEquals("Selected priority not matched", priority.toUpperCase(), orderconf.shipmentPriority.getAttribute("textContent"));
		}
		else Assert.assertEquals("Selected priority not matched", priority.toUpperCase(), orderconf.shipmentPriority.getAttribute("textContent"));
	   
		if(upsell.equalsIgnoreCase("YES")) {
	    	Assert.assertEquals("Upsell Line is missing", "UPSELL", orderconf.upsellLine.getText());
	    }
	}

	@Then("User select the free quantity {string}")
	public void user_select_the_free_quantity(String freeQty) {
		BaseClass.select.selectByVisibleText(orderconf.selectfreeQty, freeQty);
		 Assert.assertFalse("Discount is Enabled", orderconf.selectDiscount.isEnabled());
	}
	
	
	@Then("Free Line should be added with {string} and {string}")
	public void free_lineshould_be_added(String priority, String holdname) throws InterruptedException {
		BaseClass.getDriver().switchTo().defaultContent();
		Thread.sleep(5000);
		user_click_on_the_specification_button_of_the_product();
		String[] hold = holdname.split(",");
		for (String h : hold) {
			assertTrue("Holds not applied or matching. ",
					orderconf.currentHolds.getAttribute("textContent").contains(h.toUpperCase()));
		}
		user_select_the_shipment_priority(priority);
		Assert.assertFalse(BaseClass.CustProd.internalNotes.getText().isEmpty());
		BaseClass.orderconf.saveBtn.click();
		BaseClass.getDriver().switchTo().defaultContent();
		Thread.sleep(5000);
		Assert.assertEquals("EXTRA_FREE Line is missing", "EXTRA_FREE", orderconf.freeLine.getAttribute("textContent"));
		if(priority.equalsIgnoreCase("EXPRESS")) {
			Assert.assertEquals("Selected priority not matched", priority.toUpperCase(), orderconf.shipmentPriority.getAttribute("textContent"));
		}
		else Assert.assertEquals("Selected priority not matched", priority.toUpperCase(), orderconf.shipmentPriority.getAttribute("textContent"));	      
	}
	
	@Then("User select the discount {string}")
	public void user_select_the_discount(String discount) {
		BaseClass.select.selectByVisibleText(orderconf.selectDiscount, discount);
		 Assert.assertFalse("Free Quantity is Enabled", orderconf.selectfreeQty.isEnabled());
	}
	
	@Then("Discount Line should be added with {string} and {string} and {string}")
	public void discount_line_should_be_added(String priority, String holdname, String discount) throws InterruptedException {
		BaseClass.getDriver().switchTo().defaultContent();
		Thread.sleep(5000);
		user_click_on_the_specification_button_of_the_product();
		String[] hold = holdname.split(",");
		for (String h : hold) {
			assertTrue("Holds not applied or matching. ",
					orderconf.currentHolds.getAttribute("textContent").contains(h.toUpperCase()));
		}
		user_select_the_shipment_priority(priority);
		Assert.assertFalse(BaseClass.CustProd.internalNotes.getText().isEmpty());
		BaseClass.orderconf.saveBtn.click();
		BaseClass.getDriver().switchTo().defaultContent();
		Thread.sleep(5000);
		Assert.assertEquals("Discount is missing", discount.replaceFirst("%", ""), orderconf.lineDiscountPercentage.getAttribute("textContent"));
		Assert.assertEquals("Discount is not matching with line", orderconf.cartDiscount.getText(), orderconf.lineDiscount.getText().replaceFirst("€", "").trim());
		
		if(priority.equalsIgnoreCase("EXPRESS")) {
			Assert.assertEquals("Selected priority not matched", priority.toUpperCase(), orderconf.shipmentPriority.getAttribute("textContent"));
		}
		else Assert.assertEquals("Selected priority not matched", priority.toUpperCase(), orderconf.shipmentPriority.getAttribute("textContent"));	   
	    
	   
	}
	
	
	
	@Then("User removes a line item {string}")
	public void user_removes_a_line_item_from(String line) {
	int lines = orderconf.totalLines.size();
	orderconf.removeLine(line);
	BaseClass.CustProd.okBtn.click();
	Assert.assertEquals(lines-1,orderconf.totalLines.size()-1);
		
	}

	@Then("User click on product name and confirm the Imprint {string}")
	public void user_click_on_product_name_and_confirm_the_imprint(String addImprint) throws InterruptedException {
	    	 try {
				CustProd.okBtn.click();
			} catch (Exception e) {
				e.getMessage();
			}
			if (addImprint.equalsIgnoreCase("yes")) {
				BaseClass.orderconf.imprintLogoConfAndValid();
				BaseClass.getDriver().switchTo().defaultContent();
				try {
					CustProd.okBtn.click();
				} catch (Exception e) {
					e.getMessage();
				}
			}
		}
	
	@Then("Validate additional charges line quantity is equal to main line + Upsell Qty")
	public void validate_additional_charges_line_quantity_is_equal_to_main_line_upsell_qty() {
		int mainLineQty = Integer.valueOf(BaseClass.wait.until(ExpectedConditions.visibilityOf(orderconf.mainLineQty)).getText());
		int AddChrgLineQty = Integer.valueOf(BaseClass.wait.until(ExpectedConditions.visibilityOf(orderconf.additionalChargesLineQty)).getText());
		int upsellQty = Integer.valueOf(orderconf.upsellQty.getText());
		Assert.assertTrue("Mismatch found in Main line Qty : " + mainLineQty + " and Additional Charges line Qty : " + AddChrgLineQty, (AddChrgLineQty == mainLineQty + upsellQty) );
	}
	@Then("Validate additional charges line quantity and main line quantity are same")
	public void validate_additional_charges_line_quantity_and_main_line_quantity_are_same() {
		
			String mainLineQty = BaseClass.wait.until(ExpectedConditions.visibilityOf(orderconf.mainLineQty)).getText();
			String AddChrgLineQty = BaseClass.wait.until(ExpectedConditions.visibilityOf(orderconf.additionalChargesLineQty)).getText();
			Assert.assertTrue("Mismatch found in Main line Qty : " + mainLineQty + " and Additional Charges line Qty : " + AddChrgLineQty, (mainLineQty.trim().contentEquals(AddChrgLineQty.trim()) ) );
	}
	@Then("Validate the Template {string}")	
	public void validate_Template(String newTemplate) throws InterruptedException {
		
	    Thread.sleep(7000);	
		BaseClass.wait.until(ExpectedConditions.elementToBeClickable(BaseClass.orderconf.viewImprint)).click();
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
		Assert.assertTrue("Template is not present or not matched with uploaded",KeyCodeProductSteps.template_Url.contains(BaseClass.orderconf.templateUrl.getAttribute("href")));
		BaseClass.orderconf.add_change_template.click();			
		BaseClass.CustProd.templateName.sendKeys(newTemplate);
		BaseClass.CustProd.searchTemplate.click();			
		Thread.sleep(5000);		
		BaseClass.CustProd.selectTemplate.click();		
		BaseClass.getDriver().switchTo().defaultContent();		
		CustProd.okBtn.click();
		Thread.sleep(4000);
		CustProd.okBtn.click();
		BaseClass.wait.until(ExpectedConditions.elementToBeClickable(BaseClass.orderconf.viewImprint)).click();	
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
		Assert.assertFalse("Template Not Changed", BaseClass.orderconf.templateUrl.getAttribute("href").contains(KeyCodeProductSteps.template_Url));
		BaseClass.orderconf.confImprint.click();
		BaseClass.getDriver().switchTo().defaultContent();	
		CustProd.okBtn.click();
	}
	@When("User enter saleRep {string} and select orderType {string} and select paymentMethod {string} and select legitInterest {string} and select callOption {string} and select mailOption {string} and select rentOption {string} and select EmailOption {string} and click PlaceOrder")
	public void i_enter_sale_rep_and_select_order_type_and_select_payment_method_and_select_legit_interest_and_select_call_option_and_select_mail_option_and_select_rent_option_and_select_email_option_and_click_place_order(
			String saleRep, String orderType, String paymentMethod, String legitInterest, String callOption,
			String mailOption, String rentOption, String EmailOption) throws InterruptedException {
		 
		BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.orderconf.paymentMethod));
		Thread.sleep(3000);
		
		if(BaseClass.orderconf.billToEmail.getAttribute("textContent").equals("-")) {		
			BaseClass.select.selectByVisibleText(BaseClass.orderconf.noEmailReason, "Customer does not have an email");
		}
		try {
			if(!orderconf.salesRepHeader.getAttribute("textContent").contains(",")){
				BaseClass.orderconf.salesRep.click();
				BaseClass.orderconf.salesRep.sendKeys(saleRep);
				//BaseClass.action.contextClick(BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.orderconf.saleRepselect)));
				BaseClass.orderconf.salesRep.sendKeys(Keys.ENTER);;
				BaseClass.wait.until(ExpectedConditions.attributeToBeNotEmpty(BaseClass.orderconf.salesRepEmail,"value"));
				map.put("Salesperson",saleRep);
			}else {
				map.put("Salesperson", orderconf.salesRepHeader.getText());
			}
		} catch (Exception e2) {
			map.put("Salesperson",saleRep);
			System.out.println(e2.getCause());
	
		}
		BaseClass.select.selectByVisibleText(BaseClass.orderconf.orderType, orderType.toUpperCase());
		if (!paymentMethod.isEmpty()) {
			BaseClass.select.selectByVisibleText(BaseClass.orderconf.paymentMethod, paymentMethod);
		}
		   if(!BaseClass.orderconf.customerCountry.getAttribute("textContent").contains("US") && !BaseClass.orderconf.customerCountry.getAttribute("textContent").contains("CA")) {
			   if(Hooks.sceanrioName.contains("GDPR legitimate value NO")) {
				   BaseClass.select.selectByVisibleText(BaseClass.orderconf.legitInterest, legitInterest.toUpperCase());
					Assert.assertEquals("NO", BaseClass.select.getFirstSelectedOption(BaseClass.orderconf.callOption).getText().toUpperCase());
					Assert.assertEquals("NO", BaseClass.select.getFirstSelectedOption(BaseClass.orderconf.mailOption).getText().toUpperCase());
					Assert.assertEquals("NO", BaseClass.select.getFirstSelectedOption(BaseClass.orderconf.rentOption).getText().toUpperCase());
					Assert.assertEquals("NO",BaseClass.select.getFirstSelectedOption(BaseClass.orderconf.emailOption).getText().toUpperCase());
				} else if(Hooks.sceanrioName.contains("GDPR value - N/A")){
					  BaseClass.select.selectByVisibleText(BaseClass.orderconf.legitInterest, legitInterest.toUpperCase());
					  Thread.sleep(4000);
					Assert.assertEquals("N/A", BaseClass.select.getFirstSelectedOption(BaseClass.orderconf.callOption).getText().toUpperCase());
					Assert.assertEquals("N/A", BaseClass.select.getFirstSelectedOption(BaseClass.orderconf.mailOption).getText().toUpperCase());
					Assert.assertEquals("N/A", BaseClass.select.getFirstSelectedOption(BaseClass.orderconf.rentOption).getText().toUpperCase());
					Assert.assertEquals("N/A",BaseClass.select.getFirstSelectedOption(BaseClass.orderconf.emailOption).getText().toUpperCase());
				}
		          else { 
			BaseClass.select.selectByVisibleText(BaseClass.orderconf.legitInterest, legitInterest.toUpperCase());
			Thread.sleep(4000);
			BaseClass.select.selectByVisibleText(BaseClass.orderconf.callOption, callOption.toUpperCase());
			BaseClass.select.selectByVisibleText(BaseClass.orderconf.mailOption, mailOption.toUpperCase());
			BaseClass.select.selectByVisibleText(BaseClass.orderconf.rentOption, rentOption.toUpperCase());
			BaseClass.select.selectByVisibleText(BaseClass.orderconf.emailOption, EmailOption.toUpperCase());		
		   }
		}
		BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.orderconf.qouteOrder));
	    Assert.assertEquals("Tax not matched with Keycode Page", Double.valueOf(KeyCodeProductSteps.tax), Double.valueOf(BaseClass.orderconf.ordertax.getText()));	    
		Assert.assertEquals("Total Price not matched with Keycode Page", Double.valueOf(KeyCodeProductSteps.total), Double.valueOf(BaseClass.orderconf.cartTotal.getText()));
	   
	    if(BaseClass.orderconf.customerCountry.getAttribute("textContent").contains("US") || BaseClass.orderconf.customerCountry.getAttribute("textContent").contains("CA")) {
	    Assert.assertEquals("Shippment Priority not matched", KeyCodeProductSteps.shipMethodNA, orderconf.shipmentPriority.getAttribute("textContent"));
	    Assert.assertEquals("Shippment price not matched", KeyCodeProductSteps.shipCharge, orderconf.shippingCharge.getAttribute("textContent").replaceAll("[^0-9.]", "")); 	
	    }    
	    if (KeyCodeProductSteps.newLogo) {
	    	Assert.assertFalse(orderconf.getExtraCharges("Logo Charge").isEmpty());    	
	    }
	    if (KeyCodeProductSteps.setup) {
	    	Assert.assertFalse(orderconf.getExtraCharges("Setup Charge").isEmpty());
	    	if (KeyCodeProductSteps.logoAsBefore) {
	    		Assert.assertFalse(orderconf.getExtraCharges("Logo as Before Charge").isEmpty());
			}
	    }
		
	    // Sending Data to Excel for Validation
		try {
			map.put("Shipping", orderconf.lineShipping.getText().replaceAll("[^0-9.]", ""));
			map.put("DiscountPercentage", orderconf.lineDiscountPercentage.getText());
			map.put("ProductTotal", orderconf.lineProductTotal.getAttribute("value"));
		} catch (Exception e1) {
            e1.printStackTrace();
		}
		if (Hooks.sceanrioName.contains("Hold")) {
			map.put("TestName", "Validate_Applied_Holds");
	
			if (Hooks.sceanrioName.contains("price query Hold")) {
				map.put("Modifications", "Price query Hold");

			} else if (Hooks.sceanrioName.contains("Apply CSR Hold")) {
				map.put("Modifications", "CSR APPROVAL");

			} else if (Hooks.sceanrioName.contains("price query and CSR Hold")) {
				map.put("Modifications", "Price query Hold|CSR APPROVAL");
			}

		}
		else if (Hooks.sceanrioName.contains("PCT")) {
			map.put("TestName", "Validate_PCT_OrderDetails");
		} else {
			map.put("TestName", "Validate_OrderDetails_newOrder");
			map.put("S.Num", String.valueOf(rownumber - 1));
		}
		map.put("Customer", orderconf.customerName.getText());
		map.put("Customer Number", orderconf.customerNumber.getText());
		map.put("CustomerCountry", orderconf.customerCountry.getText());
		map.put("Total", orderconf.cartTotal.getAttribute("textContent"));
		map.put("Subtotal", orderconf.cartSubtotal.getAttribute("value"));
		map.put("Tax", orderconf.cartTax.getAttribute("textContent"));
		map.put("Request Date", orderconf.orderRequestDate.getAttribute("value"));
		map.put("Order Type", orderType);
		map.put("Notes", "Adding internal notes Automation");
		getLineItemDetails(orderconf.lineProductCode, "Ordered Item");
		getLineItemDetails(orderconf.lineItemPrice, "Unit Selling Price");
		getLineItemDetails(orderconf.linePackagePrice, "PackagePrice");
		getLineItemDetails(orderconf.lineType, "LineType");
		getLineItemDetails(orderconf.lineQty, "Qty");
		map.put("Environment", "r12-" + BaseClass.config.getEnv());
		BaseClass.wait.until(ExpectedConditions.elementToBeClickable(BaseClass.orderconf.qouteOrder));
		if (Hooks.sceanrioName.contains("Sample") || Hooks.sceanrioName.contains("Quote")) {
			BaseClass.wait.until(ExpectedConditions.elementToBeClickable(BaseClass.orderconf.qouteOrder)).click();
		} else {
			BaseClass.wait.until(ExpectedConditions.elementToBeClickable(BaseClass.orderconf.placeOrder)).click();

		}
		BaseClass.CustProd.okBtn.click();
		if(Hooks.sceanrioName.contains("GDPR value - N/A")){
			Assert.assertTrue("Validate the GDPRS Intrest must be Yes OR NO", BaseClass.CustProd.errorList.get(0).getText().contains("GDPR Interest must be \"YES\" or \"NO\""));
		 }	
	}
	@Then("User validate discounted logo and setup charge")
	public void user_validate_discounted_logo_and_setup() throws InterruptedException {
			BaseClass.orderconf.verifyDiscountedSetupAndLogoCharge(KeyCodeProductSteps.setupCharge,KeyCodeProductSteps.logoCharge);
		
	}
	@Then("Order should placed and order number generated")
	public void order_should_placed_and_order_number_generated() {
		BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.orderconf.manageOrder));
		map.put("MatrixOrder", BaseClass.orderconf.orderNumbers.get(1).getText());
		try {
			map.put("ABCOrderNumber", BaseClass.orderconf.orderNumbers.get(2).getText().split("[\n]")[0]);
		} catch (Exception e) {	
			e.printStackTrace();
		}	
			if(Hooks.sceanrioName.contains("OrdersForModication")) {
			 map.put("S.Num", String.valueOf(rownumber2 - 1));
			for (@SuppressWarnings("rawtypes")
			Map.Entry m : map.entrySet()){
			BaseClass.excel.e.setCellData("OrdersForModification", (String) m.getKey(),rownumber2, (String) m.getValue());
			  }
			}
			else {
			map.put("S.Num", String.valueOf(rownumber - 1));
			for (@SuppressWarnings("rawtypes")		
			Map.Entry m : map.entrySet()) {
			BaseClass.excel.e.setCellData("MatrixOrder", (String) m.getKey(), rownumber, (String) m.getValue());
			}
		}
			
	}

	@Then("Hold should be/not visible in order generation page {string}")
	public void hold_should_be_visible_in_manage_order_page(String holdname) {
		Iterator<WebElement> actualHolds = BaseClass.orderconf.headerHolds.iterator();
		Iterator<String> appliedHolds = Arrays.asList(holdname.split(",")).iterator();
		if (Hooks.sceanrioName.contains("Remove hold")) {
			while (actualHolds.hasNext()) {
				assertTrue("Hold applied on order confirmation page",
						!actualHolds.next().getText().contains(appliedHolds.next().toUpperCase()));
			}
		}
		else {
			while (appliedHolds.hasNext()) {
				assertTrue("Hold not applied on order confirmation page",
						actualHolds.next().getText().contains(appliedHolds.next().toUpperCase()));
			}

		}

	}
	@Then("User validates accessory {string} as additional charge details")
	public void validate_accessory_as_additional_charge_details(String accessory) throws InterruptedException {		
		Assert.assertEquals("Accessory name was not correct",accessory,BaseClass.orderconf.getProductName("Accessories"));
		Assert.assertEquals("Accessory quantity was not correct",KeyCodeProductSteps.accQty,BaseClass.orderconf.getProductQty("Accessories"));
		Assert.assertEquals("Accessory product code was not correct",KeyCodeProductSteps.accProductCode,BaseClass.orderconf.getProductCode("Accessories"));
		Assert.assertEquals("Accessory Total was not correct",KeyCodeProductSteps.accPrice,String.format("%.2f", Float.parseFloat(BaseClass.orderconf.getProductPrice("Accessories"))));
					
	}
	@Then("On Manage order page verify lines are created")
	public void on_manage_order_page_verify_lines_are_created() {
		BaseClass.orderconf.manageOrder.click();
		if(KeyCodeProductSteps.newLogo) {
			Assert.assertFalse(manageOrder.getLineItemPrice("Logocharge").getAttribute("textContent").isEmpty());		
		}
		if(KeyCodeProductSteps.setup) {
			Assert.assertFalse(manageOrder.getLineItemPrice("Setupcharge").getAttribute("textContent").isEmpty());		
		}
	}
	@Then("User verify premimum product line")
	public void verify_premium_product_line() {
		String lineType=orderconf.getLineType("PREMIUM").getAttribute("textContent");		
		Assert.assertTrue("Line Type Premium is missing",lineType.equalsIgnoreCase("PREMIUM"));
	    Assert.assertEquals("Premium product price is not 0",orderconf.getLineItemPrice("PREMIUM").getAttribute("textContent").replaceAll("[^0-9a-zA-Z|._-]", ""),"0");		
	    Assert.assertEquals("Premium product quantity is not 1",orderconf.getLineItemQuantity("PREMIUM").getAttribute("textContent").replaceAll("[^0-9a-zA-Z|._-]", ""),"1");		
	}
	
	
	
	@Then("User click on product name and check logo")
	public void user_click_product_name_to_check_logo() throws InterruptedException {	
		BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.orderconf.viewImprint));
		BaseClass.js.scrollTilElement(BaseClass.orderconf.viewImprint);
		BaseClass.js.click(BaseClass.orderconf.viewImprint);
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
		BaseClass.orderconf.verifyLogo();
		BaseClass.orderconf.confImprint.click();
		BaseClass.getDriver().switchTo().defaultContent();
	}
	
	
	@Then("User click on product name and add logo {string} and confirm the Imprint {string}")
	public void user_click_on_product_name_and_add_logo_confirm_the_imprint(String addLogo,String AddImprint) throws InterruptedException {
		if (AddImprint.equalsIgnoreCase("yes")) {
			BaseClass.orderconf.imprintLogoConfAndValid(addLogo);
		} 
		BaseClass.getDriver().switchTo().defaultContent();
	}
	public void getLineItemDetails(List<WebElement> itemtype, String coloumnName) {
		StringBuilder orderList = new StringBuilder();
		int size = orderconf.lineProductCode.size();
		for (int i = 0; i < size; i++)

		{

			String text = itemtype.get(i).getText();
			if (text.equals("")) {
				text = "Logo Charge";
			}

			if (i == size - 1) {
				orderList.append(text);

			} else {
				orderList.append(text + "|");
			}

		}
		map.put(coloumnName, orderList.toString().replaceAll("[^0-9a-zA-Z|._-]", ""));

	}

}
