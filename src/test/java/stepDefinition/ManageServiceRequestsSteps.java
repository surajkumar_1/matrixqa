package stepDefinition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import org.junit.Assert;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import base.BaseClass;
import hooks.Hooks;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;


public class ManageServiceRequestsSteps {

	String customerNo, orderNo, srType, srNo, countActions;
	String actAssignee, actDescription, actType, actStatus, actResolution;
	int rownumber = BaseClass.excel.e.getRowCount("ServiceRequest") + 1;
	SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

	@Then("User clicks on Customer Care")
	public void i_click_on_customer_care() {
		BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.homePage.options.get(3))).click();
	}

	@Given("User enter the order number {string} on Manage Service Request page")
	public void user_enter_the_order_number_on_manage_service_request_page(String ordernumber) {
		BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.manageSR.heading));
		BaseClass.manageSR.orderNoField.sendKeys(ordernumber);

	}

	@Given("User enter the customer number {string} on Manage Service Request page")
	public void user_enter_the_customer_number_on_manage_service_request_page(String customerNumber) {
		BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.manageSR.heading));
		BaseClass.manageSR.customerNoField.sendKeys(customerNumber);

	}

	@Given("User enter the sr number {string} on Manage Service Request page")
	public void user_enter_the_sr_number_on_manage_service_request_page(String srNumber) {
		BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.manageSR.heading));
		BaseClass.manageSR.srNoField.sendKeys(srNumber);

	}

	@Given("User enter the assignee {string} on Manage Service Request page")
	public void user_enter_the_assignee_on_manage_service_request_page(String assignee) {
		BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.manageSR.heading));
		BaseClass.manageSR.actionAssigneeField.sendKeys(assignee);

	}

	@Given("User click on Search button on Manage Service Request page")
	public void user_click_on_search_button_on_customer_care_page() throws InterruptedException {
		BaseClass.manageSR.searchBtn.click();
		Thread.sleep(5000);
		customerNo = BaseClass.manageSR.customerNo.getText();
		orderNo = BaseClass.manageSR.orderNo.getText();
		srType = BaseClass.manageSR.srType.getText();
		srNo = BaseClass.manageSR.srNo.getText().trim();

	}

	@Given("User click on Search button without entering mandatory fields on Manage Service Request page")
	public void user_click_on_search_button_without_entering_mandatory_fields_on_customer_care_page() {
		BaseClass.manageSR.searchBtn.click();
	}

	@Given("User click on Clear button on Manage Service Request page")
	public void user_click_on_clear_button_on_customer_care_page() {
		boolean display = false;
		BaseClass.manageSR.clear.click();
		Assert.assertEquals("Order number field was not clear", "", BaseClass.manageSR.orderNoField.getText());
		Assert.assertEquals("Customer number field was not clear", "", BaseClass.manageSR.customerNoField.getText());
		Assert.assertEquals("SR number field was not clear", "", BaseClass.manageSR.srNoField.getText());
		Assert.assertEquals("Action assignee field was not clear", "",
				BaseClass.manageSR.actionAssigneeField.getText());

		
		try {
			if (BaseClass.manageSR.searchResultTable.isDisplayed())
				display = true;
		} catch (NoSuchElementException e) {
			display = false;
		}
		Assert.assertFalse("Search result was not clear", display);

	}

	@Then("Error message to enter search criteria displays")
	public void error_message_displays_on_customer_care_page() {

		Assert.assertTrue("error message was not displayed", BaseClass.manageSR.errorMsg.isDisplayed());
		Assert.assertEquals("Expected error message was not present", "Search with one of the available criteria",
				BaseClass.manageSR.errorMsg.getText());
	}

	@Then("Error message to enter mandatory fields for creating service request displays")
	public void error_message_displays_for_creating_sr() {
		Assert.assertEquals("Expected error message was not present", "Problem Code must have some value.",
				BaseClass.sr.error.get(0).getText());
		Assert.assertTrue("Expected error message was not present",
				BaseClass.sr.error.get(1).getText().contains("Service Request Description:"));
		Assert.assertTrue("Expected error message was not present",
				BaseClass.sr.error.get(1).getText().contains("(Max Limit: 4000 characters) must have some value."));
	}

	@Given("User click on create SR button from search results on Manage Service Request page")
	public void user_click_on_create_sr_button_on_customer_care_page() throws InterruptedException {
		String mainWin = BaseClass.getDriver().getWindowHandle();
		BaseClass.manageSR.createSR.click();
		Set<String> handles = BaseClass.getDriver().getWindowHandles();
		BaseClass.getDriver().switchTo().window(mainWin).close();

		for (String handle : handles) {
			if (!handle.contains(mainWin)) {
				BaseClass.getDriver().switchTo().window(handle);
				break;
			}
		}

		Thread.sleep(2000);
		Assert.assertEquals(
				"Expected note below service request description for not entering personal information was not present",
				"Please do not enter any Personal Information of the Customer!",
				BaseClass.sr.srDescriptionNote.getText());
		Assert.assertEquals("Expected characters limit note below service request description was not present",
				"(Max Limit: 4000 characters)", BaseClass.sr.srDescriptionLimitLabel.getText());
		Assert.assertEquals("Page Heading was not displayed", "Request Information", BaseClass.sr.heading.getText());
		Assert.assertEquals("Customer Number was not displayed", customerNo, BaseClass.sr.customerNo.getText());
		Assert.assertEquals("Order Number was not displayed", orderNo, BaseClass.sr.orderNo.getText());

	}

	@Given("User click on SR number from search results on Manage Service Request page")
	public void user_click_on_sr_number_button_on_customer_care_page() throws InterruptedException {

		BaseClass.js.scrollTilElement(BaseClass.manageSR.actionsBtn);

		String mainWin = BaseClass.getDriver().getWindowHandle();
		
		BaseClass.manageSR.srNo.click();
		Set<String> handles = BaseClass.getDriver().getWindowHandles();

		BaseClass.getDriver().switchTo().window(mainWin).close();
		for (String handle : handles) {
			if (!handle.contains(mainWin)) {
				BaseClass.getDriver().switchTo().window(handle);
				break;
			}
		}
		Thread.sleep(6000);

		Assert.assertEquals("Page Heading was not displayed", "Request Information", BaseClass.sr.heading.getText());
		Assert.assertEquals("Service Request number was not displayed", srNo, BaseClass.sr.sr.getText());
		Assert.assertEquals("Customer Number was not displayed", customerNo, BaseClass.sr.customerNo.getText());
		Assert.assertEquals("Order Number was not displayed", orderNo, BaseClass.sr.orderNo.getText());
		Assert.assertTrue("Service Request type was not displayed",
				BaseClass.select.getFirstSelectedOption(BaseClass.sr.requestType).getText().equalsIgnoreCase(srType));

	}

	@Given("User clicks on back button")
	public void user_click_on_back_button_on_service_request_page() {
		BaseClass.pageRefresh();
		BaseClass.sr.back.click();
		
	}

	@Given("User create SR -requestType{string} description{string} category{string} problem code{string} responsible person{string}")
	public void user_enters_details_to_create_service_request(String requestType, String description, String category,
			String code, String person) throws InterruptedException {
		user_select_request_type_on_service_request_page(requestType);
		user_enters_service_description_on_service_request_page(description);
		user_enters_problem_category_on_service_request_page(category);
		user_enters_problem_code_on_service_request_page(code);
		user_enters_responsible_person_on_service_request_page(person);
		Thread.sleep(5000);
		if (Hooks.sceanrioName.contains("Create Service")) {
			user_clicks_create_service_request();
		} else if (Hooks.sceanrioName.contains("Update Service")) {
			user_clicks_update_service_request();
		}

		user_click_on_back_button_on_service_request_page();
		user_click_on_search_button_on_customer_care_page();
		user_click_on_sr_number_button_on_customer_care_page();
		user_validate_service_request_details(requestType, description, category, code, person);
	}

	@Given("User validate created service request details")
	public void user_validate_service_request_details(String requestType, String description, String category,
			String code, String person) {

		Assert.assertEquals(
				"Expected note below service request description for not entering personal information was not present",
				"Please do not enter any Personal Information of the Customer!",
				BaseClass.sr.srDescriptionNote.getText());
		Assert.assertEquals("Expected characters limit note below service request description was not present",
				"(Max Limit: 4000 characters)", BaseClass.sr.srDescriptionLimitLabel.getText());
		Assert.assertEquals("Page Heading was not displayed", "Request Information", BaseClass.sr.heading.getText());
		Assert.assertEquals("Customer Number was not displayed", customerNo, BaseClass.sr.customerNo.getText());
		Assert.assertEquals("Order Number was not displayed", orderNo, BaseClass.sr.orderNo.getText());
		Assert.assertEquals("Service request number was not displayed", srNo, BaseClass.sr.sr.getText());
		Assert.assertTrue("Service Request type was not displayed", BaseClass.select
				.getFirstSelectedOption(BaseClass.sr.requestType).getText().equalsIgnoreCase(requestType));
		Assert.assertEquals("Service request problem category was not displayed", category,
				BaseClass.sr.problemCategory.getAttribute("value"));
		Assert.assertEquals("Service request problem code was not displayed", code,
				BaseClass.sr.problemCode.getAttribute("value"));
		Assert.assertEquals("Service request responsible person was not displayed", person,
				BaseClass.sr.responsiblePerson.getAttribute("value"));
		Assert.assertTrue("Service request description was not displayed",
				BaseClass.sr.srDescription.getAttribute("value").contains(description));
	}

	@Given("User selects request type {string}")
	public void user_select_request_type_on_service_request_page(String requestType) {
		BaseClass.select.selectByVisibleText(BaseClass.sr.requestType, requestType);
	}

	@Given("User enters service description {string}")
	public void user_enters_service_description_on_service_request_page(String description) {
		BaseClass.sr.srDescription.sendKeys(description);
	}

	@Given("User selects problem category {string}")
	public void user_enters_problem_category_on_service_request_page(String category) throws InterruptedException {
		BaseClass.js.click(BaseClass.sr.problemCategorySelect);
		BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.sr.problemCategoryInput));
		Thread.sleep(5000);

		BaseClass.sr.problemCategoryInput.sendKeys(category);
		BaseClass.sr.problemCategorySearch.click();
		BaseClass.sr.selectValueFromDropdown(BaseClass.getDriver(), category);
	}

	@Given("User selects problem code {string}")
	public void user_enters_problem_code_on_service_request_page(String code) throws InterruptedException {
		BaseClass.js.click(BaseClass.sr.problemCodeSelect);
		BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.sr.problemCodeInput));
		Thread.sleep(5000);

		BaseClass.sr.problemCodeInput.sendKeys(code);
		BaseClass.sr.problemCodeSearch.click();
		BaseClass.sr.selectValueFromDropdown(BaseClass.getDriver(), code);
	}

	@Given("User selects responsible person {string}")
	public void user_enters_responsible_person_on_service_request_page(String person) throws InterruptedException {
		BaseClass.js.click(BaseClass.sr.responsiblePersonSelect);
		BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.sr.responsiblePersonInput));
		Thread.sleep(5000);

		BaseClass.sr.responsiblePersonInput.sendKeys(person);
		BaseClass.sr.responsiblePersonSearch.click();
		BaseClass.sr.selectValueFromDropdown(BaseClass.getDriver(), person);
	}

	@Given("User clicks create service request button")
	public void user_clicks_create_service_request() throws InterruptedException {
		Date date = new Date();
		BaseClass.sr.createSR.click();
		
		try {
		srNo = BaseClass.sr.sr.getText();
		BaseClass.excel.e.setCellData("ServiceRequest", "SRNo", rownumber, srNo);
		BaseClass.excel.e.setCellData("ServiceRequest", "OrderNo", rownumber, orderNo);
		BaseClass.excel.e.setCellData("ServiceRequest", "Date", rownumber, formatter.format(date));
		}
		catch(NoSuchElementException e)
		{
			System.out.println(e);
		}
		

	}

	@Given("User clicks update service request button")
	public void user_clicks_update_service_request() throws InterruptedException {
		BaseClass.sr.updateSR.click();
	}

	@Given("User clicks add new action button")
	public void user_clicks_add_new_action_request() throws InterruptedException {
		try {
			countActions = BaseClass.sr.actionPagination.getAttribute("textContent").split("of")[1].trim();
		} catch (NoSuchElementException e) {
			countActions = "0";
		}
		
		BaseClass.sr.addNewAction.click();
      
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(BaseClass.sr.actionFrame));
		Thread.sleep(2000);
		Assert.assertEquals("Action SR number was not displayed", srNo, BaseClass.sr.actionSRNumber.getText());
		Assert.assertEquals("Expected characters limit note below action request description was not present",
				"(Max Limit: 2000 characters)", BaseClass.sr.actionDescriptionLimitLabel.getText());
		Assert.assertEquals("Expected characters limit note below action request resolution was not present",
				"(Max Limit: 2000 characters)", BaseClass.sr.actionResolutionLimitLabel.getText());

	}

	@Given("User clicks edit action request")
	public void user_clicks_edit_action_request() throws InterruptedException {
		BaseClass.sr.editAction.click();
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(BaseClass.sr.actionFrame));
		Thread.sleep(2000);
		Assert.assertEquals("Action SR number was not displayed", srNo, BaseClass.sr.actionSRNumber.getText());
		Assert.assertEquals("Expected characters limit note below action request description was not present",
				"(Max Limit: 2000 characters)", BaseClass.sr.actionDescriptionLimitLabel.getText());
		Assert.assertEquals("Expected characters limit note below action request resolution was not present",
				"(Max Limit: 2000 characters)", BaseClass.sr.actionResolutionLimitLabel.getText());

	}

	
	@Given("User select action type for action request {string}")
	public void user_select_action_type_for_action_creation(String actionType) {
		BaseClass.select.selectByVisibleText(BaseClass.sr.actionType, actionType);
	}

	@Given("User selects action status for action request {string}")
	public void select_action_status_for_action_creation(String actionStatus) {
		BaseClass.select.selectByVisibleText(BaseClass.sr.actionStatus, actionStatus);
	}

	@Given("User selects assignee for action request {string}")
	public void select_assignee_for_action_creation(String assignee) throws InterruptedException {

		BaseClass.js.click(BaseClass.sr.actionAssigneeSelect);
		BaseClass.getDriver().switchTo().defaultContent();
		BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.sr.actionAssigneeInput));
		Thread.sleep(5000);
		BaseClass.sr.actionAssigneeInput.sendKeys(assignee);
		BaseClass.sr.actionAssigneeSearch.click();
		BaseClass.sr.selectValueFromDropdown(BaseClass.getDriver(), assignee);
	}

	@Given("User enters description for action request {string}")
	public void enter_description_for_action_creation(String description) {
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(BaseClass.sr.actionFrame));
		BaseClass.sr.actionDescription.clear();
		BaseClass.sr.actionDescription.sendKeys(description);
	}

	@Given("User enters resolution for action request {string}")
	public void enter_resolution_for_action_creation(String resolution) {
		actResolution = resolution;
		BaseClass.sr.actionResolution.clear();
		BaseClass.sr.actionResolution.sendKeys(resolution);
		actType=BaseClass.select.getFirstSelectedOption(BaseClass.sr.actionType).getText();
		actAssignee=BaseClass.sr.actionAssigneeValue.getAttribute("value");
		actDescription=BaseClass.sr.actionDescription.getAttribute("value");
	}

	@Given("User create Action request-assignee{string} description{string} actionStatus{string} actionType{string}")
	public void user_enters_details_to_create_action_request(String assignee, String description, String actionStatus,
			String actionType) throws InterruptedException {
		actAssignee = assignee;
		actDescription = description;
		actStatus = actionStatus;
		actType = actionType;
		
		if (Hooks.sceanrioName.contains("Create Action")||Hooks.sceanrioName.contains("Create multiple Action")) {
			user_clicks_add_new_action_request();
		} else if (Hooks.sceanrioName.contains("Update Action")) {
			user_clicks_edit_action_request();
		}
		
		select_assignee_for_action_creation(actAssignee);
		enter_description_for_action_creation(actDescription);
		select_action_status_for_action_creation(actStatus);
		user_select_action_type_for_action_creation(actType);
		
		

		if (Hooks.sceanrioName.contains("Create Action")||Hooks.sceanrioName.contains("Create multiple Action")) {
			click_add_action_for_action_creation();
		} else if (Hooks.sceanrioName.contains("Update Action")) {
			click_update_action_for_action_creation();
		}

		
		BaseClass.getDriver().switchTo().defaultContent();
		BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.sr.actionButton));
		Thread.sleep(3000);

		if (Hooks.sceanrioName.contains("Create Action")||Hooks.sceanrioName.contains("Create multiple Action")) {
			Assert.assertEquals("Action request was not added", Integer.parseInt(countActions) + 1,
					Integer.parseInt(BaseClass.sr.actionPagination.getAttribute("textContent").split("of")[1].trim()));
		}
		
		
		Assert.assertEquals("SR no for created action request was not displayed", srNo,
				BaseClass.sr.actionSRNo.getText());
		
		Assert.assertEquals("Action Type for created action request was not displayed", actType,
				BaseClass.sr.actionSRType.getText());
		Assert.assertEquals("Action Status for created action request was not displayed", actStatus,
				BaseClass.sr.actionSRStatus.getText());
		Assert.assertEquals("Action Assignee for created action request was not displayed", actAssignee,
				BaseClass.sr.actionAssignee.getText());
		Assert.assertEquals("Action description for created action request was not displayed", actDescription,
				BaseClass.sr.actionDescrip.getText());

	}

	@Given("User clicks add action button")
	public void click_add_action_for_action_creation() {
		BaseClass.js.scrollTilElement(BaseClass.sr.addAction);
		BaseClass.sr.addAction.click();

	}

	@Given("User clicks update action button")
	public void click_update_action_for_action_creation() {
		BaseClass.js.scrollTilElement(BaseClass.sr.updateAction);
		BaseClass.sr.updateAction.click();

	}

	@Then("User clicks update and close button")
	public void click_update_and_close_button() throws InterruptedException {
		BaseClass.sr.updateAndCloseAction.click();
		BaseClass.getDriver().switchTo().defaultContent();
		BaseClass.wait.until(ExpectedConditions.elementToBeClickable(BaseClass.sr.ok));
		BaseClass.js.click(BaseClass.sr.ok);
		Thread.sleep(2000);
		Assert.assertEquals("SR no for created action request was not displayed", srNo,
				BaseClass.sr.actionSRNo.getText());
		Assert.assertEquals("Action Type for created action request was not displayed", actType,
				BaseClass.sr.actionSRType.getText());
		Assert.assertEquals("Action Status for created action request was not displayed", "Closed",
				BaseClass.sr.actionSRStatus.getText());
		Assert.assertEquals("Action Assignee for created action request was not displayed", actAssignee,
				BaseClass.sr.actionAssignee.getText());
		Assert.assertEquals("Action description for created action request was not displayed", actDescription,
				BaseClass.sr.actionDescrip.getText());
		Assert.assertEquals("Action resolution for created action request was not displayed", actResolution,
				BaseClass.sr.actionResol.getText());
	}

	@Then("Error message to enter mandatory fields for creating action request displays")
	public void error_message_displays_for_manadatory_Fields() {

		Assert.assertEquals("Expected error message was not present", BaseClass.sr.error.get(0).getText(),
				"Assignee: must have some value.");
		Assert.assertEquals("Expected error message was not present", BaseClass.sr.error.get(1).getText(),
				"Action Type: must have some value.");
	}

	@Given("Validate User cannot enter description greater than 4000 characters")
	public void user_enters_more_than_4000_chars_in_description() throws InterruptedException {
		String des = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis diam. Pellentesque ut neque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In dui magna, posuere eget, vestibulum et, tempor auctor, justo. In ac felis quis tortor malesuada pretium. Pellentesque auctor neque nec urna. Proin sapien ipsum, porta a, auctor quis, euismod ut, mi. Aenean viverra rhoncus pede. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut non enim eleifend felis pretium feugiat. Vivamus quis mi. Phasellus a est. Phas these are extra words";
		BaseClass.sr.srDescription.clear();
		BaseClass.sr.srDescription.sendKeys(des);
		Thread.sleep(2000);
		Assert.assertTrue(BaseClass.sr.srDescription.getAttribute("value").length() == 4000);
	}


	@Given("Validate User cannot enter description and resolution greater than 2000 characters")
	public void user_enters_more_than_2000_chars_in_description_and_resolution() throws InterruptedException {
		String des = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibextrachar";
		BaseClass.sr.actionDescription.clear();
		BaseClass.sr.actionDescription.sendKeys(des);
		Thread.sleep(2000);
		Assert.assertTrue(BaseClass.sr.actionDescription.getAttribute("value").length() == 2000);
		BaseClass.sr.actionResolution.clear();
		BaseClass.sr.actionResolution.sendKeys(des);
		Thread.sleep(2000);
		Assert.assertTrue(BaseClass.sr.actionResolution.getAttribute("value").length() == 2000);
	}
	
	@Given("User clicks reopen action request")
	public void user_clicks_reopen_action_request() throws InterruptedException {
		actAssignee = BaseClass.sr.actionAssignee.getText();
		actDescription = BaseClass.sr.actionDescrip.getText();
		actType = BaseClass.sr.actionSRType.getText();
		actResolution = BaseClass.sr.actionResol.getText();
		BaseClass.sr.reopenAction.click();
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(BaseClass.sr.actionFrame));
		Assert.assertEquals("Action SR number was not displayed", srNo, BaseClass.sr.reopenSR.getText());
		BaseClass.sr.reopen.click();
		BaseClass.getDriver().switchTo().defaultContent();
		Assert.assertEquals("Success message for reopening action request was not displayed",
				"Action Reopened Successfully.", BaseClass.sr.successMsg.getText());
		Thread.sleep(2000);
		Assert.assertEquals("SR no for created action request was not displayed", srNo,
				BaseClass.sr.actionSRNo.getText());
		Assert.assertEquals("Action Type for created action request was not displayed", actType,
				BaseClass.sr.actionSRType.getText());
		Assert.assertEquals("Action Status for created action request was not displayed", "Open",
				BaseClass.sr.actionSRStatus.getText());
		Assert.assertEquals("Action Assignee for created action request was not displayed", actAssignee,
				BaseClass.sr.actionAssignee.getText());
		Assert.assertEquals("Action resolution for created action request was not displayed", actResolution,
				BaseClass.sr.actionResol.getText());

	}


}
