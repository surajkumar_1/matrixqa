package stepDefinition;

import static base.BaseClass.homePage;
import static base.BaseClass.login;
import static base.BaseClass.manageCustomer;
import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.openqa.selenium.support.ui.ExpectedConditions;

import base.BaseClass;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;

public class HomePageSteps {

	@Given("User is on Home Page")
	public void user_is_on_home_page() {
		BaseClass.wait.until(ExpectedConditions.visibilityOf(login.loggedInUser));
		Assert.assertEquals("Home", BaseClass.getDriver().getTitle());
	}

	@When("User Clicks on Manage Customer")
	public void user_clicks_on_manage_customer() {		
		homePage.manageCustomerBtn.click();
		BaseClass.wait.until(ExpectedConditions.visibilityOf(manageCustomer.customerNo));
		assertEquals("Manage Customer", BaseClass.getDriver().getTitle());
	}

	
}
