package stepDefinition;

import static base.BaseClass.copyOrder;
import static base.BaseClass.manageOrder;
import static base.BaseClass.orderconf;

import java.awt.AWTException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import java.util.stream.Stream;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import base.BaseClass;
import constants.AssertInfo;
import hooks.Hooks;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class ManageOrderSteps {
	String holdname, billToadd, shipToadd, subTotal, tax, total, custNo, orderNo, shipcharge, orderNoABC,
			cancelReason, abcStatus, accQty, accPrice, orderType,salesRep,lineItemsCnt;
	int page = 0, lines;
	int sizeItemsManageOrder;
	String shipQty,shipPrice,logoQty,logoPrice,discountQty,discountPrice,charges,dateCreated,addChargeQty, addChargePrice;;
    boolean shipDisplay,logoDisplay,discountDisplay,logoFlag,imprintFlag;
    ArrayList<ArrayList<String>> lineItems;
    DecimalFormat df = new DecimalFormat("#.00");
	@Then("I click on Manage Orders")
	public void i_click_on_manage_orders() {
		BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.homePage.manageOrder)).click();

	}

	@Given("User enter the order number {string} on manage order page")
	public void user_enter_the_order_number_on_manage_order_page(String ordernumber) {
		Stream<String> oracleStatus = Stream.of("BOOKED", "ENTERED", "CLOSED");
		manageOrder.orderNumberField.sendKeys(ordernumber);
		manageOrder.searchBtn.click();
		try {
			custNo = manageOrder.orderCustomerNumber.getAttribute("textContent");
			total = manageOrder.total.getAttribute("textContent");
			Assert.assertTrue("Orcale order number not Present",BaseClass.wait.until(ExpectedConditions.attributeToBeNotEmpty(manageOrder.oracleOrderNumber, "textContent")));
			Assert.assertTrue("Orcale Status not matching or Present", oracleStatus.anyMatch(s -> s.contains(manageOrder.oracleOrderStatus.getAttribute("textContent"))));
			orderNo = manageOrder.oracleOrderNumber.getAttribute("textContent");
			subTotal = manageOrder.subTotal.getAttribute("textContent");
			tax = manageOrder.tax.getAttribute("textContent");
			shipcharge = manageOrder.shippingCharge.getAttribute("textContent");
			orderNoABC=ordernumber;
			lines = manageOrder.getTotalLine();
			charges=manageOrder.orderCharges.getAttribute("textContent");
			dateCreated=manageOrder.dateCreated.getAttribute("textContent");
			orderType=manageOrder.orderType.getAttribute("textContent");
			salesRep=manageOrder.salesRep.getAttribute("textContent");
		} catch (Exception e) {
			e.getMessage();
		}
	
	}

	@Then("Invalid Order message will display")
	public void invalid_order_message_will_display() {
		Assert.assertTrue("Invalid Message Order didn't display",manageOrder.invalidOrderMsg.isDisplayed());
	}
	
	@Then("User Update the request date of the order number given {string}")
	public void user_update_the_request_date_of_the_order_number_given(String date) {
		BaseClass.wait.until(ExpectedConditions.visibilityOf(manageOrder.updateRequestDateBtn));
		manageOrder.requestDate.clear();
		manageOrder.requestDate.sendKeys(date);
		manageOrder.updateRequestDateBtn.click();
		BaseClass.pageRefresh();
		Assert.assertEquals(manageOrder.requestDate.getAttribute("value"), date);

	}

	@Then("User click on Email preview button")
	public void user_click_on_email_preview_button() throws InterruptedException {
		lineItems = new ArrayList<ArrayList<String>>();

		int next;
		if (lines % 5 == 0)
			next = lines / 5 - 1;
		else
			next = lines / 5;

		int size;

		// to move to next page for getting item details in manage order
		for (int i = 0; i <= next; i++) {
			size = BaseClass.manageOrder.getAllLineItemNameWithoutCancelledstatus().size();
			sizeItemsManageOrder += size;

			for (int j = 0; j < size; j++) {
				ArrayList<String> line = new ArrayList<String>();
				BaseClass.exceptionHandle.visibilityOfAll(BaseClass.manageOrder.getAllLineTypeWithoutCancelledstatus());


				if (BaseClass.manageOrder.getAllLineTypeWithoutCancelledstatus().get(j).getAttribute("textContent")
						.trim().equalsIgnoreCase("Shipping")
						|| BaseClass.manageOrder.getAllLineTypeWithoutCancelledstatus().get(j)
								.getAttribute("textContent").trim().equalsIgnoreCase("Discount")) {
					line.add(BaseClass.manageOrder.getAllLineTypeWithoutCancelledstatus().get(j)
							.getAttribute("textContent").trim());

										
				} else if (BaseClass.manageOrder.getAllLineTypeWithoutCancelledstatus().get(j)
						.getAttribute("textContent").trim().equalsIgnoreCase("Logocharge"))
					line.add("LogoCharge");
				else if (BaseClass.manageOrder.getAllLineTypeWithoutCancelledstatus().get(j).getAttribute("textContent")
						.trim().equalsIgnoreCase("Setupcharge"))
					line.add("SetupCharge");
				else
					line.add(BaseClass.manageOrder.getAllLineItemNameWithoutCancelledstatus().get(j)
							.getAttribute("textContent").trim());

				line.add(BaseClass.manageOrder.getAllLineItemPriceWithoutCancelledstatus().get(j)
						.getAttribute("textContent").trim());
				line.add(BaseClass.manageOrder.getAllLineItemQuantityWithoutCancelledstatus().get(j)
						.getAttribute("textContent").trim());
				lineItems.add(line);

			}
			Thread.sleep(2000);

			// click on next button to move on next page
			if (i != next) {
				BaseClass.manageOrder.nextButton.click();
				Thread.sleep(2000);
			}
		}
			
			shipToadd =  manageOrder.shipToAddress.getAttribute("textContent");
			billToadd =manageOrder.billToAddress.getAttribute("textContent");

			BaseClass.wait.until(ExpectedConditions.elementToBeClickable(manageOrder.emailPreviewBtn));
			BaseClass.js.scrollTilElement(BaseClass.manageOrder.timer);
			Thread.sleep(5000);
		BaseClass.wait.until(ExpectedConditions.elementToBeClickable(manageOrder.emailPreviewBtn)).click();
		Assert.assertEquals("Preview Details Page",BaseClass.getDriver().getTitle());

	}

	@Then("User clicks on the create button for creating a new address {string}")
	public void user_clicks_on_the_create_button_for_creating_a_new_address(String primary)
			throws InterruptedException {
		orderconf.createBtn.click();
		BaseClass.getDriver().switchTo().defaultContent();
		Thread.sleep(4000);
		BaseClass.wait.until(ExpectedConditions.invisibilityOf(BaseClass.CustProd.processSpinner));
		Thread.sleep(5000);
		if (Hooks.sceanrioName.contains("Create New Bill")) {
			BaseClass.js.click(BaseClass.wait.until(ExpectedConditions.visibilityOf(manageOrder.changeBillToBtn)));
		}
		else if (Hooks.sceanrioName.contains("Create New Ship")) {
			BaseClass.js.click(BaseClass.wait.until(ExpectedConditions.visibilityOf(manageOrder.changeShipToBtn)));
		}
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));		
		Iterator<WebElement> address = manageOrder.newAddressLine1.iterator();
		int count=0;
		while(address.hasNext()) {
			WebElement temp = address.next();
			if (temp.getText().contains(Hooks.randomInt)) {
				Assert.assertTrue("The address is not created", temp.getText().contains(Hooks.randomInt));
				if (primary.equalsIgnoreCase("YES")) {
					Assert.assertEquals("Primary Address not created", "Y",
							temp.findElement(By.xpath("//td[text()='"+temp.getText()+"']//following-sibling::td[last()]")).getText());						
				} else
					Assert.assertEquals("Non Primary Address not created", "N",
							temp.findElement(By.xpath("//td[text()='"+temp.getText()+"']//following-sibling::td[last()]")).getText());
				count++;
			}else if(manageOrder.newAddressLine1.size()==count){
				Assert.fail("The address is not created "+Hooks.randomInt);
				}
		}	
	}
	@Then("User clicks on create new bill to button in manage order page")
	public void user_clicks_on_create_new_bill_to_button_in_manage_order_page() {
		manageOrder.createNewBillToBtn.click();
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(manageOrder.iframe));

	}

	@Then("User clicks on create new SHIP to button in manage order page")
	public void user_clicks_on_create_new_ship_to_button_in_manage_order_page() {
		manageOrder.createNewShipToBtn.click();
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(manageOrder.iframe));

	}

	@Then("User clicks on change bill to button")
	public void user_clicks_on_change_bill_to_button() throws IOException, InterruptedException {
		Thread.sleep(5000);
		String addBefore = manageOrder.billToAddress.getText();
		String[] result = addBefore.split("\\R", 6);
		String addLine1 = result[0];
		String pin = addBefore.substring(addBefore.indexOf(",")).replaceAll("[^0-9-]","");	
		BaseClass.js.click(BaseClass.wait.until(ExpectedConditions.visibilityOf(manageOrder.changeBillToBtn)));
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(manageOrder.iframe));
		Iterator<WebElement> address = manageOrder.addresses.iterator();
		while(address.hasNext()) {
			WebElement temp= address.next();
			if(!temp.getText().contains(addLine1) && !temp.getText().contains(pin) ) {
			BaseClass.js.click(temp.findElement(By.cssSelector("input[name='f01']")));
			break;
		}
	} 
		manageOrder.saveBtnChangeBillToShipTo.click();
		BaseClass.getDriver().switchTo().defaultContent();
		BaseClass.wait.until(ExpectedConditions.visibilityOf(manageOrder.manageHold));
		BaseClass.getDriver().navigate().refresh();
		Thread.sleep(10000);
		Assert.assertFalse("Address did not change or check script", addBefore.replaceAll("[\\s,\n]", "").equalsIgnoreCase(manageOrder.billToAddress.getAttribute("textContent").replaceAll("[\\s,\n,\u00a0]", "")));

	}
	@Then("User clicks on change ship to button")
	public void user_clicks_on_change_ship_to_button() throws InterruptedException {	
		Thread.sleep(5000);
		String addBefore = manageOrder.shipToAddress.getText();
		String[] result = addBefore.split("\\R", 6);
		String addLine1 = result[0];
		String pin = addBefore.substring(addBefore.indexOf(",")).replaceAll("[^0-9-]","");	
		BaseClass.js.click(BaseClass.wait.until(ExpectedConditions.elementToBeClickable(manageOrder.changeShipToBtn)));
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(manageOrder.iframe));
		Iterator<WebElement> address = manageOrder.addresses.iterator();
		while(address.hasNext()) {
			WebElement temp= address.next();
			if(!temp.getText().contains(addLine1) && !temp.getText().contains(pin) ) {
			BaseClass.js.click(temp.findElement(By.cssSelector("input[name='f02']")));
			break;
		}
	} 
		manageOrder.saveBtnChangeBillToShipTo.click();
		BaseClass.getDriver().switchTo().defaultContent();
		BaseClass.wait.until(ExpectedConditions.visibilityOf(manageOrder.manageHold));
		BaseClass.getDriver().navigate().refresh();
		Thread.sleep(5000);
		Assert.assertFalse("Address did not change or check script", addBefore.replaceAll("[\\s,\n]", "").equalsIgnoreCase(manageOrder.shipToAddress.getAttribute("textContent").replaceAll("[\\s,\n,\u00a0]", "")));
		
	}


	@Then("User click on Copy Order button")
	public void user_click_on_copy_order_button() {	
		BaseClass.wait.until(ExpectedConditions.elementToBeClickable(manageOrder.copyOrderBtn));
		BaseClass.js.click(manageOrder.copyOrderBtn);
		BaseClass.exceptionHandle.visibilityOf(copyOrder.copyOrderBtn);
		Assert.assertEquals(orderType, BaseClass.select.getFirstSelectedOption(copyOrder.orderType).getText());
		Assert.assertEquals(salesRep, BaseClass.select.getFirstSelectedOption(copyOrder.salesRep).getText());
	}
	@Then("Validate salesrep dropdown")
	public void user_can_see_Salesrep_dropdown() { 
	WebElement option = BaseClass.select.getFirstSelectedOption(copyOrder.salesRep);
	Assert.assertEquals(salesRep, option.getText());
}
	@Then ("Validate ordertype on Copy page")
	public void user_can_Validate_Order_Type() {
		WebElement option = BaseClass.select.getFirstSelectedOption(copyOrder.orderType);
		Assert.assertEquals(orderType, option.getText());
	
	}
	
	@Then("Validate request date")
	public void user_can_see_Request_Date () {
		 Calendar cal = Calendar.getInstance();
		 cal.add(Calendar.DAY_OF_WEEK, 5);	
         Assert.assertEquals("Request Date is not +5 days ", new SimpleDateFormat("dd-MMM-YY").format(cal.getTime()), copyOrder.requestDate.getAttribute("value"));
	
     }
	@Then("Validate Email address salesrep")
	public void user_can_see_email_address() {
	Assert.assertTrue("Sales rep email",copyOrder.copySalesRepEmail.isDisplayed());	
     }
	@Then("User click on Manage Hold button")
	public void user_click_on_manage_hold_button() {
		BaseClass.wait.until(ExpectedConditions.elementToBeClickable(manageOrder.manageHold));
		BaseClass.js.click(manageOrder.manageHold);
		abcStatus = manageOrder.abcStatusHeader.getAttribute("textContent");
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(manageOrder.iframe));
		BaseClass.wait.until(ExpectedConditions.attributeToBeNotEmpty(manageOrder.currentAppliedHold, "textContent"));
		holdname = manageOrder.currentAppliedHold.getText();
	}

	@Then("User select hold to be apply/remove from the list {string}")
	public void user_select_hold_to_be_applied_from_the_list(String holdname) {		
		if (Hooks.sceanrioName.toUpperCase().contains("Apply hold".toUpperCase())) {
			Stream<String> abc_Status = Stream.of("Processing", "Confirmed", "Open", "Initial");
			if (holdname.toUpperCase().contains("CSR")) {
				Assert.assertTrue("Abc Header Status is not found as expected",
						abc_Status.anyMatch(s -> s.contains(abcStatus)));
			}
			Assert.assertFalse(holdname + " Hold is already Applied",
					manageOrder.currentAppliedHold.getText().contains(holdname));
			Assert.assertTrue(holdname + " Hold is already Applied",
					orderconf.selectHold(holdname).getAttribute("class").contains("checkbox"));
			String[] hold = holdname.split(",");
			for (String h : hold) {
				orderconf.selectHold(h).click();
			}
		} else if (Hooks.sceanrioName.toUpperCase().contains("Remove hold".toUpperCase())) {		
			String[] hold = holdname.split(",");
			for (String h : hold) {
				Assert.assertTrue(h + " Hold is not present in current hold list",
						manageOrder.currentAppliedHold.getAttribute("textContent").contains(h.toUpperCase()));
				if(Hooks.sceanrioName.toUpperCase().contains("Order Confirmation".toUpperCase())) {
				Assert.assertTrue(h + " Hold is not present to remove",
						orderconf.selectHold(h).getAttribute("class").contains("checkbox"));
				}
				else {
					Assert.assertTrue(h + " Hold is not present to remove",
						orderconf.selectHold(h).getAttribute("class").contains("radio"));
					}
			
				orderconf.selectHold(h).click();
			}
		} else {

			Assert.assertFalse("something wrong with scenario", true);
		}
	}

	@Then("User write apply/release comments {string}")
	public void user_write_comments(String comment) {	
		if (Hooks.sceanrioName.toUpperCase().contains("Apply hold".toUpperCase())) {
			manageOrder.enterCommentForHold.sendKeys(comment);
		} else if (Hooks.sceanrioName.toUpperCase().contains("Remove hold".toUpperCase())) {
			manageOrder.releaseCommentForHold.sendKeys(comment);
		}		
	}

	@Then("User click on Apply/Remove hold button")
	public void user_click_on_apply_hold_button() throws InterruptedException {

		if (Hooks.sceanrioName.toUpperCase().contains("Apply hold".toUpperCase())) {
			manageOrder.applyHoldBtn.click();
		} else if (Hooks.sceanrioName.toUpperCase().contains("Remove hold".toUpperCase())) {
			manageOrder.removeHold.click();
		} else {

			Assert.assertFalse("something wrong with scenario", true);
		}
		Thread.sleep(3000);
		BaseClass.getDriver().switchTo().defaultContent();
		BaseClass.wait.until(ExpectedConditions.visibilityOf(manageOrder.manageHold));
	}

	@Then("User validate the {string} is applied/removed")
	public void user_validate_if_the_hold_is_applied_removed(String holdname) throws InterruptedException {
		user_click_on_manage_hold_button();
		if (Hooks.sceanrioName.toUpperCase().contains("Apply hold".toUpperCase())) {
			Assert.assertTrue(holdname+ " is not present in current hold list", manageOrder.currentAppliedHold.getText().contains(holdname));
			Assert.assertTrue(holdname+ "is not present in remove hold list",orderconf.selectHold(holdname).getAttribute("class").contains("radio"));
			BaseClass.excel.copyExcelData("OrdersForModification", "ModifiedOrder", "ABCOrderNumber", orderNoABC);
			BaseClass.excel.e.setCellData("ModifiedOrder", "S.Num", BaseClass.excel.sheet2row, String.valueOf(BaseClass.excel.sheet2row-1));
			BaseClass.excel.e.setCellData("ModifiedOrder", "OracleNumber", BaseClass.excel.sheet2row, orderNo);
			BaseClass.excel.e.setCellData("ModifiedOrder", "TestName", BaseClass.excel.sheet2row, "Validate_Released_Holds");
			BaseClass.excel.e.setCellData("ModifiedOrder", "Modifications", BaseClass.excel.sheet2row, holdname);
			
		} else if (Hooks.sceanrioName.toUpperCase().contains("Remove hold".toUpperCase())) {
		 	Assert.assertFalse(holdname+  " is present in current hold list", manageOrder.currentAppliedHold.getText().contains(holdname));
		  //  Assert.assertTrue(holdname+" is present in remove list", orderconf.selectHold(holdname).getAttribute("class").contains("checkbox"));	
			BaseClass.excel.copyExcelData("OrdersForModification", "ModifiedOrder", "ABCOrderNumber", orderNoABC);
			BaseClass.excel.e.setCellData("ModifiedOrder", "S.Num", BaseClass.excel.sheet2row, String.valueOf(BaseClass.excel.sheet2row-1));
			BaseClass.excel.e.setCellData("ModifiedOrder", "OracleNumber", BaseClass.excel.sheet2row, orderNo);
			BaseClass.excel.e.setCellData("ModifiedOrder", "TestName", BaseClass.excel.sheet2row, "Validate_Released_Holds");
			BaseClass.excel.e.setCellData("ModifiedOrder", "Modifications", BaseClass.excel.sheet2row, holdname);	
		
		} 

	}


	@Then("User click on Copy Order button in copy order page")
	public void user_click_on_copy_order_button_in_copy_order_page() {
		BaseClass.wait.until(ExpectedConditions.attributeToBeNotEmpty(copyOrder.copySalesRepEmail, "value"));
		copyOrder.copyOrderBtn.click();
		BaseClass.wait.until(ExpectedConditions.elementToBeClickable(copyOrder.okBtn)).click();

	}

	

//	@Then("User click on Cancel button of Abc Order Number Holds")
//	public void user_click_on_cancel_button_of_abc_order_number_holds() {
//		BaseClass.wait.until(ExpectedConditions.elementToBeClickable(manageOrder.cancelBtn)).click();
//		BaseClass.getDriver().switchTo().defaultContent();
//
//	}

	@Then("User click on Holds History button and Hold Lengend button and Validate the Holds")
	public void user_click_on_holds_history_button_and_hold_lengend_button_and_validate_the_holds() {
		
		BaseClass.wait.until(ExpectedConditions.elementToBeClickable(manageOrder.cancelBtn)).click();
		BaseClass.getDriver().switchTo().defaultContent();
		manageOrder.holdHistoryBtn.click();
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(manageOrder.iframe));
		Set<String> appliedholds = new  HashSet<String>(Arrays.asList(holdname.split("\\n")));
	   Iterator<String> set = appliedholds.iterator();
		while (set.hasNext()) {
			Assert.assertEquals("Credit Check Failure",set.next());
			Assert.assertEquals("ABC Platform Hold",set.next());
			Assert.assertEquals("PRICE QUERY",set.next());
		}
	
		Set <WebElement> set2 =	new  HashSet<WebElement>(manageOrder.holdHistAppiedHoldName);
		System.out.println(set2.stream().anyMatch(holds->appliedholds.contains(holds.getText())));
		
		
//			while (set2.()) {
//				String string =  set2.next().getText();
//				System.out.println(string);
//			}
//		appliedholds.stream()
//		List<String> appliedholds = new ArrayList<String>(Arrays.asList(holdname.split("\\n")));
//		List<String> holdlist = new ArrayList<String>();
//
//		int size = manageOrder.holdHistoryHoldName.size();
//		for (int i = 0; i < size; i++) {
//			holdlist.add(manageOrder.holdHistoryHoldName.get(i).getText());
//		}
//		List<String> holdsLegendHold = new ArrayList<String>();
//		manageOrder.holdsLegendBtn.click();
//		for (int i = 0; i < size; i++) {
//			holdsLegendHold.add(manageOrder.holdsLegendHoldName.get(i).getText());
//		}
//		Assert.assertEquals(true, appliedholds.stream().anyMatch(element -> holdlist.contains(element)));
//		Assert.assertEquals(true, appliedholds.stream().anyMatch(element -> holdsLegendHold.contains(element)));

	}

	
	@Then("User select the ship contact {string}")
	public void user_select_the_ship_contact(String contact) {
		BaseClass.wait.until(ExpectedConditions.visibilityOf(manageOrder.updateRequestDateBtn));
		BaseClass.select.selectByVisibleText(manageOrder.selectShipContact, contact);

	}

	@Then("User click OK button to update ship/bill to contact")
	public void user_click_ok_button_if_want_to_update_ship_to_contact() {
		BaseClass.CustProd.okBtn.click();
	}

	@Then("Validate shipto contact is updated {string}")
	public void validate_if_the_shipto_contact_is_updated_or_not(String contact) {
		BaseClass.wait.until(ExpectedConditions.visibilityOf(manageOrder.bigprocessEle));
		BaseClass.pageRefresh();
		BaseClass.wait.until(ExpectedConditions.visibilityOf(manageOrder.updateRequestDateBtn));
		WebElement option = BaseClass.select.getFirstSelectedOption(manageOrder.selectShipContact);
		Assert.assertEquals(contact, option.getText());

	}

	@Then("User select the bill contact {string}")
	public void user_select_the_bill_contact(String contact) {
		BaseClass.wait.until(ExpectedConditions.visibilityOf(manageOrder.updateRequestDateBtn));
		BaseClass.select.selectByVisibleText(manageOrder.selectBillContact, contact);
			
	}

	@Then("Validate billto contact is updated {string}")
	public void validate_if_the_billto_contact_is_updated_or_not(String contact) {
		BaseClass.wait.until(ExpectedConditions.visibilityOf(manageOrder.bigprocessEle));
		BaseClass.pageRefresh();
		BaseClass.wait.until(ExpectedConditions.visibilityOf(manageOrder.updateRequestDateBtn));
		WebElement option = BaseClass.select.getFirstSelectedOption(manageOrder.selectBillContact);
		Assert.assertEquals(option.getText(), contact);
	}

	@Then("User validate logo and imprint for inhosue product {string}")
	public void user_validate_logo_and_imprint_for_inhosue_product(String keycode)
			throws AWTException, InterruptedException, IOException {
		BaseClass.select.selectByVisibleText(BaseClass.emailPre.emailLang, "English");
		manageOrder.searchBtn.click();
		Thread.sleep(5000);
		BaseClass.wait.until(ExpectedConditions.attributeContains(BaseClass.emailPre.subjectLine, "value", "National Pen - Approval Needed"));
		
		Assert.assertTrue("customer number did not match or missing",
				BaseClass.emailPre.orderDetails.get(1).getText().contains(custNo));
		
		Assert.assertTrue("Order number did not match or missing",
				BaseClass.emailPre.orderDetails.get(2).getText().contains(orderNo));
		
		Assert.assertTrue("Keycode did not match or missing",
				BaseClass.emailPre.orderDetails.get(3).getText().contains(keycode));
		
		Assert.assertEquals("Order Total did not match or missing", total,
				BaseClass.emailPre.totalprice.getText().replaceAll("[^0-9.]", ""));
		
		Assert.assertEquals("Subject Line did not match or missing", "National Pen - Approval Needed - " + orderNo,
				BaseClass.emailPre.subjectLine.getAttribute("value"));
		
		Assert.assertEquals("Email instruction did not match or missing",
				"Please click anywhere on the preview below to copy the information and open a new e-mail. Once the e-mail is opened, simply paste the information (Ctrl+V).",
				BaseClass.emailPre.emailMsg.getText().trim());
				
		Assert.assertTrue("Product Image is missing",
				BaseClass.emailPre.productImg.getAttribute("src").contains("https://natpen.scene7.com"));
		
		Assert.assertTrue("Ship To Address not matched or missing", (BaseClass.emailPre.shipToAdd.getText()
				.replaceAll("\\s+", "") + "\"").contains(StringUtils.substringBefore((shipToadd.replaceAll("\\s+", "").replaceAll(",", "") + "\""), "ShipToContact")));

		Assert.assertTrue("Bill To Address not matched or missing", (BaseClass.emailPre.billToAdd.getText()
				.replaceAll("\\s+", "") + "\"").contains(StringUtils.substringBefore((billToadd.replaceAll("\\s+", "").replaceAll(",", "") + "\""), "BillToContact")));


		/*
		 * Dimension i = BaseClass.getDriver().manage().window().getSize();
		 * BaseClass.js.scrollTilElement(BaseClass.emailPre.totalprice); Robot robot =
		 * new Robot(); Thread.sleep(1000); int x = (i.getWidth()/2); int y =
		 * (i.getHeight()/2); robot.mouseMove(x,y);
		 * robot.mousePress(InputEvent.BUTTON1_MASK);
		 * robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
		 */

	}

	@Then("User validate only logo for inhosue product {string}")
	public void user_validate_only_logo_for_inhosue_product(String keycode) throws InterruptedException {
		BaseClass.select.selectByVisibleText(BaseClass.emailPre.emailLang, "English");
		manageOrder.searchBtn.click();
		Thread.sleep(5000);
		BaseClass.wait.until(ExpectedConditions.attributeContains(BaseClass.emailPre.subjectLine, "value", "National Pen - Approval Needed"));
		Assert.assertTrue("customer number did not match or missing",
				BaseClass.emailPre.orderDetails.get(1).getText().contains(custNo));
		
		Assert.assertTrue("Order number did not match or missing",
				BaseClass.emailPre.orderDetails.get(2).getText().contains(orderNo));
		
		Assert.assertTrue("Keycode did not match or missing",
				BaseClass.emailPre.orderDetails.get(3).getText().contains(keycode));
		
		Assert.assertEquals("Order Total did not match or missing", total,
				BaseClass.emailPre.totalprice.getText().replaceAll("[^0-9.]", ""));
		
		Assert.assertEquals("Subject Line did not match or missing", "National Pen - Approval Needed - " + orderNo,
				BaseClass.emailPre.subjectLine.getAttribute("value"));
		
		Assert.assertEquals("Email instruction did not match or missing",
				"Please click anywhere on the preview below to copy the information and open a new e-mail. Once the e-mail is opened, simply paste the information (Ctrl+V).",
				BaseClass.emailPre.emailMsg.getText().trim());

		Assert.assertEquals("Only Logo Instruction did not match or missing",
				"Please refer to the attached PDF which will show your personalized imprint or artwork on chosen product.",
				BaseClass.emailPre.onlyLogoInst.getText());
		
		Assert.assertTrue("Ship To Address not matched or missing", (BaseClass.emailPre.shipToAdd.getText()
				.replaceAll("\\s+", "") + "\"").contains(StringUtils.substringBefore((shipToadd.replaceAll("\\s+", "").replaceAll(",", "") + "\""), "ShipToContact")));

		Assert.assertTrue("Bill To Address not matched or missing", (BaseClass.emailPre.billToAdd.getText()
				.replaceAll("\\s+", "") + "\"").contains(StringUtils.substringBefore((billToadd.replaceAll("\\s+", "").replaceAll(",", "") + "\""), "BillToContact")));

	}

	@Then("User validate only imprint for inhosue product {string}")
	public void user_validate_only_imprint_for_inhosue_product(String keycode) throws InterruptedException {
		BaseClass.select.selectByVisibleText(BaseClass.emailPre.emailLang, "English");
		manageOrder.searchBtn.click();
		Thread.sleep(5000);
		BaseClass.wait.until(ExpectedConditions.attributeContains(BaseClass.emailPre.subjectLine, "value", "National Pen - Approval Needed"));
		
		Assert.assertTrue("customer number did not match or missing",
				BaseClass.emailPre.orderDetails.get(1).getText().contains(custNo));
		
		Assert.assertTrue("Order number did not match or missing",
					BaseClass.emailPre.orderDetails.get(2).getText().contains(orderNo));
		
		Assert.assertTrue("Keycode did not match or missing",
				BaseClass.emailPre.orderDetails.get(3).getText().contains(keycode));
		
		Assert.assertEquals("Order Total did not match or missing", total,
				BaseClass.emailPre.totalprice.getText().replaceAll("[^0-9.]", ""));
		
		Assert.assertEquals("Subject Line did not match or missing", "National Pen - Approval Needed - " + orderNo,
					BaseClass.emailPre.subjectLine.getAttribute("value"));
		
		Assert.assertEquals("Email instruction did not match or missing",
				"Please click anywhere on the preview below to copy the information and open a new e-mail. Once the e-mail is opened, simply paste the information (Ctrl+V).",
				BaseClass.emailPre.emailMsg.getText().trim());
		
		Assert.assertTrue("Imprint Line did not match or missing", 
				BaseClass.emailPre.imprintPre.getText().contains("Updated Imprint 1"));
		
		Assert.assertTrue("Product Image is missing",
				BaseClass.emailPre.productImg.getAttribute("src").contains("https://natpen.scene7.com"));

		Assert.assertTrue("Ship To Address not matched or missing", (BaseClass.emailPre.shipToAdd.getText()
				.replaceAll("\\s+", "") + "\"").contains(StringUtils.substringBefore((shipToadd.replaceAll("\\s+", "").replaceAll(",", "") + "\""), "ShipToContact")));

		Assert.assertTrue("Bill To Address not matched or missing", (BaseClass.emailPre.billToAdd.getText()
				.replaceAll("\\s+", "") + "\"").contains(StringUtils.substringBefore((billToadd.replaceAll("\\s+", "").replaceAll(",", "") + "\""), "BillToContact")));

	}

	@Then("User validate no logo and imprint for inhosue product {string}")
	public void user_validate_no_logo_and_imprint_for_inhosue_product(String keycode) throws InterruptedException {
		BaseClass.select.selectByVisibleText(BaseClass.emailPre.emailLang, "English");
		manageOrder.searchBtn.click();
		Thread.sleep(5000);
		BaseClass.wait.until(ExpectedConditions.attributeContains(BaseClass.emailPre.subjectLine, "value", "National Pen - Approval Needed"));
		
		Assert.assertTrue("customer number did not match or missing",
				BaseClass.emailPre.orderDetails.get(1).getText().contains(custNo));
	   
		Assert.assertTrue("Order number did not match or missing",
					BaseClass.emailPre.orderDetails.get(2).getText().contains(orderNo));
		
		Assert.assertTrue("Keycode did not match or missing",
				BaseClass.emailPre.orderDetails.get(3).getText().contains(keycode));
		
		Assert.assertEquals("Order Total did not match or missing", total,
				BaseClass.emailPre.totalprice.getText().replaceAll("[^0-9.]", ""));
		
		Assert.assertEquals("Subject Line did not match or missing", "National Pen - Approval Needed - " + orderNo,
					BaseClass.emailPre.subjectLine.getAttribute("value"));
		
		Assert.assertEquals("Email instruction did not match or missing",
				"Please click anywhere on the preview below to copy the information and open a new e-mail. Once the e-mail is opened, simply paste the information (Ctrl+V).",
				BaseClass.emailPre.emailMsg.getText().trim());
		
		Assert.assertEquals("No logo and Imprint instruction did not match or missing",
				"This product will NOT have personalised imprint or artwork on it. The product will be received completely blank",
				BaseClass.emailPre.noLogoImp.getText());
		
		Assert.assertTrue("Ship To Address not matched or missing", (BaseClass.emailPre.shipToAdd.getText()
				.replaceAll("\\s+", "") + "\"")
				.contains(StringUtils.substringBefore((shipToadd.replaceAll("\\s+", "")
						.replaceAll(",", "") + "\""), "ShipToContact")));
		
		Assert.assertTrue("Bill To Address not matched or missing", (BaseClass.emailPre.billToAdd.getText()
				.replaceAll("\\s+", "") + "\"").contains(StringUtils.substringBefore((billToadd.replaceAll("\\s+", "").replaceAll(",", "") + "\""), "BillToContact")));
	}

	@Then("User validate logo and imprint for Vendor product {string}")
	public void user_validate_logo_and_imprint_for_Vendor_product(String keycode) throws InterruptedException {
		BaseClass.select.selectByVisibleText(BaseClass.emailPre.emailLang, "English");
		manageOrder.searchBtn.click();
		Thread.sleep(5000);
		BaseClass.wait.until(ExpectedConditions.attributeContains(BaseClass.emailPre.subjectLine, "value", "National Pen - Approval Needed"));
		
		Assert.assertTrue("customer number did not match or missing",
				BaseClass.emailPre.orderDetails.get(1).getText().contains(custNo));
	    
		Assert.assertTrue("Order number did not match or missing",
					BaseClass.emailPre.orderDetails.get(2).getText().contains(orderNo));
		
//	    Assert.assertTrue("Keycode did not match or missing",
//				BaseClass.emailPre.orderDetails.get(3).getText().contains(keycode));
		
	    Assert.assertEquals("Order Total did not match or missing", total,
				BaseClass.emailPre.totalprice.getText().replaceAll("[^0-9.]", ""));
	    
		Assert.assertEquals("Subject Line did not match or missing", "National Pen - Approval Needed - " + orderNo,
					BaseClass.emailPre.subjectLine.getAttribute("value"));
		
		Assert.assertTrue("Imprint Line did not match or missing", 
				BaseClass.emailPre.imprintPre.getText().contains("Imprint 1"));
		
		Assert.assertEquals("Email instruction did not match or missing",
				"Please click anywhere on the preview below to copy the information and open a new e-mail. Once the e-mail is opened, simply paste the information (Ctrl+V).",
				BaseClass.emailPre.emailMsg.getText().trim());
		
		Assert.assertTrue("Ship To Address not matched or missing", (BaseClass.emailPre.shipToAdd.getText()
				.replaceAll("\\s+", "") + "\"")
				.contains(StringUtils.substringBefore((shipToadd.replaceAll("\\s+", "")
						.replaceAll(",", "") + "\""), "ShipToContact")));	
	
		Assert.assertTrue("Bill To Address not matched or missing",
				(BaseClass.emailPre.billToAdd.getText().replaceAll("\\s+", "") + "\"").contains(
						StringUtils.substringBefore((billToadd.replaceAll("\\s+", "").replaceAll(",", "") + "\""),
								"BillToContact")));

	}

	@Then("User enters saleRep {string} and verify logo and imprint")
	public void user_enters_sale_rep_and_and_verify_logo_and_imprint(String salesRep) {
		if (Hooks.sceanrioName.contains("T89") || Hooks.sceanrioName.contains("Copy Order")) {
			Assert.assertEquals("true", copyOrder.copyOrderImp.getAttribute("readonly"));
			BaseClass.select.selectByVisibleText(copyOrder.salesRep, salesRep);
		}
		BaseClass.exceptionHandle.visibilityOf(copyOrder.copyFrom);
		Assert.assertEquals(orderNo, copyOrder.copyFrom.getText());
		Assert.assertFalse("Imprint is missing", copyOrder.copyImpPre.getText().isEmpty());
		Assert.assertTrue("Logo is missing", copyOrder.copyOrderImg.getAttribute("src").contains("https://uploads.documents.cimpress.io"));
	}

	@Then("User enters saleRep {string} and verify logo")
	public void user_enters_saleRep_and_verify_logo(String salesRep) {
		if(Hooks.sceanrioName.contains("Copy Order"))
		BaseClass.select.selectByVisibleText(copyOrder.salesRep, salesRep);
		Assert.assertEquals(orderNo, copyOrder.copyFrom.getText());
		Assert.assertTrue("Imprint is Present", copyOrder.copyImpPre.getText().isEmpty());
		Assert.assertTrue("Logo is missing",
				copyOrder.copyOrderImg.getAttribute("src").contains("https://uploads.documents.cimpress.io"));
	}

	@Then("User enters saleRep {string} and verify imprint")
	public void user_enters_saleRep_and_verify_imprint(String salesRep) throws InterruptedException {
		Thread.sleep(2000);
		BaseClass.getDriver().navigate().refresh();
		Thread.sleep(3000);
		//implines = manageOrder.imprintLines.size();
		if(Hooks.sceanrioName.contains("Copy Order"))
		BaseClass.select.selectByVisibleText(copyOrder.salesRep, salesRep);
		Assert.assertEquals(orderNo, copyOrder.copyFrom.getText());
		Assert.assertFalse("Imprint is missing", copyOrder.copyImpPre.getText().isEmpty());

	}

	@Then("User enters saleRep {string}")
	public void user_enters_saleRep(String salesRep) throws InterruptedException {
		BaseClass.wait.until(ExpectedConditions.textToBePresentInElement(copyOrder.copyFrom, orderNo));
		if(Hooks.sceanrioName.contains("Copy Order"))
		BaseClass.select.selectByVisibleText(copyOrder.salesRep, salesRep);
		Assert.assertEquals(orderNo, copyOrder.copyFrom.getText());
		Assert.assertTrue("Imprint is Present", copyOrder.copyImpPre.getText().isEmpty());

	}


	@Then("Order should place successfully")
	public void order_should_place_successfully() {
		BaseClass.wait.until(ExpectedConditions.visibilityOf(copyOrder.copySuccessMsg));
		System.out.println(BaseClass.manageOrder.orderNumberField.getAttribute("value"));
		BaseClass.excel.copyExcelData("MatrixOrder", "CopyOrder", "ABCOrderNumber", orderNoABC);
		BaseClass.excel.e.setCellData("CopyOrder", "CopyOrderNumber", BaseClass.excel.sheet2row, BaseClass.manageOrder.orderNumberField.getAttribute("value"));
		BaseClass.excel.e.setCellData("CopyOrder", "S.Num", BaseClass.excel.sheet2row, String.valueOf(BaseClass.excel.sheet2row-1));
		BaseClass.excel.e.setCellData("CopyOrder", "OracleNumber", BaseClass.excel.sheet2row, orderNo);
		BaseClass.excel.e.setCellData("CopyOrder", "TestName", BaseClass.excel.sheet2row, "Copy Order");		

	}

	@Then("User clicks on Redo order")
	public void user_clicks_on_Redo_order() {
		 Assert.assertEquals("Oracle Order Status is not Closed or missing",
		 "CLOSED", BaseClass.manageOrder.oracleOrderStatus.getText());
//		 Assert.assertEquals("ABC Line Header status is not Closed or missing",
//		 "Closed", manageOrder.abcStatusHeader.getText());
//		 Assert.assertEquals("ABC Line status is not Closed or missing", "Closed",
//		 manageOrder.getAbcLineStatus("Mainitem").getText());
		 BaseClass.action.moveToElement(BaseClass.manageOrder.redoOrder);
		 BaseClass.manageOrder.redoOrder.click();
	}

	

@Then("User clicks on {string} to update")
	public void user_clicks_on_item_to_update(String lineType) {
	Assert.assertEquals("ABC Status is not Booked", "BOOKED", manageOrder.oracleOrderStatus.getAttribute("textContent"));
	manageOrder.getUpdateLine(lineType).click();
	BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
	if (lineType.equalsIgnoreCase("PREMIUM")) {
		Assert.assertEquals("Warning for updation of premium product did not display", AssertInfo.PREMIUM_ITEM_UPDATE_LINE, BaseClass.manageOrder.updatePremiumProductText.getText());
		BaseClass.getDriver().switchTo().defaultContent();
		BaseClass.manageOrder.closeUpdateOrderPopup.click();	
	}
	}

	@Then("User enters price {string} and Qty {string} and apply changes for {string}")
	public void user_enters_price_and_qty_and_apply_changes(String price, String qty, String lineType) throws InterruptedException {
		float mainprice, subtotal, item2price=0, logoPrice=0, shipcharge, tax=0,setup=0;
		int mainqty, item2Qty=0;	
		if(!lineType.equals("Free")) {
		manageOrder.updatePrice.clear();
		manageOrder.updatePrice.sendKeys(price);
		}
		if(manageOrder.updateQty.getAttribute("type").equals("text")) {
		manageOrder.updateQty.clear();
		manageOrder.updateQty.sendKeys(qty);
		}
		orderconf.applychg.click();
		BaseClass.getDriver().switchTo().defaultContent();
		Thread.sleep(10000);
		 mainqty = Integer.parseInt(manageOrder.getLineItemQty("Mainitem").getAttribute("textContent"));
		 mainprice = Float.parseFloat(manageOrder.getLineItemPrice("Mainitem").getAttribute("textContent"));	
		 BaseClass.excel.copyExcelDataUpdateLine("OrdersForModification", "ModifiedOrder", "ABCOrderNumber", orderNoABC);	     
	     try {
	    	 logoPrice = Float.parseFloat(manageOrder.getLineItemPrice("Logocharge").getAttribute("textContent"));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	     
	     try {
	    	 setup = Float.parseFloat(manageOrder.getLineItemPrice("Setupcharge").getAttribute("textContent"));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	  
	     subtotal = Float.parseFloat(manageOrder.subTotal.getAttribute("textContent"));
	     tax = Float.parseFloat(manageOrder.tax.getAttribute("textContent"));
	     shipcharge = Float.parseFloat(manageOrder.shippingCharge.getAttribute("textContent"));
	     if(!lineType.equalsIgnoreCase("Mainitem")) {
	    	 item2Qty= Integer.parseInt(manageOrder.getLineItemQty(lineType).getText());
	    	 item2price= Float.parseFloat(manageOrder.getLineItemPrice(lineType).getText());
	     }
		Assert.assertEquals(lineType+" QTY not matching or missing", qty, manageOrder.getLineItemQty(lineType).getText());
		Assert.assertEquals(lineType+" Price not matching or missing", price, manageOrder.getLineItemPrice(lineType).getText());
		
		Assert.assertEquals("Updated SubTotal not matching or missing",
				String.valueOf(df.format((mainprice * mainqty)+(item2price*item2Qty))), String.valueOf(df.format(subtotal)));
		
		
		Assert.assertEquals("Updated Total not matching or missing",
				String.valueOf(df.format((mainprice * mainqty)+(item2price*item2Qty)+logoPrice + shipcharge + tax+setup)), df.format(Float.parseFloat(manageOrder.total.getText())));
		
		getLineItemDetails(manageOrder.lineItemPrice, "Unit Selling Price");
		getLineItemDetails(manageOrder.lineQty, "Qty");
			
		//BaseClass.excel.e.setCellData("ModifiedOrder","PackagePrice", BaseClass.excel.sheet2row, );
		BaseClass.excel.e.setCellData("ModifiedOrder", "S.Num", BaseClass.excel.sheet2row, String.valueOf(BaseClass.excel.sheet2row-1));
		BaseClass.excel.copyExcelDataUpdateLine("OrdersForModification", "ModifiedOrder", "ABCOrderNumber", orderNoABC);
		BaseClass.excel.e.setCellData("ModifiedOrder", "Total", BaseClass.excel.sheet2row-1, manageOrder.total.getText());
		BaseClass.excel.e.setCellData("ModifiedOrder","Subtotal", BaseClass.excel.sheet2row-1,  manageOrder.subTotal.getText());
		BaseClass.excel.e.setCellData("ModifiedOrder","Tax", BaseClass.excel.sheet2row-1,manageOrder.tax.getText());
		BaseClass.excel.e.setCellData("ModifiedOrder", "OracleNumber", BaseClass.excel.sheet2row-1, orderNo);
		BaseClass.excel.e.setCellData("ModifiedOrder", "TestName", BaseClass.excel.sheet2row-1, "Validate_Modification");		
		BaseClass.excel.e.setCellData("ModifiedOrder", "Modifications", BaseClass.excel.sheet2row-1,lineType+"_Modification" );
	
	
	}

	@Then("User click on add dependent line")
	public void user_clicks_on_add_discount_ship() {
		Assert.assertEquals("oracle status is not in BOOKED status", "BOOKED", manageOrder.oracleOrderStatus.getAttribute("textContent"));
		Assert.assertEquals("Order is not in BOOKED status", "BOOKED", manageOrder.mainItem.get(3).getAttribute("textContent"));
		manageOrder.addDepLine.click();
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(manageOrder.depLineFrame));

	}

	@When("User select dependent line type {string}")
	public void user_enters_discount_price_and_apply_changes(String lineType) {
		manageOrder.selectDependentLineType(lineType);
		
	}
	@When("User select additional item {string} and click on save")
	public void user_select_additional_item_enter_quantity_and_price(String item) throws InterruptedException {
		BaseClass.wait.until(ExpectedConditions.elementToBeClickable(manageOrder.addChargesItem)).click();
		BaseClass.getDriver().switchTo().defaultContent();
		BaseClass.accOnly.selectAccessory(item);
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(manageOrder.depLineFrame));
		BaseClass.wait.until(ExpectedConditions.invisibilityOf(BaseClass.manageOrder.bigprocessEle));
		BaseClass.manageOrder.saveBtn4.click();
		BaseClass.getDriver().switchTo().defaultContent();
	}

	@Then("Additional charges Line with item {string} should be added")
	public void additional_charges_Line_with_should_be_added(String item) {
		BaseClass.wait.until(ExpectedConditions.elementToBeClickable(manageOrder.total));
		addChargeQty = BaseClass.manageOrder.getAddDepedentLineQTY(item).getAttribute("textContent");
		addChargePrice = BaseClass.manageOrder.getAddDepedentLinePrice(item).getAttribute("textContent");

		Assert.assertTrue("New line having" + item + "was not added", BaseClass.manageOrder
				.getAllLineItemNameWithoutCancelledstatus().stream().filter(x -> x.getText().equals(item)).count() > 0);
		Assert.assertEquals("Quantity is not correct for additional charges",
				BaseClass.manageOrder.getLineItemQty("Mainitem").getAttribute("textContent"), addChargeQty);
		Assert.assertEquals("Subtotal is not matching after adding additional charges", subTotal,
				BaseClass.manageOrder.subTotal.getAttribute("textContent"));
		Assert.assertEquals("Charges is not matching after adding additional charges",
				String.valueOf(new DecimalFormat().format(
						(Float.valueOf(addChargeQty) * Float.valueOf(addChargePrice)) + Float.valueOf(charges))),
				BaseClass.manageOrder.orderCharges.getAttribute("textContent"));
		Assert.assertEquals("Total is not matching after adding additional charges",
				String.valueOf(Float.valueOf(BaseClass.manageOrder.orderCharges.getAttribute("textContent"))
						+ Float.valueOf(subTotal)
						+ Float.valueOf(BaseClass.manageOrder.tax.getAttribute("textContent"))),
				BaseClass.manageOrder.total.getAttribute("textContent"));
	}
	@Then("User select discount percentage {string} and click on save")
	public void discount_line_is_added(String discount) {
		BaseClass.wait.until(ExpectedConditions.elementToBeClickable(manageOrder.selectDiscountPercentage(discount))).click();
		BaseClass.wait.until(ExpectedConditions.elementToBeClickable(manageOrder.saveBtn3)).click();
		manageOrder.checkMsgDepLineAdded("Others", AssertInfo.DISCOUNT_LINE);

	}
	
	 @Then("Discount line should be added")
	 public void discount_line_should_be_added() {
		BaseClass.getDriver().navigate().refresh();
		Assert.assertNotEquals("Total not changed after Discount ",total,
				manageOrder.total.getAttribute("textContent"));	 
	 } 
	 
	 @When ("User select logo charges {string} and add logo Charge")
		public void user_select_logo_charges(String logoCharge) {
		  Float logocharge=0.00f;
		  Float logoAsBefore=0.00f;
		  BaseClass.wait.until(ExpectedConditions.elementToBeClickable(manageOrder.logoChargeinput));
			if (logoCharge.equalsIgnoreCase("logoCharge")) {
				BaseClass.js.click(manageOrder.logoChargeinput);
				logocharge = Float.valueOf(manageOrder.logoCharge.getAttribute("value"));
			} else if (logoCharge.equalsIgnoreCase("logoAsBefore")) {
				BaseClass.js.click(manageOrder.logoAsBeforeChargeinput);
				logoAsBefore = Float.valueOf(manageOrder.logoAsBeforeCharge.getAttribute("value"));
			} else if (logoCharge.equalsIgnoreCase("both")) {
				BaseClass.js.click(manageOrder.logoChargeinput);
				BaseClass.js.click(manageOrder.logoAsBeforeChargeinput);
				logocharge = Float.valueOf(manageOrder.logoCharge.getAttribute("value"));
				logoAsBefore = Float.valueOf(manageOrder.logoAsBeforeCharge.getAttribute("value"));
			}
			 BaseClass.wait.until(ExpectedConditions.elementToBeClickable(manageOrder.saveBtn2)).click();
		    manageOrder.checkMsgDepLineAdded("Add Logo", AssertInfo.LOGO_CHARGE_LINE);
			Assert.assertEquals("LogoCharge is not matching after adding LogoCharge",
					String.valueOf(new DecimalFormat().format(logoAsBefore+logocharge+Float.valueOf(shipcharge))), manageOrder.orderCharges.getAttribute("textContent"));			
		Assert.assertEquals("Total is not matching after adding LogoCharge",String.valueOf(new DecimalFormat().format(
	   Float.valueOf(subTotal)+Float.valueOf(manageOrder.tax.getAttribute("textContent"))+Float.valueOf(manageOrder.orderCharges.getAttribute("textContent")))), manageOrder.total.getAttribute("textContent"));
		}
	 
	@When("User select accessory item {string} and select item and enter quantity {string} and price {string} and click on save")
	public void user_select_accessory_item_and_select_item_and_enter_quantity(String item, String qty, String price) throws InterruptedException {
		accQty=qty;
		BaseClass.wait.until(ExpectedConditions.elementToBeClickable(manageOrder.accessoryItem)).sendKeys(item);
		BaseClass.getDriver().switchTo().defaultContent();
		BaseClass.accOnly.selectAccessory(item);
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(manageOrder.depLineFrame));
		BaseClass.manageOrder.accessoryQTY.sendKeys(qty);
		Thread.sleep(5000);
		if(!price.isEmpty()) {
			manageOrder.accessoryPrice.clear();
			manageOrder.accessoryPrice.sendKeys(price);
			accPrice=price;
		}else {
			accPrice=BaseClass.js.value(manageOrder.accessoryPrice);
		}
		BaseClass.manageOrder.saveBtn.click();
	}
	
	@Then ("Accessory Line with {string} should be added")
	public void accessory_Line_with_should_be_added(String line) {
		BaseClass.getDriver().switchTo().defaultContent();
		BaseClass.wait.until(ExpectedConditions.elementToBeClickable(manageOrder.total));		
		Assert.assertEquals("Total is not matching after adding Accessory",String.valueOf(new DecimalFormat().format((Float.valueOf(accPrice)*Float.valueOf(accQty)) + 
				Float.valueOf(subTotal)+Float.valueOf(manageOrder.tax.getAttribute("textContent"))+Float.valueOf(manageOrder.orderCharges.getAttribute("textContent")))), manageOrder.total.getAttribute("textContent"));	
		Assert.assertEquals("New Line not added", lines+1, manageOrder.getTotalLine());
		
	}
	
	 @Then ("Upsell Line should be added")
	 public void upsell_line_should_be_added() {
		 String upsellTotal= manageOrder.upsellTotal.getAttribute("textContent");
		 BaseClass.wait.until(ExpectedConditions.elementToBeClickable(manageOrder.saveBtn3)).click();
		 manageOrder.checkMsgDepLineAdded("Others", AssertInfo.UPSELL_LINE);
		 BaseClass.wait.until(ExpectedConditions.visibilityOf(manageOrder.total));
		Assert.assertEquals("New Line not added", lines+1, manageOrder.getTotalLine());
		Assert.assertEquals("Total is not matching after adding Upsell",String.valueOf(new DecimalFormat().format(Float.valueOf(upsellTotal) + 
				Float.valueOf(subTotal)+Float.valueOf(manageOrder.tax.getAttribute("textContent"))+Float.valueOf(manageOrder.orderCharges.getAttribute("textContent")))), manageOrder.total.getAttribute("textContent"));
        
		 
	 }

    @Then("User select the upsell and free {string}")
	public void user_select_the_upsell_and_free(String free) throws InterruptedException {
			Assert.assertTrue("Upsell NO is not selected ByDefault",BaseClass.CustProd.upsellNo.isSelected());		
			BaseClass.CustProd.upsellYes.click();
			BaseClass.CustProd.freeQty(free).click();	
			BaseClass.manageOrder.saveBtn3.click();
			BaseClass.getDriver().switchTo().defaultContent();
	}

   @Then("verify Upsell & Free lines are added")
     public void verify_upsell_free_lines_are_added_in_manage_order_page() throws InterruptedException {
	BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.manageOrder.upSellLine)).isDisplayed();
	BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.manageOrder.freeLine)).isDisplayed();
  }
     @Then("User select the upsell and discount {string}")
     public void user_select_the_upsell_and_discount(String discount) throws InterruptedException {
	BaseClass.wait.until(ExpectedConditions.elementToBeClickable(manageOrder.selectDiscountPercentage(discount))).click();
	Assert.assertTrue("Upsell NO is not selected ByDefault",BaseClass.CustProd.upsellNo.isSelected());
	BaseClass.CustProd.upsellYes.click();
	BaseClass.wait.until(ExpectedConditions.elementToBeClickable(manageOrder.saveBtn3)).click();
	BaseClass.getDriver().switchTo().defaultContent();
}
     @Then("verify Upsell and discount lines are added")
 	public void verify_Upsell_and_discount_lines_are_added_in_manage_order_page() {
    	 BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.manageOrder.upSellLine)).isDisplayed();
         BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.manageOrder.discountLine)).isDisplayed();
 		}
	@Then("click on cancellation history")
	public void click_on_cancellation_history() {
		manageOrder.cancellationHistoryBtn.click();
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));

	}

	@Then("User can see cancellation History")
	public void user_can_see_cancellation_history() {
		Assert.assertTrue("Cancellation History not visible", manageOrder.cancellationHistoryTable.isDisplayed());

	}

	@Then("User selects the cancellation category {string} and reason {string}")
	public void user_selects_the_cancellation_reason(String category, String reason) {
			BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
			BaseClass.select.selectByVisibleText(manageOrder.selectChangeCategory, category);
			BaseClass.select.selectByVisibleText(manageOrder.selectCancelReason, reason);
     		cancelReason= reason;
			manageOrder.submitBtn.click();
	}

	@Then("Validate Pay button displayed")
	public void user_can_see_Pay_Button() {
		boolean display = false;
		if (BaseClass.getDriver().getTitle().contains("Order Confirmation - Order Details")) {
			BaseClass.orderconf.manageOrder.click();
		}
		for (int i = 0; i < 3; i++) {
			BaseClass.exceptionHandle.visibilityOf(manageOrder.searchBtn);
			try {
				display = BaseClass.manageOrder.payButton.isDisplayed();
				if (display)
					break;
			} catch (Exception e) {
				manageOrder.searchBtn.click();
				continue;
			}
		}
		Assert.assertTrue("Pay button not visible", display);
	}
	@Then("User Validate {string} is cancelled")
	public void user_validate_if_the_item_is_cancelled(String lineType) {
		BaseClass.getDriver().switchTo().defaultContent();
		Assert.assertTrue("Message did not displayed or missing",
				BaseClass.wait.until(ExpectedConditions.visibilityOf(manageOrder.cancelLineMsg)).isDisplayed());
		BaseClass.pageRefresh();
			manageOrder.getCancelLine(lineType).click();
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
		Assert.assertTrue("CIP hold has been applied for this Line. Cancellation is in progress. **NOT SHOWING** ",BaseClass.wait.until(ExpectedConditions.visibilityOf(manageOrder.cancelledOrderMessageDisplay)).isDisplayed());
		BaseClass.excel.copyExcelData("OrdersForModification", "ModifiedOrder", "ABCOrderNumber", orderNoABC);
		BaseClass.excel.e.setCellData("ModifiedOrder", "S.Num", BaseClass.excel.sheet2row, String.valueOf(BaseClass.excel.sheet2row-1));
		BaseClass.excel.e.setCellData("ModifiedOrder", "OracleNumber", BaseClass.excel.sheet2row, orderNo);
		BaseClass.excel.e.setCellData("ModifiedOrder", "TestName", BaseClass.excel.sheet2row, "Validate_Cancellation");		
		BaseClass.excel.e.setCellData("ModifiedOrder", "Reason", BaseClass.excel.sheet2row, cancelReason);
		BaseClass.excel.e.setCellData("ModifiedOrder", "Modifications", BaseClass.excel.sheet2row,lineType+"_Cancellation" );
		BaseClass.getDriver().switchTo().defaultContent();
		BaseClass.js.click(BaseClass.manageOrder.closeCancelOrderPopup);
	}

	@Then("User Click on cancel {string} Line Item")
	public void user_click_on_cancel_line_Item(String lineType) {
//		Assert.assertEquals("Line Status not mathing or missing ", "BOOKED",
//				manageOrder.getlLineStatus(lineType).getAttribute("textContent"));
			manageOrder.getCancelLine(lineType).click();
		
		

	}


	@When("User clicks on book order button")
	public void user_clicks_on_book_order_button() {
        manageOrder.oracleOrderStatus.getAttribute("textContent").equalsIgnoreCase("Entered");
        manageOrder.getlLineStatus("Mainitem").getAttribute("textContent").equalsIgnoreCase("Entered");
       BaseClass.js.click(BaseClass.wait.until(ExpectedConditions.elementToBeClickable(manageOrder.bookOrder)));
		
	}

	@Then("Order status should change to Booked")

	public void order_status_change_should_to_booked() {
		Assert.assertTrue("order is not BOOKED or status not matching",BaseClass.wait.until(ExpectedConditions.attributeToBe(manageOrder.oracleOrderStatus,"textContent", "BOOKED")));
		Assert.assertEquals("order is not BOOKED or status not matching","BOOKED", manageOrder.getlLineStatus("Mainitem").getAttribute("textContent"));
		
	} 
	
	
	@Then("User clicks Update in lineType Shipping and selects shipment as express")
	public void user_select_express_shipment(){
		double expressPrice;
		String style = "";
		String lineType= "Shipping";
		String highlightedStyle = "border-color: red; border-width: medium;";
		double diffInPrice;		
		double shippingPrice=Double.parseDouble(BaseClass.manageOrder.getLineItemPrice(lineType).getAttribute("textContent"));
		Double orderCharges = Double.parseDouble(manageOrder.orderCharges.getAttribute("textContent"));	
		manageOrder.getUpdateLine(lineType).click();
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
		BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.manageOrder.updatePrice));
		
		style = BaseClass.manageOrder.updatePrice.getAttribute("style");
		
		if (BaseClass.manageOrder.shipmentPriorityStandard.isSelected()) {	
			// border should not be highlighted in red
			Assert.assertFalse("Ordered Price border should not be highlighted in red for standard shipping", style.contains(highlightedStyle));	

			BaseClass.js.click(BaseClass.manageOrder.shipmentPriorityExpress);
			Assert.assertTrue("Express shipment was not selected",
					BaseClass.manageOrder.shipmentPriorityExpress.isSelected());
			BaseClass.wait.until(ExpectedConditions.attributeToBeNotEmpty(BaseClass.manageOrder.updatePrice, "value"));

			// For express shipment, price should be greater than standard
			expressPrice = Double.parseDouble(BaseClass.manageOrder.updatePrice.getAttribute("value"));
			diffInPrice=expressPrice-shippingPrice;
			Assert.assertTrue("Express shipment price was not greater than standard shipment price", diffInPrice>0);
				
			// border should be highlighted in red after selecting express shipment
			style = BaseClass.manageOrder.updatePrice.getAttribute("style");
			Assert.assertTrue("Ordered Price border should be highlighted in red", style.contains(highlightedStyle));
			
			Assert.assertEquals("Shipping price updation message was not displayed",
					"Shipping Price is updated based on selection.", BaseClass.manageOrder.shipmentMessage.getText());	
			   orderconf.applychg.click();
			   BaseClass.getDriver().switchTo().defaultContent();
			   BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.manageOrder.total));		
			   manageOrder.orderCharges.getAttribute("textContent");
			   //Adding SubTotal+Tax+OrderCharges+PriceDifference
				Assert.assertEquals("Total was not updated after changing shipping method from Standard to Express ", 
						 Double.parseDouble(manageOrder.subTotal.getAttribute("textContent"))+diffInPrice+Double.parseDouble(manageOrder.tax.getAttribute("textContent"))+orderCharges, 
						Double.parseDouble(BaseClass.manageOrder.total.getAttribute("textContent")), 0.0001);	
				}			
			}	
	
	
	@Then("User checks the lines status for {string} is cancelled")
	public void user_checks_lines_statuses(String lineType) {
		Assert.assertEquals("Line Status is not CANCELLED", "CANCELLED",
				manageOrder.getlLineStatus(lineType).getAttribute("textContent"));		
	}
	@Then("Validate Oracle Status {string} and ABC Status {string} and validate the pay button")
	public void validate_status_oracle_abc_(String oracleStatus, String abcStatus) {	
		Assert.assertTrue("Expected status is not matched with dispalayed status of ABC and Oracle",
				manageOrder.oracleOrderStatus.getAttribute("textContent").equalsIgnoreCase(oracleStatus)
						&& manageOrder.abcStatusHeader.getAttribute("textContent").equalsIgnoreCase(abcStatus));
		try {
			Assert.assertFalse("Pay button is Displayed", BaseClass.manageOrder.payButton.isDisplayed());
		} catch (Exception e) {
			e.getMessage();
		}
	}
	@Then("User validate order line items")
	public void user_validate_line_items() throws InterruptedException
			 {	
		String emailPreSubTotal=BaseClass.emailPre.subtotalprice.getText().replaceAll("[^\\-\\d.]+|\\.(?!\\d)", "");
		String emailPreTax=BaseClass.emailPre.tax.getText().replaceAll("[^\\-\\d.]+|\\.(?!\\d)", "");
		String emailPreTotal=BaseClass.emailPre.totalprice.getText().replaceAll("[^\\-\\d.]+|\\.(?!\\d)", "");		
		//validation for Order subtotal, tax and Total  between email preview and manage order 
		Assert.assertEquals("Order Total did not match or missing", total,emailPreTotal);
		Assert.assertEquals("Order SubTotal did not match or missing", df.format(Double.parseDouble(subTotal)+Double.parseDouble(charges)),
				 emailPreSubTotal);
		if(tax.equals("0"))
			tax="0.00";
		Assert.assertEquals("Order Tax did not match or missing", tax, emailPreTax);
		//validation of all line items having other than cancelled or blank status
		ArrayList<ArrayList<String>> emailPreAllProducts = new ArrayList<ArrayList<String>>();	
		double expectedSubtotal=0.0;
		int sizeItemsEmailPrev=BaseClass.emailPre.productDescription.size();
		for(int j=0;j<sizeItemsEmailPrev;j++)
		{
			String prodPrice=BaseClass.emailPre.productPrice.get(j).getText().replaceAll("[^\\-\\d.]+|\\.(?!\\d)", "").trim();
			String prodQuant=BaseClass.emailPre.productQuantity.get(j).getText().trim();		
			String actualLineTotal= BaseClass.emailPre.lineTotal.get(j).getText().replaceAll("[^\\-\\d.]+|\\.(?!\\d)", "").trim();
			ArrayList<String> emailPreProduct=new ArrayList<String>();
			emailPreProduct.add(BaseClass.emailPre.productDescription.get(j).getText().trim());
			emailPreProduct.add(prodPrice);
			emailPreProduct.add(prodQuant);
		    emailPreAllProducts.add(emailPreProduct);
			expectedSubtotal+=Double.parseDouble(actualLineTotal);		
		}
		Assert.assertEquals("Total number of line item not maching in Email Preview with Manage order",sizeItemsManageOrder,sizeItemsEmailPrev);
			
		//compare lineItems from Manage order and emailPreAllProducts
		for(int a=0;a<sizeItemsEmailPrev;a++)
		{
					
			Assert.assertTrue("Email Preview line item was incorrect",lineItems.get(a).get(0).contains(emailPreAllProducts.get(a).get(0)));
			Assert.assertEquals("Line item Price was incorrect",lineItems.get(a).get(1),emailPreAllProducts.get(a).get(1));
			Assert.assertEquals("Line item Quanity was incorrect",lineItems.get(a).get(2),emailPreAllProducts.get(a).get(2));
					
		}			
		//comparing expected subtotal(by multiplying and adding individual items quantity and price) and actual visible Subtotal on email preview
		Assert.assertEquals("SubTotal was missing or incorrect ", df.format(expectedSubtotal), emailPreSubTotal);
	
		//compare total by adding subtotal and tax
		Assert.assertEquals("Total was missing or incorrect ", df.format(expectedSubtotal+Double.parseDouble(emailPreTax)), emailPreTotal);	
		
	}
	@Then("User clicks on Fax preview button")
	public void user_click_on_fax_preview_button() throws InterruptedException {
		
		lineItems = new ArrayList<ArrayList<String>>();	
		
		
		int next;
		if(lines%5==0)
			next=lines/5-1;
		else
			next=lines/5;
		
		int size;
		
		//to move to next page for getting item details in manage order
		for(int i=0;i<=next;i++)
		{
	
		  size=BaseClass.manageOrder.getAllLineItemNameWithoutCancelledstatus().size();
		 System.out.println("size"+size);
		 sizeItemsManageOrder+=size;
		for(int j=0;j<size;j++)
		{
			ArrayList<String> line=new ArrayList<String>();
		    line.add(BaseClass.manageOrder.getAllLineItemNameWithoutCancelledstatus().get(j).getAttribute("textContent").trim());					
			line.add(BaseClass.manageOrder.getAllLineItemPriceWithoutCancelledstatus().get(j).getAttribute("textContent").trim());
			line.add(BaseClass.manageOrder.getAllLineItemQuantityWithoutCancelledstatus().get(j).getAttribute("textContent").trim());			
			lineItems.add(line);
			
		}
		Thread.sleep(2000);
		
		//click on next button to move on next page
		if(i!=next)
		{
		BaseClass.manageOrder.nextButton.click();
		Thread.sleep(2000);
		}
		}
		shipToadd =  manageOrder.shipToAddress.getAttribute("textContent");
		billToadd =manageOrder.billToAddress.getAttribute("textContent");

		BaseClass.wait.until(ExpectedConditions.elementToBeClickable(manageOrder.faxPreviewBtn));
		BaseClass.js.scrollTilElement(BaseClass.manageOrder.timer);
		Thread.sleep(5000);
		manageOrder.faxPreviewBtn.click();
		Thread.sleep(5000);
		Assert.assertEquals("Fax Details Final",BaseClass.getDriver().getTitle());

	}

@Then("User click on cancel Order button and select the cancellation category {string} reason {string} and enter comments {string}")
	public void user_click_cancel_order_and_select_cancellation_reason(String category,String reason,String comments) throws InterruptedException {		
		BaseClass.wait.until(ExpectedConditions.visibilityOf(manageOrder.cancelBtn));
		BaseClass.manageOrder.cancelBtn.click();
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
		BaseClass.select.selectByVisibleText(manageOrder.selectChangeCategory, category);
		BaseClass.select.selectByVisibleText(manageOrder.selectCancelReason, reason);
		BaseClass.manageOrder.cancellationComments.sendKeys(comments);	
		BaseClass.js.click(BaseClass.wait.until(ExpectedConditions.visibilityOf(manageOrder.submitBtn)));
		BaseClass.getDriver().switchTo().defaultContent();
		BaseClass.wait.until(ExpectedConditions.visibilityOf(manageOrder.cancelledSuccessMsg));
		Assert.assertTrue(BaseClass.manageOrder.orderHolds.getText().contains("CANCEL IN PROCESS"));
	}
	@Then("User validate details on fax preview and fax template{string}")
	public void user_validate_fax_template_and_fax_preview_details(String keycode) throws InterruptedException {
		// Fax template validations
		Assert.assertTrue("customer number did not match or missing",
				BaseClass.faxPre.customerNumber.getText().contains(custNo));
		Assert.assertTrue("Order number did not match or missing",
				BaseClass.faxPre.orderNumber.getText().contains(orderNo));

		// Fax Preview validations
		Assert.assertTrue("customer number did not match or missing",
				BaseClass.faxPre.orderDetails.get(1).getText().contains(custNo));
		Assert.assertTrue("Order number did not match or missing",
				BaseClass.faxPre.orderDetails.get(2).getText().contains(orderNo));
		Assert.assertTrue("Keycode did not match or missing",
				BaseClass.faxPre.orderDetails.get(3).getText().split("-")[0].contains(keycode));

		Assert.assertEquals("Email instruction did not match or missing",
				"Please click anywhere on the preview below to copy the information and open a new e-mail. Once the e-mail is opened, simply paste the information (Ctrl+V).",
				BaseClass.faxPre.emailMsg.getText().trim());

		// Validation of digits in date
		// between fax preview page (eg 2022年4月28日 )and manage order page(eg 28-APR-22)
		// by converting both to similar format (2022428)
		String faxPreviewDate = BaseClass.faxPre.orderDetails.get(3).getText().split("-")[1]
				.replaceAll("[^\\-\\d.]+|\\.(?!\\d)", "").trim();
		String formattedDate = "";
		try {
			SimpleDateFormat dt = new SimpleDateFormat("dd-MMM-yy");
			Date date =  dt.parse(dateCreated);
			SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-M-dd");
			formattedDate = dt1.format(date).replaceAll("-", "");
		} catch (Exception e) {
			System.out.println(e);
		}

		Assert.assertEquals("Date did not match or missing", formattedDate, faxPreviewDate);

	}
	@Then("User validate order line items for Fax Preview")
	public void user_validate_line_items_fax_preview() throws InterruptedException {

		String faxPreSubTotal = BaseClass.faxPre.subtotalprice.getText().replaceAll("[^\\-\\d.]+|\\.(?!\\d)", "");
		String faxPreTax = BaseClass.faxPre.tax.getText().replaceAll("[^\\-\\d.]+|\\.(?!\\d)", "");
		String faxPreTotal = BaseClass.faxPre.totalprice.getText().replaceAll("[^\\-\\d.]+|\\.(?!\\d)", "");

		// validation for Order subtotal, tax and Total between fax preview and manage order
		Assert.assertEquals("Order Total did not match or missing", total, faxPreTotal);

		Assert.assertEquals("Order SubTotal did not match or missing", Integer.parseInt(subTotal) +Integer.parseInt(charges),
				Integer.parseInt(faxPreSubTotal));

		Assert.assertEquals("Order Tax did not match or missing", tax, faxPreTax);

		// Get all line items
		ArrayList<ArrayList<String>> faxPreAllProducts = new ArrayList<ArrayList<String>>();
		int expectedSubtotal = 0;
		int sizeItemsFaxPrev = BaseClass.faxPre.productDescription.size();
		for (int j = 0; j < sizeItemsFaxPrev; j++) {
			String prodPrice = BaseClass.faxPre.productPrice.get(j).getText().replaceAll("[^\\-\\d.]+|\\.(?!\\d)", "")
					.trim();
			String prodQuant = BaseClass.faxPre.productQuantity.get(j).getText().trim();

			String actualLineTotal = BaseClass.faxPre.lineTotal.get(j).getText()
					.replaceAll("[^\\-\\d.]+|\\.(?!\\d)", "").trim();
			ArrayList<String> faxPreProduct = new ArrayList<String>();

			faxPreProduct.add(BaseClass.faxPre.productDescription.get(j).getText().trim());
			faxPreProduct.add(prodPrice);
			faxPreProduct.add(prodQuant);

			faxPreAllProducts.add(faxPreProduct);
			expectedSubtotal += Double.parseDouble(actualLineTotal);
		}

		Assert.assertEquals("Total number of line items not maching in fax Preview with Manage order",
				sizeItemsManageOrder, sizeItemsFaxPrev);
		
		// compare lineItems from Manage order and emailPreAllProducts
		for (int a = 0; a < sizeItemsManageOrder; a++) {
			Assert.assertTrue("Fax Preview line item was incorrect",
					lineItems.get(a).get(0).toLowerCase().contains(faxPreAllProducts.get(a).get(0).toLowerCase()));
			Assert.assertEquals("Line item Price was incorrect", lineItems.get(a).get(1),
					faxPreAllProducts.get(a).get(1));
			Assert.assertEquals("Line item Quantity was incorrect", lineItems.get(a).get(2),
					faxPreAllProducts.get(a).get(2));

		}

		// comparing expected subtotal(by multiplying and adding individual items
		// quantity and price) and actual visible Subtotal on email preview

		 Assert.assertEquals("SubTotal was missing or incorrect ", expectedSubtotal,
		 Integer.parseInt(faxPreSubTotal));

		// compare total by adding subtotal and tax
		 Assert.assertEquals("Total was missing or incorrect ",
		 Integer.parseInt(faxPreSubTotal)+Integer.parseInt(faxPreTax),
		 Integer.parseInt(faxPreTotal));

	}
	
	
	@Then("Validate PCT Number is displayed")
	public void validate_pct_number_is_displayed() {
		String custNum = BaseClass.wait.until(ExpectedConditions.visibilityOf(manageOrder.orderCustomerNumber)).getText();
		String pctNum = "";
		try {
			pctNum = manageOrder.pctNumber.getAttribute("data-clipboard-source");
		}
		catch (Exception e) {
		}
		Assert.assertTrue("PCT number not found in manage order page", pctNum.contains(custNum));
	}
	
	@Then("Read the number of line items, imprint and logo of parent order")
	public void read_the_number_of_line_items_imprint_and_logo_of_parent_order() throws InterruptedException {
		
		//Read line items count
		lineItemsCnt = String.valueOf(manageOrder.getTotalLine());
					
		BaseClass.wait.until(ExpectedConditions.visibilityOf(manageOrder.viewImpAndLogo)).click();
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(manageOrder.iframe));
		
		
		//Read logo is displayed or not
		try {
			logoFlag = manageOrder.logoImage.isDisplayed();			
		}
		catch (Exception e) {
		}
		
		//Read Imprint text is displayed or not
		List<WebElement> imprints  = manageOrder.imprintLines;
		ListIterator<WebElement> imprint = imprints.listIterator();
		if (!imprints.isEmpty()) {
			if (!imprint.next().getAttribute("value").isEmpty()) {
				imprintFlag = true;
			}			
		}
		
		BaseClass.getDriver().switchTo().defaultContent();
		BaseClass.js.click(manageOrder.close);
		Thread.sleep(2000);
		
	}
	@Then("Validate the number of line items, imprint and logo of parent order and copy")
	public void validate_the_number_of_line_items_imprint_and_logo_of_parent_order_and_copy() {		
		//Validate line items count of Parent & Child order are same
		String childOrderLinesCnt = BaseClass.wait.until(ExpectedConditions.visibilityOf(copyOrder.lineItemsCount)).getText().split("of")[1];
		Assert.assertTrue("mismatch found in parent order lines cnt : " + lineItemsCnt + " & child order lines cnt : " + childOrderLinesCnt ,  lineItemsCnt.equals(childOrderLinesCnt.trim()));
		
		//Validate parent & child order's logo
		boolean childLogoFlag = false;
		try {
			childLogoFlag = copyOrder.copyOrderImg.isDisplayed();
		}
		catch (Exception e) {
		}
		
		Assert.assertTrue("mismatch found in parent order logo flag : " + logoFlag + " & child logo flag : " + childLogoFlag, (logoFlag == childLogoFlag));
		//Validate parent & child order's imprint
		Assert.assertTrue("Imprint is missing", (imprintFlag == !copyOrder.copyImpPre.getText().isEmpty()));
		
	}
	
	
	@Then("User update Line Notes {string}")
	public void user_updates_line_notes(String newNote) throws InterruptedException {
		String mainWin = BaseClass.getDriver().getWindowHandle();
				manageOrder.viewLineNotes.click();
				Set<String> handles = BaseClass.getDriver().getWindowHandles();
				for (String handle : handles) {
					if (!handle.contains(mainWin)) {
						BaseClass.getDriver().switchTo().window(handle);
						break;
					}
				}
	    BaseClass.manageOrder.lineNotes.clear();
		BaseClass.manageOrder.lineNotes.sendKeys(newNote);
		BaseClass.manageOrder.saveLineNotesChanges.click();
		BaseClass.getDriver().switchTo().window(mainWin);
		BaseClass.wait.until(ExpectedConditions.visibilityOf(manageOrder.viewLineNotes));
		//Thread.sleep(2000);
		manageOrder.viewLineNotes.click();
		handles = BaseClass.getDriver().getWindowHandles();
		for (String handle : handles) {
			if (!handle.contains(mainWin)) {
				BaseClass.getDriver().switchTo().window(handle);
				break;
			}
		}
		BaseClass.wait.until(ExpectedConditions.visibilityOf(manageOrder.lineNotes));
		Assert.assertEquals("Line not was not updated", newNote, manageOrder.lineNotes.getAttribute("textContent"));
		
	}
	public void getLineItemDetails(List<WebElement> itemtype, String coloumnName) {
		StringBuilder orderList = new StringBuilder();
		int size = itemtype.size();
		int temp=1;
		for (int i = 0; i < size; i++)

		{

			String text = itemtype.get(i).getText();
			if (text.contains(manageOrder.shippingCharge.getAttribute("textContent"))) {
				text = "";
			}

			if (i == size - 1) {
				orderList.append(text);

			} 
			
			if(text.equals("1") && temp==1) {
				text="";
				temp+=1;
			}
			else {
				orderList.append(text + "|");
			}

		}

		String finalList = orderList.toString();
		BaseClass.excel.e.setCellData("ModifiedOrder", coloumnName, BaseClass.excel.sheet2row,finalList );

	}

}
