package stepDefinition;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;

import base.BaseClass;
import hooks.Hooks;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class PlaceOrderSteps {
	
	Map<String, String> map = new HashMap<String, String>();
	String cartTotal;
	String tax;
	public static String urlSerachPage=" "; 
	

	@Then("User click on Place an order")
	public void i_click_on_place_an_order() {
		BaseClass.homePage.placeOrder.click();
	}

	@When("User Enter customer number {string} and click on serach")
	public void i_enter_customer_number_and_click_on_serach(String customerNO) {
		BaseClass.placeOrder.customerNo.sendKeys(customerNO);
		BaseClass.placeOrder.searchBtn.click();
		map.put("CustomerNumber", customerNO);
	}

	@Then("Details should be display")
	public void details_should_be_display() {
		BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.placeOrder.customerNoLink));
		Assert.assertTrue("Customer not found or wrong Customer Number",
				BaseClass.placeOrder.customerNoLink.isDisplayed());
	}

	@When("User click on place order and select reason {string}")
	public void i_click_on_place_order_and_select_reason(String reason) {
		BaseClass.action.moveToElement(BaseClass.placeOrder.primeAddPlaceOrder);
		BaseClass.wait.until(ExpectedConditions.elementToBeClickable(BaseClass.placeOrder.primeAddPlaceOrder)).click();
		try {
			BaseClass.getDriver().switchTo().frame(0);
			Assert.assertTrue("Popup did not display", BaseClass.placeOrder.proceedWithOrderBtn.isDisplayed());
			i_click_on_proceed_with_the_order_and_select_reason(reason);
		} catch (Exception e) {
			e.getMessage();
		}
		

	}

	@Then("User click on Place Order Popup should open")
	public void i_click_on_place_order_popup_should_open() {
		BaseClass.js.scrollTilElement(BaseClass.placeOrder.primeAddPlaceOrder);
		BaseClass.placeOrder.primeAddPlaceOrder.click();
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
		Assert.assertTrue("Popup did not display", BaseClass.placeOrder.proceedWithOrderBtn.isDisplayed());
		
	}

	@When("User click on proceed with the order and select reason {string}")
	public void i_click_on_proceed_with_the_order_and_select_reason(String reason) {
		BaseClass.placeOrder.proceedWithOrderBtn.click();
		BaseClass.select.selectByVisibleText(BaseClass.placeOrder.selectReason, reason);
		BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.placeOrder.proceedBtn));
		BaseClass.placeOrder.proceedBtn.click();
		BaseClass.getDriver().switchTo().defaultContent();
		BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.productSearch.keycode));
		Assert.assertEquals("Products Search", BaseClass.getDriver().getTitle());

	}

	@Then("select reason to place order display and select reason")
	public void select_reason_to_place_order_display_and_select_reason() throws InterruptedException {
		BaseClass.placeOrder.proceedWithOrderBtn.click();
		List<WebElement> option = BaseClass.select.getOptions(BaseClass.placeOrder.selectReason);
		for (int i = 1; i < option.size(); i++) {
			Assert.assertTrue("Popup did not display", BaseClass.placeOrder.proceedWithOrderBtn.isDisplayed());
			option = BaseClass.select.getOptions(BaseClass.placeOrder.selectReason);
			BaseClass.select.selectByValue(BaseClass.placeOrder.selectReason, option.get(i).getAttribute("value"));
			System.out.println(option.get(i).getAttribute("value"));
			BaseClass.placeOrder.proceedBtn.click();
			BaseClass.getDriver().switchTo().defaultContent();
			BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.productSearch.keycode));
			Assert.assertEquals("Products Search", BaseClass.getDriver().getTitle());
			BaseClass.getDriver().navigate().back();
			BaseClass.getDriver().navigate().refresh();
			BaseClass.wait.until(ExpectedConditions.elementToBeClickable(BaseClass.placeOrder.primeAddPlaceOrder));
			BaseClass.js.scrollTilElement(BaseClass.placeOrder.primeAddPlaceOrder);
			Actions action = new Actions(BaseClass.getDriver());
			action.moveToElement(BaseClass.placeOrder.primeAddPlaceOrder).click().build().perform();
			BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
			BaseClass.placeOrder.proceedWithOrderBtn.click();

		}

	}

	@Then("User click on add accessories product")
	public void i_click_on_add_accessories_product() {
		 urlSerachPage = BaseClass.getDriver().getCurrentUrl();
		BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.productSearch.addAccessories));
		BaseClass.js.click(BaseClass.productSearch.addAccessories);
	}

	@Then("User search accessory item {string} and select item and enter quantity {string}")
	public void i_search_accessory_item_and_select_item_and_enter_quantity(String item, String qty)
			throws InterruptedException {
		BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.accOnly.serachAcc));
		BaseClass.accOnly.serachAcc.click();
		BaseClass.accOnly.accessoryName.sendKeys(item);
		BaseClass.accOnly.selectAccessory(item);
		Thread.sleep(5000);
		BaseClass.accOnly.productQty.click();
		BaseClass.accOnly.productQty.sendKeys(qty);
		if(!urlSerachPage.contains("US,") && !urlSerachPage.contains("CA,")) {
		if (BaseClass.accOnly.shipCharge.getAttribute("value").isEmpty()) {
			BaseClass.accOnly.shipCharge.sendKeys("10.99");
		}
		Thread.sleep(2000);
		BaseClass.accOnly.shipCharge.click();
		}
		BaseClass.CustProd.productTotal.click();
		BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.CustProd.refreshTotalBtn));
		Thread.sleep(2000);
		//Assert.assertEquals("ACCESSORY ONLY", BaseClass.CustProd.specialInst.getText());
//		tax = BaseClass.CustProd.tax.getText();
//		cartTotal = BaseClass.CustProd.totalWithTax.getText();
//		map.put("ProductCode", item);
//		map.put("Quantity", BaseClass.accOnly.productQty.getAttribute("value"));
//		map.put("Price", BaseClass.accOnly.productPrice.getAttribute("value"));
	}

	@Then("User enters the keycode {string} for the inhouse product")
	public void user_enters_the_keycode_for_the_inhouse_product(String keycode) {
		
		BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.productSearch.keycode));
		urlSerachPage = BaseClass.getDriver().getCurrentUrl();
		BaseClass.productSearch.keycode.sendKeys(keycode);
	}

	@Then("User click on Filter button")
	public void user_click_on_filter_button() {
		
		BaseClass.productSearch.filterBtn.click();
	}

	@Then("Validate if the product is correct {string}")
	public void validate_if_the_product_is_correct(String keycode) {
		BaseClass.exceptionHandle.visibilityOf(BaseClass.productSearch.searchProductName);
			Assert.assertEquals(keycode.substring(3).toUpperCase(), BaseClass.productSearch.searchProductFamily.getText());
		
	}

	@Then("Validate product is not displayed in search result")
	public void validate_if_the_product_is_not_displayed_in_search_result() {
		 BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.productSearch.errorMessage));
		 if(Hooks.sceanrioName.contains("Vendor")) {
	    	Assert.assertTrue(BaseClass.productSearch.errorMessage.getText().contains("Entered Search is not valid for Country"));

	}else {
		Assert.assertTrue(BaseClass.productSearch.errorMessage.getText().contains("Entered Keycode is not valid for Country"));
		
	}
		 }

	@Then("Validate the value of decoration in searched result")
	public void validate_the_value_of_decoration_in_searched_result() throws InterruptedException{		
		    BaseClass.productSearch.keycode.sendKeys(Keys.ENTER);
		    Thread.sleep(5000);
			Assert.assertEquals("Decoration is not matched or missing",BaseClass.productSearch.searchProductDecoration.getText(), BaseClass.select.getOptions(BaseClass.productSearch.selectDecoration).get(1).getText());
	}

	@Then("User enters the vendor item keycode {string}")
	public void user_enters_the_vendor_item_keycode(String keycode) {
		BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.productSearch.productCode));
		BaseClass.productSearch.productCode.sendKeys(keycode);

	}

	@Then("Validate vendor product is correct {string}")
	public void validate_if_the_product_is_correct_for_vendor_product(String keycode) {
		BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.productSearch.searchProductNameVendor));			
		Assert.assertEquals(keycode,BaseClass.productSearch.searchProductFamilyVendor.getText());

	}

	@Then("User enters the vendor item name {string}")
	public void user_enters_the_vendor_item_name(String name) {
		BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.productSearch.nameDescription));
		BaseClass.productSearch.nameDescription.sendKeys(name);
	}

	@Then("User select the product code if the vendor item name is correct from description")
	public void user_select_the_product_code_if_the_vendor_item_name_is_correct_from_description() {
		BaseClass.wait
		.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(BaseClass.productSearch.frameDescriptionSearch));
		BaseClass.wait
				.until(ExpectedConditions.visibilityOf(BaseClass.productSearch.productCodeDescriptionSearch));
		BaseClass.productSearch.productCodeDescriptionSearch.click();
		BaseClass.getDriver().switchTo().defaultContent();

	}

	@Then("Validate the product code is not displayed for incorrect vendor item name")
	public void validate_the_product_code_is_not_displayed_for_incorrect_vendor_item_name() {
		BaseClass.wait.until(
				ExpectedConditions.frameToBeAvailableAndSwitchToIt(BaseClass.productSearch.frameDescriptionSearch));	
		      Assert.assertTrue(BaseClass.productSearch.searchIcon.isDisplayed());
	}

	@Then("User clicks on the name of the displayed search result")
	public void user_clicks_on_the_name_of_the_displayed_search_result() {
		BaseClass.productSearch.searchProductName.click();
	}

	@Then("User clicks on the name of the displayed search result for Vendor Item")
	public void user_clicks_on_the_name_of_the_displayed_search_result_for_vendor_item() {
	
		BaseClass.wait.until(ExpectedConditions.elementToBeClickable(BaseClass.productSearch.searchProductNameVendor))
				.click();
	}

	@When("User navigates to product search Page by entering customer {string} and reason {string} and keycode {string}")
	public void user_navigates_to_product_search_page_by_entering_customer_and_reason_and_keycode(String customernumber,
			String reason, String keycode) {
		i_enter_customer_number_and_click_on_serach(customernumber);
		details_should_be_display();
		i_click_on_place_order_and_select_reason(reason);
		user_enters_the_keycode_for_the_inhouse_product(keycode);
		user_click_on_filter_button();
		  if(!Hooks.sceanrioName.toLowerCase().contains("mail campaign") && !Hooks.sceanrioName.contains("Premium Product")) {
	            validate_if_the_product_is_correct(keycode);
	             }
		  }

	@When("User navigates to product search Page by entering customer {string} and reason {string} and keycode {string} for vendor product")
	public void user_navigates_to_product_search_page_by_entering_customer_and_reason_and_keycode_for_vendor_product(
			String customernumber, String reason, String keycode) {
		i_enter_customer_number_and_click_on_serach(customernumber);
		details_should_be_display();
		i_click_on_place_order_and_select_reason(reason);
		user_enters_the_vendor_item_keycode(keycode);
		urlSerachPage = BaseClass.getDriver().getCurrentUrl();
		user_click_on_filter_button();
		validate_if_the_product_is_correct_for_vendor_product(keycode);	
	}

	
	@When("User navigates to product search Page by PCT number {string} and reason {string}")
	public void user_navigates_to_product_search_page_by_PCT_number_and_reason(String PCTnumber, String reason) throws InterruptedException {
		BaseClass.placeOrder.pctNo.sendKeys(PCTnumber);
		BaseClass.placeOrder.searchBtn.click();
		details_should_be_display();
		BaseClass.action.moveToElement(BaseClass.placeOrder.primeAddPlaceOrder);
		BaseClass.wait.until(ExpectedConditions.elementToBeClickable(BaseClass.placeOrder.primeAddPlaceOrder)).click();
		try {
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
		BaseClass.placeOrder.contiNoEmail.click();
		
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
		try {
        Thread.sleep(5000);
			BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.placeOrder.proceedWithOrderBtn)).click();;
			BaseClass.placeOrder.proceedWithOrderBtn.click();
			BaseClass.select.selectByVisibleText(BaseClass.placeOrder.selectReason, reason);
			BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.placeOrder.proceedBtn)).click();
			BaseClass.getDriver().switchTo().defaultContent();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} 

	}

	@When("User navigates to product search Page by entering customer {string} and reason {string}")
	public void user_navigates_to_product_search_page_by_entering_customer_and_reason(String customernumber,
			String reason) {
		i_enter_customer_number_and_click_on_serach(customernumber);
		details_should_be_display();
		i_click_on_place_order_and_select_reason(reason);
	}
	@Then("User clicks on the place sample order")
	public void user_clicks_on_the_place_sample_order() {
		BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.productSearch.sampleOrder)).click();
	}
	
	
}
