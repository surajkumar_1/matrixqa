package stepDefinition;

import static base.BaseClass.copyOrder;
import static base.BaseClass.manageOrder;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import base.BaseClass;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class RedoOrderSteps {
	String mainPrice, mainQty, logoSrc, shipCharge, accPrice, accQty, UpsellPrice, UpsellQty, FreeQty, orderNo, Discount, LogoCharge;
	int implines;
	List<String> attributes;

	@Then("Update main price {string} and quantity {string}")
	public void update_main_price_and_quantity(String price, String qty) {
		BaseClass.redoOrder.updateQtyPriceBtn.click();
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
		BaseClass.redoOrder.updatePrice.clear();
		BaseClass.redoOrder.updatePrice.sendKeys(price);
		BaseClass.redoOrder.updateQty.clear();
		BaseClass.redoOrder.updateQty.sendKeys(qty);
		BaseClass.redoOrder.applyBtn.click();
		BaseClass.getDriver().switchTo().defaultContent();
		BaseClass.exceptionHandle.attributeToBe(BaseClass.redoOrder.mainPrice, "textContent", price);
		Assert.assertEquals(price, BaseClass.redoOrder.mainPrice.getAttribute("textContent"));
		Assert.assertEquals(qty, BaseClass.redoOrder.mainQty.getAttribute("textContent"));
		mainPrice =price;
		mainQty=qty;
	}

	@Then("Update logo of Redo order")
	public void update_logo() throws InterruptedException {
		BaseClass.redoOrder.uploadLogoYes.click();
		BaseClass.redoOrder.uploadNewlogo.click();
		String logoBefore = BaseClass.copyOrder.copyOrderImg.getAttribute("src");
		BaseClass.redoOrder.inputlogo
				.sendKeys(System.getProperty("user.dir")+"\\src\\main\\resources\\TestData\\Example.jpg");
		Thread.sleep(2000);
		BaseClass.redoOrder.uploadlogo.click();
		BaseClass.wait.until(ExpectedConditions.attributeToBeNotEmpty(BaseClass.redoOrder.uploadedLogo, "src"));
		BaseClass.redoOrder.updatelogo.click();
		Assert.assertNotNull(BaseClass.copyOrder.copyOrderImg.getAttribute("src"));
		Assert.assertNotEquals(logoBefore, BaseClass.copyOrder.copyOrderImg.getAttribute("src"));
		logoSrc = copyOrder.copyOrderImg.getAttribute("src");
	}

	@Then("Update attributes of Redo order")
	public void update_attributes() throws InterruptedException {
		attributes = new ArrayList<String>();
		Thread.sleep(5000);
		BaseClass.redoOrder.updateAttribute.click();
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
		BaseClass.redoOrder.showAttribute.click();
		BaseClass.exceptionHandle.visibilityOf(BaseClass.redoOrder.saveAndConti);
		List<WebElement> options = BaseClass.redoOrder.selectAttribute;
		if (!options.isEmpty()) {
			ListIterator<WebElement> option = options.listIterator();
			while (option.hasNext()) {
				Thread.sleep(3000);
				WebElement opt = option.next();
				int index = BaseClass.select.getOptions(opt).size() - 2;
				BaseClass.select.selectByIndex(opt, index);
				attributes.add(BaseClass.select.getFirstSelectedOption(opt).getText());
			}
		}
		BaseClass.redoOrder.saveAndConti.click();
		BaseClass.getDriver().switchTo().defaultContent();
		for (String option : attributes) {
			BaseClass.wait
					.until(ExpectedConditions.textToBePresentInElement(BaseClass.redoOrder.mainDescription, option));
			Assert.assertTrue("selected Attribute not found in Main Line Decsription ",
					BaseClass.redoOrder.mainDescription.getAttribute("textContent").contains(option));
		}
	}

	@Then("Update ship charge {string}")
	public void update_ship_charge(String shipCharge) {
		BaseClass.redoOrder.updateShipBtn.click();
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
		BaseClass.redoOrder.updatePrice.clear();
		BaseClass.redoOrder.updatePrice.sendKeys(shipCharge);
		BaseClass.redoOrder.applyBtn.click();
		BaseClass.getDriver().switchTo().defaultContent();
		Assert.assertTrue("Shipping Price Not updated ", BaseClass.wait
				.until(ExpectedConditions.textToBePresentInElement(BaseClass.redoOrder.updateShipCharge, shipCharge)));
		this.shipCharge = shipCharge;
	}

	@Then("Update the imprint of Redo order")
	public void user_update_the_imprint_of_redo_order() throws InterruptedException {
		implines = manageOrder.imprintLines.size();
		Thread.sleep(5000);
		Iterator<WebElement> imp = manageOrder.imprintLines.iterator();
		while (imp.hasNext()) {
			WebElement line = imp.next();
			line.clear();
			line.sendKeys("Redo Order update");
		}
	}
	
	
	@When("User click on Place redo order")
	public void user_click_on_Place_redo_order() throws InterruptedException {
		BaseClass.wait.until(ExpectedConditions.attributeToBeNotEmpty(copyOrder.copySalesRepEmail, "value"));
		Thread.sleep(3000);
		BaseClass.action.clickOn(manageOrder.placeredoOrder);
		copyOrder.okBtn.click();
		BaseClass.wait.until(ExpectedConditions.visibilityOf(copyOrder.copySuccessMsg));
		//Assert.assertEquals("Total price not matching or missing", total, manageOrder.total.getText());
//		manageOrder.viewImpAndLogo.click();
//		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));

	}
	@Then("Redo order should place successfully with imprint and same logo")
	public void redo_order_should_place_successfully_with_imprint_and_same_logo() throws InterruptedException {
		System.out.println(BaseClass.manageOrder.orderNumberField.getAttribute("value"));
		Thread.sleep(10000);
		BaseClass.js.click(BaseClass.manageOrder.searchBtn);
        Assert.assertEquals("Main Price Not updated in Manage Order", mainPrice, BaseClass.redoOrder.mangeOrderMainPrice.getAttribute("textContent"));
        Assert.assertEquals("Main Qty Not updated in Manage Order",mainQty, BaseClass.redoOrder.mangeOrderMainQty.getAttribute("textContent"));
        Assert.assertEquals("Shipping Price Not updated in Manage Order", shipCharge, BaseClass.redoOrder.mangeOrderShipPrice.getAttribute("textContent"));
        for (String option : attributes) {
			Assert.assertTrue("selected Attribute not found in Main Line Decsription ",
					BaseClass.redoOrder.mangeOrderMainDesc.getAttribute("textContent").contains(option.toUpperCase()));
		}
        BaseClass.redoOrder.viewImprintAndLogoBtn.click();
        BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
		for (int i = 0; i < implines; i++) {
			Assert.assertFalse("Redo order imprint missing",
					manageOrder.imprintLines.get(i).getAttribute("value").isEmpty());
		}
		Assert.assertEquals("Logo is not matching or missing", logoSrc, copyOrder.copyOrderImg.getAttribute("src"));
	}
	
	
	@Then("Redo order should place successfully with same logo")
	public void redo_order_should_place_successfully_with_same_logo() throws InterruptedException {
		System.out.println(BaseClass.manageOrder.orderNumberField.getAttribute("value"));
		Thread.sleep(15000);
		BaseClass.js.click(BaseClass.manageOrder.searchBtn);
		Assert.assertEquals("Main Price Not updated in Manage Order", mainPrice,
				BaseClass.redoOrder.mangeOrderMainPrice.getAttribute("textContent"));
		Assert.assertEquals("Main Qty Not updated in Manage Order", mainQty,
				BaseClass.redoOrder.mangeOrderMainQty.getAttribute("textContent"));
		Assert.assertEquals("Shipping Price Not updated in Manage Order", shipCharge,
				BaseClass.redoOrder.mangeOrderShipPrice.getAttribute("textContent"));
		for (String option : attributes) {
			Assert.assertTrue("selected Attribute not found in Main Line Decsription ",
					BaseClass.redoOrder.mangeOrderMainDesc.getAttribute("textContent").contains(option.toUpperCase()));
		}
		Thread.sleep(10000);
		BaseClass.redoOrder.viewImprintAndLogoBtn.click();
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
		Assert.assertEquals("Logo is not matching or missing", logoSrc, copyOrder.copyOrderImg.getAttribute("src"));
	}

	@Then("Redo order should place successfully with updated imprint")
	public void redo_order_should_place_successfully_with_updated_imprint() throws InterruptedException {	
		System.out.println(BaseClass.manageOrder.orderNumberField.getAttribute("value"));
		Thread.sleep(15000);
		BaseClass.js.click(BaseClass.manageOrder.searchBtn);
        Assert.assertEquals("Main Price Not updated in Manage Order", mainPrice, BaseClass.redoOrder.mangeOrderMainPrice.getAttribute("textContent"));
        Assert.assertEquals("Main Qty Not updated in Manage Order",mainQty, BaseClass.redoOrder.mangeOrderMainQty.getAttribute("textContent"));
        Assert.assertEquals("Shipping Price Not updated in Manage Order", shipCharge, BaseClass.redoOrder.mangeOrderShipPrice.getAttribute("textContent"));
        for (String option : attributes) {
			Assert.assertTrue("selected Attribute not found in Main Line Decsription ",
					BaseClass.redoOrder.mangeOrderMainDesc.getAttribute("textContent").contains(option.toUpperCase()));
		}
        BaseClass.redoOrder.viewImprintAndLogoBtn.click();
        BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
		for (int i = 0; i < implines; i++) {
			Assert.assertEquals("Redo order imprint missing or not matching", "Redo Order update",
					manageOrder.imprintLines.get(i).getAttribute("value"));

		}
	}

	@Then("Redo order should place successfully with imprint")
	public void redo_order_should_place_successfully_with_imprint() throws InterruptedException {
		System.out.println(BaseClass.manageOrder.orderNumberField.getAttribute("value"));
		Thread.sleep(15000);
		BaseClass.js.click(BaseClass.manageOrder.searchBtn);
		Thread.sleep(10000);
        Assert.assertEquals("Main Price Not updated in Manage Order", mainPrice, BaseClass.redoOrder.mangeOrderMainPrice.getAttribute("textContent"));
        Assert.assertEquals("Main Qty Not updated in Manage Order",mainQty, BaseClass.redoOrder.mangeOrderMainQty.getAttribute("textContent"));
        Assert.assertEquals("Shipping Price Not updated in Manage Order", shipCharge, BaseClass.redoOrder.mangeOrderShipPrice.getAttribute("textContent"));
        for (String option : attributes) {
			Assert.assertTrue("selected Attribute not found in Main Line Decsription ",
					BaseClass.redoOrder.mangeOrderMainDesc.getAttribute("textContent").contains(option.toUpperCase()));
		}
        BaseClass.redoOrder.viewImprintAndLogoBtn.click();
        BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
		for (int i = 0; i <= implines; i++) {
			Assert.assertFalse("Redo order imprint missing",
					manageOrder.imprintLines.get(i).getAttribute("value").isEmpty());

		}
	}

	@Then("Redo order should place successfully")
	public void redo_order_should_place_successfully() throws InterruptedException {
		System.out.println(BaseClass.manageOrder.orderNumberField.getAttribute("value"));
		Thread.sleep(15000);
		BaseClass.js.click(BaseClass.manageOrder.searchBtn);
		Thread.sleep(10000);
        Assert.assertEquals("Main Price Not updated in Manage Order", mainPrice, BaseClass.redoOrder.mangeOrderMainPrice.getAttribute("textContent"));
        Assert.assertEquals("Main Qty Not updated in Manage Order",mainQty, BaseClass.redoOrder.mangeOrderMainQty.getAttribute("textContent"));
        Assert.assertEquals("Shipping Price Not updated in Manage Order", shipCharge, BaseClass.redoOrder.mangeOrderShipPrice.getAttribute("textContent"));
        BaseClass.redoOrder.viewImprintAndLogoBtn.click();
        BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
		Assert.assertTrue("Redo order imprint is present for non-customized",
					manageOrder.imprintLines.isEmpty());
		boolean logo = true;
		try {
			logo = copyOrder.copyOrderImg.getAttribute("src").isEmpty();
		} catch (Exception e) {
			e.printStackTrace();
		}
		Assert.assertTrue("Redo order Logo is present for non-customized", logo);
	}
	
	@Then("Update accessory price {string} and quantity {string}")
	public void update_acc_price_and_quantity(String price, String qty) {
		BaseClass.redoOrder.updateQtyPriceBtnAcc.click();
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
		BaseClass.redoOrder.updatePrice.clear();
		BaseClass.redoOrder.updatePrice.sendKeys(price);
		BaseClass.redoOrder.updateQty.clear();
		BaseClass.redoOrder.updateQty.sendKeys(qty);
		BaseClass.redoOrder.applyBtn.click();
		BaseClass.getDriver().switchTo().defaultContent();
		BaseClass.wait.until(ExpectedConditions.attributeToBe(BaseClass.redoOrder.accPrice, "textContent", price));
		Assert.assertEquals(price, BaseClass.redoOrder.accPrice.getAttribute("textContent"));
		Assert.assertEquals(qty, BaseClass.redoOrder.accQty.getAttribute("textContent"));
		accPrice =price;
		accQty=qty;
	}
	
	@Then("Redo order should place successfully for accessory")
	public void redo_order_should_place_successfully_for_accessory() throws InterruptedException {
		Thread.sleep(10000);
		BaseClass.js.click(BaseClass.manageOrder.searchBtn);
        Assert.assertEquals("Accessory Price Not updated in Manage Order", accPrice, BaseClass.redoOrder.mangeOrderAccPrice.getAttribute("textContent"));
        Assert.assertEquals("Accessory Qty Not updated in Manage Order",accQty, BaseClass.redoOrder.mangeOrderAccQty.getAttribute("textContent"));        
	}
	
	@Then("Update upsell price {string} and quantity {string}")
	public void update_upsell_price_and_quantity(String price, String qty) {
		BaseClass.redoOrder.updateQtyPriceBtnUpsell.click();
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
		BaseClass.redoOrder.updatePrice.clear();
		BaseClass.redoOrder.updatePrice.sendKeys(price);
		BaseClass.redoOrder.updateQty.clear();
		BaseClass.redoOrder.updateQty.sendKeys(qty);
		BaseClass.redoOrder.applyBtn.click();
		BaseClass.getDriver().switchTo().defaultContent();
		BaseClass.wait.until(ExpectedConditions.attributeToBe(BaseClass.redoOrder.upsellPrice, "textContent", price));
		Assert.assertEquals(price, BaseClass.redoOrder.upsellPrice.getAttribute("textContent"));
		Assert.assertEquals(qty, BaseClass.redoOrder.UpsellQty.getAttribute("textContent"));
		UpsellPrice =price;
		UpsellQty=qty;
	}
	@Then("Redo order should place successfully for upsell")
	public void redo_order_should_place_successfully_for_upsell() throws InterruptedException {
		Thread.sleep(10000);
		BaseClass.js.click(BaseClass.manageOrder.searchBtn);
        Assert.assertEquals("Upsell Price Not updated in Manage Order", UpsellPrice, BaseClass.redoOrder.mangeOrderUpsellPrice.getAttribute("textContent"));
        Assert.assertEquals("Upsell Qty Not updated in Manage Order", UpsellQty, BaseClass.redoOrder.mangeOrderUpsellQty.getAttribute("textContent")); 
     
	}
	@Then("Update free quantity {string}")
	public void update_free_quantity(String qty) {
		BaseClass.redoOrder.updateQtyBtnFree.click();
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
		BaseClass.redoOrder.updateQty.clear();
		BaseClass.redoOrder.updateQty.sendKeys(qty);
		BaseClass.redoOrder.applyBtn.click();
		BaseClass.getDriver().switchTo().defaultContent();
		BaseClass.wait.until(ExpectedConditions.attributeToBe( BaseClass.redoOrder.freeQty, "textContent", qty));
		Assert.assertEquals(qty, BaseClass.redoOrder.freeQty.getAttribute("textContent"));
		FreeQty =qty;
	}
	@Then("Redo order should place successfully for freeqty")
	public void redo_order_should_place_successfully_for_freeqty() throws InterruptedException {
		System.out.println(BaseClass.manageOrder.orderNumberField.getAttribute("value"));
		Thread.sleep(10000);
		BaseClass.js.click(BaseClass.manageOrder.searchBtn);
		Thread.sleep(10000);
        Assert.assertEquals("Free Qty Not updated in Manage Order", FreeQty, BaseClass.redoOrder.mangeOrderFreeQty.getAttribute("textContent")); 
     
	}
	@Then("Verify logo and imprint")
	public void user_verify_logo_and_imprint() throws InterruptedException {
		manageOrder.viewImpAndLogo.click();
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
		Assert.assertTrue("Imprint Line did not match or missing", 
				BaseClass.manageOrder.imp.getText().contains("No Imprint Option is selected at the time of Order Creation Process"));

		Assert.assertTrue("No logo and Imprint instruction did not match or missing",
				BaseClass.manageOrder.logo.isSelected());
		manageOrder.cancel.click();
		BaseClass.getDriver().switchTo().defaultContent();
		
		}
	@Then("Redo order should place successfully for non-customized order")
	public void redo_order_should_place_successfully_for_noncustomized_order() throws InterruptedException {
		System.out.println(BaseClass.manageOrder.orderNumberField.getAttribute("value"));
		Thread.sleep(10000);
		BaseClass.js.click(BaseClass.manageOrder.searchBtn);
		Thread.sleep(10000);
		BaseClass.redoOrder.viewImprintAndLogoBtn.click();
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
		Assert.assertTrue("Imprint Line did not match or missing", 
				BaseClass.manageOrder.imp.getText().contains("No Imprint Option is selected at the time of Order Creation Process"));

		Assert.assertTrue("No logo and Imprint instruction did not match or missing",
				BaseClass.manageOrder.logo.isSelected());
		manageOrder.cancel.click();
		BaseClass.getDriver().switchTo().defaultContent();
	
	}
	@Then("Update discount {string}")
	public void update_discount_price(String price) {
		BaseClass.redoOrder.updateBtnDisc.click();
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
		BaseClass.redoOrder.updatePrice.clear();
		BaseClass.redoOrder.updatePrice.sendKeys(price);
		BaseClass.redoOrder.applyBtn.click();
		BaseClass.getDriver().switchTo().defaultContent();
		BaseClass.wait.until(ExpectedConditions.attributeToBe( BaseClass.redoOrder.discount, "textContent", price));
		Assert.assertEquals(price, BaseClass.redoOrder.discount.getAttribute("textContent"));
		Discount =price;
	}
	@Then("Redo order should place successfully for update discount")
	public void redo_order_should_place_successfully_for_discount() throws InterruptedException {
		System.out.println(BaseClass.manageOrder.orderNumberField.getAttribute("value"));
		Thread.sleep(10000);
		BaseClass.js.click(BaseClass.manageOrder.searchBtn);
		Thread.sleep(10000);
		BaseClass.js.click(BaseClass.manageOrder.nextButton);
        Assert.assertEquals("Discount Not updated in Manage Order", Discount, BaseClass.redoOrder.mangeOrderDiscount.getAttribute("textContent")); 
     
	}
	@Then("Update logocharge {string}")
	public void update_logocharge(String price) {
		BaseClass.redoOrder.updateBtnLogo.click();
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
		BaseClass.redoOrder.updatePrice.clear();
		BaseClass.redoOrder.updatePrice.sendKeys(price);
		BaseClass.redoOrder.applyBtn.click();
		BaseClass.getDriver().switchTo().defaultContent();
		BaseClass.wait.until(ExpectedConditions.attributeToBe( BaseClass.redoOrder.logoCharge, "textContent", price));
		Assert.assertEquals(price, BaseClass.redoOrder.logoCharge.getAttribute("textContent"));
		LogoCharge =price;
	}
	@Then("Redo order should place successfully for update logocharge")
	public void redo_order_should_place_successfully_for_logocharge() throws InterruptedException {
		Thread.sleep(10000);
		BaseClass.js.click(BaseClass.manageOrder.searchBtn);
        Assert.assertEquals("Logocharge Not updated in Manage Order", LogoCharge, BaseClass.redoOrder.mangeOrderLogocharge.getAttribute("textContent")); 
     
	}
}
