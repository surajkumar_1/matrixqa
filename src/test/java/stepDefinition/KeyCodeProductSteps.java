package stepDefinition;

import static base.BaseClass.CustProd;
import static base.BaseClass.orderconf;
import static base.BaseClass.productSearch;


import java.awt.AWTException;
import java.util.List;
import java.util.Map;


import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import base.BaseClass;
import hooks.Hooks;
import io.cucumber.java.en.Then;
import pages.KeyCodeProduct;


public class KeyCodeProductSteps {

	static double tax;
	static double total;
    static String shipMethodNA,setupCharge,logoCharge, template_Url = "";;
    static String shipCharge;
    public static boolean newLogo;
    public static boolean logoAsBefore;
    public static boolean setup;
    static String accProductCode,accPrice,accQty;
	
	@Then("User select product options")
	public void user_select_product_options() throws InterruptedException {
		BaseClass.wait.until(ExpectedConditions.visibilityOf(CustProd.returnToProductSearch));
		CustProd.productOptions();
		
	}
	@Then ("Click on Product and printing modification")
	public void click_on_product_and_printing_modification() {
		long pastholds =  CustProd.holds.stream().filter(ele -> ele.isDisplayed()).count();
		BaseClass.js.click(CustProd.productAndPrintLegacy);
		Assert.assertTrue("Select logo visible", BaseClass.wait.until(ExpectedConditions.invisibilityOf(CustProd.uploadLogoYes)));
		Assert.assertTrue("Production Line Notes not visible",CustProd.productionNotes.isDisplayed());
		Assert.assertTrue("New Holds not visible", CustProd.holds.stream().filter(ele -> ele.isDisplayed()).count()>pastholds);
		
	}
	@Then("Enter Imprint Lines {string}")
	public void enter_imprint_lines(String addImprint) throws AWTException, InterruptedException {
		if (addImprint.equalsIgnoreCase("NO")) {
			BaseClass.js.click(CustProd.clearImprint);
		} else if (addImprint.equalsIgnoreCase("YES")) {
			BaseClass.CustProd.imprintLines();
			try {
				if(BaseClass.CustProd.colorPick.isDisplayed()) {
					BaseClass.CustProd.selectFullColor();}
				else BaseClass.CustProd.imprintColor.click();
			} catch (Exception e) {
				e.getMessage();
			}
		}

	}
	@Then("Select logo option {string}")
	public void Select_logo_option(String LogoOption){
		if(LogoOption.contains("Save Logo")) {
			BaseClass.CustProd.saveLogobtn.click();
			
			Assert.assertTrue("Validate the popup content of Save logo option", BaseClass.CustProd.scrapLogoPopup.getText().contains("Do you confirm the order as a logo order, it will get bypassed by the art department?"));
		}else {
			BaseClass.CustProd.sendToArtDeptBtn.click();
			
			Assert.assertTrue("Validate the popup content of sent to art dept option", BaseClass.CustProd.scrapLogoPopup.getText().contains("Do you confirm the order as a logo order, it will get sent to the art department?"));
		}
		
		BaseClass.manageCustomer.gdprOKAddNewContact.click();
	}
	@Then("User Upload new logo {string}")
	public void user_upload_logo_(String logo) {		
		if(logo.equalsIgnoreCase("YES")) {
		BaseClass.js.click(CustProd.uploadLogoYes);	
		BaseClass.js.click(BaseClass.wait.until(ExpectedConditions.visibilityOf(CustProd.newLogo)));
		CustProd.inputLogo
				.sendKeys(System.getProperty("user.dir") + "/src/main/resources/TestData/NPLogoTesting.PNG");
		CustProd.uploadLogo.click();
		newLogo = BaseClass.wait.until(ExpectedConditions.elementToBeSelected(CustProd.logoCharge));
		Assert.assertTrue("Logo charge is not selected",newLogo);
		Assert.assertTrue("Proof Required is not selected",BaseClass.wait.until(ExpectedConditions.elementToBeSelected(CustProd.proofReqiuredYes)));
		}	
		if(logo.equalsIgnoreCase("NO")) {
			BaseClass.js.click(CustProd.uploadLogoYes);	
			BaseClass.js.click(CustProd.logoAsBefore);
			logoAsBefore = BaseClass.wait.until(ExpectedConditions.elementToBeSelected(CustProd.logoAsBeforeCharge));
			Assert.assertTrue("logoAsBefore charge is not selected",logoAsBefore);
			BaseClass.CustProd.specialInst.sendKeys("Order Reference Test");
			Assert.assertTrue("Proof Required is not selected",BaseClass.wait.until(ExpectedConditions.elementToBeSelected(CustProd.proofReqiuredYes)));
		}
		if(Hooks.sceanrioName.contains("Vendor item")) {
			setup = BaseClass.wait.until(ExpectedConditions.elementToBeSelected(CustProd.setupCharge));
			Assert.assertTrue("Setup Charge is not selected", setup);
		}
	}

	@Then("User select template {string}")
    public void then_user_select_template(String template) throws InterruptedException {
	BaseClass.CustProd.chooseTemplate.click();
	BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
	BaseClass.CustProd.templateName.sendKeys(template);
	BaseClass.CustProd.searchTemplate.click();
	Thread.sleep(5000);
	//BaseClass.wait.until(ExpectedConditions.numberOfElementsToBeLessThan("", 2));
	BaseClass.CustProd.selectTemplate.click();
	BaseClass.getDriver().switchTo().defaultContent();
	CustProd.okBtn.click();
	template_Url = CustProd.url_template.getAttribute("href");
}


	@Then("User enters anniversary {string}")
	public void user_enters_anniversary(String anniversary) throws InterruptedException {
		BaseClass.CustProd.anniversary.sendKeys("Anni");		
		BaseClass.CustProd.clearImprint.click();
		CustProd.refreshTotalBtn.click();
		CustProd.checkOutBtn.click();
		CustProd.okBtn.click();
		Assert.assertEquals("Error did not display or wrong message", "Anniversary must be a numeric number between 0 and 999.",
				BaseClass.CustProd.errorList.get(0).getText());
		BaseClass.CustProd.closeError.click();
		BaseClass.CustProd.anniversary.clear();
		BaseClass.CustProd.anniversary.sendKeys(anniversary);
		
	}


	@Then("User click on return to product search button")
	public void user_click_on_return_to_product_search_button() {

		CustProd.returnToProductSearch.click();
	}

	@Then("Validate if the user is navigated to product search page")
	public void validate_if_the_user_is_navigated_to_product_search_page() {
		BaseClass.wait.until(ExpectedConditions.visibilityOf(productSearch.filterBtn));
		if (productSearch.filterBtn.isDisplayed()) {
			System.out.println("Return to product seacrh is working fine");
		}
	}

	@Then("User click on Cancel order button")
	public void user_click_on_cancel_order_button() {

		CustProd.cancelOrderBtn.click();

	}

	@Then("Validate if the Cancel button is working as expected")
	public void validate_if_the_cancel_button_is_working_as_expected() {

	}

	@Then("User click on CheckOut button")
	public void user_click_on_check_out_button() throws InterruptedException {
		Thread.sleep(3000);
		if(BaseClass.CustProd.refreshTotalBtn.isDisplayed())
		BaseClass.js.click(BaseClass.wait.until(ExpectedConditions.elementToBeClickable(CustProd.refreshTotalBtn)));
		Thread.sleep(3000);
		BaseClass.wait.until(ExpectedConditions.attributeToBeNotEmpty(CustProd.totalWithTax,"textContent"));
		if(Hooks.sceanrioName.toLowerCase().contains("mail campaign")) {
            BaseClass.wait.until(ExpectedConditions.attributeToBeNotEmpty(CustProd.discountedSetupCharge, "value"));
            BaseClass.wait.until(ExpectedConditions.attributeToBeNotEmpty(CustProd.discountedLogoCharge, "value"));
            Assert.assertTrue("Discounted setup Charges is not selected",CustProd.discountedSetupCharge.isSelected());        
            Assert.assertFalse("Discounted setup Price is 0",CustProd.discountedSetupPrice.getAttribute("value").trim().equalsIgnoreCase("0"));        
            Assert.assertTrue("Discounted logo Charges is not selected",CustProd.discountedLogoCharge.isSelected());        
            Assert.assertFalse("Discounted logo Price is 0",CustProd.discountedLogoPrice.getAttribute("value").trim().equalsIgnoreCase("0"));        
            setupCharge=CustProd.discountedSetupPrice.getAttribute("value").trim();
            logoCharge=CustProd.discountedLogoPrice.getAttribute("value").trim();
        }
//		try {
//			while(CustProd.totalAccessorieCharges.getText().matches("[1-9]")) {
//				CustProd.refreshTotalBtn.click();
//				if(CustProd.accessoriesCharges.getText().matches("[1-9]")) {
//					break;
//				}
//			}
//		} catch (Exception e2) {
//			
//		}
//		BaseClass.js.click(CustProd.refreshTotalBtn);
//		Thread.sleep(5000);
		double actualaccessoriesCharges = 0;
		double discountCharges = 0;
		double extraCharges = 0;
		double accessoriesCharges = 0;
		double shippingCharges = 0;
		double perUnitExtraCharges = 0;
		try {
			discountCharges = getPrice(CustProd.discountCharges);

		} catch (Exception e1) {

			System.out.println(e1.getMessage());
		}
		
		try {
			extraCharges = getPrice(CustProd.extraCharges);
		} catch (Exception e1) {

			System.out.println(e1.getMessage());
		}
		try {
			accessoriesCharges = getPrice(CustProd.accessoriesCharges);
		} catch (Exception e1) {

			System.out.println(e1.getMessage());
		}	
		try {
			shippingCharges = getPrice(CustProd.shippingCharges);
		} catch (Exception e1) {
			e1.getMessage();
		}
		try {
			perUnitExtraCharges = getPrice(CustProd.perUnitExtraCharges);
		}
		catch (Exception e1) {
			System.out.println(e1.getMessage());
		}
		double productTotal=getPrice(CustProd.productTotal);
		double handlingCharges = getPrice(CustProd.handlingCharges);
		double subTotal = getPrice(CustProd.subTotal);
		tax = getPrice(CustProd.tax);
	    total = getPrice(CustProd.totalWithTax);
        try {
			actualaccessoriesCharges = getPrice(CustProd.totalAccessorieCharges);
		} catch (Exception e1) {
			System.out.println(e1.getMessage());
			
		}
        Assert.assertEquals("Accessorie Charges is not matching or missing", String.valueOf(actualaccessoriesCharges), String.valueOf(accessoriesCharges));	
		Assert.assertEquals("SubTotal not matching or missing ", String.valueOf(Math.round((productTotal-discountCharges+extraCharges + accessoriesCharges + shippingCharges + perUnitExtraCharges + handlingCharges)*100.0)/100.0), String.valueOf(subTotal));	
		Assert.assertEquals("Total not matching or missing ", String.valueOf(Math.round((productTotal-discountCharges+extraCharges + accessoriesCharges + shippingCharges +perUnitExtraCharges+ handlingCharges+tax)*100.0)/100.0), String.valueOf(total));
		shipCharge = CustProd.shippingCharges.getAttribute("textContent");
		BaseClass.wait.until(ExpectedConditions.elementToBeClickable(CustProd.checkOutBtn)).click();
	    CustProd.okBtn.click();
	    BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.orderconf.productAddedMsg));
	    try {
   		 if(CustProd.okBtn.isDisplayed())
   		 {
			CustProd.okBtn.click();
   		 }
	    } catch (Exception e) {
			e.printStackTrace();
		}	
	    
	}


	@Then("User click on refresh total button")
	public void user_click_on_refresh_total_button() {

		CustProd.refreshTotalBtn.click();
	}

	@Then("User click on click here to ADD template for internal notes")
	public void user_click_on_click_here_to_add_template_for_internal_notes() {

		CustProd.addTemplInternalNotes.click();
	}

	@Then("User click on clear Template button for clearing internal notes")
	public void user_click_on_clear_template_button_for_clearing_internal_notes() {

		CustProd.clearTemplInternalNotes.click();
	}

	@Then("User sends the internal notes")
	public void user_sends_the_internal_notes() {
			CustProd.internalNotes.sendKeys("Adding internal notes Automation");
	}

	@Then("User selects the discount percentage {string}")
	public void user_selects_the_discount_percentage(String percentage) {
		CustProd.discountPercent(percentage).click();
	}

	@Then("User selects the Free QTY {string}")
	public void user_selects_the_free_qty(String freeqty) {
			CustProd.freeQty(freeqty).click();
	}

	@Then("User select the upsell {string}")
	public void user_select_the_upsell(String text) {
		if (text.equalsIgnoreCase("YES")) {
			if(Hooks.sceanrioName.contains("Order Confirmation")) {
				BaseClass.wait.until(ExpectedConditions.elementToBeClickable(orderconf.upsell)).click();
			}
			else {	
			Assert.assertTrue("Upsell NO is not selected ByDefault",CustProd.upsellNo.isSelected());
			CustProd.upsellYes.click();
			}
		}
	}

	@Then("User add accessories {string} and enter qty of the accessories {string}")
	public void user_add_accessories_if_required_and_enter_qty_of_the_accessories(String accessory, String qty) {	
			try {
				CustProd.selectAccessory(accessory);
				CustProd.setAccesssoryQty(accessory, qty);
				int temp=0;
				while(CustProd.accessoriesCharges.getText().equals("0")){
					Thread.sleep(2000);
					CustProd.refreshTotalBtn.click();
					temp++;
					if(temp>5) break;
				}
			}
			catch (Exception e) {
				System.out.println("***********Accessories charges coloum is not present for the Product********");

			}
	}

	@Then("User select the presetQty {string}")
	public void user_selects_the_presetqty(String presetQty) {
		int row = CustProd.totalRows.size();
		for (int i = 0; i < row; i++) {
			String text = CustProd.qtycoloum.get(i).getText();
			if (text.equals(presetQty)) {
				BaseClass.js.click(CustProd.qtycoloum.get(i));
				break;
			}
		}
	}
	@Then ("User select shipping method {string} and shipping price {string}")
	  public void  user_select_shipping_method_and_shipping_price(String shipMethod, String shipPrice) throws InterruptedException{
			if(PlaceOrderSteps.urlSerachPage.contains("US,")) {
				if (shipMethod.isEmpty()) {
					BaseClass.wait
							.until(ExpectedConditions.attributeToBeNotEmpty(BaseClass.CustProd.shipPrice, "value"));
					Thread.sleep(3000);
					Assert.assertEquals("UPS GROUND",
							BaseClass.select.getFirstSelectedOption(BaseClass.CustProd.selectShipMethod).getText());
					Assert.assertTrue("Shipping Price is less than 1",
							Float.parseFloat(BaseClass.CustProd.shipPrice.getAttribute("value")) > 1);
				} else if (!shipMethod.isEmpty() && !shipMethod.equals("FEDERAL EXPRESS")) {
					BaseClass.select.selectByVisibleText(BaseClass.CustProd.selectShipMethod, shipMethod);
					BaseClass.wait
					.until(ExpectedConditions.attributeToBeNotEmpty(BaseClass.CustProd.shipPrice, "value"));
					Assert.assertTrue("Shipping Price is less than 1",
							Float.parseFloat(BaseClass.CustProd.shipPrice.getAttribute("value")) > 1);
				} else if (shipMethod.equals("FEDERAL EXPRESS")) {	
					BaseClass.exceptionHandle.visibilityOf(BaseClass.CustProd.selectShipMethod);
					BaseClass.select.selectByVisibleText(BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.CustProd.selectShipMethod)), shipMethod);
					BaseClass.action.tab();
					BaseClass.action.enterNumber(shipPrice);
					BaseClass.CustProd.shipInstruct.sendKeys("FEDERAL EXPRESS Instruct");
					Thread.sleep(2000);
					BaseClass.js.click(CustProd.refreshTotalBtn);
					while(!BaseClass.CustProd.shippingCharges.getAttribute("textContent").equals(shipPrice)) {
						BaseClass.js.click(CustProd.refreshTotalBtn);
					};	
					Assert.assertEquals("Shipping price not updating", shipPrice,
							BaseClass.CustProd.shippingCharges.getAttribute("textContent"));
				}
			}
			else if (PlaceOrderSteps.urlSerachPage.contains("CA,")) {
				if (shipMethod.isEmpty()) {
					BaseClass.wait
							.until(ExpectedConditions.attributeToBeNotEmpty(BaseClass.CustProd.shipPrice, "value"));
					Thread.sleep(3000);
					Assert.assertEquals("UPS STANDARD",
							BaseClass.select.getFirstSelectedOption(BaseClass.CustProd.selectShipMethod).getText());
					Assert.assertTrue("Shipping Price is less than 1",
							Float.parseFloat(BaseClass.CustProd.shipPrice.getAttribute("value")) > 1);
				}
			}		
			shipMethodNA = BaseClass.select.getFirstSelectedOption(BaseClass.CustProd.selectShipMethod).getAttribute("textContent");
		}
	 
	@Then("User selects the customQty and enter the qty {string} and click on the setqty")
	public void user_selects_the_customqty_and_enter_the_qty_and_click_on_the_setqty(String qty) throws InterruptedException {
		BaseClass.js.click(CustProd.customQty);
		Thread.sleep(3000);
		CustProd.inputCustQty.sendKeys(qty);
		CustProd.setQty.click();
		Assert.assertTrue("Entered Custome not entred or not equals",CustProd.customQtyValues.get(0).getText().contains(qty));
		
	}

	@Then("User apply holds {string}")
	public void user_apply_holds(String holds) {
		String[] hold = holds.split(",");
		for (String h : hold) {
			orderconf.selectHold(h).click();
			
		}
	}
	
  @Then("User click on Add Non-Marketed Vendor Product")
	public void user_clicks_on_the_place_NonMarketed_Vendor_order() {
		PlaceOrderSteps.urlSerachPage = BaseClass.getDriver().getCurrentUrl();
		BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.productSearch.addNonMarketedVenProd)).click();
	}
	@Then("User enters vendor product details")
	public void user_enters_vendor_product_details(List<Map<String, String>> vendor) {
		BaseClass.CustProd.vendorProdQty.sendKeys(vendor.get(0).get("qty"));
		BaseClass.CustProd.vendorProdPrice.sendKeys(vendor.get(0).get("price"));
		BaseClass.CustProd.vendorQutId.sendKeys(vendor.get(0).get("quoteId"));
		BaseClass.CustProd.vendorProdCode.sendKeys(vendor.get(0).get("prodCode"));
		BaseClass.CustProd.vendorProdPrint.sendKeys(vendor.get(0).get("printProcess"));
		BaseClass.CustProd.vendorProdName.sendKeys(vendor.get(0).get("productName"));
		BaseClass.CustProd.vendorProdColor.sendKeys(vendor.get(0).get("productColor"));
		BaseClass.CustProd.vendorProdLogoColor.sendKeys(vendor.get(0).get("logoColor"));
		BaseClass.CustProd.vendorProdImpColor.sendKeys(vendor.get(0).get("imprintColor"));
		BaseClass.CustProd.vendorProdImpPos.sendKeys(vendor.get(0).get("imprintPos"));
		try {
			BaseClass.CustProd.vendorProdShipChg.sendKeys(vendor.get(0).get("shipping"));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	@Then("User remove the template and confirm the Imprint")
	public void User_remove_the_template_and_confirm_the_Imprint() throws InterruptedException {
	        BaseClass.orderconf.removeTemplate();
	           
	}
	 
	@Then("verify if template is removed")
	public void verify_if_template_is_removed() throws InterruptedException {
	Assert.assertTrue("Template was not removed", BaseClass.orderconf.verifyTemplateIsDisplayed());
	                    
	}

//Author - Sandhya
	@Then("Validate Special Packaging Charge checkbox is auto-selected")
	public void validate_special_packaging_charge_checkbox_is_auto_selected() {
		boolean splPkgChrg = false;
		try {
			splPkgChrg = CustProd.splPackagingCharge.isSelected();
		} catch (Exception e) {
			Assert.assertTrue("Special packaging charge field is not available", false);
		}
		Assert.assertTrue("Special packaging charge checkbox is not selected", splPkgChrg);
	}

//Author - Sandhya		
	@Then("Validate the quantity of Special Packaging Charge is same as main line quantity {string}")
	public void validate_the_quantity_of_special_packaging_charge_is_same_as_main_line_quantity(String mainLineQty)
			throws InterruptedException {
		Thread.sleep(4000);
		String splPkgChargeQty = CustProd.splPkgChargeQty.getAttribute("value");
		Assert.assertTrue("Mismatch found in main line Qty : " + mainLineQty + " & Special packaging charge Qty : "
				+ splPkgChargeQty, splPkgChargeQty.equalsIgnoreCase(mainLineQty));
	}

//Author - Sandhya	
	@Then("Validate the quantity of Special Packaging Charge is same as main line quantity {string} + Upsell")
	public void validate_the_quantity_of_special_packaging_charge_is_same_as_main_line_quantity_upsell(
			String mainLineQty) throws InterruptedException {

		int upsellQty = Integer.valueOf(CustProd.upsellQty.getText());
		int mainQty = Integer.valueOf(mainLineQty);
		BaseClass.wait.until(ExpectedConditions.attributeContains(CustProd.splPkgChargeQty, "value",String.valueOf(mainQty + upsellQty)));
		int splPkgChargeQty = Integer.valueOf(CustProd.splPkgChargeQty.getAttribute("value"));
		Assert.assertTrue("Mismatch found in main line Qty : " + (mainQty + upsellQty)
				+ " & Special packaging charge Qty : " + splPkgChargeQty, (splPkgChargeQty == mainQty + upsellQty));
	}
	@Then("User selects accessory {string} as additional charge")
	public void select_accessory_as_additional_charge(String accessory) throws InterruptedException {	
		CustProd.selectAccessoryByName(accessory);
		Thread.sleep(10000);
		BaseClass.wait.until(ExpectedConditions.attributeToBeNotEmpty(CustProd.getSelectedAccessoryQuantity(accessory), "value"));	
		accProductCode=CustProd.getAccessoryProductName(accessory).getText();
	        accPrice=CustProd.getAccessoryLineTotal(accessory).getAttribute("value");
	        accQty=CustProd.getSelectedAccessoryQuantity(accessory).getAttribute("value");
		Assert.assertEquals("Main line and accessory Quantity was not equal", CustProd.selectedMainLineQty.getText(), accQty);
		Assert.assertEquals("Total Accessories charges was not equal to Line Total", accPrice, String.format("%.2f", Float.parseFloat(CustProd.totalAccessorieCharges.getAttribute("textContent"))));
	  
	}
	
	@Then("User click {string} and validate the default template {string}")
	public void user_add_validate_template(String btnclick, String pagename) throws InterruptedException{
		String expectedString ="Reason for price query: \n\nSupervisor Approved: \n\nQuote ID: \n\nUnit Price: \n\nTotal Free Qty: \n\nUpsell Qty & Price: \n\nLogo Charge: \n\nAccessories: \n\nShipping & Handling: \n\nTotal Price with S&H+ VAT:  ";
		if(pagename.contains("orderconfirmation")) {
			BaseClass.orderconf.specificationsBtn.click();
			BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(orderconf.frameProductSpecifications));
			Thread.sleep(2000);
		}else {
			BaseClass.CustProd.addTemplInternalNotes.click();
			Thread.sleep(2000);			
			Assert.assertTrue("Validate the Special instruction default value", BaseClass.js.value(BaseClass.CustProd.specialInst).contains("ACCESSORY ONLY"));
		}	
		String notes[] =BaseClass.js.value(BaseClass.CustProd.internalNotes).split("\n");
		for(int i=0;i<=notes.length-1;i++) {
			Assert.assertTrue("Validate the string in notes",expectedString.contains(notes[i]));
			}
	
		if(pagename.contains("orderconfirmation")) {
			BaseClass.orderconf.cancelBtn.click();
		}
		 try {
				CustProd.okBtn.click();
			} catch (Exception e) {
				e.printStackTrace();
			}
	}
	@Then("User validates premium item image")
	public void user_validate_premium_item_image() {	
		Assert.assertTrue("Premium item image is missing",
				CustProd.premiumProduct.getAttribute("src").contains("https://assets.documents.cimpress.io/v2/fulfillers"));
	}
	public static double getPrice(WebElement ele) {	
		return Double.parseDouble(ele.getAttribute("textContent").replaceAll("[^0-9.]", "").trim());
		}
}
