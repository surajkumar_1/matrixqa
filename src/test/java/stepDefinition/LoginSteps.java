package stepDefinition;

import static base.BaseClass.login;
import static org.junit.Assert.assertEquals;


import java.util.Iterator;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import base.BaseClass;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LoginSteps {

	@Given("User enter the username and password")
	public void user_enter_the_username_and_password() {
		BaseClass.login.loginID.sendKeys(BaseClass.config.getUserName());
		login.password.sendKeys(BaseClass.config.getPassword());
	}

	@When("User clicks on login button")
	public void user_clicks_on_login_button() {
		login.signinBtn.click();
	}

	@Then("Validate the logged in user")
	public void validate_the_logged_in_user() {
		assertEquals("UserName is not matching or not loggedIn", BaseClass.config.getUserName().toLowerCase(),
				login.loggedInUser.getText());
	}


	@Given("Enter invalid user and password {string} and {string}")
	public void enter_invalid_user_and_password_and(String username, String password) {
		login.loginID.sendKeys(username);
		login.password.sendKeys(password);

	}

	@Then("Validate user is not able to login")
	public void validate_user_is_not_able_to_login() {
		assertEquals("Incorrect username and/or password", login.errorMessage.getText());

	}

	@Then("Validate all the quicklinks are working fine")
	public void validate_all_the_quicklinks_are_working_fine() {
		Iterator<WebElement> links = login.trainingLinks.iterator();
		while (links.hasNext()) {
			links.next().sendKeys(Keys.chord(Keys.CONTROL, Keys.ENTER));
		}
		assertEquals(login.trainingLinks.size(), BaseClass.getDriver().getWindowHandles().size()-1);
	}

}
