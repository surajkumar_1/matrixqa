package stepDefinition;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.support.ui.ExpectedConditions;

import base.BaseClass;
import hooks.Hooks;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class CreateCustomerSteps {

	String address1;
	String address2;
	String city;
	String state;
	String province;
	String county;
	String postalCode;
	Map<String, String> map = new HashMap<String, String>();
	@When("User navigate to Create Customer")
	public void i_navigate_to_create_customer() {
		BaseClass.homePage.manageCustomerBtn.click();
		BaseClass.js.click(BaseClass.manageCustomer.addNewCustomer);
	}

	@Then("User Select country {string} and enter CustomerName {string} and TaxRegistrationNumber {string} and Select taxcode {string}")
	public void i_select_country_and_enter_customer_name_and_tax_registration_number_and_select_taxcode(String country,
			String CustomerName, String TaxRegistrationNumber, String taxcode) {
		BaseClass.select.selectByVisibleText(BaseClass.createCustomer.selectCountry, country);
	    BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.createCustomer.timer));
	   //BaseClass.select.selectByVisibleText(BaseClass.createCustomer.taxCode, taxcode);
		BaseClass.createCustomer.customerName.sendKeys(CustomerName);
		//BaseClass.createCustomer.taxRegistrationNum.sendKeys(TaxRegistrationNumber);
		map.put("CustomerName", CustomerName);
		map.put("Country", BaseClass.select.getFirstSelectedOption(BaseClass.createCustomer.selectCountry).getText());
		
	}

	@Then("User Select Title {string} and Enter FirstName {string} and LastName {string} and Email {string} and ContactPhone {string} and MobileNumber {string} and FaxNumber {string}")
	public void i_select_title_and_enter_first_name_and_last_name_and_contact_phone_and_mobile_number_and_fax_number(
			String title, String firstName, String lastName, String email, String contactPhone, String mobileNumber,
			String faxNumber) {

		BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.createCustomer.contactTitle));
		BaseClass.select.selectByValue(BaseClass.createCustomer.contactTitle, title);
		BaseClass.createCustomer.contactFirstName.sendKeys(firstName);
		BaseClass.createCustomer.contactLastName.sendKeys(lastName);
		BaseClass.createCustomer.customerEmailAddress.sendKeys(email);
		BaseClass.createCustomer.contactPhone.sendKeys(contactPhone);

	}

	@Then("User Enter BillTo details Address1 {string} Address2 and {string} and city {string} and state {string} and county {string} and PostalCode {string} and Province {string}")
	public void i_enter_bill_to_details_address1_address2_and_and_city_and_county_and_postal_code_and_province(
			String address1, String address2, String city, String state, String county, String postalCode,
			String province) {

		this.address1 = address1;
		this.address2 = address2;
		this.city = city;
		this.state = state;
		this.county = county;
		this.province = province;
		this.postalCode = postalCode;

		BaseClass.createCustomer.billToALOne.sendKeys(address1);
		BaseClass.createCustomer.billToALTwo.sendKeys(address2);
		BaseClass.createCustomer.billToCity.sendKeys(city);
		if (Hooks.sceanrioName.contains("US")) {
			BaseClass.select.selectByVisibleText(BaseClass.createCustomer.selectBillToState, state);
		} else if (Hooks.sceanrioName.contains("CA")) {

			BaseClass.select.selectByVisibleText(BaseClass.createCustomer.selectBillToProvince, province);
		}
		else {
			BaseClass.createCustomer.billToState.sendKeys(state);
			BaseClass.createCustomer.billToCounty.sendKeys(county);
			BaseClass.createCustomer.billToProvince.sendKeys(province);
		}
		BaseClass.createCustomer.billToPostalCode.sendKeys(postalCode);

	}

	@Then("User Enter ShipTo details Address1 {string} Address2 and {string} and city {string} and state {string} and county {string} and PostalCode {string} and Province {string}")
	public void i_enter_ship_to_details_address1_address2_and_and_city_and_county_and_postal_code_and_province(
			String address1, String address2, String city, String state, String county, String postalCode,
			String province) {

		String ship = "ShipTo ";
		BaseClass.createCustomer.shipToALOne.sendKeys(ship + address1);
		BaseClass.createCustomer.shipToALTwo.sendKeys(ship + address2);
		BaseClass.createCustomer.shipToCity.sendKeys(ship + city);
		BaseClass.createCustomer.shipToState.sendKeys(ship + state);
		BaseClass.createCustomer.shipToCounty.sendKeys(ship + county);
		BaseClass.createCustomer.shipToProvince.sendKeys(ship + province);
		BaseClass.createCustomer.shipToPostalCode.sendKeys(ship + postalCode);

	}

	@Then("User click on Same for ShipTo button")
	public void i_click_on_same_for_ship_to_button() {

		BaseClass.createCustomer.sameForShipToBtn.click();
		BaseClass.wait.until(
				ExpectedConditions.attributeToBeNotEmpty(BaseClass.createCustomer.shipToPostalCode, "value"));
		String postalCode = BaseClass.createCustomer.shipToPostalCode.getAttribute("value");
		Assert.assertEquals(address1, BaseClass.createCustomer.shipToALOne.getAttribute("value"));
		Assert.assertEquals(address2, BaseClass.createCustomer.shipToALTwo.getAttribute("value"));
		Assert.assertEquals(city, BaseClass.createCustomer.shipToCity.getAttribute("value"));
		if (Hooks.sceanrioName.contains("US")) {
			Assert.assertEquals(state, BaseClass.select.getFirstSelectedOption(BaseClass.createCustomer.selectShipToState).getText());
			if(postalCode.contains("-"))
			{
			Assert.assertEquals("Postal code formate not correct",5,BaseClass.createCustomer.shipToPostalCode.getAttribute("value").split("-")[0].length());
			Assert.assertFalse("Postal code formate not correct",BaseClass.createCustomer.shipToPostalCode.getAttribute("value").split("-")[1].isEmpty());
			}
		}
		else if(Hooks.sceanrioName.contains("CA")) {
			Assert.assertEquals(province, BaseClass.select.getFirstSelectedOption(BaseClass.createCustomer.selectShipToProvince).getText());
			Assert.assertEquals("Postal code formate not correct",3,BaseClass.createCustomer.shipToPostalCode.getAttribute("value").split(" ")[0].length());
			Assert.assertFalse("Postal code formate not correct",BaseClass.createCustomer.shipToPostalCode.getAttribute("value").split(" ")[1].isEmpty());
		}
		else {
			Assert.assertEquals(state, BaseClass.createCustomer.shipToState.getAttribute("value"));
			Assert.assertEquals(county, BaseClass.createCustomer.shipToCounty.getAttribute("value"));
			Assert.assertEquals(province, BaseClass.createCustomer.shipToProvince.getAttribute("value"));
		}
		
	}

	@When("User click Create Customer button")
	public void i_click_create_customer_button() {
		BaseClass.createCustomer.createCustomerBtn.click();
	}

	@Then("User select reason for No Email {string}")
	public void i_select_reason_for_no_email(String reason) {
		BaseClass.createCustomer.customerEmailAddress.clear();
		BaseClass.createCustomer.contactLastName.click();
		BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.createCustomer.selectReasonNoEmail));
		BaseClass.select.selectByVisibleText(BaseClass.createCustomer.selectReasonNoEmail, reason);
		try {
			BaseClass.createCustomer.okBtn.click();
		} catch (Exception e) {
		
			e.printStackTrace();
		}
	}

	@Then("Customer is created")
	public void customer_is_created() {
		int count=1;
		while(count < 2) {
	        try {
	        	Assert.assertTrue("Customer is not created or value is Empty",BaseClass.wait.until(ExpectedConditions.attributeToBeNotEmpty(BaseClass.placeOrder.customerNo, "value")));
	            break;
	        } catch(StaleElementReferenceException e) {
	        }
	        count++;
	    }
    	map.put("NewCustomerNumber", BaseClass.placeOrder.customerNo.getAttribute("value"));
		map.put("Date", new SimpleDateFormat("dd-MMM-yyyy").format(new Date()).toUpperCase());
		int rownumbber = BaseClass.excel.e.getRowCount("NewCustomers");
		for (@SuppressWarnings("rawtypes")
		Map.Entry m : map.entrySet()) {
			BaseClass.excel.e.setCellData("NewCustomers", (String) m.getKey(), rownumbber + 1, (String) m.getValue());
		}

	}

	@Then("User validate max limit of AddressLine1 and AddressLine2 of BillTo and ShipTo")
	public void i_validate_max_limit_of_address_line11_and_address_line2_of_bill_to_and_ship_to() {

		String addline = "123456789012345678901234567890123456789012345678901111111111111111111111";
		//String addline2 = "123456789012345678901234567890123456789012345678901111111111111111111111";

		BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.createCustomer.billToALOne));
		BaseClass.createCustomer.billToALOne.sendKeys(addline);
		BaseClass.createCustomer.billToALTwo.sendKeys(addline);
		BaseClass.createCustomer.shipToALOne.sendKeys(addline);
		BaseClass.createCustomer.shipToALTwo.sendKeys(addline);

		Assert.assertEquals(50, BaseClass.createCustomer.billToALOne.getAttribute("value").length());
		Assert.assertEquals(30, BaseClass.createCustomer.billToALTwo.getAttribute("value").length());
		Assert.assertEquals(50, BaseClass.createCustomer.shipToALOne.getAttribute("value").length());
		Assert.assertEquals(30, BaseClass.createCustomer.shipToALTwo.getAttribute("value").length());

	}

	@Then("User validate clear button functioaliy of BillTo and ShipTo")
	public void i_validate_clear_button_functioaliy_of_bill_to_and_ship_to() {

		BaseClass.createCustomer.billToCity.sendKeys("city");
//		BaseClass.createCustomer.billToState.sendKeys("state");
//		BaseClass.createCustomer.billToProvince.sendKeys("province");
		BaseClass.createCustomer.billToCounty.sendKeys("county");
		BaseClass.createCustomer.billToPostalCode.sendKeys("099990");
		BaseClass.createCustomer.shipToCity.sendKeys("city");
//		BaseClass.createCustomer.shipToState.sendKeys("state");
//		BaseClass.createCustomer.shipToProvince.sendKeys("province");
		BaseClass.createCustomer.shipToCounty.sendKeys("county");
		BaseClass.createCustomer.shipToPostalCode.sendKeys("099990");
		BaseClass.createCustomer.billToClear.click();
		Assert.assertEquals("", BaseClass.createCustomer.billToALOne.getAttribute("value"));
		Assert.assertEquals("", BaseClass.createCustomer.billToALTwo.getAttribute("value"));
		Assert.assertEquals("", BaseClass.createCustomer.billToCity.getAttribute("value"));
		//Assert.assertEquals("", BaseClass.createCustomer.billToState.getAttribute("value"));
		Assert.assertEquals("", BaseClass.createCustomer.billToPostalCode.getAttribute("value"));
		BaseClass.createCustomer.shipToClear.click();
		Assert.assertEquals("", BaseClass.createCustomer.shipToALOne.getAttribute("value"));
		Assert.assertEquals("", BaseClass.createCustomer.shipToALTwo.getAttribute("value"));
		Assert.assertEquals("", BaseClass.createCustomer.shipToCity.getAttribute("value"));
		Assert.assertEquals("", BaseClass.createCustomer.shipToCounty.getAttribute("value"));
		//Assert.assertEquals("", BaseClass.createCustomer.shipToState.getAttribute("value"));
		//Assert.assertEquals("", BaseClass.createCustomer.shipToProvince.getAttribute("value"));
		Assert.assertEquals("", BaseClass.createCustomer.shipToPostalCode.getAttribute("value"));
	}
	
	@Then("User Click convert prospect to customer - {string}")
	public void i_Fill_Details_Search_PCT(String pct) {
		BaseClass.placeOrder.pctNo.sendKeys(pct);
		BaseClass.placeOrder.searchBtn.click();	
		BaseClass.action.moveToElement(BaseClass.placeOrder.prospect);
		BaseClass.wait.until(ExpectedConditions.elementToBeClickable(BaseClass.placeOrder.prospect)).click();
		Assert.assertFalse(BaseClass.createCustomer.customerName.getAttribute("value").isEmpty());
		Assert.assertFalse(BaseClass.createCustomer.contactPhone.getAttribute("value").isEmpty());
		Assert.assertFalse(BaseClass.createCustomer.billToALOne.getAttribute("value").isEmpty());
		Assert.assertFalse(BaseClass.createCustomer.billToCity.getAttribute("value").isEmpty());
		if (BaseClass.select.getFirstSelectedOption(BaseClass.createCustomer.billToCountry).getText().equals("United States")) {
			Assert.assertFalse(BaseClass.select.getFirstSelectedOption(BaseClass.createCustomer.selectBillToState).getText().isEmpty());
		} else if (BaseClass.select.getFirstSelectedOption(BaseClass.createCustomer.billToCountry).getText().equals("Canada")) {
			Assert.assertFalse(BaseClass.select.getFirstSelectedOption(BaseClass.createCustomer.selectBillToProvince).getText().isEmpty());
		}
		else {
			Assert.assertFalse(BaseClass.createCustomer.billToState.getAttribute("value").isEmpty());
			Assert.assertFalse(BaseClass.createCustomer.billToCounty.getAttribute("value").isEmpty());
			Assert.assertFalse(BaseClass.createCustomer.billToProvince.getAttribute("value").isEmpty());
		}
		Assert.assertFalse(BaseClass.createCustomer.billToPostalCode.getAttribute("value").isEmpty());
	}
		
	@When("User click Create Prospect Customer button")
	public void i_click_create_prospect_customer_button() {
		BaseClass.createCustomer.createCustomerBtn.click();
		BaseClass.wait.until(ExpectedConditions.elementToBeClickable(BaseClass.createCustomer.okBtn)).click();
	}
}
