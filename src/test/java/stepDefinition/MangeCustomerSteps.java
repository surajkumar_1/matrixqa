package stepDefinition;
import static base.BaseClass.manageCustomer;
import static base.BaseClass.orderconf;

import java.time.Duration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import base.BaseClass;
import hooks.Hooks;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
public class MangeCustomerSteps {
	String customerNo;
	Map<String, String> map = new HashMap<String, String>();
	@When("User enter customer number {string} and click on search")
	public void i_enter_customer_number_and_click_on_search(String customerNo) {
		this.customerNo = customerNo;
		manageCustomer.customerNo.sendKeys(customerNo);
		manageCustomer.searchBtn.click();

	}

	@Then("validate customer deatils")
	public void validate_customer_deatils() {
		BaseClass.wait.until(ExpectedConditions.visibilityOf(manageCustomer.displayCustomerNo));
		Assert.assertEquals(customerNo, manageCustomer.displayCustomerNo.getAttribute("textContent"));

	}
	@Then("User click on place order and select reason cutomerpage {string}")
	public void i_click_on_place_order_and_select_reason_Customerpage(String reason) throws InterruptedException {
		BaseClass.manageCustomer.placeOrder.click();
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
		try {
		Thread.sleep(2000);
		new PlaceOrderSteps().i_click_on_proceed_with_the_order_and_select_reason(reason);
		} catch (Exception e) {
			e.getMessage();
		}

}

	@When("User click on clear button")
	public void i_click_on_clear_button() {

		manageCustomer.clearSearchBtn.click();
	}

	@Then("All field should be clear")
	public void all_field_should_be_clear() {
		Assert.assertEquals("", manageCustomer.customerNo.getAttribute("value"));
	}

	@Then("click on PCT Lookup and get PCT Number")
	public void click_on_pct_lookup_and_get_pct_number() {
		map.put("CustomerNumber", manageCustomer.displayCustomerNo.getAttribute("textContent"));
		manageCustomer.pctBtn.click();
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
		BaseClass.wait.until(ExpectedConditions.visibilityOf(manageCustomer.pctNo));
		map.put("PCTNumber", manageCustomer.pctNo.getAttribute("textContent"));
		System.out.println(manageCustomer.pctNo.getAttribute("textContent"));
		BaseClass.getDriver().switchTo().defaultContent();
		System.out.println(manageCustomer.closePctFrame.isDisplayed());
		manageCustomer.closePctFrame.click();

	}

	@Then("click on GDPR and get Preferences")
	public void click_on_gdpr_and_get_preferences() {
		manageCustomer.gdprBtn.click();
		BaseClass.wait.until(ExpectedConditions.visibilityOfAllElements(manageCustomer.gdprPref));
		map.put("LegitimateInterest", manageCustomer.gdprPref.get(0).getAttribute("textContent"));
		map.put("CallPreference", manageCustomer.gdprPref.get(1).getAttribute("textContent"));
		map.put("RentPreference", manageCustomer.gdprPref.get(2).getAttribute("textContent"));
		map.put("MailPreference", manageCustomer.gdprPref.get(3).getAttribute("textContent"));
		map.put("EmailPreference", manageCustomer.gdprPref.get(4).getAttribute("textContent"));
		manageCustomer.closeGdpr.click();

	}

	@Then("click on View Account details and get credit Limit")
	public void click_on_view_account_details_and_get_credit_limit() {
		BaseClass.action.clickOn(manageCustomer.viewActDeatils);
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
		BaseClass.wait.until(ExpectedConditions.visibilityOf(manageCustomer.creditLimit));
		System.out.println(manageCustomer.creditLimit.getAttribute("textContent").split(":")[1]);
		map.put("CreditLimit", manageCustomer.creditLimit.getAttribute("textContent").split(":")[1]);
		BaseClass.getDriver().switchTo().defaultContent();
		manageCustomer.closeviewAcFrame.click();
		int rownumber = BaseClass.excel.e.getRowCount("CustomerDetails");
		for (@SuppressWarnings("rawtypes")
		Map.Entry m : map.entrySet()) {
			System.out.println(m.getValue());
			BaseClass.excel.e.setCellData("CustomerDetails", (String) m.getKey(), 1 + rownumber, (String) m.getValue());
		}
	}

	@Then("Check deatils of from order history")
	public void check_deatils_of_from_order_history() {

		manageCustomer.viewOrderDetails.click();
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
		Iterator<WebElement> li = manageCustomer.lineItems.listIterator();
		while (li.hasNext()) {
			WebElement webElement = (WebElement) li.next();

			System.out.println(webElement.getAttribute("textContent"));
		}
		BaseClass.getDriver().switchTo().defaultContent();
		manageCustomer.closePctFrame.click();

	}

	@When("User click on order number")
	public void i_click_on_order_number() {
		BaseClass.wait.until(ExpectedConditions.elementToBeClickable(manageCustomer.orderNumber)).click();
	}

	@Then("Manage Order should open")
	public void manage_order_should_open() {
		Assert.assertEquals("Manage Order", BaseClass.getDriver().getTitle());

	}
	@Then("Click on add BillTo/ShipTo button")
	public void click_on_add_bill_to_button() {
		if(Hooks.sceanrioName.contains("BillTo")) {
		BaseClass.js.click(manageCustomer.addBillToBtn);
		}else if(Hooks.sceanrioName.contains("ShipTo")) {
			BaseClass.js.click(manageCustomer.addShipToBtn);
		}
		BaseClass.wait
		.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(manageCustomer.switchFrameCreateBillTo));
	}
	
	@Then("Validate the max limit of the address line field")
	public void validate_the_max_limit_of_the_address_line_field() {
		if (orderconf.addLine1.getAttribute("value").length() > 50 || orderconf.addLine2.getAttribute("value").length()>30) {
			Assert.fail("Then Number of character is greater than the specified limit mentioned");
		}
	}
	
	
	
	@Then("User click on create button to add new address with primary {string}")
	public void user_click_on_create_button_to_add_address_with_primary(String primary) {
	orderconf.createBtn.click();
	BaseClass.getDriver().switchTo().defaultContent();
	if (Hooks.sceanrioName.contains("BillTo")) {
		String billToAdd1 = BaseClass.wait.until(ExpectedConditions
				.visibilityOf(manageCustomer.getbillToAddress1(Hooks.randomInt))).getAttribute("textContent");
		Assert.assertTrue("New BillTo Address not created",
				billToAdd1.contains(Hooks.randomInt));
		if (primary.equalsIgnoreCase("YES")) {
			Assert.assertEquals("Primary address not created", "Y",
					manageCustomer.checkIsPrimaryForBillTO(billToAdd1).getAttribute("textContent"));
		} else {
			Assert.assertEquals("Non Primary address not created", "N",
					manageCustomer.checkIsPrimaryForBillTO(billToAdd1).getAttribute("textContent"));
		}

	}  if (Hooks.sceanrioName.contains("ShipTo")) {	
		String shipToAdd1 =BaseClass.wait.until(ExpectedConditions
				.visibilityOf(manageCustomer.getShipToAddress1(Hooks.randomInt))).getAttribute("textContent");
		Assert.assertTrue("New BillTo Address not created", shipToAdd1.contains(Hooks.randomInt));	
		if (primary.equalsIgnoreCase("YES")) {
			Assert.assertEquals("Primary address not created", "Y",
					manageCustomer.checkIsPrimaryForShipTO(shipToAdd1).getAttribute("textContent"));
		} else {
			Assert.assertEquals("Non Primary address not created", "N",
					manageCustomer.checkIsPrimaryForShipTO(shipToAdd1).getAttribute("textContent"));
		}
	}
}
	
	@Then("Select the reason for no email entered {string}")
	public void select_the_reason_for_no_email_entered_reason(String reason) {
		orderconf.email.clear();
		manageCustomer.createBtn.click();
		BaseClass.wait.until(ExpectedConditions.visibilityOf(manageCustomer.createNewAddressError));
		BaseClass.select.selectByValue(BaseClass.createCustomer.selectReasonNoEmail, reason);
		BaseClass.getDriver().switchTo().defaultContent();
		try {
			BaseClass.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
			BaseClass.manageCustomer.gdprOKAddNewContact.click();

		} catch (Exception e) {
			e.getCause();
		}
		BaseClass.getDriver().switchTo().frame(0);
	}

	@Then("click on update BillTo/ShipTo button")
	public void click_on_update_bill_to_button() {
		if (Hooks.sceanrioName.contains("BillTo")) {
			manageCustomer.updateBillTo.click();
			BaseClass.wait
					.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(manageCustomer.switchFrameUpdateBillTo));
		} else if (Hooks.sceanrioName.contains("ShipTo")) {
			manageCustomer.updateShipTo.click();
			BaseClass.wait
					.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(manageCustomer.switchFrameUpdateShipTo));
		}

	}
	
	@Then("verify contact is Added")
	public void verify_contact_is_added() {
		Assert.assertTrue("Created new Contact First Name not matched", manageCustomer.getContactFirstName(Hooks.randomInt).getAttribute("textContent").contains(Hooks.randomInt));
		System.out.println(manageCustomer.getContactFirstName(Hooks.randomInt).getAttribute("textContent"));
		if(Hooks.sceanrioName.contains("without Email")) {
			Assert.assertEquals("Created new Contact has Email", "-", manageCustomer.getContactEmail("-").getAttribute("textContent"));
		}
		else Assert.assertTrue("Created new Contact Email not matched",manageCustomer.getContactEmail(Hooks.randomInt).getAttribute("textContent").contains(Hooks.randomInt));
	
	}
	@Then("User click on BillTo/ShipTo view contacts")
	public void user_clicks_on_bill_to_view_contacts() {
		if (Hooks.sceanrioName.contains("BillTo")) {
			BaseClass.js.scrollTilElement(BaseClass.manageCustomer.accountDetails);
			BaseClass.action.clickOn(manageCustomer.billToViewContacts);
			BaseClass.wait
					.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(manageCustomer.frameContactDetails));
			BaseClass.wait.until(ExpectedConditions.visibilityOf(manageCustomer.accountNumberContactDetails));
		} else if (Hooks.sceanrioName.contains("ShipTo")) 
			{
			BaseClass.js.scrollTilElement(BaseClass.manageCustomer.addBillToBtn);
			BaseClass.action.clickOn(manageCustomer.shipToViewContacts);
			BaseClass.wait
					.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(manageCustomer.frameContactDetails));
			BaseClass.wait.until(ExpectedConditions.visibilityOf(manageCustomer.accountNumberContactDetails));
		}
	}
	@Then("Click on create button")
	public void click_on_create_button_of_addnewcontact() throws InterruptedException {	
		BaseClass.js.click(manageCustomer.createBtn);
		BaseClass.getDriver().switchTo().defaultContent();
		Thread.sleep(5000);
	}
	@Then("User click on Apply changes button")
	public void user_click_on_apply_changes_button_of_update_contact_details() throws InterruptedException {	
		BaseClass.js.click(manageCustomer.applyChangesBtn);
        BaseClass.getDriver().switchTo().defaultContent();
		BaseClass.wait.until(ExpectedConditions.visibilityOf(manageCustomer.addBillToBtn));
		Thread.sleep(5000);
	}


	@Then("User click on Add New Contact button")
	public void user_clicks_on_add_new_contact_button() {		
		manageCustomer.addNewContactbtnContactDetails.click();
	}
	@Then("Click on Same for ShipTo")
	public void click_on_same_for_shipto() {
	BaseClass.js.click(BaseClass.manageCustomer.sameForShipTo);
	Assert.assertTrue("Same for ShipTo not selected",BaseClass.manageCustomer.sameForShipTo.isSelected());
	}
	
	@Then("User select the reason for no email {string}")
	public void user_selects_the_reason_for_no_email(String reason) throws InterruptedException {
		orderconf.email.clear();
		BaseClass.js.click(manageCustomer.createBtn);
		BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.createCustomer.selectReasonNoEmail));
		BaseClass.select.selectByValue(BaseClass.createCustomer.selectReasonNoEmail, reason);
		if(reason.equalsIgnoreCase("Customer does not want to receive promotions")){
			BaseClass.getDriver().switchTo().defaultContent();
			BaseClass.CustProd.okBtn.click();
			BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt( manageCustomer.frameContactDetails));
		}

	}
	
	@Then("User select the reason for no email in update conatct {string}")
	public void user_selects_the_reason_for_no_email_in_update_conatct(String reason) throws InterruptedException {
		orderconf.email.clear();
		BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.createCustomer.selectReasonNoEmail));
		BaseClass.select.selectByValue(BaseClass.createCustomer.selectReasonNoEmail, reason);
		if(reason.equalsIgnoreCase("Customer does not want to receive promotions")){
			BaseClass.getDriver().switchTo().defaultContent();
			BaseClass.CustProd.okBtn.click();
			BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt( manageCustomer.frameContactDetails));
		}

	}
	@Then("click on Account number to update contact details")
	public void click_on_account_number_to_update_contact_details() {

		manageCustomer.accountNumberContactDetails.click();
	}
	
	@Then("Verify BillTo/ShipTo address is updated")
	public void verify_update_address() {	
		BaseClass.getDriver().switchTo().defaultContent();
		if (Hooks.sceanrioName.contains("BillTo")) {
			String billToAdd1 = BaseClass.wait.until(ExpectedConditions
					.visibilityOf(manageCustomer.getbillToAddress1(Hooks.randomInt))).getAttribute("textContent");
			Assert.assertTrue("New BillTo Address not created",
					billToAdd1.contains(Hooks.randomInt));

		}  if (Hooks.sceanrioName.contains("ShipTo")) {
			String shipToAdd1 = BaseClass.wait.until(ExpectedConditions
					.visibilityOf(manageCustomer.getShipToAddress1(Hooks.randomInt))).getAttribute("textContent");
			Assert.assertTrue("New BillTo Address not created", shipToAdd1.contains(Hooks.randomInt));	
		}
		
	}
	
}
