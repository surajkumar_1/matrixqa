package hooks;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;


import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.asserts.SoftAssert;

import base.BaseClass;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import stepDefinition.KeyCodeProductSteps;

public class Hooks {
	public static String sceanrioName;
	public static String randomInt;
	public static SoftAssert softAssert;
	@Before
	public void beforeScenario(Scenario scenario) throws IOException {
		BaseClass.initDriver();
		BaseClass.getDriver().get("https://apex-" + BaseClass.config.getEnv() + ".pens.com/apex/f?p=106");
		sceanrioName = scenario.getName();
		randomInt = String.valueOf(new Random().nextInt(10000));
		softAssert = new SoftAssert();
		KeyCodeProductSteps.newLogo = false;
		KeyCodeProductSteps.setup = false;
		KeyCodeProductSteps.logoAsBefore = false;
	}

	@After(order = 2)
	public void takeScereenShot(Scenario scenario) throws IOException {
		byte[] screnshot = (((TakesScreenshot) BaseClass.getDriver()).getScreenshotAs(OutputType.BYTES));
		scenario.attach(screnshot, "image/png", null);
		
	}

    @After(order = 1)
	public void logOutAndClose() throws IOException, InterruptedException {
		Thread.sleep(2000);
		BaseClass.getDriver().switchTo().defaultContent();
		try {
			BaseClass.js.click(BaseClass.login.loggedInUser);
			BaseClass.login.signOutBtn.click();
		} catch (Exception e1) {

			System.out.println(e1.getMessage());
		}
		try {
			BaseClass.getDriver().switchTo().alert().accept();
		} catch (Exception e) {

			System.out.println(e.getMessage());
		}
		try {
			BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.login.signinBtn));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		BaseClass.teardown();
	}

	@After(order = 0)
	public void reportGenerateAndSave() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				try {
					Thread.sleep(5000);
					Files.move(Paths.get("./target/MatrixQAReport.html"), Paths.get("./Reports/MatrixQAReport_"
							+ LocalDateTime.now().format(DateTimeFormatter.ofPattern("L-d-YYYY H-mm-s")) + ".html"),
							StandardCopyOption.REPLACE_EXISTING);
				} catch (IOException | InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
	}
}
