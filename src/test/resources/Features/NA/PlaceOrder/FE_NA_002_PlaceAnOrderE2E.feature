Feature: Place order E2E

  Background: Login to user account
    Given User enter the username and password
    When User clicks on login button
    Then Validate the logged in user
    Given User is on Home Page
    Then User click on Place an order

   @OAT204
  Scenario Outline: Place Order with default shipping method-US
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    And User Upload new logo "<newLogo>"
    Then User sends the internal notes
    Then User select shipping method "<shipMethod>" and shipping price "<shipPrice>"
    Then User click on CheckOut button
    Then User click on product name and confirm the Imprint "<addImprint>"
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated
    Then Validate Pay button displayed

    Examples: 
      | customerNumber | reason    | keycode | addImprint | newLogo | presetQty | freeqty | shipMethod | shipPrice | saleRep       | orderType | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod |
      |       18504192 | New Order | TUSLNS  | Yes        | No      |       100 |      10 |            |           | ACKLEY, JULIA | WEB-EMAIL | Yes           | No         | No         | No         | No          |               |
  @OAT207
  Scenario Outline: Place Order with shipping method (other than Federal and UPS ground)-US
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    And User Upload new logo "<newLogo>"
    Then User sends the internal notes
    Then User select shipping method "<shipMethod>" and shipping price "<shipPrice>"
    Then User click on CheckOut button
    Then User click on product name and confirm the Imprint "<addImprint>"
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated
    Then Validate Pay button displayed

    Examples: 
      | customerNumber | reason    | keycode | addImprint | newLogo | presetQty | freeqty | shipMethod      | shipPrice | saleRep       | orderType | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod |
      |       18504192 | New Order | TUSLNS  | Yes        | No      |       100 |      10 | UPS 2ND DAY AIR |           | ACKLEY, JULIA | WEB-EMAIL | Yes           | No         | No         | No         | No          |               |
  @OAT208
  Scenario Outline: Place Order with federal express shipping method (instructions mandatory)-US
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    And User Upload new logo "<newLogo>"
    Then User sends the internal notes
    Then User select shipping method "<shipMethod>" and shipping price "<shipPrice>"
    Then User click on CheckOut button
    Then User click on product name and confirm the Imprint "<addImprint>"
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated
    Then Validate Pay button displayed

    Examples: 
      | customerNumber | reason    | keycode | addImprint | newLogo | presetQty | freeqty | shipMethod      | shipPrice | saleRep       | orderType | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod |
      |       18504192 | New Order | TUSLNS  | Yes        | No      |       100 |      10 | FEDERAL EXPRESS |     50.99 | ACKLEY, JULIA | WEB-EMAIL | Yes           | No         | No         | No         | No          |               |
  @OAT206
  Scenario Outline: Place Order with default shipping method-CA
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    And User Upload new logo "<newLogo>"
    Then User sends the internal notes
    Then User select shipping method "<shipMethod>" and shipping price "<shipPrice>"
    Then User click on CheckOut button
    Then User click on product name and confirm the Imprint "<addImprint>"
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated
    Then Validate Pay button displayed

    Examples: 
      | customerNumber | reason    | keycode | addImprint | newLogo | presetQty | freeqty | shipMethod | shipPrice | saleRep       | orderType     | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod |
      |       16360427 | New Order | TCALNS  | Yes        | yes     |       100 |      10 |            |           | ACKLEY, JULIA | WEB-EMAIL-PPC | Yes           | No         | No         | No         | No          |               |

  @AccessoryOnlyFederalShipping @OAT262
  Scenario Outline: Order Accessories only order with Federal express shipping
    When User Enter customer number "<customerNumber>" and click on serach
    Then Details should be display
    When User click on place order and select reason "<reason>"
    Then User click on add accessories product
    Then User search accessory item "<item>" and select item and enter quantity "<qty>"
    Then User select shipping method "<shipMethod>" and shipping price "<shipPrice>"
    Then User click on CheckOut button
    Then User click on product name and confirm the Imprint "<addImprint>"
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated

    Examples: 
      | customerNumber | reason    | item               | qty | shipMethod      | shipPrice |addImprint| saleRep       | orderType | paymentMethod | legitInterest | callOption | mailOption | rentOption | EmailOption |
      |       18504192 | New Order | ACC-XXX-VELCX-XX-X | 100 | FEDERAL EXPRESS |     50.99 | No       |ACKLEY, JULIA | WEB-EMAIL |               | YES           | no         | no         | no         | no          |

   @AccessoryOnlyDefaultShipping @OAT210
  Scenario Outline: Order Accessories only order with default shipping method
    When User Enter customer number "<customerNumber>" and click on serach
    Then Details should be display
    When User click on place order and select reason "<reason>"
    Then User click on add accessories product
    Then User search accessory item "<item>" and select item and enter quantity "<qty>"
    Then User select shipping method "<shipMethod>" and shipping price "<shipPrice>"
    Then User click on CheckOut button
     Then User click on product name and confirm the Imprint "<addImprint>"
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated

    Examples: 
      | customerNumber | reason    | item               | qty | shipMethod | shipPrice |addImprint |saleRep       | orderType | paymentMethod | legitInterest | callOption | mailOption | rentOption | EmailOption |
      |       18504192 | New Order | ACC-XXX-VELCX-XX-X | 100 |            |           | No        |ACKLEY, JULIA | WEB-EMAIL |               | YES           | no         | no         | no         | no          |

  @SpecializedVendorProduct @OAT249
  Scenario Outline: Placing Order, Specialized Vendor flow
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>" for vendor product
    Then User clicks on the name of the displayed search result for Vendor Item
    Then User select product options
    Then Click on Product and printing modification
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    And User Upload new logo "<newLogo>"
    Then User add accessories "<accessories>" and enter qty of the accessories "<accessoriesQty>"
    Then User select shipping method "<shipMethod>" and shipping price "<shipPrice>"
    Then User sends the internal notes
    Then User click on CheckOut button
    Then User click on product name and confirm the Imprint "<addImprint>"
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated
    Then On Manage order page verify lines are created

    Examples: 
      | customerNumber | reason    | keycode        | addImprint | newLogo | presetQty | shipMethod | shipPrice | accessories        | accessoriesQty | saleRep           | orderType | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod |
      |       20236956 | New Order | VDS-61125-J705 | yes        |         |       144 |            |           | ACC-MUH-GBXXX-XX-X |            100 | DEVRIES, NICOLAAS | WEB-EMAIL | no            | no         | no         | no         | no          |               |

  @VendorProduct @OAT224
  Scenario Outline: Placing Order, Place Vendor order with accessories
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>" for vendor product
    Then User clicks on the name of the displayed search result for Vendor Item
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    And User Upload new logo "<newLogo>"
    Then User add accessories "<accessories>" and enter qty of the accessories "<accessoriesQty>"
    Then User select shipping method "<shipMethod>" and shipping price "<shipPrice>"
    Then User sends the internal notes
    Then User click on CheckOut button
    Then User click on product name and confirm the Imprint "<addImprint>"
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated
    Then On Manage order page verify lines are created

    Examples: 
      | customerNumber | reason    | keycode       | addImprint | newLogo | presetQty | shipMethod | shipPrice | accessories        | accessoriesQty | saleRep           | orderType | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod |
      |       20236956 | New Order | VDS-61125-261 | yes        |         |       750 |            |           | ACC-MUH-GBXXX-XX-X |            100 | DEVRIES, NICOLAAS | WEB-EMAIL | no            | no         | no         | no         | no          |               |

  #@OAT238
  #Scenario Outline: Place Mail campaign Order
    #When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    #Then User clicks on the name of the displayed search result
    #Then User select product options
    #And Enter Imprint Lines "<addImprint>"
    #Then User select the presetQty "<presetQty>"
    #And User Upload new logo "<newLogo>"
    #Then User select shipping method "<shipMethod>" and shipping price "<shipPrice>"
    #Then User sends the internal notes
    #Then User click on CheckOut button
    #Then User click on product name and confirm the Imprint "<addImprint>"
    #When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    #Then User validate discounted logo and setup charge
    #Then Order should placed and order number generated
#
    #Examples: 
      #| customerNumber | reason    | keycode | addImprint | newLogo | presetQty | saleRep           | orderType    | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod | shipMethod | shipPrice |
      #|       18504192 | New Order | 79WB13  | Yes        | YES     |       100 | DEVRIES, NICOLAAS | FBO MAIL-NPC | No            | No         | No         | No         | No          |               |            |           |

  @OAT219
  Scenario Outline: Place Email campaign Order
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    And User Upload new logo "<newLogo>"
    Then User select shipping method "<shipMethod>" and shipping price "<shipPrice>"
    Then User sends the internal notes
    Then User click on CheckOut button
    Then User click on product name and confirm the Imprint "<addImprint>"
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then User validate discounted logo and setup charge
    Then Order should placed and order number generated

    Examples: 
      | customerNumber | reason    | keycode | addImprint | newLogo | presetQty | saleRep           | orderType    | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod | shipMethod | shipPrice |
      |       18504192 | New Order | 1KDA01  | Yes        | YES     |       100 | DEVRIES, NICOLAAS | FBO MAIL-NPC | No            | No         | No         | No         | No          |               |            |           |

  @PCT-ProspectToCustomer @OAT32
  Scenario Outline: Convert Prospect to customer - NA
    Then User Click convert prospect to customer - "<PCTNumber>"
    Then User Select Title "<Title>" and Enter FirstName "<FirstName>" and LastName "<LastName>" and Email "<Email>" and ContactPhone "<ContactPhone>" and MobileNumber "<MobileNumber>" and FaxNumber "<FaxNumber>"
    Then User select reason for No Email "<Reason>"
    Then User click Create Prospect Customer button
    Then Customer is created
    Then User select product options

    Examples: 
      | Title | FirstName        | LastName     | Email | Country       | PCTNumber       | Reason                                       |
      | MR.   | Auto Canada Test | Last Name    |       | Canada        | P101895767WRA02 | Customer does not want to receive promotions |
      | MR.   | Auto US Test     | US Last Name |       | United States | P7099376979SC02 | Customer does not want to receive promotions |

   @OAT252
  Scenario Outline: Order Accessories only product Validate Notes
    When User Enter customer number "<customerNumber>" and click on serach
    Then Details should be display
    When User click on place order and select reason "<reason>"
    Then User click on add accessories product
    Then User search accessory item "<item>" and select item and enter quantity "<qty>"
    Then User select shipping method "<shipMethod>" and shipping price "<shipPrice>"
    Then User click "add template" and validate the default template "creation page"
    Then User click on CheckOut button
    Then User click "Specifications" and validate the default template "orderconfirmation"
   Then User click on product name and confirm the Imprint "<addImprint>"
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated

    Examples: 
      | customerNumber | reason    | item               | qty | shipMethod      | shipPrice |addImprint| saleRep       | orderType | paymentMethod | legitInterest | callOption | mailOption | rentOption | EmailOption |
      |       18504192 | New Order | ACC-XXX-VELCX-XX-X | 100 | UPS 2ND DAY AIR |     50.99 |No        | ACKLEY, JULIA | WEB-EMAIL |               | YES           | no         | no         | no         | no          |

  @OAT263
  Scenario Outline: Place Order-Main line item with accessory as an additional charge
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    And User selects accessory "<accessory>" as additional charge
    Then User select shipping method "<shipMethod>" and shipping price "<shipPrice>"
    Then User sends the internal notes
    Then User click on CheckOut button
    Then User validates accessory "<accessory>" as additional charge details
    Then User click on product name and confirm the Imprint "<addImprint>"
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated

    Examples: 
      | customerNumber | reason    | keycode | addImprint | presetQty | saleRep           | orderType    | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod | shipMethod | shipPrice | accessory                                    |
      |       10872065 | New Order | TUSMUT  | Yes        |        72 | DEVRIES, NICOLAAS | FBO MAIL-NPC | No            | No         | No         | No         | No          |               |            |           | Mug Gift Boxes with Product Inserted Charges |

  @OAT221 @smoke
  Scenario Outline: Place Premium Product Order
    Given User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    When User clicks on the name of the displayed search result
    And User select product options
    And Enter Imprint Lines "<addImprint>"
    And User select the presetQty "<presetQty>"
    And User Upload new logo "<newLogo>"
    And User validates premium item image
    And User select shipping method "<shipMethod>" and shipping price "<shipPrice>"
    And User sends the internal notes
    And User click on CheckOut button
    And User click on product name and confirm the Imprint "<addImprint>"
    Then User verify premimum product line
    And User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated

    Examples: 
      | customerNumber | reason    | keycode | addImprint | newLogo | presetQty | saleRep           | orderType    | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod | shipMethod | shipPrice |
      |       18504192 | New Order | 78NB01  | Yes        | YES     |       100 | DEVRIES, NICOLAAS | FBO MAIL-NPC | No            | No         | No         | No         | No          |               |            |           |

   @OAT264
  Scenario Outline: Place an order with auto selected additional charge
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    ### Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    Then User select shipping method "<shipMethod>" and shipping price "<shipPrice>"
    Then Validate Special Packaging Charge checkbox is auto-selected
    Then Validate the quantity of Special Packaging Charge is same as main line quantity "<presetQty>"
    Then User click on CheckOut button
    Then User click on product name and confirm the Imprint "<addImprint>"
    Then Validate additional charges line quantity and main line quantity are same
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated

    Examples: 
      | customerNumber | reason    | keycode | addImprint | newLogo | presetQty | upsell | freeqty | shipMethod      | shipPrice | saleRep       | orderType | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod |
      |       18504192 | New Order | TUSMUG  | yes        | yes     |        24 |        |      10 | UPS 2ND DAY AIR |     50.99 | ACKLEY, JULIA | WEB-EMAIL |               | No         | No         | No         | No          |               |

  ###### Failing due to OAA-1017 
   @OAT268
  Scenario Outline: Place an order with auto selected additional charge with Main Line & Upsell
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    ###Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    Then User select the upsell "<upsell>"
    Then User select shipping method "<shipMethod>" and shipping price "<shipPrice>"
    Then Validate Special Packaging Charge checkbox is auto-selected
    Then Validate the quantity of Special Packaging Charge is same as main line quantity "<presetQty>" + Upsell
    Then User click on CheckOut button
    Then User click on product name and confirm the Imprint "<addImprint>"
    Then Validate additional charges line quantity is equal to main line + Upsell Qty
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated

    Examples: 
      | customerNumber | reason    | keycode | addImprint | newLogo | presetQty | upsell | freeqty | shipMethod | shipPrice | saleRep       | orderType | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod |
      |       18504192 | New Order | TUSMUG  | yes        | yes     |        36 | YES    |      10 |            |           | ACKLEY, JULIA | WEB-EMAIL |               | No         | No         | No         | No          |               |

    @OAT265
  Scenario Outline: Non-Marketed Vendor order - NA - Quote
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>"
    Then User click on Add Non-Marketed Vendor Product
    Then User enters vendor product details
      | qty | price | quoteId | prodCode                   | printProcess | productName | productColor | logoColor | imprintColor | imprintPos | shipping |
      |   5 |     9 |       2 | VDS-0000-NONMKT-XXVKT-XX-X | Laser        | productName | color        | logo      | imprint      | center     |    10.99 |
    Then User sends the internal notes
    Then User select shipping method "<shipMethod>" and shipping price "<shipPrice>"
    Then User click on CheckOut button
    Then User click on product name and confirm the Imprint "<addImprint>"
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated

    Examples: 
      | customerNumber |  keycode     | reason    | addImprint|saleRep           | orderType | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod | shipMethod |
      |       18504192 | VEN-TPC-3407 | New Order | No        |DEVRIES, NICOLAAS | MAIL-NPC  | No            | no         | no         | no         | No          |               |            |

   @OAT290
  Scenario Outline: Design Template order - Change template at order confirmation page
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    Then User select template "<template>"
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    Then User select the upsell "<upsell>"
    Then User sends the internal notes
    Then User select shipping method "<shipMethod>" and shipping price "<shipPrice>"
    Then User click on CheckOut button
    Then User click on product name and confirm the Imprint "<addImprint>"
    Then Validate the Template "<updateTemplate>"
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated
    Examples: 
      | customerNumber | reason    | keycode |shipMethod | template             | updateTemplate        | addImprint | newLogo | presetQty | percentage | freeQty | accessories | accessoriesQty | saleRep           | orderType | paymentMethod |
      |       18504192 | New Order | TUSCTQ  |           |Rainbow               | Light House           | Yes        | yes     |       100 |         10 |      10 | No          |            100 | DEVRIES, NICOLAAS | MAIL-NPC  |               |

      