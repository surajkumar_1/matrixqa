Feature: Redo Order Functionality
  "Order should be in closed status"

  Background: Login to user account
    Given User enter the username and password
    When User clicks on login button
    Then Validate the logged in user
    Given User is on Home Page
    Then I click on Manage Orders 
 
 
 
 @ValidatePaybuttonRedoOrder @OAT236
    Scenario Outline: Redo Order, Pay button validation
    Given User enter the order number "<orderNumber>" on manage order page
    Then User clicks on Redo order
    Then User enters saleRep "<salesRep>" and verify imprint
    When User click on Place redo order
    ##Then Redo order should place successfully
    Then Validate Pay button displayed
    
     Examples: 
      | orderNumber  | salesRep           |
      |    45623818  | DEVRIES,JESSICA    |
