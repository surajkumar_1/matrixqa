#Suraj.Kumar@pens.com
Feature: Copy order functionality
  "User not able to change price, qty, imprint and logo."

  Background: Login to user account
    Given User enter the username and password
    When User clicks on login button
    Then Validate the logged in user
    Given User is on Home Page
    Then I click on Manage Orders
           
     @OAT223 
    Scenario Outline: Copy Order-Email campaign, LOGO + Imprint
    Given User enter the order number "<orderNumber>" on manage order page
    Then User click on Copy Order button
    Then User enters saleRep "<salesRep>" and verify logo and imprint
    Then User click on Copy Order button in copy order page
    Then Order should place successfully

    Examples: 
      | orderNumber  | salesRep          |
      |     11517432 | AALDERS, CHANTAL  |
      
     @OAT239 
    Scenario Outline: Copy Order-Mail campaign, LOGO + Imprint
    Given User enter the order number "<orderNumber>" on manage order page
    Then User click on Copy Order button
    Then User enters saleRep "<salesRep>" and verify logo and imprint
    Then User click on Copy Order button in copy order page
    Then Order should place successfully

    Examples: 
      | orderNumber  | salesRep          |
      |     11517446 | AALDERS, CHANTAL  |   
   
   @OAT235
    Scenario Outline: Copy Order, Pay button validation
    Given User enter the order number "<orderNumber>" on manage order page
    Then User click on Copy Order button
    #Then User enters saleRep "<salesRep>" and verify logo and imprint
    Then User click on Copy Order button in copy order page
    Then Order should place successfully
    Then Validate Pay button displayed
    
     Examples: 
      | orderNumber  | salesRep           |
      |    45623799  | DEVRIES, NICOLAAS  |
       
 
      
   