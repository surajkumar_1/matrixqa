Feature: Manage Order Page

  Background: Login to user account
    Given User enter the username and password
    When User clicks on login button
    Then Validate the logged in user
    Given User is on Home Page
    Then I click on Manage Orders


 ##### Take orderNumber having MUG as main line product
 @OAT286
  Scenario Outline: Add Additional Charge line
    Given User enter the order number "<orderNumber>" on manage order page
    Then User click on add dependent line
    Then User select dependent line type "<depLineType>"
    When User select additional item "<item>" and click on save
    Then Additional charges Line with item "<item>" should be added

    Examples: 
       | orderNumber      | depLineType        | item                     |
       |ONCU7UE-58C35-3U1 | Additional Charges | Special Packaging Charge |



   @OAT230
  Scenario Outline: Validate Pay button is not displayed when the ABC line status as cancelled and oracle status is closed
    Given User enter the order number "<orderNumber>" on manage order page
    Then Validate Oracle Status "<oracleStatus>" and ABC Status "<ABCstatus>" and validate the pay button

    Examples: 
      | orderNumber       | oracleStatus | ABCstatus |
      | ONEC5HJ-39J69-2Y6 | Closed       | Cancelled |

  @OAT245
  Scenario Outline: Update Premium Product Order
    Given User enter the order number "<orderNumber>" on manage order page
    Then User clicks on "<lineType>" to update
    Then User clicks on "<lineType2>" to update
    Then User enters price "<price>" and Qty "<qty>" and apply changes for "<lineType2>"

    Examples: 
      | orderNumber       | lineType | lineType2 | price | qty |
      | ONMZ8XG-05E20-6O5 | Premium  | Mainitem  |     2 | 200 |

  @OAT253
  Scenario Outline: Manage Order-Cancel lines in Order with Premium Product
    Given User enter the order number "<orderNumber>" on manage order page
    Then User Click on cancel "<lineType>" Line Item
    Then User selects the cancellation category "<category>" and reason "<reason>"
    Then User Validate "<lineType>" is cancelled
    And User Click on cancel "<lineType2>" Line Item
    Then User selects the cancellation category "<category>" and reason "<reason>"
    Then User Validate "<lineType2>" is cancelled

    Examples: 
      | orderNumber       | lineType    | lineType2  |category   | reason                |
      | 45636224          | Premium     |  Mainitem  | STOCK     | ST-OS : Out of stock  |    

      