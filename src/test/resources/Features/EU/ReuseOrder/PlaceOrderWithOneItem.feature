Feature: Place Order E2E with single Line

  Background: Login to user account
    Given User enter the username and password
    When User clicks on login button
    Then Validate the logged in user
    Given User is on Home Page
    Then User click on Place an order

  @OnlyLogo
  Scenario Outline: Place Only LogoOrder (OrdersForModication)
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    And User Upload new logo "<newLogo>"
    Then User sends the internal notes
    Then User click on CheckOut button
    Then User click on product name and confirm the Imprint "<addImprint>"
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated

    Examples: 
      | customerNumber | reason    | keycode | addImprint | newLogo | presetQty | freeqty | saleRep           | orderType      | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod |
     #|       20288518 | New Order | TDELNS  | Yes        | yes     |       100 |      10 | DEVRIES, NICOLAAS | DE_WEB INBOUND | Yes           | No         | No         | No         | Yes         |               |
      |       20288518 | New Order | TDELNS  | No         | yes     |       100 |      10 | DEVRIES, NICOLAAS | DE_WEB INBOUND | Yes           | No         | No         | No         | Yes         |               |
      #|       20288518 | New Order | TDELNS  | Yes        |         |       100 |      10 | DEVRIES, NICOLAAS | DE_WEB INBOUND | Yes           | No         | No         | No         | Yes         |               |
      #|       20288518 | New Order | TDELNS  | No         |         |       100 |      10 | DEVRIES, NICOLAAS | DE_WEB INBOUND | Yes           | No         | No         | No         | Yes         |               |
      #|       20288518 | New Order | TDELNS  | No         | yes     |       100 |      10 | DEVRIES, NICOLAAS | DE_WEB INBOUND | Yes           | No         | No         | No         | Yes         |               |

  @mainLine
  Scenario Outline: Place only main line Item (OrdersForModication)
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    Then User sends the internal notes
    Then User click on CheckOut button
    Then User click on product name and confirm the Imprint "<addImprint>"
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated

    Examples: 
      | customerNumber | reason    | keycode | addImprint | newLogo | presetQty | saleRep           | orderType      | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod |
      |       20288518 | New Order | TDELPG  | No         |         |       100 | DEVRIES, NICOLAAS | DE_WEB INBOUND | Yes           | No         | No         | No         | No          |               |

  # |       18145728 | New Order | TPTLPG  | Yes        |         | 100         |   DEVRIES, NICOLAAS | PT_WEB INBOUND | Yes           | No         | No         | No         | Yes         |               |
  #|       18145728 | New Order | TPTLPG  | Yes        | yes     | No       |       100 |      10 | DEVRIES, NICOLAAS | PT_WEB INBOUND | Yes           | No         | No         | No         | Yes         |               |
  #|       18145728 | New Order | TPTLPG  | Yes        | yes     | No       |       100 |      10 | DEVRIES, NICOLAAS | PT_WEB INBOUND | Yes           | No         | No         | No         | Yes         |               |
  #|       18145728 | New Order | TPTLPG  | Yes        | yes     | No       |       100 |      10 | DEVRIES, NICOLAAS | PT_WEB INBOUND | Yes           | No         | No         | No         | Yes         |               |
  @FreeQty
  Scenario Outline: Place the Free Qty (OrdersForModication)
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    And User Upload new logo "<newLogo>"
    Then User selects the Free QTY "<freeqty>"
    Then User sends the internal notes
    Then User click on CheckOut button
    Then User click on product name and confirm the Imprint "<addImprint>"
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated

    Examples: 
      | customerNumber | reason    | keycode | addImprint | newLogo | presetQty | freeqty | saleRep           | orderType      | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod |
      |       20288518 | New Order | TDELNS  | Yes        | yes     |       100 |      10 | DEVRIES, NICOLAAS | DE_WEB INBOUND | Yes           | No         | No         | No         | Yes         |               |

  @WithAccessory
  Scenario Outline: Place only With Accessory (OrdersForModication)
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    And User Upload new logo "<newLogo>"
    Then User select the presetQty "<presetQty>"
    Then User add accessories "<accessories>" and enter qty of the accessories "<accessoriesQty>"
    Then User sends the internal notes
    Then User click on CheckOut button
    Then User click on product name and confirm the Imprint "<addImprint>"
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated

    Examples: 
      | customerNumber | reason    | keycode | addImprint | newLogo |    | presetQty | accessories        | accessoriesQty | saleRep           | orderType      | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod |
      |       20288518 | New Order | TDELPG  | Yes        | yes     | No |       100 | ACC-XXX-CPBCX-XX-X |            100 | DEVRIES, NICOLAAS | DE_WEB INBOUND | Yes           | No         | No         | No         | No          |               |

  #|       18145728 | New Order | TPTLPG  | Yes        | yes     | No       |       100 | ACC-XXX-GBXXX-XX-X |            100 | DEVRIES, NICOLAAS | PT_WEB INBOUND | Yes           | No         | No         | No         | Yes         |               |
  #|       18145728 | New Order | TPTLPG  | Yes        | yes     | No       |       100 | ACC-XXX-XJX0X-XX-X |            100 | DEVRIES, NICOLAAS | PT_WEB INBOUND | Yes           | No         | No         | No         | Yes         |               |
  @WithUpsell
  Scenario Outline: Place only With Upsell (OrdersForModication)
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    And User Upload new logo "<newLogo>"
    Then User select the upsell "<upsell>"
    Then User sends the internal notes
    Then User click on CheckOut button
    Then User click on product name and confirm the Imprint "<addImprint>"
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated

    Examples: 
      | customerNumber | reason    | keycode | addImprint | newLogo | upsell | presetQty | saleRep           | orderType      | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod |
      |       20288518 | New Order | TDELPG  | Yes        | yes     | Yes    |       100 | DEVRIES, NICOLAAS | DE_WEB INBOUND | Yes           | No         | No         | No         | No          |               |

 @PriceAndCSRHold
  Scenario Outline: Apply price query and CSR Hold Functionality (OrdersForModication)
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    And User Upload new logo "<newLogo>"
    Then User select the presetQty "<presetQty>"
    Then User sends the internal notes
    Then User apply holds "<hold>"
    Then User click on CheckOut button
    Then User click on product name and confirm the Imprint "<addImprint>"
    Then verify the current hold is applied "<hold>"
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated
   ## Then Hold should be visible in manage order page "<hold>"

    Examples: 
      | customerNumber | reason    | keycode | addImprint | newLogo | presetQty | saleRep           | orderType      | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod | hold      |
      |       20288518 | New Order | TDELNS  | Yes        | yes     |       100 | DEVRIES, NICOLAAS | DE_WEB INBOUND | YES           | no         | no         | no         | YES         |               | Price,CSR |

  @QuoteOrder
  Scenario Outline: Placing Quote order (OrdersForModication)
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    And User Upload new logo "<newLogo>"
    Then User select the upsell "<upsell>"
    ##Then User add accessories "<accessories>" and enter qty of the accessories "<accessoriesQty>"
    Then User selects the Free QTY "<freeQty>"
    Then User sends the internal notes
    Then User click on CheckOut button
    Then User click on product name and confirm the Imprint "<addImprint>"
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated
    Then On Manage order page verify lines are created

    Examples: 
      | customerNumber | reason    | keycode | addImprint | newLogo | presetQty | freeQty | accessories | accessoriesQty | saleRep           | orderType      | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod |
      |       18145728 | New Order | TPTLNS  | Yes        | yes     |       500 |      10 | No          |            100 | DEVRIES, NICOLAAS | PT_WEB INBOUND | Yes           | No         | No         | No         | Yes         |               |

  @WithAllLines
  Scenario Outline: Place only With Accessory (OrdersForModication)
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    And User Upload new logo "<newLogo>"
    Then User select the presetQty "<presetQty>"
    Then User select the upsell "<upsell>"
    Then User selects the Free QTY "<freeqty>"
    Then User add accessories "<accessories>" and enter qty of the accessories "<accessoriesQty>"
    Then User sends the internal notes
    Then User click on CheckOut button
    Then User click on product name and confirm the Imprint "<addImprint>"
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated

    Examples: 
      | customerNumber | reason    | keycode | addImprint | newLogo | presetQty | upsell | freeqty | accessories        | accessoriesQty | saleRep           | orderType | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod |
      |       11868445 | New Order | TUSLPG  | Yes        | yes     |       100 | yes    |      10 | ACC-XXX-CPBCX-XX-X |            100 | DEVRIES, NICOLAAS | MAIL-NPC  | Yes           | No         | No         | No         | Yes         |               |

  @Pricehold
  Scenario Outline: Apply price query Hold Functionality (OrdersForModication)
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    Then User select template "<template>"
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    Then User select the upsell "<upsell>"
    Then User add accessories "<accessories>" and enter qty of the accessories "<accessoriesQty>"
    Then User selects the discount percentage "<percentage>"
    Then User selects the Free QTY "<freeqty>"
    Then User sends the internal notes
    Then User apply holds "<hold>"
    Then User click on CheckOut button
    Then User click on product name and confirm the Imprint "<addImprint>"
    Then verify the current hold is applied "<hold>"
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated
    Then Hold should be visible in order generation page "<hold>"

    Examples: 
      | customerNumber | reason    | keycode | addImprint | newLogo | presetQty | freeqty | accessories | accessoriesQty | saleRep           | orderType      | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod | hold  |
      |       18145728 | New Order | TPTLPG  | Yes        | yes     |       100 |      10 | No          |            100 | DEVRIES, NICOLAAS | PT_WEB INBOUND | YES           | no         | no         | no         | YES         |               | Price |
