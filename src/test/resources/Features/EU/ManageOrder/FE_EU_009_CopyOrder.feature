Feature: Copy order functionality
  "User not able to change price, qty, imprint and logo."

  Background: Login to user account
    Given User enter the username and password
    When User clicks on login button
    Then Validate the logged in user
    Given User is on Home Page
    Then I click on Manage Orders

  @smoke @OAT35
  Scenario Outline: Copy Order, LOGO + Imprint
    Given User enter the order number "<orderNumber>" on manage order page
    Then User click on Copy Order button
    Then User enters saleRep "<salesRep>" and verify logo and imprint
    Then User click on Copy Order button in copy order page
    Then Order should place successfully

    Examples: 
      | orderNumber       | salesRep          |
      | ONHZ2NM-44C35-8D6 | DEVRIES, NICOLAAS |

  @OAT33
  Scenario Outline: Copy Order, only Logo
    Given User enter the order number "<orderNumber>" on manage order page
    Then User click on Copy Order button
    Then User enters saleRep "<salesRep>" and verify logo
    Then User click on Copy Order button in copy order page
    Then Order should place successfully

    Examples: 
      | orderNumber       | salesRep          |
      | ONEU5WO-73O15-4T4 | DEVRIES, NICOLAAS |

  @OAT34
  Scenario Outline: Copy Order, only Imprint
    Given User enter the order number "<orderNumber>" on manage order page
    Then User click on Copy Order button
    Then User enters saleRep "<salesRep>" and verify imprint
    Then User click on Copy Order button in copy order page
    Then Order should place successfully

    Examples: 
      | orderNumber       | salesRep          |
      | ONUP0ZI-90W22-4S1 | DEVRIES, NICOLAAS |

  @OAT241 @OAT63
  Scenario Outline: Copy Order, Non-Customized Order
    Given User enter the order number "<orderNumber>" on manage order page
    Then User click on Copy Order button
    Then User enters saleRep "<salesRep>"
    Then User click on Copy Order button in copy order page
    Then Order should place successfully

    Examples: 
      | orderNumber       | salesRep          |
      | ONNC3PC-06V88-8D9 | DEVRIES, NICOLAAS |

  @OAT246
  Scenario Outline: Copy Vendor Order , LOGO + Imprint
    Given User enter the order number "<orderNumber>" on manage order page
    Then User click on Copy Order button
    Then User enters saleRep "<salesRep>" and verify logo and imprint
    Then User click on Copy Order button in copy order page
    Then Order should place successfully

    Examples: 
      | orderNumber       | salesRep         |
      | ONES3ZY-66U90-6G0 | DEVRIES, JESSICA |

  @OAT247
  Scenario Outline: Copy Vendor Order, only Imprint
    Given User enter the order number "<orderNumber>" on manage order page
    Then User click on Copy Order button
    Then User enters saleRep "<salesRep>" and verify imprint
    Then User click on Copy Order button in copy order page
    Then Order should place successfully

    Examples: 
      | orderNumber       | salesRep         |
      | ONSM7EI-99U11-9Q0 | DEVRIES, JESSICA |

  @OAT248
  Scenario Outline: Copy Vendor Order, only Logo
    Given User enter the order number "<orderNumber>" on manage order page
    Then User click on Copy Order button
    Then User enters saleRep "<salesRep>" and verify logo
    Then User click on Copy Order button in copy order page
    Then Order should place successfully

    Examples: 
      | orderNumber       | salesRep          |
      | ONEU5WO-73O15-4T4 | DEVRIES, NICOLAAS |

  @OAT250
  Scenario Outline: Copy PCT Order
    Given User enter the order number "<orderNumber>" on manage order page
    Then Validate PCT Number is displayed
    Then Read the number of line items, imprint and logo of parent order
    Then User click on Copy Order button
    Then Validate the number of line items, imprint and logo of parent order and copy
    Then User click on Copy Order button in copy order page
    Then Order should place successfully

    Examples: 
      | orderNumber | salesRep          |
      |    45623650 | DEVRIES, NICOLAAS |

  @OAT251
  Scenario Outline: Copy Accessory only Order
    Given User enter the order number "<orderNumber>" on manage order page
    Then Read the number of line items, imprint and logo of parent order
    Then User click on Copy Order button
    Then Validate the number of line items, imprint and logo of parent order and copy
    Then User click on Copy Order button in copy order page
    Then Order should place successfully

    Examples: 
      | orderNumber | salesRep      |
      |    45629142 | ACKLEY, JULIA |

  @OAT258
  Scenario Outline: Copy Order, Validate Salesrep dropdown
    Given User enter the order number "<orderNumber>" on manage order page
    Then User click on Copy Order button
    Then Validate salesrep dropdown
    Then User click on Copy Order button in copy order page
    Then Order should place successfully

    Examples: 
      | orderNumber |
      |    45636545 |

  @OAT254
  Scenario Outline: Copy Order, Validate Ordertype
    Given User enter the order number "<orderNumber>" on manage order page
    Then User click on Copy Order button
    Then Validate ordertype on Copy page
    Then User click on Copy Order button in copy order page
    Then Order should place successfully

    Examples: 
      | orderNumber       |
      | ONJM9NI-57Q38-4M5 |

  @OAT259
  Scenario Outline: Copy Order, Validate request date
    Given User enter the order number "<orderNumber>" on manage order page
    Then User click on Copy Order button
    Then Validate request date
    Then User click on Copy Order button in copy order page
    Then Order should place successfully

    Examples: 
      | orderNumber       |
      | ONJM9NI-57Q38-4M5 |

  @OAT274
  Scenario Outline: Copy Order,  Validate sales rep ,request date order type and Salesrep email
    Given User enter the order number "<orderNumber>" on manage order page
    Then User click on Copy Order button
    Then Validate salesrep dropdown
    Then Validate Email address salesrep
    Then Validate ordertype on Copy page
    Then Validate request date
    Then User click on Copy Order button in copy order page
    Then Order should place successfully

    Examples: 
      | orderNumber       |
      | ONJM9NI-57Q38-4M5 |
