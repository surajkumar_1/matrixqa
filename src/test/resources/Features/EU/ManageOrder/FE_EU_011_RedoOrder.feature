#Author: suraj.kumar@cimpress.com
Feature: Redo Order Functionality
  "Order should be in closed status"

  Background: Login to user account
    Given User enter the username and password
    When User clicks on login button
    Then Validate the logged in user
    Given User is on Home Page
    Then I click on Manage Orders

  @smoke @OAT43
  Scenario Outline: REDO Order (Logo + Imprint)
    Given User enter the order number "<orderNumber>" on manage order page
    Then User clicks on Redo order
    Then User enters saleRep "<salesRep>" and verify logo and imprint
    Then Update main price "<price>" and quantity "<qty>"
    Then Update logo of Redo order
    Then Update attributes of Redo order
    Then Update ship charge "<shipCharge>"
    Then Update the imprint of Redo order
    When User click on Place redo order
    Then Redo order should place successfully with imprint and same logo

    Examples: 
      | orderNumber       | price | qty | shipCharge | salesRep          |
      | ONXZ2GD-13B14-9C7 |    55 | 200 |      20.99 | DEVRIES, NICOLAAS |

  @OAT41
  Scenario Outline: REDO Order (Only Logo Order)
    Given User enter the order number "<orderNumber>" on manage order page
    Then User clicks on Redo order
    Then User enters saleRep "<salesRep>" and verify logo
    Then Update main price "<price>" and quantity "<qty>"
    Then Update logo of Redo order
    Then Update attributes of Redo order
    Then Update ship charge "<shipCharge>"
    When User click on Place redo order
    Then Redo order should place successfully with same logo

    Examples: 
      | orderNumber       | price | qty | shipCharge | salesRep          |
      | ONKG2BQ-86F20-4Y9 |    55 | 200 |      20.99 | DEVRIES, NICOLAAS |

 @OAT87
  Scenario Outline: REDO Order (Only Imprint Order)
    Given User enter the order number "<orderNumber>" on manage order page
    Then User clicks on Redo order
    Then User enters saleRep "<salesRep>" and verify imprint
    Then Update main price "<price>" and quantity "<qty>"
    Then Update attributes of Redo order
    Then Update ship charge "<shipCharge>"
    Then Update the imprint of Redo order
    When User click on Place redo order
    Then Redo order should place successfully with updated imprint

    Examples: 
      | orderNumber       | price | qty | shipCharge | salesRep          |
      | ONPX3UB-45N08-2E3 |    55 | 200 |      20.99 | DEVRIES, NICOLAAS |

  @OAT44
  Scenario Outline: REDO Order - Non Customized update price and qty
    Given User enter the order number "<orderNumber>" on manage order page
    Then User clicks on Redo order
    Then User enters saleRep "<salesRep>"
    Then Update main price "<price>" and quantity "<qty>"
    Then Update ship charge "<shipCharge>"
    When User click on Place redo order
    Then Redo order should place successfully

    Examples: 
      | orderNumber       | price | qty | shipCharge | salesRep          |
      | ONMH5DT-35K86-2A0 |    55 | 200 |      20.99 | DEVRIES, NICOLAAS |
 
 @OAT42
  Scenario Outline: REDO Order (Only Imprint Order- No Update in imprint)
    Given User enter the order number "<orderNumber>" on manage order page
    Then User clicks on Redo order
    Then User enters saleRep "<salesRep>" and verify imprint
    Then Update main price "<price>" and quantity "<qty>"
    Then Update attributes of Redo order
    Then Update ship charge "<shipCharge>"
    When User click on Place redo order
    Then Redo order should place successfully with imprint

    Examples: 
      | orderNumber       | price | qty | shipCharge | salesRep          |
      | ONPX3UB-45N08-2E3 |    55 | 200 |      20.99 | DEVRIES, NICOLAAS |

  @OAT233 @OAT234
  Scenario Outline: REDO Order (update accessory price and qty)
    Given User enter the order number "<orderNumber>" on manage order page
    Then User clicks on Redo order
    Then User enters saleRep "<salesRep>" and verify logo and imprint
    Then Update accessory price "<price>" and quantity "<qty>"
    When User click on Place redo order
    Then Redo order should place successfully for accessory

    Examples: 
      | orderNumber       | price | qty | shipCharge | salesRep          |
      | ONFM0HK-73K33-0H8 |  1.39 |  20 |      30.99 | DEVRIES, NICOLAAS |

  @OAT242 @OAT243
  Scenario Outline: REDO Order (update upsell price and qty)
    Given User enter the order number "<orderNumber>" on manage order page
    Then User clicks on Redo order
    Then User enters saleRep "<salesRep>" and verify logo and imprint
    Then Update upsell price "<price>" and quantity "<qty>"
    When User click on Place redo order
    Then Redo order should place successfully for upsell

    Examples: 
      | orderNumber       | price | qty | shipCharge | salesRep          |
      | ONFM0HK-73K33-0H8 |  1.39 |  20 |      30.99 | DEVRIES, NICOLAAS |

  @OAT244
  Scenario Outline: REDO Order (update free qty)
    Given User enter the order number "<orderNumber>" on manage order page
    Then User clicks on Redo order
    Then User enters saleRep "<salesRep>" and verify imprint
    Then Update free quantity "<qty>"
    When User click on Place redo order
    Then Redo order should place successfully for freeqty

    Examples: 
      | orderNumber       | qty | salesRep          |
      | ONCY7IF-97Y14-3E7 |  20 | DEVRIES, NICOLAAS |

  @OAT255
  Scenario Outline: REDO Order - Non Customized
    Given User enter the order number "<orderNumber>" on manage order page
    Then Verify logo and imprint
    Then User clicks on Redo order
    Then User enters saleRep "<salesRep>"
    When User click on Place redo order
    Then Redo order should place successfully for non-customized order

    Examples: 
      | orderNumber       | salesRep          |
      | ONMH5DT-35K86-2A0 | DEVRIES, NICOLAAS |

  @OAT260
  Scenario Outline: REDO Order (update discount price)
    Given User enter the order number "<orderNumber>" on manage order page
    Then User clicks on Redo order
    Then User enters saleRep "<salesRep>"
    Then Update discount "<price>"
    When User click on Place redo order
    Then Redo order should place successfully for update discount

    Examples: 
      | orderNumber       | price | salesRep          |
      | ONOW5GO-97Z51-6H2 |   -20 | DEVRIES, NICOLAAS |

  @OAT261
  Scenario Outline: REDO Order (update Logo charge )
    Given User enter the order number "<orderNumber>" on manage order page
    Then User clicks on Redo order
    Then User enters saleRep "<salesRep>"
    Then Update logocharge "<price>"
    When User click on Place redo order
    Then Redo order should place successfully for update logocharge

    Examples: 
      | orderNumber       | price | salesRep          |
      | ONOW5GO-97Z51-6H2 |    20 | DEVRIES, NICOLAAS |
