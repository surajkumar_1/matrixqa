####Author: sneha.maheshwari@pens.com
Feature: Fax Preview of Test of differnt Orders

  Background: Login to user account
    Given User enter the username and password
    When User clicks on login button
    Then Validate the logged in user
    Given User is on Home Page
    Then I click on Manage Orders

   @OAT191 @smoke
  Scenario Outline: Fax Preview page-order line
    Given User enter the order number "<orderNumber>" on manage order page
    Then User clicks on Fax preview button
    Then User validate details on fax preview and fax template"<keycode>"
    Then User validate order line items for Fax Preview

    Examples: 
      | orderNumber | keycode    |
      |    45623839 | TJPEWC JP  |
