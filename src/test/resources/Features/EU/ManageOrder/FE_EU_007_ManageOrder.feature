Feature: Manage Order Page

  Background: Login to user account
    Given User enter the username and password
    When User clicks on login button
    Then Validate the logged in user
    Given User is on Home Page
    Then I click on Manage Orders

  @InvalidOrder
  Scenario Outline: Manage order, Search a Invalid order
    Given User enter the order number "<orderNumber>" on manage order page
    Then Invalid Order message will display

    ###Then User Update the request date of the order number given "<date>"
    Examples: 
      | orderNumber |
      |      271400 |

  @Cancellation
  Scenario Outline: Cancelletion History of Orders
    Given User enter the order number "<orderNumber>" on manage order page
    And click on cancellation history
    Then User can see cancellation History

    Examples: 
      | orderNumber       |
      | ONZX6WO-92D07-3Z1 |

  ##Update Order Each
  @BookOrder @OAT105
  Scenario Outline: Manage Order - Book An Order
    Given User enter the order number "<orderNumber>" on manage order page
    When User clicks on book order button
    Then Order status should change to Booked

    Examples: 
      | orderNumber       |
      | ONBB3VU-69E75-2R2 |

  @cancelOrder @smoke @OAT77
  Scenario Outline: Manage order, Cancel an Order
    Given User enter the order number "<orderNumber>" on manage order page
    Then User click on cancel Order button and select the cancellation category "<category>" reason "<reason>" and enter comments "<comments>"

    Examples: 
      | orderNumber       | reason                                | category | comments     |
      | ONOE5QV-27G65-0X9 | PC-CH : Customer unhappy with charges | PRICING  | cancellation |

  @createNewBillToAddress
  Scenario Outline: Manage order, Create New BillTo Address
    Given User enter the order number "<orderNumber>" on manage order page
    Then User clicks on create new bill to button in manage order page
    Then User enters the address details "<primary>"
      | country | addline1    | addline2    | city    | postal | county    | prov    | state    |
      | Germany | NewAddLine1 | NewAddLine2 | Newcity |  09899 | Newcounty | Newprov | newstate |
    Then User enters the contact information
      | email          | firstname | lastname | phone      | mobile  | fax |
      | email@pens.com | firstname | lastname | 8989900333 | 8990033 |  33 |
    Then User clicks on the create button for creating a new address "<primary>"

    Examples: 
      | orderNumber       | reason     | primary |
      | ONJP4TN-47H05-5A9 | TEST ORDER | yes     |

  @createNewShipToAddress
  Scenario Outline: Manage order, Create New ShipTo Address
    Given User enter the order number "<orderNumber>" on manage order page
    Then User clicks on create new SHIP to button in manage order page
    Then User enters the address details "<primary>"
      | country | addline1    | addline2    | city    | postal | county    | prov    | state    |
      | Germany | NewAddLine1 | NewAddLine2 | Newcity |  09899 | Newcounty | Newprov | newstate |
    Then User enters the contact information
      | email          | firstname | lastname | phone      | mobile  | fax |
      | email@pens.com | firstname | lastname | 8989900333 | 8989033 |  33 |
    Then User clicks on the create button for creating a new address "<primary>"

    Examples: 
      | orderNumber       | reason     | primary |
      | ONJP4TN-47H05-5A9 | TEST ORDER | No      |

  #####Update the logic make it dynamic to select contact
  #@SelectBillToContact @OAT83
  #Scenario Outline: Manage order, Select Bill To contact name
  #Given User enter the order number "<orderNumber>" on manage order page
  #Then User select the bill contact "<contact>"
  #Then User click OK button to update bill to contact
  #Then Validate billto contact is updated "<contact>"
  #
  #Examples:
  #| orderNumber | contact              |
  #|    41370284 | FIRST NAME LAST NAME |
  #
  #@SelectShipToContact @OAT84
  #Scenario Outline: Manage order, Select Ship To contact name
  #Given User enter the order number "<orderNumber>" on manage order page
  #Then User select the ship contact "<contact>"
  #Then User click OK button to update ship to contact
  #Then Validate shipto contact is updated "<contact>"
  #
  #Examples:
  #| orderNumber | contact     |
  #|    41375680 | FIRST NAME LAST NAME |
  #
  ##**order should be in Entered/Booked/supplyEligiable stage to change
  ##Entered/Booked/supplyEligiable Status validation pending
  @ChangeBillToAddress
  Scenario Outline: Manage order, Change billTo address
    Given User enter the order number "<orderNumber>" on manage order page
    Then User clicks on change bill to button

    Examples: 
      | orderNumber       |
      | ONJP4TN-47H05-5A9 |

  @ChangeShipToAddress @OAT85
  Scenario Outline: Manage order, Change shipTo Address
    Given User enter the order number "<orderNumber>" on manage order page
    Then User clicks on change ship to button

    Examples: 
      | orderNumber       |
      | ONJP4TN-47H05-5A9 |

  ##Artwork hold:CSR
  ###'Open', 'Initial', 'Confirmed', 'Processing'
  @removeHold @smoke
  Scenario Outline: Manage Hold- Remove hold
    Given User enter the order number "<orderNumber>" on manage order page
    Then User click on Manage Hold button
    Then User select hold to be remove from the list "<holdname>"
    Then User write release comments "<comments>"
    Then User click on Remove hold button
    Then User validate the "<holdname>" is removed

    Examples: 
      | orderNumber       | holdname          | comments                         |
      | ONPD0XN-38O08-1K5 | CSR APPROVAL HOLD | Hold released by test automation |
      | ONPD0XN-38O08-1K5 | PRICE QUERY       | Hold released by test automation |

  @ApplyHolds @smoke @OAT152
  Scenario Outline: Manage Hold- Apply hold
    Given User enter the order number "<orderNumber>" on manage order page
    Then User click on Manage Hold button
    Then User select hold to be apply from the list "<holdname>"
    Then User write apply comments "<comments>"
    Then User click on Apply hold button
    Then User validate the "<holdname>" is applied

    Examples: 
      | orderNumber       | holdname    | comments                        |
      | ONPD0XN-38O08-1K5 | PRICE QUERY | hold applied by test automation |

  #@HoldHistory @OAT26
  #Scenario Outline: Manage order, Manage Hold and Hold History Validation
  #Given User enter the order number "<orderNumber>" on manage order page
  #Then User click on Manage Hold button
  #Then User click on Holds History button and Hold Lengend button and Validate the Holds
  #
  #Examples:
  #| orderNumber          |
  #|    ONIZ2PZ-50Z41-7Z2 |
  ##**Line type is Case-sensitive data **Use ABC number
  ###***For orders that only have main one of the below line and shipping and logo charge
  @UpdateLine @smoke @OAT79 @OAT78
  Scenario Outline: Update Line Item Quantity and Price
    Given User enter the order number "<orderNumber>" on manage order page
    Then User clicks on "<lineType>" to update
    Then User enters price "<price>" and Qty "<qty>" and apply changes for "<lineType>"

    Examples: 
      | orderNumber       | lineType  | price | qty |
      | ONIP0LZ-60W28-8B4 | Mainitem  |   2.5 | 500 |
      | ONMS1IS-29I98-7H3 | Free      |     0 | 100 |
      | ONZX6WO-92D07-3Z1 | Upsell    |   100 | 600 |
      | ONOB5DG-41C11-9A3 | Accessory |   2.5 | 600 |

  ###Ask What should be order status to cancel Line Iten except BOOK
  @cancelLine @smoke @OAT103 @OAT98 @OAT99
  Scenario Outline: Manage Order-Cancel Line item
    Given User enter the order number "<orderNumber>" on manage order page
    Then User Click on cancel "<lineType>" Line Item
    Then User selects the cancellation category "<category>" and reason "<reason>"
    Then User Validate "<lineType>" is cancelled

    Examples: 
      | orderNumber       | lineType  | category | reason               |
      | ONIP0LZ-60W28-8B4 | Mainitem  | STOCK    | ST-OS : Out of stock |
      | ONMS1IS-29I98-7H3 | Free      | STOCK    | ST-OS : Out of stock |
      | ONZX6WO-92D07-3Z1 | Upsell    | STOCK    | ST-OS : Out of stock |
      | ONOB5DG-41C11-9A3 | Accessory | STOCK    | ST-OS : Out of stock |

  @AddDepedentDiscount @smoke @OAT167
  Scenario Outline: Adding discount line
    Given User enter the order number "<orderNumber>" on manage order page
    Then User click on add dependent line
    Then User select dependent line type "<depLineType>"
    When User select discount percentage "<discount>" and click on save
    Then Discount line should be added

    Examples: |##depLineType(Case-senesitive)
      | orderNumber       | depLineType | discount |
      | ONPY8PC-86U67-9H8 | Others      |       10 |

  @AddDepedentAccessory @smoke @OAT164
  Scenario Outline: Adding Accessory line
    Given User enter the order number "<orderNumber>" on manage order page
    Then User click on add dependent line
    Then User select dependent line type "<depLineType>"
    When User select accessory item "<item>" and select item and enter quantity "<qty>" and price "<price>" and click on save
    Then Accessory Line with "<item>" should be added

    Examples: ##LineTpe(Case-senesitive)
      | orderNumber       | depLineType     | item               | qty | price |
      | ONPY8PC-86U67-9H8 | Add Accessories | ACC-XXX-J4XH3-XX-X |  90 |  0.75 |

  @AddDepedentUpsell @smoke @OAT166
  Scenario Outline: Adding Upsell line
    Given User enter the order number "<orderNumber>" on manage order page
    Then User click on add dependent line
    Then User select dependent line type "<depLineType>"
    Then User select the upsell "<upsell>"
    Then Upsell Line should be added

    Examples: ##LineTpe(Case-senesitive)
      | orderNumber       | depLineType | upsell |
      | ONPY8PC-86U67-9H8 | Others      | yes    |

  @OAT165 @smoke
  Scenario Outline: Adding Logo Charge line
    Given User enter the order number "<orderNumber>" on manage order page
    Then User click on add dependent line
    Then User select dependent line type "<depLineType>"
    Then User select logo charges "<logoCharge>" and add logo Charge

    Examples: |##LineTpe(Case-senesitive)|logoAsBefore/logocharge/both|
      | orderNumber       | depLineType | logoCharge |
      | ONPY8PC-86U67-9H8 | Add Logo    | logoCharge |

  @OAT176
  Scenario Outline: Update Line Item Shipping method from Standard to Express for Order Quote or Booked Orders
    Given User enter the order number "<orderNumber>" on manage order page
    Then User clicks Update in lineType Shipping and selects shipment as express

    Examples: 
      | orderNumber       |
      | ONXM9AH-21P40-2A8 |

  @OAT177
  Scenario Outline: Manage Order-Update Shipping method when line has been cancelled
    Given User enter the order number "<orderNumber>" on manage order page
    #Then User checks the lines status for "<lineType>" is cancelled
    Then User clicks Update in lineType Shipping and selects shipment as express

    Examples: 
      | orderNumber       | lineType |
      | ONZX6WO-92D07-3Z1 | Upsell   |

  @OAT288
  Scenario Outline: Update line notes
    Given User enter the order number "<orderNumber>" on manage order page
    Then User update Line Notes "<updatedLineNotes>"

    Examples: 
      | orderNumber       | updatedLineNotes              |
      | ONFR0PR-81U59-7E7 | Automation updated line notes |
      
   @OAT283
  Scenario Outline: Adding Free & Upsell Line
    Given User enter the order number "<orderNumber>" on manage order page
    Then User click on add dependent line
    Then User select dependent line type "<depLineType>"
    Then User select the upsell and free "<free>"
    Then verify Upsell & Free lines are added

    Examples: ##LineTpe(Case-senesitive)
      | orderNumber      | depLineType |free  |
      |ONVF7JL-55J89-6D4 | Others      | 10   |
  
  @OAT285
  Scenario Outline: Adding Upsell & discount line
    Given User enter the order number "<orderNumber>" on manage order page
    Then User click on add dependent line
    Then User select dependent line type "<depLineType>"
    Then User select the upsell and discount "<discount>"
    Then verify Upsell and discount lines are added

    Examples: ##LineTpe(Case-senesitive)
      | orderNumber       | depLineType |free  | discount |
      |ONGQ6QH-22L80-8T6  | Others      | 10   | 10			  |