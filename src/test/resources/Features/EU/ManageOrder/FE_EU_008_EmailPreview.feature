#Author: suraj.kumar@pens.com
Feature: Email Preview of Test of differnt Orders

  Background: Login to user account
    Given User enter the username and password
    When User clicks on login button
    Then Validate the logged in user
    Given User is on Home Page
    Then I click on Manage Orders
   @smoke @OAT29
  Scenario Outline: Email Preview page- Inhouse(Logo + Imprint)
    Given User enter the order number "<orderNumber>" on manage order page
    Then User click on Email preview button
    Then User validate logo and imprint for inhosue product "<keycode>"
    Then User validate order line items
   
    Examples: 
      | orderNumber | keycode |
      |    45623814 | TDELNS  |
  @OAT28
  Scenario Outline: Email Preview page- Inhouse(only logo)
    Given User enter the order number "<orderNumber>" on manage order page
    Then User click on Email preview button
    Then User validate only logo for inhosue product "<keycode>"
    Then User validate order line items

    Examples: 
      | orderNumber | keycode |
      |    45623821 | TDELNS  |
  @OAT30
  Scenario Outline: Email Preview page- Inhouse(only imprint)
    Given User enter the order number "<orderNumber>" on manage order page
    Then User click on Email preview button
    Then User validate only imprint for inhosue product "<keycode>"
    Then User validate order line items

    Examples: 
      | orderNumber | keycode |
      |    45623817 | TDELNS  |
  @OAT62
  Scenario Outline: Email Preview page- Inhouse(Non-customize)
    Given User enter the order number "<orderNumber>" on manage order page
    Then User click on Email preview button
    Then User validate no logo and imprint for inhosue product "<keycode>"
    Then User validate order line items

    Examples: 
      | orderNumber | keycode |
      |    45623820 | TDELNS  |

 @OAT31
  Scenario Outline: Email Preview page- Vendor(imprint + Logo)
    Given User enter the order number "<orderNumber>" on manage order page
    Then User click on Email preview button
    Then User validate logo and imprint for Vendor product "<keycode>"
    Then User validate order line items

    Examples: 
      | orderNumber       | keycode |
      | ONFW8RG-56D10-2S9 | VENDOR  |
