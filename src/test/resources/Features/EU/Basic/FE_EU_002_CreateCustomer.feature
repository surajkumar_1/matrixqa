#Author-Suraj.kumar@pens.com
Feature: Create New Customer

  Background: 
    Given User enter the username and password
    When User clicks on login button
    Then Validate the logged in user
    Then User navigate to Create Customer

  @CreatesameShipTo @OAT7
  Scenario Outline: Create New Customer - ShipTo same as Bill To
    Then User Select country "<country>" and enter CustomerName "<CustomerName>" and TaxRegistrationNumber "<TaxRegNO>" and Select taxcode "<taxCode>"
    Then User Select Title "<Title>" and Enter FirstName "<FirstName>" and LastName "<LastName>" and Email "<Email>" and ContactPhone "<ContactPhone>" and MobileNumber "<MobileNumber>" and FaxNumber "<FaxNumber>"
    Then User Enter BillTo details Address1 "<AddressL1>" Address2 and "<AddressL2>" and city "<city>" and state "<state>" and county "<county>" and PostalCode "<PostalCode>" and Province "<Province>"
    Then User click on Same for ShipTo button
    When User click Create Customer button
    Then Customer is created

    Examples: 
      | country | CustomerName  | TaxRegNO | taxCode | Title | FirstName | LastName | Email        | ContactPhone | MobileNumber | FaxNumber | AddressL1        | AddressL2     | city    | state          | county | PostalCode | Province |
      | Germany | Customer Name |      090 | EUVATX  | MR.   | FirstName | LastName | PT@email.com |   9132348844 |   2123238844 |    999999 | R Carne Azeda 75 | Rua Pombal 83 | Funchal | lha da Madeira |        | 9060-203   | Province |

  @OAT7
  Scenario: Create New Customer - Field Validations
    Then User validate max limit of AddressLine1 and AddressLine2 of BillTo and ShipTo
    Then User validate clear button functioaliy of BillTo and ShipTo
  @OAT7
  Scenario Outline: Create New Customer - with different ShipTo and BillTo
    Then User Select country "<country>" and enter CustomerName "<CustomerName>" and TaxRegistrationNumber "<TaxRegNO>" and Select taxcode "<taxCode>"
    Then User Select Title "<Title>" and Enter FirstName "<FirstName>" and LastName "<LastName>" and Email "<Email>" and ContactPhone "<ContactPhone>" and MobileNumber "<MobileNumber>" and FaxNumber "<FaxNumber>"
    Then User Enter BillTo details Address1 "<AddressL1>" Address2 and "<AddressL2>" and city "<city>" and state "<state>" and county "<county>" and PostalCode "<PostalCode>" and Province "<Province>"
    Then User Enter ShipTo details Address1 "<AddressL1>" Address2 and "<AddressL2>" and city "<city>" and state "<state>" and county "<county>" and PostalCode "<PostalCode>" and Province "<Province>"
    When User click Create Customer button
    Then Customer is created

    Examples: 
      | country | CustomerName  | TaxRegNO | taxCode | Title | FirstName | LastName | Email        | ContactPhone | MobileNumber | FaxNumber | AddressL1        | AddressL2     | city    | state          | county | PostalCode | Province |
      | Germany | Customer Name |      090 | EUVATX  | MR.   | FirstName | LastName | PT@email.com |   9132348844 |   2123238844 |    999999 | R Carne Azeda 75 | Rua Pombal 83 | Funchal | lha da Madeira |        | 9060-203   | Province |

  @CreatewithoutEmail @OAT7
  Scenario Outline: Create New Customer - without Email Address
    Then User Select country "<country>" and enter CustomerName "<CustomerName>" and TaxRegistrationNumber "<TaxRegNO>" and Select taxcode "<taxCode>"
    Then User Select Title "<Title>" and Enter FirstName "<FirstName>" and LastName "<LastName>" and Email "<Email>" and ContactPhone "<ContactPhone>" and MobileNumber "<MobileNumber>" and FaxNumber "<FaxNumber>"
    Then User Enter BillTo details Address1 "<AddressL1>" Address2 and "<AddressL2>" and city "<city>" and state "<state>" and county "<county>" and PostalCode "<PostalCode>" and Province "<Province>"
    Then User click on Same for ShipTo button
    Then User select reason for No Email "<Reason>"
    When User click Create Customer button
    Then Customer is created

    Examples: 
      | country | CustomerName  | TaxRegNO | taxCode | Title | FirstName | LastName | Email        | ContactPhone | MobileNumber | FaxNumber | AddressL1        | AddressL2     | city    | state          | county | PostalCode | Province | Reason                                       |
      | Germany | Customer Name |      090 | EUVATX  | MR.   | FirstName | LastName | PT@email.com |   9132348844 |   2123238844 |    999999 | R Carne Azeda 75 | Rua Pombal 83 | Funchal | lha da Madeira |        | 9060-203   | Province | Customer does not want to receive promotions |
