#Author: sneha.maheshwari@pens.com
Feature: Customer Care-Service Requests Functionality

  Background: Login to user account
    Given User enter the username and password
    When User clicks on login button
    Then Validate the logged in user
    Given User is on Home Page
    Then User clicks on Customer Care

  ###update new oracle order number every time in orderNumber
  @OAT192 @smoke
  Scenario Outline: Create Service Request
    Given User enter the order number "<orderNumber>" on Manage Service Request page
    Then User click on Search button on Manage Service Request page
    When User click on create SR button from search results on Manage Service Request page
    Then User create SR -requestType"<requestType>" description"<description>" category"<problemCategory>" problem code"<problemCode>" responsible person"<responsiblePerson>"

    Examples: 
      | orderNumber | requestType | description    | problemCategory | problemCode               | responsiblePerson |
      |     45624079 | Collections | descriptionSR1 | Customer        | CU-II : Imprint incorrect | AALDERS, CHANTAL  |
   
  #####  Use Order number from previous scenario
  @OAT202
  Scenario Outline: Update Service Request
    Given User enter the order number "<orderNumber>" on Manage Service Request page
    Then User click on Search button on Manage Service Request page
    Then User click on SR number from search results on Manage Service Request page
    Then User create SR -requestType"<requestType>" description"<description>" category"<problemCategory>" problem code"<problemCode>" responsible person"<responsiblePerson>"

    Examples: 
      | orderNumber | requestType | description          | problemCategory      | problemCode                              | responsiblePerson |
      |    45624079 | Box Return  | updatedDescriptionSR | Executive Management | EC-TE : Executive Decision to Ship Early | AASE, SVEIN       |
  
  #Use Order number from previous scenario
  @OAT198
  Scenario Outline: Create Action Request
    Given User enter the order number "<orderNumber>" on Manage Service Request page
    Then User click on Search button on Manage Service Request page
    When User click on SR number from search results on Manage Service Request page
    Then User create Action request-assignee"<assignee>" description"<description>" actionStatus"<actionStatus>" actionType"<actionType>"

    Examples: 
      | orderNumber | actionType  | actionStatus | assignee    | description       |
      |    45624079 | Collections | Open         | AT ACCOUNTS | actionDescription |
   
  #Use Order number from previous scenario
  @OAT199
  Scenario Outline: Update Action Request
    Given User enter the order number "<orderNumber>" on Manage Service Request page
    Then User click on Search button on Manage Service Request page
    When User click on SR number from search results on Manage Service Request page
    Then User create Action request-assignee"<assignee>" description"<description>" actionStatus"<actionStatus>" actionType"<actionType>"

    Examples: 
      | orderNumber | actionType     | actionStatus | assignee           | description              |
      |    45624079 | Expedite Order | Open         | ALEXANDER, CHELSEY | updatedActionDescription |
 
  #####  Use Order number from previous scenario
  @OAT199
  Scenario Outline: Close Action Request
    Given User enter the order number "<orderNumber>" on Manage Service Request page
    Then User click on Search button on Manage Service Request page
    When User click on SR number from search results on Manage Service Request page
    Then User clicks edit action request
    And User enters resolution for action request "<resolution>"
    Then User clicks update and close button

    Examples: 
      | orderNumber | resolution       |
      |    45624079 | ActionResolution |
  
  ##Use Order number from previous scenario
  @OAT203
  Scenario Outline: Reopen closed Action Request
    Given User enter the order number "<orderNumber>" on Manage Service Request page
    Then User click on Search button on Manage Service Request page
    When User click on SR number from search results on Manage Service Request page
    Then User clicks reopen action request

    Examples: 
      | orderNumber | resolution       |
      |    45624079 | ActionResolution |
  
  Scenario Outline: User should not be able to create Action Request without mandatory fields
    Given User enter the order number "<orderNumber>" on Manage Service Request page
    Then User click on Search button on Manage Service Request page
    When User click on SR number from search results on Manage Service Request page
    When User clicks add new action button
    Then User clicks add action button
    Then Error message to enter mandatory fields for creating action request displays

    Examples: 
      | orderNumber |
      |    45615830 |
  

  Scenario Outline: Validate Action request description and resolution field character limit
    Given User enter the order number "<orderNumber>" on Manage Service Request page
    Then User click on Search button on Manage Service Request page
    When User click on SR number from search results on Manage Service Request page
    Then User clicks add new action button
    And Validate User cannot enter description and resolution greater than 2000 characters

    Examples: 
      | orderNumber |
      |    45615830 |
      


  Scenario Outline: User should not be able to create Service Request without mandatory fields
    Given User enter the order number "<orderNumber>" on Manage Service Request page
    Then User click on Search button on Manage Service Request page
    When User click on create SR button from search results on Manage Service Request page
    Then User selects request type "<requestType>"
    Then User clicks create service request button
    Then Error message to enter mandatory fields for creating service request displays

    Examples: 
      | orderNumber | requestType |
      |    45623906 | Collections |


  Scenario Outline: Validate Service request description field character limit
    Given User enter the order number "<orderNumber>" on Manage Service Request page
    Then User click on Search button on Manage Service Request page
    When User click on create SR button from search results on Manage Service Request page
    And Validate User cannot enter description greater than 4000 characters

    Examples: 
      | orderNumber | requestType |
      |    45623836 | Collections |
   

  Scenario Outline: Manage Service Requests:Search without entering any value in search fields
    Given User click on Search button without entering mandatory fields on Manage Service Request page
    Then Error message to enter search criteria displays

  Scenario Outline: Search Service Request with sr number
    Given User enter the sr number "<srNumber>" on Manage Service Request page
    Then User click on Search button on Manage Service Request page
    Then User click on SR number from search results on Manage Service Request page

    Examples: 
      | srNumber   |
      |    4173953 |
   
  @OAT200
  Scenario Outline: Search Service Request with assignee
    Given User enter the assignee "<assignee>" on Manage Service Request page
    Then User click on Search button on Manage Service Request page
    Then User click on SR number from search results on Manage Service Request page

    Examples: 
      | assignee            |
      | AIT HELLAL, YASMINA |
  

  Scenario Outline: Manage Service Requests:Clear fields
    Given User enter the order number "<orderNumber>" on Manage Service Request page
    Then User click on Search button on Manage Service Request page
    Then User click on Clear button on Manage Service Request page

    Examples: 
      | orderNumber |
      |    44992991 |
  

  Scenario Outline: Search Service Request with customer number and Service request number
    Given User enter the customer number "<customerNumber>" on Manage Service Request page
    Then User click on Search button on Manage Service Request page
    Given User enter the sr number "<srNumber>" on Manage Service Request page
    Then User click on Search button on Manage Service Request page
    Then User click on SR number from search results on Manage Service Request page

    Examples: 
      | customerNumber | srNumber   |
      |       18631502 | 4173953 |
    

  @createMultipleActionRequests
  Scenario Outline: Create multiple Action Requests
    Given User enter the order number "<orderNumber>" on Manage Service Request page
    Then User click on Search button on Manage Service Request page
    When User click on SR number from search results on Manage Service Request page
    Then User create Action request-assignee"<assignee>" description"<description>" actionStatus"<actionStatus>" actionType"<actionType>"
    Then User create Action request-assignee"<assignee>" description"<description>" actionStatus"<actionStatus>" actionType"<actionType>"

    Examples: 
      | orderNumber | actionType | actionStatus | assignee    | description       |
      |    45623441 | BOX RETURN | Open         | AT ACCOUNTS | actionDescription |
