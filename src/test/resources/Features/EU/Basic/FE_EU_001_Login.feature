Feature: Test for Login functionality
   
  Scenario: User should be able to login to application and validate logged in user
    Given User enter the username and password
    When User clicks on login button
    Then Validate the logged in user

   
   Scenario Outline: User is not able to login for incorrect username and password
    Given Enter invalid user and password "<username>" and "<password>"
    When User clicks on login button
    Then Validate user is not able to login

    Examples: 
      | username | password |
      | abcscsc  |   123455 |
 
   
  Scenario: Validate all the quick links are working fine
    Given User enter the username and password
    When User clicks on login button
    Then Validate the logged in user
    Then Validate all the quicklinks are working fine
