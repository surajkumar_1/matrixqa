##Author:suraj.kumar@pens.com
Feature: Order Confirmation Page

  Background: Login to user account
    Given User enter the username and password
    When User clicks on login button
    Then Validate the logged in user
    Given User is on Home Page
    Then User click on Place an order

  @AddNewBillToAddress @OAT17
  Scenario Outline: Order Confirmation -- Create New BillTo Address
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    Then User click on CheckOut button
    Then User click on select different BillTo button
    Then User click on Add New BillTo button
    Then User enters the address details "<primary>"
      | country | addline1    | addline2    | city    | postal | county    | prov    | state    |
      | Germany | NewAddLine1 | NewAddLine2 | Newcity |  09899 | Newcounty | Newprov | newstate |
    Then User enters the contact information
      | email          | firstname | lastname | phone      | mobile | fax |
      | email@pens.com | firstname | lastname | 8989900333 |    123 | 123 |
    Then User click on create button to create new address with primary "<primary>"

    Examples: 
      | customerNumber | reason    | keycode | addImprint | presetQty | notes | primary |
      |       20288518 | New Order | TDELNS  | No         |       100 | Yes   | No      |

  @AddNewShipToAddress @OAT17
  Scenario Outline: Order Confirmation -- Create New ShipTo Address
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    Then User click on CheckOut button
    Then User click on select different ShipTo button
    Then User click on Add New ShipTo button
    Then User enters the address details "<primary>"
      | country | addline1    | addline2    | city    | postal | county    | prov    | state    |
      | Germany | NewAddLine1 | NewAddLine2 | Newcity |  09899 | Newcounty | Newprov | newstate |
    Then User enters the contact information
      | email          | firstname | lastname | phone      | mobile | fax |
      | email@pens.com | firstname | lastname | 8989900333 |    123 | 123 |
    Then User click on create button to create new address with primary "<primary>"

    Examples: 
      | customerNumber | reason    | keycode | addImprint | presetQty | notes | primary |
      |       20288518 | New Order | TDELNS  | No         |       100 | Yes   | No      |

  @AddNewBillToConatctWithoutEmail @OAT17
  Scenario Outline: Order Confirmation -- Create New BillTo Conatct Without Email
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    Then User click on CheckOut button
    Then User click on select different BillTo button
    Then User click on Add BillTo conatct
    Then User enters the contact information
      | email          | firstname | lastname | phone      | mobile | fax |
      | email@pens.com | firstname | lastname | 8989900333 |    123 | 123 |
    And Select the reason for no email entered "<noEmailreason>"
    Then User click on create to add new contact

    Examples: 
      | customerNumber | reason    | keycode | addImprint | presetQty | notes | primary | noEmailreason                                |
      |       20288518 | New Order | TDELNS  | No         |       100 | Yes   | No      | Customer does not have an email              |
      |       20288518 | New Order | TDELNS  | No         |       100 | Yes   | No      | Customer does not know their email           |
      |       20288518 | New Order | TDELNS  | No         |       100 | Yes   | No      | Sight Entry – no email on order form         |
      |       20288518 | New Order | TDELNS  | No         |       100 | Yes   | No      | Customer does not want to receive promotions |

  @AddNewShipToConatctWithoutEmail @OAT17
  Scenario Outline: Order Confirmation -- Create New ShipTo Conatct Without Email
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    Then User click on CheckOut button
    Then User click on select different ShipTo button
    Then User click on Add ShipTo conatct
    Then User enters the contact information
      | email          | firstname | lastname | phone      | mobile | fax |
      | email@pens.com | firstname | lastname | 8989900333 |    123 | 123 |
    And Select the reason for no email entered "<noEmailreason>"
    Then User click on create to add new contact

    Examples: 
      | customerNumber | reason    | keycode | addImprint | presetQty | notes | primary | noEmailreason                                |
      |       20288518 | New Order | TDELNS  | No         |       100 | Yes   | No      | Customer does not have an email              |
      |       20288518 | New Order | TDELNS  | No         |       100 | Yes   | No      | Customer does not know their email           |
      |       20288518 | New Order | TDELNS  | No         |       100 | Yes   | No      | Sight Entry – no email on order form         |
      |       20288518 | New Order | TDELNS  | No         |       100 | Yes   | No      | Customer does not want to receive promotions |

  ###***We can make Non-Primary Address to Primary**Can't Make a Primary address to Non-Primary.
  @BillToUpdateAddress @OAT17
  Scenario Outline: Order Confirmation -- BillTo - Update Address
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    Then User click on CheckOut button
    Then User click on select different BillTo button
    Then User click on edit address button
    Then User enters the address details "<primary>"
      | country | addline1    | addline2    | city    | postal | county    | prov    | state    |
      | Germany | NewAddLine1 | NewAddLine2 | Newcity |  09899 | Newcounty | Newprov | newstate |
    Then User click on apply changes button to update address with primary "<primary>"

    Examples: 
      | customerNumber | reason    | keycode | addImprint | presetQty | primary |
      |       20288518 | New Order | TDELPG  | Yes        |       500 | Yes     |

  @ShipToUpdateAddress @OAT17
  Scenario Outline: Order Confirmation -- ShipTo - Update Address
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    Then User click on CheckOut button
    Then User click on select different ShipTo button
    Then User click on edit address button
    Then User enters the address details "<primary>"
      | country | addline1    | addline2    | city    | postal | county    | prov    | state    |
      | Germany | NewAddLine1 | NewAddLine2 | Newcity |  09899 | Newcounty | Newprov | newstate |
    Then User click on apply changes button to update address with primary "<primary>"

    Examples: 
      | customerNumber | reason    | keycode | addImprint | presetQty | primary |
      |       20288518 | New Order | TDELPG  | Yes        |       100 | YES     |

  @AddBillToContact @OAT17
  Scenario Outline: Order Confirmation -- BillTo - Add Conatct
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    Then User click on CheckOut button
    Then User click on select different BillTo button
    Then User click on Add BillTo conatct
    Then User enters the contact information
      | firstname | lastname | phone      | mobile | fax | email          |
      | firstname | lastname | 8989900333 |    123 | 123 | email@pens.com |
    Then User click on create to add new contact

    Examples: 
      | customerNumber | reason    | keycode | addImprint | presetQty |
      |       20288518 | New Order | TDELPG  | Yes        |       100 |

  @AddShipToContact @OAT17
  Scenario Outline: Order Confirmation -- ShipTo - Add Conatct
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    Then User click on CheckOut button
    Then User click on select different ShipTo button
    Then User click on Add ShipTo conatct
    Then User enters the contact information
      | firstname | lastname | phone      | mobile | fax | email          |
      | firstname | lastname | 8989900333 |    123 | 123 | email@pens.com |
    Then User click on create to add new contact

    Examples: 
      | customerNumber | reason    | keycode | addImprint | presetQty |
      |       20288518 | New Order | TDELPG  | Yes        |       100 |

  @EditBillToContact
  Scenario Outline: Order Confirmation -- BillTo - Edit Conatct
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    Then User click on CheckOut button
    Then User click on select different BillTo button
    Then User click on edit BillTo conatct
    Then User enters the contact information
      | firstname | lastname | phone      | mobile | fax | email          |
      | firstname | lastname | 8989900333 |    123 | 123 | email@pens.com |
    Then User click on apply changes button to update contact

    Examples: 
      | customerNumber | reason    | keycode | addImprint | presetQty |
      |       20288518 | New Order | TDELPG  | Yes        |       100 |

  @EditShipToContact
  Scenario Outline: Order Confirmation -- ShipTo - Edit Conatct
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    Then User click on CheckOut button
    Then User click on select different ShipTo button
    Then User click on edit ShipTo conatct
    Then User enters the contact information
      | firstname | lastname | phone      | mobile | fax | email          |
      | firstname | lastname | 8989900333 |    123 | 123 | email@pens.com |
    Then User click on apply changes button to update contact

    Examples: 
      | customerNumber | reason    | keycode | addImprint | presetQty |
      |       20288518 | New Order | TDELPG  | Yes        |       100 |

  @SelectBillToAddress @smoke @OAT17
  Scenario Outline: Order Confirmation -- Select Other Bill to Address
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    Then User click on CheckOut button
    Then User click on select different BillTo button
    Then User select BillTo Address and click on save

    Examples: 
      | customerNumber | reason    | keycode | addImprint | presetQty |
      |       20288837 | New Order | TDELPG  | Yes        |       100 |

  @SelectShipToAddress @OAT17
  Scenario Outline: Order Confirmation -- Select Other ShipTo Address
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    Then User click on CheckOut button
    Then User click on select different ShipTo button
    Then User select ShipTo Address and click on save

    Examples: 
      | customerNumber | reason    | keycode | addImprint | presetQty |
      |       20288518 | New Order | TDELPG  | Yes        |       100 |

  @EditBillToEmail @OAT17
  Scenario Outline: Order Confirmation -- BillTo -- Edit Email
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    Then User click on CheckOut button
    Then User Edit BillTo email

    Examples: 
      | customerNumber | reason    | keycode | addImprint | presetQty |
      |       20288518 | New Order | TDELPG  | Yes        |       100 |

  @SpecificationsUpsell @smoke @OAT190
  Scenario Outline: Order Confirmation -- Line Details and Specifications add Upsell
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    Then User click on CheckOut button
    Then User click on the specification button of the product
    Then User apply holds "<hold>"
    Then User select the upsell "<upsell>"
    Then User sends the internal notes
    When User click on the save button
    Then Changes should be added "<priority>" and "<hold>" and "<upsell>"

    Examples: 
      | customerNumber | reason    | keycode | priority | notes | hold            | addImprint | uploadLogo | presetQty | upsell |
      |       20288518 | New Order | TDELPG  | express  | yes   | price query,CSR | No         | Yes        |       100 | yes    |

  @SpecificationsFreeQty @OAT186
  Scenario Outline: Order Confirmation -- Line Details and Specifications add Free Line
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    Then User click on CheckOut button
    Then User click on the specification button of the product
    Then User apply holds "<hold>"
    Then User select the free quantity "<FreeQty>"
    Then User sends the internal notes
    When User click on the save button
    Then Free Line should be added with "<priority>" and "<hold>"

    Examples: 
      | customerNumber | reason    | keycode | priority | notes | hold            | addImprint | uploadLogo | presetQty | FreeQty |
      |       20288518 | New Order | TDELPG  | express  | yes   | price query,CSR | No         | Yes        |       100 | 5%      |

  @SpecificationsDiscount @OAT189
  Scenario Outline: Order Confirmation -- Line Details and Specifications add Discount
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    Then User click on CheckOut button
    Then User click on the specification button of the product
    Then User apply holds "<hold>"
    Then User select the discount "<discount>"
    Then User sends the internal notes
    When User click on the save button
    Then Discount Line should be added with "<priority>" and "<hold>" and "<discount>"

    Examples: 
      | customerNumber | reason    | keycode | priority | notes | hold            | addImprint | uploadLogo | presetQty | discount |
      |       20288518 | New Order | TDELPG  | express  | yes   | price query,CSR | No         | Yes        |       100 | 5%       |

  @SpecificationsApplyHold @OAT215
  Scenario Outline: Order Confirmation -- Line Details and Specifications apply price query and CSR Hold
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    Then User click on CheckOut button
    Then User click on product name and confirm the Imprint "<addImprint>"
    Then User click on the specification button of the product
    Then User apply holds "<hold>"
    When User click on the save button
    Then verify the current hold is applied "<hold>"
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated
    Then Hold should be visible in order generation page "<hold>"

    Examples: 
      | customerNumber | reason    | keycode | hold            | addImprint | uploadLogo | presetQty | saleRep           | orderType      | paymentMethod | legitInterest | callOption | mailOption | rentOption | EmailOption |
      |       20288518 | New Order | TDELPG  | price query,CSR | Yes        | Yes        |       100 | DEVRIES, NICOLAAS | DE_WEB INBOUND |               | No            | No         | No         | No         | No          |

  @SpecificationsReleaseHold @OAT216
  Scenario Outline: Order Confirmation -- Line Details and Specifications Remove hold
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    Then User apply holds "<hold>"
    Then User click on CheckOut button
    Then User click on product name and confirm the Imprint "<addImprint>"
    Then User click on the specification button of the product
    Then User select hold to be remove from the list "<hold>"
    When User click on the save button
    Then verify the current hold is released "<hold>"
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated
    Then Hold should not visible in order generation page "<hold>"

    Examples: 
      | customerNumber | reason    | keycode | hold            | addImprint | uploadLogo | presetQty | saleRep           | orderType      | paymentMethod | legitInterest | callOption | mailOption | rentOption | EmailOption |
      |       20288518 | New Order | TDELPG  | price query,CSR | Yes        | Yes        |       100 | DEVRIES, NICOLAAS | DE_WEB INBOUND |               | No            | No         | No         | No         | No          |

  @RemoveTemplate @OAT228
  Scenario Outline: Remove Template from Order confirmation page and place order
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    Then User select template "<template>"
    Then User enters anniversary "<AnniNumber>"
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    Then User click on CheckOut button
    Then User remove the template and confirm the Imprint
    And verify if template is removed
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated

    Examples: 
      | customerNumber | reason    | keycode | template    | addImprint | newLogo | presetQty | percentage | freeQty | accessories | accessoriesQty | saleRep           | orderType      | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod | AnniNumber |
      |       20288518 | New Order | TDELN7  | Anniversary | yes        | yes     |       100 |         10 |      10 | No          |            100 | DEVRIES, NICOLAAS | DE_WEB INBOUND | No            | no         | no         | no         | no          |               |          5 |

  @RemoveLine @OAT19
  Scenario Outline: Order confirmation -- Remove a Line Item
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    Then User select the upsell "<upsell>"
    Then User selects the discount percentage "<percentage>"
    Then User click on CheckOut button
    Then User removes a line item "<linetype>"

    Examples: 
      | customerNumber | reason    | keycode | linetype | addImprint | uploadLogo | presetQty | upsell | percentage | notes | add accessories | accessories qty |
      |       20288518 | New Order | TDELNS  | upsell   | No         | Yes        |       100 | Yes    |         10 | Yes   | No              |             100 |

  