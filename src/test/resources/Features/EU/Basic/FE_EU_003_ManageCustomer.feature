Feature: Manage Customer Page

  Background: Login to user account
    Given User enter the username and password
    When User clicks on login button
    Then Validate the logged in user
    Given User is on Home Page
    When User Clicks on Manage Customer
 @OAT8
  Scenario Outline: Capture customer PCT, GDPR and Credit limit
    When User enter customer number "<customerNumber>" and click on search
    Then validate customer deatils
    Then click on PCT Lookup and get PCT Number
    Then click on GDPR and get Preferences
    Then click on View Account details and get credit Limit

    Examples: 
      | customerNumber |
      |       15853537 |

  Scenario Outline: Search Customer and clear Search
    When User enter customer number "<customerNumber>" and click on search
    Then validate customer deatils
    When User click on clear button
    Then All field should be clear

    Examples: 
      | customerNumber |
      |       15853537 |

  Scenario Outline: Validate Order History
    When User enter customer number "<customerNumber>" and click on search
    Then validate customer deatils
    Then Check deatils of from order history
    When User click on order number
    Then Manage Order should open

    Examples: 
      | customerNumber |
      |       15853537 |
  @OAT9
  Scenario Outline: Manage Customer Page, Validation Address Line length for BillTo
    When User enter customer number "<customerNumber>" and click on search
    Then validate customer deatils
    Then Click on add BillTo button
    Then User enters the address details "<primary>"
      | country | addline1                                                                 | addline2                                   | city    | postal | county    | prov    | state    |
      | Germany | NewAddLine1123456789012345678901234567890123456789012345678901234567890A | NewAddLine2123456789012345678901234567890A | Newcity |  09899 | Newcounty | Newprov | newstate |
    Then Validate the max limit of the address line field

    Examples: 
      | customerNumber |
      |       15853537 |

  @Test @OAT9
  Scenario Outline: Manage Customer Page Validation Address Line length for ShipTo
    When User enter customer number "<customerNumber>" and click on search
    Then validate customer deatils
    Then Click on add ShipTo button
    Then User enters the address details "<primary>"
      | country | addline1                                                                 | addline2                                   | city    | postal | county    | prov    | state    |
      | Germany | NewAddLine1123456789012345678901234567890123456789012345678901234567890A | NewAddLine2123456789012345678901234567890A | Newcity |  09899 | Newcounty | Newprov | newstate |
    Then Validate the max limit of the address line field

    Examples: 
      | customerNumber |
      |       15853537 |

  @CreateBillToAddress @OAT9
  Scenario Outline: Manage Customer Page, Create BillTo Address with Email
    When User enter customer number "<customerNumber>" and click on search
    Then validate customer deatils
    Then Click on add BillTo button
    Then User enters the address details "<primary>"
      | country | addline1    | addline2    | city    | postal | county    | prov    | state    |
      | Germany | NewAddLine1 | NewAddLine2 | Newcity |  09899 | Newcounty | Newprov | newstate |
    Then User enters the contact information
      | email          | firstname | lastname | phone      | mobile | fax |
      | email@pens.com | firstname | lastname | 8989900333 |    123 | 123 |
    Then User click on create button to add new address with primary "<primary>"

    Examples: 
      | customerNumber | primary |
      |       15853537 | yes     |

  @CreateShipToAddress @OAT9
  Scenario Outline: Manage Customer Page, Create ShipTo Address with Email
    When User enter customer number "<customerNumber>" and click on search
    Then validate customer deatils
    Then Click on add ShipTo button
    Then User enters the address details "<primary>"
      | country | addline1    | addline2    | city    | postal | county    | prov    | state    |
      | Germany | NewAddLine1 | NewAddLine2 | Newcity |  09899 | Newcounty | Newprov | newstate |
    Then User enters the contact information
      | email          | firstname | lastname | phone      | mobile | fax |
      | email@pens.com | firstname | lastname | 8989900333 |    123 | 123 |
    Then User click on create button to add new address with primary "<primary>"

    Examples: 
      | customerNumber | primary |
      |       15853537 | No      |

  @CreateBillToAddressWithoutEmail @smoke  @OAT9
  Scenario Outline: Manage Customer Page, Create BillTo Adddress without Email
    When User enter customer number "<customerNumber>" and click on search
    Then validate customer deatils
    Then Click on add BillTo button
    Then User enters the address details "<primary>"
      | country | addline1    | addline2    | city    | postal | county    | prov    | state    |
      | Germany | NewAddLine1 | NewAddLine2 | Newcity |  09899 | Newcounty | Newprov | newstate |
    Then User enters the contact information
      | email          | firstname | lastname | phone      | mobile | fax |
      | email@pens.com | firstname | lastname | 8989900333 |    123 | 123 |
    And Select the reason for no email entered "<reason>"
    Then User click on create button to add new address with primary "<primary>"

    Examples: 
      | customerNumber | reason                                       | primary |
      |       15853537 | Customer does not have an email              |         |
      |       15853537 | Customer does not know their email           |         |
      |       15853537 | Sight Entry – no email on order form         |         |
      |       15853537 | Customer does not want to receive promotions |         |

  @CreateShipToAddressWithoutEmail @OAT9
  Scenario Outline: Manage Customer Page Create ShipTo Address without Email
    When User enter customer number "<customerNumber>" and click on search
    Then validate customer deatils
    Then Click on add ShipTo button
    Then User enters the address details "<primary>"
      | country | addline1    | addline2    | city    | postal | county    | prov    | state    |
      | Germany | NewAddLine1 | NewAddLine2 | Newcity |  09899 | Newcounty | Newprov | newstate |
    Then User enters the contact information
      | email          | firstname | lastname | phone      | mobile | fax |
      | email@pens.com | firstname | lastname | 8989900333 |    123 | 123 |
    And Select the reason for no email entered "<reason>"
    Then User click on create button to add new address with primary "<primary>"

    Examples: 
      | customerNumber | reason                                       | primary |
      |       15853537 | Customer does not have an email              |         |
      |       15853537 | Customer does not know their email           |         |
      |       15853537 | Sight Entry – no email on order form         |         |
      |       15853537 | Customer does not want to receive promotions |         |

  @SameBillToShipTo @smoke @OAT9
  Scenario Outline: Manage Customer Page Create Add BillTo Address Details Same For ShipTo
    When User enter customer number "<customerNumber>" and click on search
    Then validate customer deatils
    Then Click on add BillTo button
    Then User enters the address details "<primary>"
      | country | addline1    | addline2    | city    | postal | county    | prov    | state    |
      | Germany | NewAddLine1 | NewAddLine2 | Newcity |  09899 | Newcounty | Newprov | newstate |
    Then User enters the contact information
      | email          | firstname | lastname | phone      | mobile | fax |
      | email@pens.com | firstname | lastname | 8989900333 |    123 | 123 |
    And Click on Same for ShipTo
    Then User click on create button to add new address with primary "<primary>"

    Examples: 
      | customerNumber | primary |
      |       15853537 | YES     |

  @upadteBillToAddress @smoke @OAT9
  Scenario Outline: Manage Customer Page Update BillTo Address
    When User enter customer number "<customerNumber>" and click on search
    Then validate customer deatils
    Then click on update BillTo button
    Then User enters the address details "<primary>"
      | country | addline1    | addline2    | city    | postal | county    | prov    | state    |
      | Germany | NewAddLine1 | NewAddLine2 | Newcity |     99 | Newcounty | Newprov | newstate |
    Then User click on Apply changes button
    Then Verify BillTo address is updated

    Examples: 
      | customerNumber |
      |       15853537 |

  @UpdateShipToAddress @OAT9
  Scenario Outline: Manage Customer Page Update ShipTo Address
    When User enter customer number "<customerNumber>" and click on search
    Then validate customer deatils
    Then click on update ShipTo button
    Then User enters the address details "<primary>"
      | country | addline1    | addline2    | city    | postal | county    | prov    | state    |
      | Germany | NewAddLine1 | NewAddLine2 | Newcity |  09899 | Newcounty | Newprov | newstate |
    Then User click on Apply changes button
    Then Verify ShipTo address is updated

    Examples: 
      | customerNumber |
      |       15853537 |

  @AddBillToContact @smoke @OAT10
  Scenario Outline: Manage Customer Page, Add New BillTo Contact with Email
    When User enter customer number "<customerNumber>" and click on search
    Then User click on BillTo view contacts
    Then User click on Add New Contact button
    Then User enters the contact information
      | email          | firstname | lastname | phone      | mobile | fax |
      | email@pens.com | firstname | lastname | 8989900333 |    123 | 123 |
    Then Click on create button
    Then User click on BillTo view contacts
    Then verify contact is Added

    Examples: 
      | customerNumber |
      |       15853537 |

  @AddShipToContact @smoke @OAT10
  Scenario Outline: Manage Customer Page Add New ShipTo contact with Email
    When User enter customer number "<customerNumber>" and click on search
    Then validate customer deatils
    Then User click on ShipTo view contacts
    Then User click on Add New Contact button
    Then User enters the contact information
      | email          | firstname | lastname | phone      | mobile | fax |
      | email@pens.com | firstname | lastname | 8989900333 |    123 | 123 |
    Then Click on create button
    Then User click on ShipTo view contacts
    Then verify contact is Added

    Examples: 
      | customerNumber |
      |       15853537 |

  @AddBillToContactWithoutEmail @OAT10
  Scenario Outline: Manage Customer Page Add New BillTo contact without Email
    When User enter customer number "<customerNumber>" and click on search
    Then validate customer deatils
    Then User click on BillTo view contacts
    Then User click on Add New Contact button
    Then User enters the contact information
      | email          | firstname | lastname | phone      | mobile | fax |
      | email@pens.com | firstname | lastname | 8989900333 |    123 | 123 |
    Then User select the reason for no email "<reason>"
    Then Click on create button
    Then User click on BillTo view contacts
    Then verify contact is Added

    Examples: 
      | customerNumber | reason                                       |
      |       15853537 | Customer does not have an email              |
      |       15853537 | Customer does not know their email           |
      |       15853537 | Sight Entry – no email on order form         |
      |       15853537 | Customer does not want to receive promotions |

  @AddShipToContactWithoutEmail @OAT10
  Scenario Outline: Manage Customer Page Add New ShipTo contact without Email
    When User enter customer number "<customerNumber>" and click on search
    Then validate customer deatils
    Then User click on ShipTo view contacts
    Then User click on Add New Contact button
    Then User enters the contact information
      | email          | firstname | lastname | phone      | mobile | fax |
      | email@pens.com | firstname | lastname | 8989900333 |    123 | 123 |
    Then User select the reason for no email "<reason>"
    Then Click on create button
    Then User click on ShipTo view contacts
    Then verify contact is Added

    Examples: 
      | customerNumber | reason                                       |
      |       15853537 | Customer does not have an email              |
      |       15853537 | Customer does not know their email           |
      |       15853537 | Sight Entry – no email on order form         |
      |       15853537 | Customer does not want to receive promotions |

  @updateContactBillTo @OAT10
  Scenario Outline: Manage Customer Page, Update BillTo contact with Email
    When User enter customer number "<customerNumber>" and click on search
    Then validate customer deatils
    Then User click on BillTo view contacts
    Then click on Account number to update contact details
    Then User enters the contact information
      | email          | firstname | lastname | phone      | mobile | fax |
      | email@pens.com | firstname | lastname | 8989900333 |    123 | 123 |
    Then User click on Apply changes button
    Then User click on BillTo view contacts
    Then verify contact is Added

    Examples: 
      | customerNumber |
      |       15853537 |

  @updateShipToContact @OAT10
  Scenario Outline: Manage Customer Page Update ShipTo contact with Email
    When User enter customer number "<customerNumber>" and click on search
    Then validate customer deatils
    Then User click on ShipTo view contacts
    Then click on Account number to update contact details
    Then User enters the contact information
      | email          | firstname | lastname | phone      | mobile | fax |
      | email@pens.com | firstname | lastname | 8989900333 |    123 | 123 |
    Then User click on Apply changes button
    Then User click on ShipTo view contacts
    Then verify contact is Added

    Examples: 
      | customerNumber |
      |       15853537 |

  @updateContactBillToWithoutEmail @OAT10
  Scenario Outline: Manage Customer Page Update BillTo contact without Email
    When User enter customer number "<customerNumber>" and click on search
    Then validate customer deatils
    Then User click on BillTo view contacts
    Then click on Account number to update contact details
    Then User enters the contact information
      | email          | firstname | lastname | phone      | mobile | fax |
      | email@pens.com | firstname | lastname | 8989900333 |    123 | 123 |
    Then User select the reason for no email in update conatct "<reason>"
    Then User click on Apply changes button
    Then User click on BillTo view contacts
    Then verify contact is Added

    Examples: 
      | customerNumber | reason                                       |
      |       15853537 | Customer does not have an email              |
      |       15853537 | Customer does not know their email           |
      |       15853537 | Sight Entry – no email on order form         |
      |       15853537 | Customer does not want to receive promotions |

  @updateShipToContactWithoutEmail @OAT10
  Scenario Outline: Manage Customer Page Update ShipTo contact without Email
    When User enter customer number "<customerNumber>" and click on search
    Then validate customer deatils
    Then User click on ShipTo view contacts
    Then click on Account number to update contact details
    Then User enters the contact information
      | email          | firstname | lastname | phone      | mobile | fax |
      | email@pens.com | firstname | lastname | 8989900333 |    123 | 123 |
    Then User select the reason for no email in update conatct "<reason>"
    Then User click on Apply changes button
    Then User click on ShipTo view contacts
    Then verify contact is Added

    Examples: 
      | customerNumber | reason                                       |
      |       15853537 | Customer does not have an email              |
      |       15853537 | Customer does not know their email           |
      |       15853537 | Sight Entry – no email on order form         |
      |       15853537 | Customer does not want to receive promotions |

   @OAT281
  Scenario Outline: Place order from Manage Customer page
    When User enter customer number "<customerNumber>" and click on search
    Then validate customer deatils
    Then User click on place order and select reason cutomerpage "<reason>"
    Then User enters the keycode "<keycode>" for the inhouse product
    Then User click on Filter button
    Then Validate if the product is correct "<keycode>"
    Then Validate the value of decoration in searched result
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    And User Upload new logo "<newLogo>"
    Then User sends the internal notes
    Then User click on CheckOut button
    Then User click on product name and confirm the Imprint "<addImprint>"
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated

    Examples: 
      | customerNumber | reason    | keycode | addImprint | newLogo | presetQty | freeqty | saleRep           | orderType      | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod |
      |       15104767 | New Order | TPTLNS  | No         | yes     |       100 |      10 | DEVRIES, NICOLAAS | PT_WEB INBOUND | Yes           | No         | No         | No         | No          |               |
