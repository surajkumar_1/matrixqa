Feature: Product Search Feature

  Background: Login to user account
    Given User enter the username and password
    When User clicks on login button
    Then Validate the logged in user
    Given User is on Home Page
    Then User click on Place an order

  #@OAT11
  #Scenario Outline: Select each reasons and click on Proceed button
  #When User Enter customer number "<customerNumber>" and click on serach
  #Then Details should be display
  #Then User click on Place Order Popup should open
  #Then select reason to place order display and select reason
  #
  #Examples:
  #| customerNumber |
  #|       20288797 |
  @OAT12
  Scenario Outline: Enter Inhouse Keycode and search using Filter Operation
    When User Enter customer number "<customerNumber>" and click on serach
    Then Details should be display
    When User click on place order and select reason "<reason>"
    Then User enters the keycode "<keycode>" for the inhouse product
    Then User click on Filter button
    Then Validate if the product is correct "<keycode>"
    Then Validate the value of decoration in searched result

    Examples: 
      | customerNumber | reason    | keycode |
      |       20288797 | New Order | TDECTR  |

  @OAT12
  Scenario Outline: Enter InCorrect Inhouse Keycode and search using Filter Operation
    When User Enter customer number "<customerNumber>" and click on serach
    Then Details should be display
    When User click on place order and select reason "<reason>"
    Then User enters the keycode "<keycode>" for the inhouse product
    Then User click on Filter button
    Then Validate product is not displayed in search result

    Examples: 
      | customerNumber | reason    | keycode   |
      |       20288797 | New Order | TDECUserE |

  @OAT12
  Scenario Outline: Enter Vendor item Keycode and search using Filter Operation
    When User Enter customer number "<customerNumber>" and click on serach
    Then Details should be display
    When User click on place order and select reason "<reason>"
    Then User enters the vendor item keycode "<keycode>"
    Then User click on Filter button
    Then Validate vendor product is correct "<keycode>"

    Examples: 
      | customerNumber | reason    | keycode      |
      |       20288797 | New Order | VEN-TPC-3407 |

  @OAT12
  Scenario Outline: Enter Incorrect Vendor Product and search using Filter Operation
    When User Enter customer number "<customerNumber>" and click on serach
    Then Details should be display
    When User click on place order and select reason "<reason>"
    Then User enters the vendor item keycode "<keycode>"
    Then User click on Filter button
    Then Validate product is not displayed in search result

    Examples: 
      | customerNumber | reason    | keycode |
      |       20288797 | New Order | V       |

  @OAT12
  Scenario Outline: Enter Vendor item Name and search using Filter Operation
    When User Enter customer number "<customerNumber>" and click on serach
    Then Details should be display
    When User click on place order and select reason "<reason>"
    Then User enters the vendor item name "<name>"
    Then User click on Filter button
    Then User select the product code if the vendor item name is correct from description
    Then Validate vendor product is correct "<keycode>"

    Examples: 
      | customerNumber | reason    | keycode      | name          |
      |       20288797 | New Order | VEN-TPC-3407 | Mini Pringles |

  @OAT12
  Scenario Outline: Enter incorrect Vendor item Name and search using Filter Operation
    When User Enter customer number "<customerNumber>" and click on serach
    Then Details should be display
    When User click on place order and select reason "<reason>"
    Then User enters the vendor item name "<name>"
    Then User click on Filter button
    Then Validate the product code is not displayed for incorrect vendor item name

    Examples: 
      | customerNumber | reason    | name         |
      |       20288797 | New Order | sjdnkadjnkdj |
