Feature: Place order E2E


"***addImprint set to 'NO' to clear imprint Don't left Blank!!"
 
 
  Background: Login to user account
    Given User enter the username and password
    When User clicks on login button
    Then Validate the logged in user
    Given User is on Home Page
    Then User click on Place an order

  @OnlyLogo
  Scenario Outline: Place Only Logo Order
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    And User Upload new logo "<newLogo>"
    Then User sends the internal notes
    Then User click on CheckOut button
    Then User click on product name and confirm the Imprint "<addImprint>"
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated
    Examples: 
      | customerNumber | reason    | keycode | addImprint | newLogo | presetQty | saleRep           | orderType      | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod |
      |       20288518 | New Order | TDELNS  |   No       | yes     |       100 | DEVRIES, NICOLAAS | DE_WEB INBOUND | yes           | no         | no         | no         | No          |               |
 
  @customeQty @smoke
  Scenario Outline: Placing Matrix Order, Custom Qty
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    And User Upload new logo "<newLogo>"
    Then User selects the customQty and enter the qty "<customQty>" and click on the setqty
    Then User select the upsell "<upsell>"
    Then User add accessories "<accessories>" and enter qty of the accessories "<accessoriesQty>"
    Then User selects the discount percentage "<percentage>"
    Then User sends the internal notes
    Then User click on CheckOut button
    Then User click on product name and confirm the Imprint "<addImprint>"
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated

    Examples: 
      | customerNumber | reason    | keycode | addImprint | newLogo | upsell | customQty | percentage | accessories        | accessoriesQty | saleRep           | orderType      | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod | 
      |       20288518 | New Order | TDELNS  | Yes        | yes     |        |      1001 |         10 | ACC-XXX-GBXXX-XX-X |            100 | DEVRIES, NICOLAAS | DE_WEB INBOUND | yes           | no         | no         | no         | No          |               |

  @FreeQty
  Scenario Outline: Place the Free Qty
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    And User Upload new logo "<newLogo>"
    Then User selects the Free QTY "<freeqty>"
    Then User sends the internal notes
    Then User click on CheckOut button
    Then User click on product name and confirm the Imprint "<addImprint>"
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated

    Examples: 
      | customerNumber | reason    | keycode | addImprint | newLogo | presetQty | freeqty | saleRep           | orderType      | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod |
      |       20288518 | New Order | TDELNS  | Yes        | yes     |       100 |      10 | DEVRIES, NICOLAAS | DE_WEB INBOUND | No            | No         | No         | No         | No          |               |

  ###Either DiscounPercent or Free quantity can be selected
  @DiscounPercent
  Scenario Outline: Placing Matrix Order, User selects the Discount Percentage
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    And User Upload new logo "<newLogo>"
    Then User add accessories "<accessories>" and enter qty of the accessories "<accessoriesQty>"
    Then User selects the discount percentage "<percentage>"
    Then User sends the internal notes
    Then User click on CheckOut button
    Then User click on product name and confirm the Imprint "<addImprint>"
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated

    Examples: 
      | customerNumber | reason    | keycode | addImprint | newLogo | presetQty | percentage | accessories        | accessoriesQty | saleRep           | orderType      | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod |
      |       20288518 | New Order | TDELNS  | Yes        | yes     |       100 |         10 | ACC-XXX-GBXXX-XX-X |            100 | DEVRIES, NICOLAAS | DE_WEB INBOUND | Yes           | No         | No         | No         | No          |               |

  @PriceAndCSRHold @OAT150 @OAT151
  Scenario Outline: Apply price query and CSR Hold Functionality
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    ## Then User select template "<template>"
    And Enter Imprint Lines "<addImprint>"
    And User Upload new logo "<newLogo>"
    Then User select the presetQty "<presetQty>"
    Then User select the upsell "<upsell>"
    ##Then User add accessories "<accessories>" and enter qty of the accessories "<accessoriesQty>"
    ##Then User selects the discount percentage "<percentage>"
    ##Then User selects the Free QTY "<freeQty>"
    Then User sends the internal notes
    Then User apply holds "<hold>"
    Then User click on CheckOut button
    Then User click on product name and confirm the Imprint "<addImprint>"
    Then verify the current hold is applied "<hold>"
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated
    Then Hold should be visible in order generation page "<hold>"

    Examples: 
      | customerNumber | reason    | keycode | addImprint | newLogo | presetQty | freeQty | accessories        | accessoriesQty | saleRep           | orderType      | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod | hold      |
      |       20288518 | New Order | TDELNS  | Yes        | yes     |       100 |      10 | ACC-XXX-GBXXX-XX-X |            100 | DEVRIES, NICOLAAS | DE_WEB INBOUND | YES           | no         | no         | no         | No          |               | PRICE,CSR |

  #@CSRHold @OAT150
  #Scenario Outline: Apply CSR Hold Functionality
  #When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
  #Then User clicks on the name of the displayed search result
  #Then User select product options
  ## Then User select template "<template>"
  #And Enter Imprint Lines "<addImprint>"
  #And User Upload new logo "<newLogo>"
  #Then User select the presetQty "<presetQty>"
  #Then User select the upsell "<upsell>"
  #Then User add accessories "<accessories>" and enter qty of the accessories "<accessoriesQty>"
  ##Then User selects the discount percentage "<percentage>"
  ##Then User selects the Free QTY "<freeQty>"
  #Then User sends the internal notes
  #Then User apply holds "<hold>"
  #Then User click on CheckOut button
  #Then User click on product name and confirm the Imprint "<addImprint>"
  #Then verify the current hold is applied "<hold>"
  #When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
  #Then Order should placed and order number generated
  # Then Hold should be visible in order generation page "<hold>"
  #
  #Examples:
  #| customerNumber | reason    | keycode | addImprint | newLogo | presetQty | freeQty | accessories | accessoriesQty | saleRep           | orderType      | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod | hold |
  #|       18145728 | New Order | TDELNS  | Yes        | yes     |       100 |      10 | No          |            100 | DEVRIES, NICOLAAS | DE_WEB INBOUND | YES           | no         | no         | no         | YES         |               | CSR  |
  @VendorSampleOrder @smoke @OAT101
  Scenario Outline: Non-Marketed vendor Sample order
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>" for vendor product
    Then User clicks on the place sample order
    Then User enters vendor product details
      | qty | price | quoteId | prodCode | printProcess | productName | productColor | logoColor | imprintColor | imprintPos | shipping |
      |   5 |     9 |       2 | VEN2     | Laser        | productName | color        | logo      | imprint      | center     |    10.99 |
    Then User sends the internal notes
    Then User click on CheckOut button
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated

    Examples: 
      | customerNumber | reason    | keycode      | saleRep           | orderType      | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod |
      |       20288518 | New Order | VEN-TPC-3407 | DEVRIES, NICOLAAS | DE_WEB INBOUND | No            | no         | no         | no         | No          |               |

  @VendorProduct @smoke
  Scenario Outline: Placing Order, Vendor item Quote order
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>" for vendor product
    Then User clicks on the name of the displayed search result for Vendor Item
    ##Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    And User Upload new logo "<newLogo>"
    ## Then User selects the Free QTY "<freeQty>"
    Then User sends the internal notes
    Then User select the upsell "<upsell>"
    Then User apply holds "<hold>"
    Then User click on CheckOut button
    Then User click on product name and confirm the Imprint "<addImprint>"
    Then verify the current hold is applied "<hold>"
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated
    Then Hold should be visible in order generation page "<hold>"

    ## Then On Manage order page verify lines are created
    Examples: 
      | customerNumber | reason    | keycode      | addImprint | newLogo | presetQty | freeQty | upsell | saleRep           | orderType      | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod | hold      |
      |       19888752 | New Order | VEN-EFU-6891 | Yes        | no      |       250 |      10 | yes    | DEVRIES, NICOLAAS | PT_WEB INBOUND | no            | no         | no         | no         | no          |               | Price,CSR |

  @Anniversary @smoke @OAT61
  Scenario Outline: Anniversary order
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    Then User select template "<template>"
    Then User select the presetQty "<presetQty>"   
    Then User enters anniversary "<AnniNumber>"
    And Enter Imprint Lines "<addImprint>"
    Then User sends the internal notes
    Then User select the presetQty "<presetQty2>"
    Then User click on CheckOut button
    Then User click on product name and confirm the Imprint "<addImprint>"
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated

    Examples: 
      | customerNumber | reason    | keycode | template    | AnniNumber | addImprint | newLogo | presetQty | freeQty | accessories | accessoriesQty | saleRep           | orderType      | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod | presetQty2 |
      |       18314823 | New Order | TITLN7  | Anniversary |          5 | yes        | Yes     |       100 |      10 | No          |            100 | DEVRIES, NICOLAAS | IT_WEB INBOUND | YES           | no         | no         | no         | no          |               |         50 |

  @PCTOrder @smoke
  Scenario Outline: PCT order functionality
    When User navigates to product search Page by PCT number "<PCTnumber>" and reason "<reason>"
    ###Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    #And User Upload new logo "<newLogo>"
    ### Then User select the upsell "<upsell>"
    ###Then User add accessories "<accessories>" and enter qty of the accessories "<accessoriesQty>"
    ###Then User selects the discount percentage "<percentage>"
    ###Then User selects the Free QTY "<freeQty>"
    Then User sends the internal notes
    Then User click on CheckOut button
    Then User click on product name and confirm the Imprint "<addImprint>"
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated

    Examples: 
      | PCTnumber       | reason    | addImprint | newLogo | presetQty | freeQty | accessories | accessoriesQty | saleRep           | orderType      | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod |
      | P8322301724CC02 | New Order | Yes        | Yes     |       500 |      10 | No          |            100 | DEVRIES, NICOLAAS | IT_WEB INBOUND | YES           | no         | no         | no         | no          |               |

  @AccessoryOnly @smoke @OAT13
  Scenario Outline: Order Accessories only product
    When User Enter customer number "<customerNumber>" and click on serach
    Then Details should be display
    When User click on place order and select reason "<reason>"
    Then User click on add accessories product
    ##Then User apply holds "<hold>"
    Then User search accessory item "<item>" and select item and enter quantity "<qty>"
    Then User click on CheckOut button
    ##Then verify the current hold is applied "<hold>"
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated

    Examples: 
      | customerNumber | reason    | item               | qty | saleRep           | orderType      | paymentMethod | legitInterest | callOption | mailOption | rentOption | EmailOption | hold      |
      |       20288518 | New Order | ACC-XXX-CPBCX-XX-X | 100 | DEVRIES, NICOLAAS | DE_WEB INBOUND |               | YES           | no         | no         | no         | no          | CSR,PRICE |

   @Template
  Scenario Outline: Design Template order
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    Then User select template "<template>"
    Then User enters anniversary "<AnniNumber>"
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    Then User select the upsell "<upsell>"
    #Then User selects the discount percentage "<percentage>"
    #Then User add accessories "<accessories>" and enter qty of the accessories "<accessoriesQty>"
    Then User sends the internal notes
    Then User click on CheckOut button
    Then User click on product name and confirm the Imprint "<addImprint>"
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated

    Examples: 
      | customerNumber | reason    | keycode | template    | addImprint | newLogo | presetQty | percentage | freeQty | accessories | accessoriesQty | saleRep           | orderType      | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod |AnniNumber|
      |       20288518 | New Order | TDELN7  | Anniversary | yes        | yes     |       100 |         10 |      10 | No          |            100 | DEVRIES, NICOLAAS | DE_WEB INBOUND | YES           | no         | no         | no         | no          |               |5|

  #@Pricehold1 @OAT151
  #Scenario Outline: Apply price query Hold Functionality
  #When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
  #Then User clicks on the name of the displayed search result
  #Then User select product options
  ## Then User select template "<template>"
  #And Enter Imprint Lines "<addImprint>"
  #Then User select the presetQty "<presetQty>"
  ##  And User Upload new logo "<newLogo>"
  #Then User select the upsell "<upsell>"
  #Then User add accessories "<accessories>" and enter qty of the accessories "<accessoriesQty>"
  ##Then User selects the discount percentage "<percentage>"
  ##Then User selects the Free QTY "<freeQty>"
  #Then User sends the internal notes
  #Then User apply holds "<hold>"
  #Then User click on CheckOut button
  #Then User click on product name and confirm the Imprint "<addImprint>"
  #Then verify the current hold is applied "<hold>"
  #When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
  #Then Order should placed and order number generated
  #Then Hold should be visible in manage order page "<hold>"
  #
  #Examples:
  #| customerNumber | reason    | keycode | addImprint | newLogo | presetQty | freeQty | accessories | accessoriesQty | saleRep           | orderType      | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod | hold  |
  #|       18145728 | New Order | TDELPG  | Yes        | yes     |       100 |      10 | No          |            100 | DEVRIES, NICOLAAS | DE_WEB INBOUND | YES           | no         | no         | no         | YES         |               | Price |
 
 
  @QuoteOrder @OAT104
  Scenario Outline: Placing Quote order
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    And User Upload new logo "<newLogo>"
    Then User select the upsell "<upsell>"
    ##Then User add accessories "<accessories>" and enter qty of the accessories "<accessoriesQty>"
    Then User selects the Free QTY "<freeQty>"
    Then User sends the internal notes
    Then User click on CheckOut button
    Then User click on product name and confirm the Imprint "<addImprint>"
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated

    ########Then On Manage order page verify lines are created
    Examples: 
      | customerNumber | reason    | keycode | addImprint | newLogo | presetQty | freeQty | accessories | accessoriesQty | saleRep           | orderType      | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod |
      |       20288518 | New Order | TDELNS  | Yes        | yes     |       500 |      10 | No          |            100 | DEVRIES, NICOLAAS | DE_WEB INBOUND | Yes           | No         | No         | No         | No          |               |

   @OAT256
  Scenario Outline: Validate the GDPR legitimate value NO
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    And User Upload new logo "<newLogo>"
    Then User sends the internal notes
    Then User click on CheckOut button
    Then User click on product name and confirm the Imprint "<addImprint>"
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated

    Examples: 
      | customerNumber | reason    | keycode | addImprint | newLogo | presetQty | saleRep           | orderType      | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod |
      |       20288518 | New Order | TDELNS  | NO         | yes     |       100 | DEVRIES, NICOLAAS | DE_WEB INBOUND | No            | no         | no         | no         | No          |               |

   @OAT257
  Scenario Outline: Validate the GDPR value - N/A
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    And User Upload new logo "<newLogo>"
    Then User sends the internal notes
    Then User click on CheckOut button
    Then User click on product name and confirm the Imprint "<addImprint>"
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder

    #Then Order should placed and order number generated
    Examples: 
      | customerNumber | reason    | keycode | addImprint | newLogo | presetQty | saleRep           | orderType      | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod |
      |       20288518 | New Order | TDELNS  | NO         | yes     |       100 | DEVRIES, NICOLAAS | DE_WEB INBOUND | N/A           | no         | no         | no         | No          |               |

  @OAT231
  Scenario Outline: Place Order-update logo in order confirmation
    When User navigates to product search Page by entering customer "<customerNumber>" and reason "<reason>" and keycode "<keycode>"
    Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then User select the presetQty "<presetQty>"
    And User Upload new logo "<newLogo>"
    Then User sends the internal notes
    Then User click on CheckOut button
    Then User click on product name and add logo "<addLogo>" and confirm the Imprint "<addImprint>"
    Then User click on product name and check logo
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated

    Examples: 
      | customerNumber | reason    | keycode | addImprint | newLogo | presetQty | freeqty | saleRep           | orderType   | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod | addLogo |
      |       20288518 | New Order | TDELNS  | Yes        | yes     |       100 |      10 | DEVRIES, NICOLAAS | MAIL-NPC-EO | Yes           | No         | No         | No         | No          |               | yes     |

      
   @OAT266
  Scenario Outline: PCT order functionality - logo scrap Send to art department
    When User navigates to product search Page by PCT number "<PCTnumber>" and reason "<reason>"
    ###Then User clicks on the name of the displayed search result
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then Select logo option "<logoOption>"
    Then User select the presetQty "<presetQty>"   
    Then User select the upsell "<upsell>"
   	Then User add accessories "<accessories>" and enter qty of the accessories "<accessoriesQty>"
    #Then User selects the discount percentage "<percentage>"
   	Then User selects the Free QTY "<freeQty>"
    Then User sends the internal notes
    Then User click on CheckOut button
    Then User click on product name and confirm the Imprint "<addImprint>"
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated

    Examples: 
      | PCTnumber       | reason    | addImprint | 		logoOption 		 | presetQty | freeQty | accessories | accessoriesQty | saleRep           | orderType      | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod |
      | P8337658924DA02 | New Order | No         | Send to Art Dept  |       500 |      10 | No          |            100 | DEVRIES, NICOLAAS | IT_WEB INBOUND | YES           | no         | no         | no         | no          |               |      
	
	
	  @OAT267
    Scenario Outline: PCT order functionality - logo scrap By pass art department
    When User navigates to product search Page by PCT number "<PCTnumber>" and reason "<reason>"
    Then User select product options
    And Enter Imprint Lines "<addImprint>"
    Then Select logo option "<logoOption>"
    Then User select the presetQty "<presetQty>"   
    Then User select the upsell "<upsell>"
   	Then User add accessories "<accessories>" and enter qty of the accessories "<accessoriesQty>"
    #Then User selects the discount percentage "<percentage>"
   	Then User selects the Free QTY "<freeQty>"
    Then User sends the internal notes
    Then User click on CheckOut button
    Then User click on product name and confirm the Imprint "<addImprint>"
    When User enter saleRep "<saleRep>" and select orderType "<orderType>" and select paymentMethod "<paymentMethod>" and select legitInterest "<legitInterest>" and select callOption "<callOption>" and select mailOption "<mailOption>" and select rentOption "<rentOption>" and select EmailOption "<EmailOption>" and click PlaceOrder
    Then Order should placed and order number generated

    Examples: 
      | PCTnumber       | reason    | addImprint | logoOption | presetQty | freeQty | accessories | accessoriesQty | saleRep           | orderType      | legitInterest | callOption | mailOption | rentOption | EmailOption | paymentMethod |
      | P8337658924DA02 | New Order | No         | Save Logo  |       500 |      10 | No          |            100 | DEVRIES, NICOLAAS | IT_WEB INBOUND | YES           | no         | no         | no         | no          |               |    
	  