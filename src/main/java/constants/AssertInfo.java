package constants;

public class AssertInfo {

	private AssertInfo() {
		throw new IllegalStateException("Can't instantiate AssertInfo");
	}

	public static final String DISCOUNT_LINE = "Discount is Already Exist in the Order";

	public static final String SHIPPING_LINE = "Shipping Charge is Already Exist in the Order";

	public static final String FREE_LINE = "Free is Already Exist in the Order";
	
	public static final String UPSELL_LINE = "Upsell line is Already Exists for this line";
	
	public static final String LOGO_CHARGE_LINE = "Logo Charges are Already Exists for this line";
	
	public static final String PREMIUM_ITEM_UPDATE_LINE = "Can not Update Premium Item..";
}
