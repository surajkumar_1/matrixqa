package utilities;

import java.util.HashMap;
import java.util.Map;

public class GetDataExcel {
	public Excel e;
	public int sheet1row;
	public int sheet2row;

	public GetDataExcel(String excelPath) {
		e = new Excel(System.getProperty("user.dir") + excelPath);
	}

	static Map<String, String> map = new HashMap<>();

	public void setCellData(String sheet, String colName, int rowNum, String data) {

		e.setCellData(sheet, colName, rowNum, data);

	}

	public void copyExcelData(String copyFrom, String copyTo, String uniqueDataCol, String orderNum) {

		for (int row = 2; row <= e.getRowCount(copyFrom); row++) {
			if (e.getCellData(copyFrom, uniqueDataCol, row).equals(orderNum)) {
				sheet1row = row;
				sheet2row = e.getRowCount(copyTo) + 1;
				map.put("Customer", e.getCellData(copyFrom, "Customer", row));
				map.put("Customer Number", e.getCellData(copyFrom, "Customer Number", row));
				map.put("CustomerCountry", e.getCellData(copyFrom, "CustomerCountry", row));
				map.put("Total", e.getCellData(copyFrom, "Total", row));
				map.put("Subtotal", e.getCellData(copyFrom, "Subtotal", row));
				map.put("Tax", e.getCellData(copyFrom, "Tax", row));
				map.put("Request Date", e.getCellData(copyFrom, "Request Date", row));
				map.put("Salesperson", e.getCellData(copyFrom, "Salesperson", row));
				map.put("Order Type", e.getCellData(copyFrom, "Order Type", row));
				map.put("Ordered Item", e.getCellData(copyFrom, "Ordered Item", row));
				map.put("Unit Selling Price", e.getCellData(copyFrom, "Unit Selling Price", row));
				map.put("PackagePrice", e.getCellData(copyFrom, "PackagePrice", row));
				map.put("LineType", e.getCellData(copyFrom, "LineType", row));
				map.put("DiscountPercentage", e.getCellData(copyFrom, "DiscountPercentage", row));
				map.put("Qty", e.getCellData(copyFrom, "Qty", row));
				map.put("Shipping", e.getCellData(copyFrom, "Shipping", row));
				map.put("Notes", "Adding internal notes Automation");
				map.put("Environment", e.getCellData(copyFrom, "Environment", row));
				map.put("MatrixOrder", e.getCellData(copyFrom, "MatrixOrder", row));
				map.put("ABCOrderNumber", e.getCellData(copyFrom, "ABCOrderNumber", row));
				for (@SuppressWarnings("rawtypes")
				Map.Entry m : map.entrySet()) {
					e.setCellData(copyTo, (String) m.getKey(), sheet2row, (String) m.getValue());
				}
				break;
			}

		}
		
	}
	public void copyExcelDataUpdateLine(String copyFrom, String copyTo, String uniqueDataCol, String orderNum) {

		for (int row = 2; row <= e.getRowCount(copyFrom); row++) {
			if (e.getCellData(copyFrom, uniqueDataCol, row).equals(orderNum)) {
				sheet1row = row;
				sheet2row = e.getRowCount(copyTo) + 1;
				map.put("Customer", e.getCellData(copyFrom, "Customer", row));
				map.put("Customer Number", e.getCellData(copyFrom, "Customer Number", row));
				map.put("CustomerCountry", e.getCellData(copyFrom, "CustomerCountry", row));
//				map.put("Total", e.getCellData(copyFrom, "Total", row));
//				map.put("CartSubtotal", e.getCellData(copyFrom, "CartSubtotal", row));
//				map.put("Tax", e.getCellData(copyFrom, "Tax", row));
				map.put("Request Date", e.getCellData(copyFrom, "Request Date", row));
				map.put("Salesperson", e.getCellData(copyFrom, "Salesperson", row));
				map.put("Order Type", e.getCellData(copyFrom, "Order Type", row));
				map.put("Ordered Item", e.getCellData(copyFrom, "Ordered Item", row));
				//map.put("Unit Selling Price", e.getCellData(copyFrom, "Unit Selling Price", row));
				map.put("PackagePrice", e.getCellData(copyFrom, "PackagePrice", row));
				map.put("LineType", e.getCellData(copyFrom, "LineType", row));
				map.put("DiscountPercentage", e.getCellData(copyFrom, "DiscountPercentage", row));
				//map.put("Qty", e.getCellData(copyFrom, "Qty", row));
				map.put("Shipping", e.getCellData(copyFrom, "Shipping", row));
				map.put("Notes", "Adding internal notes Automation");
				map.put("Environment", e.getCellData(copyFrom, "Environment", row));
				map.put("MatrixOrder", e.getCellData(copyFrom, "MatrixOrder", row));
				map.put("ABCOrderNumber", e.getCellData(copyFrom, "ABCOrderNumber", row));
				for (@SuppressWarnings("rawtypes")
				Map.Entry m : map.entrySet()) {
					e.setCellData(copyTo, (String) m.getKey(), sheet2row-1, (String) m.getValue());
				}
				break;
			}

		}
	}
//	public void removeRow(String sheet, int row) {
//		BaseClass.excel.e.sheet = BaseClass.excel.e.workbook.getSheet(sheet);
//		BaseClass.excel.e.sheet.removeRow(BaseClass.excel.e.sheet.getRow(row));
//	}
}