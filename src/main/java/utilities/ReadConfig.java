package utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * 
 * Read config File Data.
 */
public class ReadConfig {

	Properties pro;
	FileInputStream fis;

	public ReadConfig() throws IOException {
		File src = new File(System.getProperty("user.dir") + "/src/main/resources/config/config.properties");

		try {
			fis = new FileInputStream(src);
		} catch (Exception e) {
			System.out.println("Exception is " + e.getMessage());
		} finally {
			pro = new Properties();
			pro.load(fis);
			fis.close();
		}
	}

	public String getEnv() {
		return pro.getProperty("env");
	}

	public String getChromePath() {
		return pro.getProperty("chromepath");
	}

	/**
	 * Returns the Path of IE driver form config file.
	 * 
	 * @return iepath {@link String}
	 */
	public String getBrowser() {
		return pro.getProperty("browser");
	}

	/**
	 * Returns the Path of firefox driver form config file.
	 * 
	 * @return firefoxpath {@link String}
	 */
	public String getFirefoxPath() {
		return pro.getProperty("firefoxpath");

	}

	/**
	 * Returns userName.
	 * 
	 * @return userName {@link String}
	 */
	public String getUserName() {
		return pro.getProperty("UserName");

	}

	/**
	 * Returns password.
	 * 
	 * @return password {@link String}
	 */
	public String getPassword() {
		return pro.getProperty("password");
	}

	
	public String getExcelPath() {
		return pro.getProperty("excelPath");
	}
	public static void main(String[] args) throws IOException {

		ReadConfig rd = new ReadConfig();
		System.out.println(rd.getExcelPath());

	}

}
