package helper;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;

public class WebAction {
	
	public String getDate() {
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
		return format.format(date).toUpperCase();
	}
	
	public boolean retryingFindClick(WebElement element) {
	    boolean result = false;
	    int attempts = 0;
	    while(attempts < 2) {
	        try {
	            element.click();
	            result = true;
	            break;
	        } catch(StaleElementReferenceException e) {
	        }
	        attempts++;
	    }
	    return result;
	}

}
