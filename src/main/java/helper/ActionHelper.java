package helper;

import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

/**
 * Action Helper for interaction with GUI.
 *
 */
public class ActionHelper {

	 Actions action;

	/**
	 * Parameterize constructor
	 * 
	 * @param driver {@link WebDriver}
	 */
	public ActionHelper(WebDriver driver) {

		action = new Actions(driver);
	}

	/**
	 * Drag And Drop The Element
	 * 
	 * @param source {@link WebElement}
	 * @param target {@link WebElement}
	 */
	public void dragAndDrop(WebElement source, WebElement target) {
		action.dragAndDrop(source, target);
		

	}

	/**
	 * Mouse Hover to the element
	 * 
	 * @param pass element {@link WebElement} to move to element.
	 */
	public void moveToElement(WebElement element) {
		action.moveToElement(element).build().perform();
		
	}
	public void tab() {
		action.sendKeys(Keys.TAB).perform();
		
	}
	public void clickOn(WebElement target) {
		action.moveToElement(target).click().build().perform();
	
	}
	
	public void doubleClickOnElement(WebElement target) {
		action.doubleClick(target).build().perform();
	}
	
	public void moveMouse() {
		action.moveByOffset(310, 1125).build().perform();
	}
	
	public void contextClick(WebElement target) {
		action.contextClick(target).build().perform();
	}
	
	public void enterNumber(String text) {
		for(char c : text.toCharArray()) {
			switch (c) {
			case '0':
				action.sendKeys(Keys.NUMPAD0).perform();
				break;
			case '1':
				action.sendKeys(Keys.NUMPAD1).perform();
				break;
			case '2':
				action.sendKeys(Keys.NUMPAD2).perform();
				break;
			case '3':
				action.sendKeys(Keys.NUMPAD3).perform();
				break;
			case '4':
				action.sendKeys(Keys.NUMPAD4).perform();
				break;
			case '5':
				action.sendKeys(Keys.NUMPAD5).perform();
				break;
			case '6':
				action.sendKeys(Keys.NUMPAD6).perform();
				break;
			case '7':
				action.sendKeys(Keys.NUMPAD7).perform();
				break;
			case '8':
				action.sendKeys(Keys.NUMPAD8).perform();
				break;
			case '9':
				action.sendKeys(Keys.NUMPAD9).perform();
				break;
			case '.':
				action.sendKeys(Keys.DECIMAL).perform();
				break;			
        default:
	     Assert.fail("Something Wrong with Input");
			}
		}
     	action.sendKeys(Keys.ENTER);			
	}
}
