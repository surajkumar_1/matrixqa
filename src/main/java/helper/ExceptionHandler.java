package helper;

import java.util.List;

import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import base.BaseClass;

public class ExceptionHandler {

	public void attributeToBe(WebElement element, String attribute, String value) {
		int count = 1;
		while (count < 2) {
			try {
				BaseClass.wait.until(ExpectedConditions.attributeToBe(element, attribute, value));
				break;
			} catch (StaleElementReferenceException e) {
			}
			count++;
		}

	}

	public void visibilityOf(WebElement element) {
		int count = 1;
		while (count < 2) {
			try {
				BaseClass.wait.until(ExpectedConditions.visibilityOf(element));
				break;
			} catch (StaleElementReferenceException e) {
			}
			count++;
		}

	}

	public void attributeToBeNotEmpty(WebElement element, String value) {
		int count = 1;
		while (count < 2) {
			try {
				BaseClass.wait.until(ExpectedConditions.attributeToBeNotEmpty(element, value));
				break;
			} catch (StaleElementReferenceException e) {
			}
			count++;
		}

	}
	public void visibilityOfAll(List<WebElement> elements) {
		int count = 1;
		while (count < 2) {
			try {
				BaseClass.wait.until(ExpectedConditions.visibilityOfAllElements(elements));
				break;
			} catch (StaleElementReferenceException e) {
			}
			count++;
		}

	}
}
