package pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RedoOrder {

	public RedoOrder(WebDriver driver) {
	PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "//span[contains(@id,'_COPY_ORDER_NUMBER_DISPLAY')]")
	public WebElement redoFrom;
	
	@FindBy(xpath = "//select[@id='P176_COPY_SALESREP_ID']")
	public WebElement selectSalesRep;
	
	@FindBy(xpath = "//input[contains(@id,'_SALESREP_EMAIL')]")
	public WebElement salesRepmail;
	
	@FindBy(xpath ="(//table[contains(@aria-label,'Region = Line Details, Report = Primary Default, View = Report, Displayed Rows Start')]//tr[position()=2]//img)[2]")
    public WebElement updateQtyPriceBtn;
	
	@FindBy(xpath ="(//table[contains(@aria-label,'Region = Line Details, Report = Primary Default, View = Report, Displayed Rows Start')]//td[contains(text(),'ACC')]//following-sibling::td//img)")
    public WebElement updateQtyPriceBtnAcc;
	
	@FindBy(xpath ="(//table[contains(@aria-label,'Region = Line Details, Report = Primary Default, View = Report, Displayed Rows Start')]//tr[position()=3]//img)[1]")
    public WebElement updateQtyPriceBtnUpsell;
	
	@FindBy(xpath ="(//table[contains(@aria-label,'Region = Line Details, Report = Primary Default, View = Report, Displayed Rows Start')]//tr[position()=3]//img)[1]")
    public WebElement updateQtyBtnFree;
	
	@FindBy(xpath ="(//table[contains(@aria-label,'Region = Line Details, Report = Primary Default, View = Report, Displayed Rows Start')]//td[contains(text(),'DISCOUNT')]//following-sibling::td//img)[1]")
    public WebElement updateBtnDisc;
	
	@FindBy(xpath ="(//table[contains(@aria-label,'Region = Line Details, Report = Primary Default, View = Report, Displayed Rows Start')]//td[contains(text(),'Logo')]//following-sibling::td//img)[1]")
    public WebElement updateBtnLogo;
	
	@FindBy(xpath ="//input[contains(@id,'_ORDERED_QTY_M')]")
    public WebElement updateQty;
	
	@FindBy(xpath ="(//table[contains(@aria-label,'Region = Line Details, Report = Primary Default, View = Report, Displayed Rows Start')]//tr[position()=2]//td)[4]")
    public WebElement mainQty;
	
	@FindBy(xpath ="(//table[contains(@aria-label,'Region = Line Details, Report = Primary Default, View = Report, Displayed Rows Start')]//td[contains(text(),'ACC')]//following-sibling::td)[2]")
    public WebElement accQty;
	
	@FindBy(xpath ="(//table[contains(@aria-label,'Region = Line Details, Report = Primary Default, View = Report, Displayed Rows Start')]//tr[position()=3]//td)[4]")
    public WebElement UpsellQty;
	

	@FindBy(xpath ="(//table[contains(@aria-label,'Region = Line Details, Report = Primary Default, View = Report, Displayed Rows Start')]//tr//td[text()='0']//preceding-sibling::td)[4]")
    public WebElement freeQty;
			
	@FindBy(xpath ="(//table[contains(@aria-label,'Region = Line Details, Report = Primary Default, View = Report, Displayed Rows Start')]//tr//td[text()='DISCOUNT']//following-sibling::td)[4]")
    public WebElement discount;
	
	@FindBy(xpath ="//input[contains(@id,'_ORDERED_PRICE_M')]")
    public WebElement updatePrice;
	
	@FindBy(xpath ="(//table[contains(@aria-label,'Region = Line Details, Report = Primary Default, View = Report, Displayed Rows Start')]//tr[position()=2]//td)[6]")
    public WebElement mainPrice;
	
	@FindBy(xpath ="(//table[contains(@aria-label,'Region = Line Details, Report = Primary Default, View = Report, Displayed Rows Start')]//td[contains(text(),'ACC')]//following-sibling::td)[4]")
    public WebElement accPrice;
	
	@FindBy(xpath ="(//table[contains(@aria-label,'Region = Line Details, Report = Primary Default, View = Report, Displayed Rows Start')]//tr[position()=3]//td)[6]")
    public WebElement upsellPrice;
	
	@FindBy(xpath ="(//table[contains(@aria-label,'Region = Line Details, Report = Primary Default, View = Report, Displayed Rows Start')]//tr//td[text()='Logo']//following-sibling::td)[4]")
    public WebElement logoCharge;
	
	@FindBy(xpath = "(//table[contains(@aria-label,'Region = Line Details, Report = Primary Default, View = Report, Displayed Rows Start')]//tr[position()=2]//td)[3]")
	public WebElement mainDescription;
	
	@FindBy(xpath = "//span[text()='Apply Changes']//parent::button")
	public WebElement applyBtn;
	
	@FindBy(xpath ="(//table[contains(@aria-label,'Region = Line Details, Report = Primary Default, View = Report, Displayed Rows Start')]//tr[position()=2]//img)[1]")
    public WebElement updateAttribute;
	
	@FindBy(xpath ="//span[text()='Show Attributes']//parent::button")
    public WebElement showAttribute;
	
	@FindBy(xpath ="//select[contains(@id,'_ATTRIBUTE_')]")
    public List<WebElement> selectAttribute;
	
	@FindBy(xpath ="//span[text()='Save & Continue']//parent::button")
    public WebElement saveAndConti;
	
	@FindBy(xpath = "//input[@type='text' and contains(@name,'IMPRINT_')]")
	public List<WebElement> imprintLines;
	
	@FindBy(xpath = "//td[text()='SHIP']//following-sibling::td/a")
	public WebElement updateShipBtn;
	
	@FindBy(xpath = "(//td[text()='SHIP']//following-sibling::td)[4]")
	public WebElement updateShipCharge;
	
	@FindBy(xpath = "//input[contains(@id,'_UPLOAD_LOGO_Y')]//following-sibling::label")
	public WebElement uploadLogoYes;
	
	@FindBy(xpath = "//label[contains(@for,'_UPLOAD_NEW_LOGO_0')]")
	public WebElement uploadNewlogo;
	
	@FindBy(xpath = "//input[contains(@id,'_IMPRINT_LOGO')]")
	public WebElement inputlogo;
	
	@FindBy(xpath = "//img[@id='imprint_src']")
	public WebElement uploadedLogo;
	
	@FindBy(xpath = "//span[text()='Click Here for Upload Logo']//parent::button")
	public WebElement uploadlogo;
	
	@FindBy(xpath = "//span[text()='Update logo charges']//parent::button")
	public WebElement updatelogo;
	
    @FindBy(xpath = "//div[@class='row']")
	public WebElement redoImpPreLines;
    
    @FindBy(xpath = "//input[@type='radio' and contains(@id,'P176_UPLOAD_LOGO_N')]")
	public WebElement redologo;
	
	//** Manage Order Page>> Redo Order
	@FindBy(xpath = "//td[text()='Mainitem']//following-sibling::td//img")
	public WebElement viewImprintAndLogoBtn;
	
	@FindBy(xpath = "(//td[text()='Mainitem']//following-sibling::td)[1]")
	public WebElement mangeOrderMainQty;
	
	@FindBy(xpath = "(//td[text()='Accessory']//following-sibling::td)[1]")
	public WebElement mangeOrderAccQty;
	
	@FindBy(xpath = "(//td[text()='Upsell']//following-sibling::td)[1]")
	public WebElement mangeOrderUpsellQty;
	
	@FindBy(xpath = "(//td[text()='Free']//following-sibling::td)[1]")
	public WebElement mangeOrderFreeQty;
	
	@FindBy(xpath = "(//td[text()='Discount']//following-sibling::td)[2]")
	public WebElement mangeOrderDiscount;
	
	@FindBy(xpath = "(//td[text()='Mainitem']//following-sibling::td)[2]")
	public WebElement mangeOrderMainPrice;
	
	@FindBy(xpath = "(//td[text()='Accessory']//following-sibling::td)[2]")
	public WebElement mangeOrderAccPrice;
	
	@FindBy(xpath = "(//td[text()='Upsell']//following-sibling::td)[2]")
	public WebElement mangeOrderUpsellPrice;
	
	@FindBy(xpath = "(//td[text()='Logocharge']//following-sibling::td)[2]")
	public WebElement mangeOrderLogocharge;
	
	@FindBy(xpath = "(//td[text()='Mainitem']//preceding-sibling::td)[2]")
	public WebElement mangeOrderMainDesc;
	
	@FindBy(xpath = "(//td[text()='Shipping']//following-sibling::td)[2]")
	public WebElement mangeOrderShipPrice;
	
}
