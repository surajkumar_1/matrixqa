
package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import base.BaseClass;

public class ServiceRequestPage {

	public ServiceRequestPage(WebDriver driver) {

		PageFactory.initElements(driver, this);

	}

	// ----------------------------------------------------//

	// SR creation
	@FindBy(xpath = "//h2[contains(@id,'R356920622987230509_heading')]")
	public WebElement heading;

	@FindBy(xpath = "//div[contains(@id,'P62_SR_NUMBER_CONTAINER')]/div[2]")
	public WebElement sr;

	@FindBy(xpath = "//span[contains(@id,'P62_SR_ACCOUNT_NUMBER')]")
	public WebElement customerNo;

	@FindBy(xpath = "//span[contains(@id,'P62_SR_ORDER_NUMBER')]")
	public WebElement orderNo;

	@FindBy(xpath = "//span[contains(@id,'P62_SR_CUSTOMER_NAME')]")
	public WebElement customerName;

	@FindBy(xpath = "//select[contains(@id,'P62_SR_TYPE')]")
	public WebElement requestType;

	@FindBy(xpath = "(//input[contains(@id,'P62_SR_PROBLEM_CATEGORY')])[2]")
	public WebElement problemCategory;

	@FindBy(xpath = "//button[contains(@id,'P62_SR_PROBLEM_CATEGORY_lov_btn')]")
	public WebElement problemCategorySelect;

	@FindBy(xpath = "//div[contains(@class,'a-PopupLOV-searchBar')]/input")
	public WebElement problemCategoryInput;

	@FindBy(xpath = "//div[contains(@class,'a-PopupLOV-searchBar')]/button")
	public WebElement problemCategorySearch;

	@FindBy(xpath = "//button[contains(@id,'P62_SR_PROBLEM_CODE_lov_btn')]")
	public WebElement problemCodeSelect;

	@FindBy(xpath = "//div[@id='PopupLov_62_P62_SR_PROBLEM_CODE_dlg']//div[@class='a-PopupLOV-searchBar']/input")
	public WebElement problemCodeInput;

	@FindBy(xpath = "//input[@id='P62_SR_PROBLEM_CODE']")
	public WebElement problemCode;

	@FindBy(xpath = "//div[@id='PopupLov_62_P62_SR_PROBLEM_CODE_dlg']//button[contains(@class,'a-Button a-PopupLOV-doSearch')]")
	public WebElement problemCodeSearch;

	@FindBy(xpath = "//input[contains(@id,'P62_SR_RESPONSIBLE_PERSON')]")
	public WebElement responsiblePerson;

	@FindBy(xpath = "//button[contains(@id,'P62_SR_RESPONSIBLE_PERSON_lov_btn')]")
	public WebElement responsiblePersonSelect;

	@FindBy(xpath = "//div[@id='PopupLov_62_P62_SR_RESPONSIBLE_PERSON_dlg']//input[contains(@class,'a-PopupLOV-search apex-item-text')]")
	public WebElement responsiblePersonInput;

	@FindBy(xpath = "//div[@id='PopupLov_62_P62_SR_RESPONSIBLE_PERSON_dlg']//button[contains(@class,'a-Button a-PopupLOV-doSearch')]")
	public WebElement responsiblePersonSearch;

	@FindBy(xpath = "//button[contains(@id,'P62_SR_PRODUCT_lov_btn')]")
	public WebElement productSelect;

	@FindBy(xpath = "//input[contains(@class,'a-PopupLOV-search apex-item-text')]")
	public WebElement productInput;

	@FindBy(xpath = "//button[contains(@class,'a-Button a-PopupLOV-doSearch')]")
	public WebElement productSearch;

	@FindBy(xpath = "//textarea[contains(@id,'P62_SR_DESCRIPTION_MAIN')]")
	public WebElement srDescription;

	@FindBy(xpath = "//span[contains(@id,'P62_SR_DESCRIPTION_MAIN_inline_help')]")
	public WebElement srDescriptionNote;

	@FindBy(xpath = "//label[contains(@id,'P62_SR_DESCRIPTION_MAIN_LABEL')]/i/b")
	public WebElement srDescriptionLimitLabel;

	@FindBy(xpath = "//button[contains(@id,'B357089218408416905')]")
	public WebElement createSR;

	@FindBy(xpath = "//button[contains(@id,'B357090711037416920')]")
	public WebElement updateSR;

	@FindBy(xpath = "//button[contains(@id,'B356921183481230514')]")
	public WebElement back;

	@FindBy(xpath = "//ul[@class='a-Notification-list htmldbUlErr']//li")
	public List<WebElement> error;

	@FindBy(xpath = "//div[contains(@id,'t_Alert_Success')]//h2")
	public WebElement srSuccessMsg;

	// ----------------------------------------------------------------------------------//

	// Action Information

	@FindBy(xpath = "//button[contains(@id,'B357086902047416882')]")
	public WebElement accountDetails;

	@FindBy(xpath = "//button[contains(@id,'B359338533205985576')]")
	public WebElement addNewAction;

	@FindBy(xpath = "//iframe")
	public WebElement actionFrame;

	@FindBy(xpath = "//table[contains(@id,'_orig')]//tr//td[1]/a/img")
	public WebElement editAction;

	@FindBy(xpath = "//table[contains(@id,'_orig')]//tr//td[1]/a/span")
	public WebElement reopenAction;

	@FindBy(xpath = "//button[contains(@id,'B357359801464914905')]")
	public WebElement reopen;

	@FindBy(xpath = "//div[contains(@id,'P64_SR')]//span")
	public WebElement reopenSR;

	@FindBy(xpath = "//span[contains(@id,'APEX_SUCCESS_MESSAGE')]//h2")
	public WebElement successMsg;

	@FindBy(xpath = "//table[contains(@id,'357087098913416883_orig')]//tr//td[2]")
	public WebElement actionSRNo;

	@FindBy(xpath = "//table[contains(@id,'357087098913416883_orig')]//tr//td[3]")
	public WebElement actionSRStatus;

	@FindBy(xpath = "//table[contains(@id,'357087098913416883_orig')]//tr//td[4]")
	public WebElement actionSRType;

	@FindBy(xpath = "//table[contains(@id,'357087098913416883_orig')]//tr//td[5]")
	public WebElement actionAssignee;

	@FindBy(xpath = "//table[contains(@id,'357087098913416883_orig')]//tr//td[7]")
	public WebElement actionDescrip;

	@FindBy(xpath = "//table[contains(@id,'357087098913416883_orig')]//tr//td[8]")
	public WebElement actionResol;

	@FindBy(xpath = "//span[@class='a-IRR-pagination-label']")
	public WebElement actionPagination;

	@FindBy(xpath = "//button[contains(@id,'actions_button')]")
	public WebElement actionButton;

//------------------------------------------------------------------------------------//	

	// Action Creation

	@FindBy(xpath = "//div[contains(@id,'P63_AC_SR_NUMBER_CONTAINER')]/div[2]")
	public WebElement actionSRNumber;

	@FindBy(xpath = "//button[contains(@id,'B343279802512061697')]")
	public WebElement orderStatusOai;

	@FindBy(xpath = "//textarea[contains(@id,'P63_AC_DESCRIPTION')]")
	public WebElement actionDescription;

	@FindBy(xpath = "//div[contains(@id,'P63_DESC_LABEL_CONTAINER')]//i")
	public WebElement actionDescriptionLimitLabel;

	@FindBy(xpath = "//textarea[contains(@id,'P63_AC_RESOLUTION')]")
	public WebElement actionResolution;

	@FindBy(xpath = "//div[contains(@id,'P63_RES_LABEL_CONTAINER')]//i")
	public WebElement actionResolutionLimitLabel;

	@FindBy(xpath = "//button[contains(@id,'B357088671904416899')]")
	public WebElement addAction;

	@FindBy(xpath = "//button[contains(@id,'B357357087835914877')]")
	public WebElement updateAction;

	@FindBy(xpath = "//button[contains(@id,'B357357466985914881')]")
	public WebElement updateAndCloseAction;

	@FindBy(xpath = "//div[@id='ui-id-4']/p")
	public WebElement actionUpdateAlert;

	@FindBy(xpath = "//button[contains(@class,'js-confirmBtn')]")
	public WebElement ok;

	@FindBy(xpath = "//select[contains(@id,'P63_AC_STATUS')]")
	public WebElement actionStatus;

	@FindBy(xpath = "//select[contains(@id,'P63_AC_TYPE')]")
	public WebElement actionType;
	
	@FindBy(xpath = "(//input[contains(@id,'P63_AC_ASSIGNEE')])[2]")
    public WebElement actionAssigneeValue;

	@FindBy(xpath = "//button[contains(@id,'P63_AC_ASSIGNEE_lov_btn')]/span")
	public WebElement actionAssigneeSelect;

	@FindBy(xpath = "//div[contains(@id,'PopupLov_63_P63_AC_ASSIGNEE_dlg')]//input")
	public WebElement actionAssigneeInput;

	@FindBy(xpath = "//button[contains(@class,'a-Button a-PopupLOV-doSearch')]")
	public WebElement actionAssigneeSearch;

	public void selectValueFromDropdown(WebDriver driver, String option) throws InterruptedException {
		Thread.sleep(5000);
		BaseClass.wait
				.until(ExpectedConditions.visibilityOf(driver
						.findElement(By.xpath("//span[@class='popup-lov-highlight' and text() = '" + option + "']"))))
				.click();
	}

}
