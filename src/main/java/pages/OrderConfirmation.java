package pages;

import static base.BaseClass.orderconf;

import java.time.Duration;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import base.BaseClass;

public class OrderConfirmation {


	public OrderConfirmation(WebDriver driver) {
		PageFactory.initElements(driver, this);
	
	}
	
	
	String newLogoSrc,oldLogoSrc;
	String primaryXpath = "//td[text()='%s']//following-sibling::td[position()=8]";
	
	@FindBy(xpath = "//h2[normalize-space()='Product Added Successfully']")
	public WebElement productAddedMsg;
	
	@FindBy(xpath = "//table[contains(@aria-label, 'Billing Details, Report = Primary Default')]//td[position()=2]")
	public WebElement billToAddLine1;
	
	@FindBy(xpath = "//table[contains(@aria-label, 'Shipping Details, Report = Primary Default')]//td[position()=2]")
	public WebElement shipToAddLine1;
	
	@FindBy(xpath = "//b[contains(text(),'Select Different Bill To')]//ancestor::button")
	public WebElement differentBillTo;
	
	@FindBy(xpath = "//b[contains(text(),'Select Different Ship To')]//ancestor::button")
	public WebElement differentShipTo;
	
	@FindBy(xpath = "//iframe[@title='Order Summary - Change Bill To/Ship To']")
	public WebElement frameBillAndShipTo;
	
	@FindBy(xpath = "//*[text()='Add New Bill To Address']//ancestor::button")
	public WebElement newBillToAddress;
	
	@FindBy(xpath = "//*[text()='Add New Ship To Address']//ancestor::button")
	public WebElement newShipToAddress;
	
	@FindBy(xpath = "//button[@aria-label='Next']")
	public WebElement nextButton;
	
	/*
	 * UpdateEmail
	 * 
	 */
	
	@FindBy(xpath = "//table[contains(@aria-label, 'Billing Details, Report = Primary Default')]//img")
	public WebElement editEmailBtn;
	
	@FindBy(xpath = "//input[contains(@id,'_EMAIL') and@ type='text']")
	public WebElement editEmail;

	@FindBy(xpath = "//span[contains(text(),'Update')]//ancestor::button")
	public WebElement updateBtn;

	@FindBy(xpath = "//table[contains(@aria-label, 'Billing Details, Report = Primary Default')]//td[position()=16 and text()]")
	public WebElement billToEmail;
	
	
	
	/*
	 * Address details -- Create New Address
	 * 
	 */
	
	@FindBy(xpath = "//select[contains(@id,'_COUNTRY')]")
	public WebElement selectCountry;
	
	@FindBy(xpath = "//input[contains(@id,'_ADDRESS1')]")
	public WebElement addLine1;

	@FindBy(xpath = "//input[contains(@id,'_ADDRESS2')]")
	public WebElement addLine2;

	@FindBy(xpath = "//input[contains(@id,'_CITY')]")
	public WebElement city;

	@FindBy(xpath = "//input[contains(@id,'_POSTALCODE')]")
	public WebElement postalCode;

	@FindBy(xpath = "//input[contains(@id,'_STATE')]")
	public WebElement state;

	@FindBy(xpath = "//input[contains(@id,'_PROVINCE')]")
	public WebElement province;

	@FindBy(xpath = "//input[contains(@id,'_COUNTY')]")
	public WebElement county;

	@FindBy(xpath = "//label[contains(@for,'_SAME_FOR_SHIP_0')]")
	public WebElement sameForShippTo;

	@FindBy(xpath = "//label[contains(@for,'_PRIMARY_FLG_0')]")
	public WebElement markPrimary;
	
	@FindBy(xpath = "//span[contains(text(),'Create')]//ancestor::button")
	public WebElement createBtn;
	
	@FindBy(xpath = "//table[contains(@aria-label, 'Details, Report = Primary Default')]//td[position()=4]")
	public List<WebElement> newAddressLine1;
	
	@FindBy(xpath = "//table[contains(@aria-label, 'Details, Report = Primary Default')]//td[position()=13]")
	public List<WebElement> firstNameList;
	
	@FindBy(xpath = "//table[contains(@aria-label, 'Details, Report = Primary Default')]//td[position()=18]")
	public WebElement createdEmail;
	
	
	/*
	 * 
	 * Contact details -- Create New contact
	 */

	@FindBy(xpath = "//input[contains(@id,'_CONTACT_FIRST_NAME')]")
	public WebElement firstName;

	@FindBy(xpath = "//input[contains(@id,'_CONTACT_LAST_NAME')]")
	public WebElement lastName;

	@FindBy(xpath = "//input[contains(@id,'_CONTACT_EMAIL')]")
	public WebElement email;

	@FindBy(xpath = "//input[contains(@id,'_CONTACT_PHONE')]")
	public WebElement phone;

	@FindBy(xpath = "//input[contains(@id,'_CONTACT_MOBILE')]")
	public WebElement mobile;

	@FindBy(xpath = "//input[contains(@id,'_CONTACT_FAX')]")
	public WebElement fax;

	@FindBy(xpath = "//select[contains(@id,'_TITLE')]")
	public WebElement selectTitle;
	
	@FindBy(xpath = "//input[contains(@id,'_TITLE')]")
	public WebElement title;
	
	@FindBy(xpath = "//select[contains(@id,'_STATUS')]")
	public WebElement selectStatus;

	@FindBy(xpath = "//td[text()='N' or text()='Y']//preceding-sibling::td/a[contains(@title,'Click Here to update')]")
	public WebElement editAddress;
	
	@FindBy(xpath = "//table[contains(@aria-label, 'Details, Report = Primary Default')]//td[text()='N' or text()='Y']//following-sibling::td[last()]/a/span")
	public WebElement addContact;
	
	@FindBy(xpath = "//table[contains(@aria-label, 'Details, Report = Primary Default')]//td[text()='N' or text()='Y']//following-sibling::td/a/img")
	public WebElement editContact;
	
	
	/*
	 * 
	 * Update Contact 
	 */

	@FindBy(xpath = "//iframe[@title='Order Summary - Contact Update']")
	public WebElement frameUpdateContact;

	/*
	 * 
	 * Specifications
	 * 
	 */

	@FindBy(xpath = "//*[text()='MAIN']//parent::td//following-sibling::td/a")
	public WebElement specificationsBtn;

	@FindBy(xpath = "//iframe[@title='Order Specifications']")
	public WebElement frameProductSpecifications;

	@FindBy(xpath = "//label[contains(@for,'_SHIPMENT_PRIORITY_1')]")
	public WebElement expressShipment;

	@FindBy(xpath = "//label[contains(@for,'_SHIPMENT_PRIORITY_0')]")
	public WebElement standardShipment;

	@FindBy(xpath = "//b[contains(text(),'Save')]//ancestor::button")
	public WebElement saveBtn;

	@FindBy(xpath = "//input[contains(@id,'_UPSELL_0')]//following-sibling::label")
	public WebElement upsell;
	
	@FindBy(xpath = "//select[contains(@id, '_FREE')]")
	public WebElement selectfreeQty;
	
	@FindBy(xpath = "//select[contains(@id, '_DISCOUNT')]")
	public WebElement selectDiscount;
	
	@FindBy(xpath = "//*[text()='MAIN']//parent::td//following-sibling::td[position()=2]")
	public WebElement shipmentPriority;
	
	@FindBy(xpath = "//*[text()='UPSELL']")
	public WebElement upsellLine;
	
	@FindBy(xpath = "//*[text()='EXTRA_FREE']")
	public WebElement freeLine;
	
	@FindBy(xpath = "//*[text()='MAIN']//parent::td//following-sibling::td[position()=3]")
	public WebElement shippingCharge;
	
	@FindBy(xpath = "//button[contains(@class,'js-confirmBtn ')]")
	public WebElement ccLimitError;
	
	@FindBy(xpath = "(//b[contains(text(),'MAIN')]//parent::td//preceding-sibling::td)[9]")
	public WebElement mainLineQty;
	
	@FindBy(xpath = "(//b[contains(text(),'Additonal Charges')]//parent::td//preceding-sibling::td)[9]")
	public WebElement additionalChargesLineQty;
		
	@FindBy(xpath = "(//b[contains(text(),'UPSELL')]//parent::td//preceding-sibling::td)[9]")
	public WebElement upsellQty;
	
	@FindBy(xpath = "//table[contains(@aria-label, 'Region = Product Details (Interactive), Report = Primary Default')]//tr[position()>1]")
	public List<WebElement> totalLines;
	
	public void removeLine(String line) {
		BaseClass.wait.until(ExpectedConditions.visibilityOf(BaseClass.getDriver().findElement(By
				.xpath("//table[contains(@aria-label, 'Region = Product Details (Interactive), Report = Primary Default')]//*[text()='"+line.toUpperCase()
						+"']//parent::td//following-sibling::td[last()]")))).click();
	}
	
	public WebElement selectHold(String holdname) {
		return BaseClass.getDriver()
				.findElement(By.xpath("//label[contains(text(),'" + holdname.toUpperCase() + "')]"));
	}

	
	@FindBy(xpath = "//span[contains(@id,'_CURRENT_HOLDS_DISPLAY')]")
	public WebElement currentHolds;

		
	/*
	 * 
	 * Fetching Value of Bill to
	 * 
	 */
	
	@FindBy(xpath = "//input[contains(@id,'_CART_SUBTOTAL')]")
	public WebElement cartSubtotal;
	
	@FindBy(xpath = "//span[contains(@id,'_TAX_DISPLAY')]")
	public WebElement cartTax;
	
	@FindBy(xpath = "//span[contains(@id,'_CART_DISCOUNT_DISPLAY')]")
	public WebElement cartDiscount;
	
	@FindBy(xpath = "//span[contains(@id,'_CART_TOTAL')]")
	public WebElement cartTotal;
	
	@FindBy(xpath = "//oj-input-date[contains(@id,'_ORDER_REQUEST_DATE')]")
	public WebElement orderRequestDate;
	
	@FindBy(xpath = "//td[@headers='CUSTOMER_NAME']")
	public WebElement customerName;
	
	@FindBy(xpath = "//td[@headers='CUSTOMER_NUMBER']")
	public WebElement customerNumber;
	
	@FindBy(xpath = "//td[@headers='SALESREP']")
	public WebElement salesRepHeader;
	
	@FindBy(xpath = "//td[@headers='COUNTRY']")
	public WebElement customerCountry;
	
	@FindBy(xpath = "//table[contains(@aria-label,'Region = Product Details (Interactive), Report = Primary Default, View')]//td[position()=2]")
	public List<WebElement> lineProductCode;
	
	@FindBy(xpath = "//table[contains(@aria-label,'Region = Product Details (Interactive), Report = Primary Default, View')]//td[position()=9]")
	public List<WebElement> lineQty;
	
	@FindBy(xpath = "//table[contains(@aria-label,'Region = Product Details (Interactive), Report = Primary Default, View')]//td[position()=3]")
	public List<WebElement> lineItemPrice;
	
	@FindBy(xpath = "//table[contains(@aria-label,'Region = Product Details (Interactive), Report = Primary Default, View')]//td[position()=4]")
	public WebElement lineDiscountPercentage;
	
	@FindBy(xpath = "//table[contains(@aria-label,'Region = Product Details (Interactive), Report = Primary Default, View')]//td[position()=5]")
	public WebElement lineDiscount;
	
	@FindBy(xpath = "//table[contains(@aria-label,'Region = Product Details (Interactive), Report = Primary Default, View')]//td[position()=8]")
	public List<WebElement> linePackagePrice;
	
	@FindBy(xpath = "//table[contains(@aria-label,'Region = Product Details (Interactive), Report = Primary Default, View')]//td[position()=15]")
	public WebElement lineProductTotal;
	
	@FindBy(xpath = "//table[contains(@aria-label,'Region = Product Details (Interactive), Report = Primary Default, View')]//td[position()=10]")
	public List<WebElement> lineType;
	
	@FindBy(xpath = "//table[contains(@aria-label,'Region = Product Details (Interactive), Report = Primary Default, View')]//td[position()=13]")
	public WebElement lineShipping;

	public String getExtraCharges(String line) {
		return BaseClass.getDriver().findElement(By.xpath("//table[contains(@aria-label,'Region = Product Details (Interactive), Report = Primary Default, View')]//td/*[contains(text(),'"+line+"')]//parent::td//following-sibling::td[position()=2]/*"))
		.getText().replaceAll("[^0-9.]", "");
	}

	
	//Confirm imprint
	
	@FindBy(xpath = "//a[@title='Click here to view/update imprint']")
	public WebElement viewImprint;

	@FindBy(xpath = "//span[normalize-space()='Clear Text']//parent::button")
	public WebElement clearText;

	@FindBy(xpath = "//span[normalize-space()='Apply Changes']//parent::button")
	public WebElement applychg;

	@FindBy(xpath = "//b[normalize-space()='Confirm Imprint']//ancestor::button")
	public WebElement confImprint;

	@FindBy(xpath = "//span[normalize-space()='Cancel']//parent::button")
	public WebElement cancelBtn;

	@FindBy(xpath = "//span[normalize-space()='Update Logo']//parent::button")
	public WebElement updateLogo;
	
	@FindBy(xpath = "//*[@id='remove_design']")
	public WebElement removeTemplate; 
	 
	@FindBy(xpath = "//img[contains(@alt,'upload')]")
	public List<WebElement> template;
	@FindBy(xpath = "//label[contains(@for,'UPLOAD_LOGO_Y')]")
	public WebElement updateLogoY;
	
	@FindBy(xpath = "//label[contains(@for,'UPLOAD_LOGO_N')]")
	public WebElement updateLogoN;
	
	@FindBy(xpath = "//input[contains(@id,'_UPLOAD_NEW_LOGO_0')]")
	public WebElement newLogo;

	@FindBy(xpath = "//input[contains(@id,'_LOGO_AS_BEFORE_0')]")
	public WebElement logoAsBefore;

	@FindBy(xpath = "//input[contains(@id,'_IMPRINT_LOGO') and @type='file']")
	public WebElement inputLogo;

	@FindBy(xpath = "//button[@id='uploadlogobtn']")
	public WebElement uploadLogo;
	
	@FindBy(xpath = "//input[contains(@id,'PROOF_REQUIRED_Y')]")
	public WebElement proofReqiuredYes;
	
	@FindBy(xpath = "//img[contains(@id,'imprint_src')]")
	public WebElement logoImg;
	
	@FindBy(xpath = "//textarea[contains(@id,'SPECIAL_REQUEST')]")
	public WebElement specialInstruction;
	
	@FindBy(xpath ="//td[@headers='TEMPLATE']//a[@href]")
	public WebElement templateUrl;
	
	@FindBy(xpath="//button[@class='t-Button t-Button--hot js-ignoreChange']")
	public WebElement add_change_template;
	
	@FindBy(xpath="//div[@class='t-Card-desc']//a[@href]")
	public WebElement updated_Template_url;

	// **Order info Selection
	@FindBy(xpath = "//select[contains(@id,'_NO_CUSTOMER_EMAIL_RESON')]")
	public WebElement noEmailReason;
	
	@FindBy(xpath = "//input[contains(@id, 'oj-inputsearch-input') and contains(@aria-labelledby,'_SALESREP_LABEL')]")
	public WebElement salesRep;

	@FindBy(xpath = "//li[@class='oj-listbox-results-depth-0 oj-listbox-result oj-listbox-result-selectable oj-hover']//span[text()]")
	public WebElement saleRepselect;

	@FindBy(xpath = "//input[contains(@id,'_SALESREP_EMAIL')]")
	public WebElement salesRepEmail;
	
	@FindBy(xpath = "//select[contains(@id,'_ORDER_TYPE')]")
	public WebElement orderType;

	@FindBy(xpath = "//select[contains(@id,'_PAYMENT_METHOD')]")
	public WebElement paymentMethod;

	@FindBy(xpath = "//select[contains(@id,'_GDPR_INTEREST')]")
	public WebElement legitInterest;

	@FindBy(xpath = "//select[contains(@id,'_GDPR_CALL')]")
	public WebElement callOption;

	@FindBy(xpath = "//select[contains(@id,'_GDPR_MAIL')]")
	public WebElement mailOption;

	@FindBy(xpath = "//select[contains(@id,'_GDPR_RENT')]")
	public WebElement rentOption;

	@FindBy(xpath = "//select[contains(@id,'_GDPR_EMAIL')]")
	public WebElement emailOption;

	@FindBy(xpath = "//span[contains(@id,'_TAX_DISPLAY')]")
	public WebElement ordertax;

	@FindBy(xpath = "//b[normalize-space()='Place Order']//ancestor::button")
	public WebElement placeOrder;

	@FindBy(xpath = "//b[normalize-space()='Quote Order']//ancestor::button")
	public WebElement qouteOrder;

	

	//Order confirmation Page
	
	@FindBy(xpath = "//h2")
	public WebElement header;
	
	@FindBy(xpath = "//div[@id]/center//b")
	public List<WebElement> orderNumbers;
	
	@FindBy(xpath = "//b[normalize-space()='Place New Order']//ancestor::button")
	public WebElement placeNewOrder;
	
	@FindBy(xpath = "//b[normalize-space()='Manage Order']//ancestor::button")
	public WebElement manageOrder;
	
	@FindBy(xpath = "//table[@aria-label='Header_Holds']//td//span")
	public List<WebElement> headerHolds;
		
		
		
	/**
	 * Imprint and Logo update validation
	 * 
	 * @throws InterruptedException
	 */
	public void imprintLogoConfAndValid(){
		int line = 0;
		BaseClass.wait.until(ExpectedConditions.elementToBeClickable(viewImprint)).click();
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
		BaseClass.orderconf.clearText.click();
		List<WebElement> imprints = BaseClass.getDriver().findElements(By.xpath(
				"//div[@class='t-Form-itemWrapper']//child::input[@type='text' and contains(@id,'_IMPRINT_')]"));
		ListIterator<WebElement> imprint = imprints.listIterator();
		if (!imprints.isEmpty()) {
			while (imprint.hasNext()) {
				line += 1;
				WebElement nxtEle = imprint.next();
				Assert.assertEquals(0, nxtEle.getAttribute("value").length());
				nxtEle.sendKeys("Updated Imprint " + line);
				Assert.assertEquals("Updated Imprint " + line, nxtEle.getAttribute("value"));
			}
			
		}
		try {
			BaseClass.CustProd.specialInst.sendKeys("update");
		} catch (Exception e) {
			e.getStackTrace();
		}
		
		/*
		 *  TODO: pending because of locator
		 *  BaseClass.CustProd.imprintPrevieStyleValidation();
		 */
			
		BaseClass.orderconf.confImprint.click();
	}

	/**
	 * Imprint and Logo update validation
	 * 
	 * @throws InterruptedException
	 */
	public void imprintLogoConfAndValid(String addLogo) throws InterruptedException{
		int line = 0;
		BaseClass.wait.until(ExpectedConditions.elementToBeClickable(viewImprint)).click();
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
		BaseClass.orderconf.clearText.click();
		List<WebElement> imprints = BaseClass.getDriver().findElements(By.xpath(
				"//div[@class='t-Form-itemWrapper']//child::input[@type='text' and contains(@id,'_IMPRINT_')]"));
		ListIterator<WebElement> imprint = imprints.listIterator();
		if (!imprints.isEmpty()) {
			while (imprint.hasNext()) {
				line += 1;
				WebElement nxtEle = imprint.next();
				Assert.assertEquals(0, nxtEle.getAttribute("value").length());
				nxtEle.sendKeys("Updated Imprint " + line);
				Assert.assertEquals("Updated Imprint " + line, nxtEle.getAttribute("value"));
			}
			
		}
		try {
			BaseClass.getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(2));
			BaseClass.CustProd.specialInst.sendKeys("update");
		} catch (Exception e) {
			System.out.println("Test"+e.getMessage());
		}
		
		/*
		 *  TODO: pending because of locator
		 *  BaseClass.CustProd.imprintPrevieStyleValidation();
		 */
		//sm
		if(addLogo.equalsIgnoreCase("YES")) {
			try {
				Thread.sleep(5000);
			if(orderconf.logoImg.isDisplayed())	
				oldLogoSrc=orderconf.logoImg.getAttribute("src");
			System.out.println("old logo"+oldLogoSrc);
			}
			catch(Exception e)
			{
				
			}
			
			
			BaseClass.js.click(orderconf.updateLogoY);	
			BaseClass.js.click(BaseClass.wait.until(ExpectedConditions.visibilityOf(orderconf.newLogo)));
			orderconf.inputLogo
					.sendKeys(System.getProperty("user.dir") + "/src/main/resources/TestData/NPLogoTesting.PNG");
			orderconf.uploadLogo.click();
		   Thread.sleep(5000);
			//clicking ok manually on error message because of defect OAA-954, otherwise logo won't be uploaded	
			BaseClass.wait.until(ExpectedConditions.attributeContains(BaseClass.orderconf.logoImg, "src", "uploads.documents.cimpress.io"));
			newLogoSrc=orderconf.logoImg.getAttribute("src");
			System.out.println("new logo"+newLogoSrc);
			orderconf.specialInstruction.sendKeys("special instructions");
			Assert.assertTrue("Proof Required is not selected",BaseClass.wait.until(ExpectedConditions.elementToBeSelected(orderconf.proofReqiuredYes)));
			Assert.assertTrue("Logo was not added",
					orderconf.logoImg.getAttribute("src").contains("uploads.documents.cimpress.io/v1/uploads"));
			if(!oldLogoSrc.isEmpty())
			{
			Assert.assertFalse("Logo was not updated",
					newLogoSrc.contains(oldLogoSrc));			
			}
			Thread.sleep(5000);
		}
		else if(addLogo.equalsIgnoreCase("No")) {			
			BaseClass.js.click(orderconf.updateLogoY);	
			BaseClass.js.click(orderconf.logoAsBefore);
			Thread.sleep(5000);
			//clicking ok manually on error message because of defect OAA-954, otherwise logo won't be uploaded	
			orderconf.specialInstruction.sendKeys("special instructions");
			Assert.assertTrue("Proof Required is not selected",BaseClass.wait.until(ExpectedConditions.elementToBeSelected(orderconf.proofReqiuredYes)));
			Assert.assertTrue("Logo was not added",
					orderconf.logoImg.getAttribute("src").contains("uploads.documents.cimpress.io/v1/uploads"));			
			Thread.sleep(5000);
		}
	//sm		
		BaseClass.orderconf.confImprint.click();
	}
	
	public void verifyLogo() {
		Assert.assertEquals("Logo was not updated",
				newLogoSrc,BaseClass.orderconf.logoImg.getAttribute("src"));			
	
		}
	public void verifyAddress(String primary, String randomInt) throws InterruptedException {
		Iterator<WebElement> address = orderconf.newAddressLine1.iterator();
		Assert.assertTrue("New Address line not created",orderconf.newAddressLine1.size()>1);
		int count = 0;
		int pagecount=0;
		while (address.hasNext()) {
			WebElement tempElement = address.next();
			BaseClass.wait.until(ExpectedConditions.visibilityOf(tempElement));
			if (tempElement.getText().contains(randomInt)) {
				Assert.assertTrue("The address is not created", tempElement.getText().contains(randomInt));
				if (primary.equalsIgnoreCase("YES")) {
					Assert.assertEquals("Primary Address not created", "Y",
							tempElement.findElement(By.xpath(String.format(primaryXpath, tempElement.getText())))
									.getAttribute("textContent"));
				} else
					Assert.assertEquals("Non Primary Address not created", "N",
							tempElement.findElement(By.xpath(String.format(primaryXpath, tempElement.getText())))
									.getAttribute("textContent"));
				return;
			}
			count++;
			if (count == orderconf.newAddressLine1.size()) {
				orderconf.nextButton.click();
				pagecount += count;
				count = 0;
				Thread.sleep(4000);
				address = orderconf.newAddressLine1.iterator();
				if (pagecount == BaseClass.manageOrder.getTotalLine()) {
					Assert.fail("The address is not created");
				}
				
			}
		}
		
	}
	public void verifyContact(String randomInt) throws InterruptedException {
		Iterator<WebElement> firstname = orderconf.firstNameList.iterator();
		int count=0;
		int pagecount=0;
		while (true) {
			WebElement tempElement = firstname.next();
			BaseClass.wait.until(ExpectedConditions.visibilityOf(tempElement));
			if (tempElement.getText().contains(randomInt)) {
				Assert.assertTrue("The contact is not created", tempElement.getText().contains(randomInt));
				return;
			}
			count++;
			if (count == orderconf.firstNameList.size()) {
				orderconf.nextButton.click();
				pagecount += count;
				count = 0;
				Thread.sleep(4000);
				firstname = orderconf.firstNameList.iterator();
				if (pagecount == BaseClass.manageOrder.getTotalLine()) {
					Assert.fail("The contact is not created");
				}
			}
		}
	}
	public void verifyNoEmail(String randomInt) {
		Iterator<WebElement> firstname = orderconf.firstNameList.iterator();
		int count = 0;
		while (firstname.hasNext()) {
			WebElement tempElement = firstname.next();
			BaseClass.wait.until(ExpectedConditions.visibilityOf(tempElement));
			if (tempElement.getText().contains(randomInt)) {
				Assert.assertEquals("Contact is created with Email", "-", 
			  tempElement.findElement(By.xpath("//td[contains(text(),'"+randomInt+"')]//following-sibling::td[position()=5]")).getText());
				
				return;
			}
			count++;
			if (count == orderconf.firstNameList.size()) {
				Assert.fail("The contact is not created");
			}

		}
		
	}
	public WebElement getProductTotal(String productName) {
		return BaseClass.getDriver()
				.findElement(By.xpath("//*[text()='" + productName + "']//parent::td//following-sibling::td[position()=14]"));
	}

/**
	 * Discounted setup and Logo charges validation
	 * 
	 * @throws InterruptedException
	 */
	public void verifyDiscountedSetupAndLogoCharge(String setupCharge,String logoCharge){
	
		String discountedSetup=BaseClass.orderconf.getProductTotal("Discounted Setup Charge").getText().replaceAll("[^\\-\\d.]+|\\.(?!\\d)", "");	;
		String discountedLogo=BaseClass.orderconf.getProductTotal("Discounted Logo Charge").getText().replaceAll("[^\\-\\d.]+|\\.(?!\\d)", "");		
		Assert.assertEquals("Discount logo charge is missing or incorrect",logoCharge, discountedLogo);
		Assert.assertEquals("Discount setup is missing or incorrect",setupCharge, discountedSetup);
		
	}
	public void removeTemplate() throws InterruptedException{
		BaseClass.wait.until(ExpectedConditions.elementToBeClickable(viewImprint)).click();
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
		Assert.assertTrue("Remove Template button is not displayed to remove",removeTemplate.isDisplayed());
		BaseClass.wait.until(ExpectedConditions.visibilityOf(removeTemplate)).click();
		Thread.sleep(1000);
		BaseClass.orderconf.confImprint.click();
		BaseClass.getDriver().switchTo().defaultContent();
		Thread.sleep(5000);
		}
		 
		public boolean verifyTemplateIsDisplayed(){
		boolean sts;
		BaseClass.wait.until(ExpectedConditions.elementToBeClickable(viewImprint)).click();
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));	 
	    sts = template.isEmpty();		
		BaseClass.orderconf.confImprint.click();
		BaseClass.getDriver().switchTo().defaultContent();
		return sts;
		 
		}
		public String getProductCode(String line) {
			return BaseClass.getDriver().findElement(By.xpath("//*[contains(text(),'"+line+"')]//parent::td//preceding-sibling::td[position()=8]"))
					.getAttribute("textContent");
		}
		
		public String getProductName(String line) {
			return BaseClass.getDriver().findElement(By.xpath("//*[contains(text(),'"+line+"')]//parent::td//preceding-sibling::td[position()=9]/span"))
			.getAttribute("textContent");
		}
		
		public String getProductPrice(String line) {
			return BaseClass.getDriver().findElement(By.xpath("//*[contains(text(),'"+line+"')]//parent::td//following-sibling::td[position()=5]/span"))
					.getAttribute("textContent").replaceAll("[^0-9.]", "");
		}
		
		public String getProductQty(String line) {
			return BaseClass.getDriver().findElement(By.xpath("//*[contains(text(),'"+line+"')]//parent::td//preceding-sibling::td[position()=1]//b"))
					.getAttribute("textContent");
		}
		
		public WebElement getLineType(String lineType) {
			return BaseClass.getDriver()
					.findElement(By.xpath("//td[10]/b[text()='" + lineType + "']"));
		}
		

	public WebElement getLineItemPrice(String lineType) {
			return BaseClass.getDriver()
					.findElement(By.xpath("//td[10]/b[text()='" + lineType + "']//ancestor::tr//td[3]"));
		}
		
		public WebElement getLineItemQuantity(String lineType) {
			return BaseClass.getDriver()
					.findElement(By.xpath("//td[10]/b[text()='" + lineType + "']//ancestor::tr//td[9]"));
		}
		
}
