package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class NavigationBar {

	public NavigationBar(WebDriver driver) {
		PageFactory.initElements(driver, this);

	}

	@FindBy(xpath = "//button[@id='t_Button_navControl']")
	public WebElement navOpen;

	@FindBy(xpath = "//a[contains(text(),'Manage Customers')]")
	public WebElement manageCustomer;

	@FindBy(xpath = "//button[@id='B47635452982081637']")
	public WebElement addNewCusBtn;

}
