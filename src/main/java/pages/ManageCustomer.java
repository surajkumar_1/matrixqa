package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import base.BaseClass;

public class ManageCustomer {

	public ManageCustomer(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}


	@FindBy(xpath = "//input[contains(@id,'_SEARCH_CSTOMER_NUM')]")
	public WebElement customerNo;

	@FindBy(xpath = "//button[@id='search_cust']")
	public WebElement searchBtn;

	@FindBy(xpath = "//input[contains(@id,'_CUSTOMER_NAME')]")
	public WebElement customerName;

	@FindBy(xpath = "//button[contains(@class,'ignoreChange')] //span[contains(text(),'Add BillTo Address')]")
	public WebElement addBillToBtn;

	@FindBy(xpath = "//iframe[@title='Create New Address']")
	public WebElement switchFrameCreateBillTo;

	@FindBy(xpath = "//input[contains(@name,'_SAME_FOR_SHIP')]")
	public WebElement sameForShipTo;

	@FindBy(xpath = "//button[contains(@onclick,'PLACE_ORDER')]")
    public WebElement placeOrder;
	
	
	
	public WebElement getbillToAddress1(String addressLine1) {
		return BaseClass.getDriver().findElement(By.xpath("//table[@aria-label='Billing Details']//td[@headers='ADDRESS1'and contains(text(),'"+addressLine1+"')]"));
	
	}

	public WebElement checkIsPrimaryForBillTO(String addressLine1) {
		
		return BaseClass.getDriver().findElement(By.xpath("//table[@aria-label='Billing Details']//td[@headers='ADDRESS1' and contains(text(),'"+addressLine1+"')]//following-sibling::td[@headers='PRIMARY_FLAG']"));
	}
	
	public WebElement getShipToAddress1(String addressLine1) {
		return BaseClass.getDriver().findElement(By.xpath("//table[@aria-label='Shipping Details']//td[@headers='ADDRESS1'and contains(text(),'"+addressLine1+"')]"));
	
	}
       public WebElement checkIsPrimaryForShipTO(String addressLine1) {
		
		return BaseClass.getDriver().findElement(By.xpath("//table[@aria-label='Shipping Details']//td[@headers='ADDRESS1' and contains(text(),'"+addressLine1+"')]//following-sibling::td[@headers='PRIMARY_FLAG']"));
	}
	

	/*
	 * 
	 * Ship To
	 * 
	 */
	@FindBy(xpath = "//button[contains(@class,'ignoreChange')] //span[contains(text(),'Add ShipTo Address')]")
	public WebElement addShipToBtn;

	/*
	 * 
	 * Update bill To
	 * 
	 */
	@FindBy(xpath = "//table[@aria-label='Billing Details']//td[@headers='PRIMARY_FLAG' and text()='N']//preceding-sibling::td/a")
	public WebElement updateBillTo;

	@FindBy(xpath = "//iframe[@title='Update BillTo Address']")
	public WebElement switchFrameUpdateBillTo;

	
	/*
	 * 
	 * Update Ship To
	 * 
	 */
	@FindBy(xpath = "//table[@aria-label='Shipping Details']//td[@headers='PRIMARY_FLAG' and text()='N']//preceding-sibling::td/a")
	public WebElement updateShipTo;

	@FindBy(xpath = "//iframe[@title='Update ShipTo Address']")
	public WebElement switchFrameUpdateShipTo;

	@FindBy(xpath = "//div[contains(@class,'a-Notification--error')]/ul/li")
	public WebElement createNewAddressError;


	/*
	 *
	 * 
	 *  Contact elements
	 */
	@FindBy(xpath = "//td[@headers='VIEW_CONTACTS']//img")
	public WebElement billToViewContacts;

	@FindBy(xpath = "//iframe[@title='Contact Details']")
	public WebElement frameContactDetails;

	@FindBy(xpath = "//td[@headers='ACCOUNT_NUMBER']")
	public WebElement accountNumberContactDetails;

	
	public WebElement getContactFirstName(String name) {
		return BaseClass.getDriver().findElement(By.xpath("//td[@headers='First Name' and contains(text(),'"+name+"')]"));
	}
	public WebElement getContactEmail(String id) {
		return BaseClass.getDriver().findElement(By.xpath("//td[@headers='EMAIL_ADDRESS' and contains(text(),'"+id+"')]"));
	}
	

	@FindBy(xpath = "//button[@id='add_new_con']")
	public WebElement addNewContactbtnContactDetails;


	@FindBy(xpath = "//button[contains(@class,'js-confirmBtn ui-button')]")
	public WebElement gdprOKAddNewContact;


	@FindBy(xpath = "//span[contains(text(),'Apply Changes')]//parent::button")
	public WebElement applyChangesBtn;

	@FindBy(xpath = "//span[contains(text(),'Cancel')]//parent::button")
	public WebElement cancelBtn;

	
	@FindBy(xpath = "//span[contains(text(),'Create')]//parent::button")
	public WebElement createBtn;
	
	/*
	 * Ship TO
	 */

	@FindBy(xpath = "//button[@id='B243029987860890578']")
	public WebElement accountDetails;

    @FindBy(xpath = "//td[@headers='VIEW_DETAILS']//img")
	public WebElement shipToViewContacts;

	@FindBy(xpath = "//span[normalize-space()='Add New Customer']")
	public WebElement addNewCustomer;

	@FindBy(xpath = "//span[normalize-space()='Clear Search']//ancestor::button")
	public WebElement clearSearchBtn;

	@FindBy(xpath = "//span[contains(@id,'_CUSTOMER_NUMBER_DISPLAY')]")
	public WebElement displayCustomerNo;

	@FindBy(xpath = "//b[normalize-space()='GDPR Preference']//ancestor::button")
	public WebElement gdprBtn;

	@FindBy(xpath = "//div[@role='dialog']//li//span[contains(@style,'color')]")
	public List<WebElement> gdprPref;

	@FindBy(xpath = "//button[normalize-space()='Close']")
	public WebElement closeGdpr;

	@FindBy(xpath = "//span[normalize-space()='PCT Lookup']//ancestor::button")
	public WebElement pctBtn;

	@FindBy(xpath = "//span[@id='P68_PCT_DISPLAY']")
	public WebElement pctNo;

	@FindBy(xpath = "//div[@class='ui-dialog ui-corner-all ui-widget ui-widget-content ui-front ui-dialog--apex t-Dialog-page--standard ui-draggable']//button[@title='Close'][normalize-space()='Close']")
	public WebElement closePctFrame;

	@FindBy(xpath = "//span[normalize-space()='View Account Details']//ancestor::button")
	public WebElement viewActDeatils;

	@FindBy(xpath = "//b[3]")
	public WebElement creditLimit;

	@FindBy(xpath = "//div[@class='ui-dialog ui-corner-all ui-widget ui-widget-content ui-front ui-dialog--apex t-Dialog-page--standard ui-draggable ui-dialog--stretch']//button[@title='Close'][normalize-space()='Close']")
	public WebElement closeviewAcFrame;

	@FindBy(xpath = "//table[@aria-label='Order History']//td[@headers='ORDER_NUMBER']//child::a")
	public WebElement orderNumber;

	@FindBy(xpath = "//table[@aria-label='Order History']//td[@headers='VIEW_DETAILS']//child::a")
	public WebElement viewOrderDetails;

	@FindBy(xpath = "//table[@aria-label]//tbody/tr/td[@headers='ITEM']")
	public List<WebElement> lineItems;
	
	
	
	
	
}
