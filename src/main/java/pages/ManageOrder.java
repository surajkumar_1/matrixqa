package pages;

import static base.BaseClass.manageOrder;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import base.BaseClass;

public class ManageOrder {
	WebDriver driver;

	public ManageOrder(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}

	/*
	 * Line Details Elements
	 */

	@FindBy(xpath = "//span[contains(text(),'Book Order')]//ancestor::button")
	public WebElement bookOrder;
	
	@FindBy(xpath = "//span[@id='P11_MESSAGE_DISPLAY']")
	public WebElement oracleMessage;

	@FindBy(xpath = "//span[@class='u-Processing-spinner']")
	public WebElement bigprocessEle;

	@FindBy(xpath = "//input[@id='P11_M_ORDER_NUMBER']")
	public WebElement orderNumberField;

	@FindBy(xpath = "//span[contains(text(),'Copy')]//ancestor::button")
	public WebElement copyOrderBtn;

	@FindBy(xpath = "//b[contains(text(),'Holds History')]//ancestor::button")
	public WebElement holdHistoryBtn;

	@FindBy(xpath = "//span[contains(text(),'Search')]//ancestor::button")
	public WebElement searchBtn;

	@FindBy(xpath = "//li[text()='Not a Valid Order.']")
	public WebElement invalidOrderMsg;
	
	@FindBy(xpath = "//span[contains(text(),'Clear Search')]//ancestor::button")
	public WebElement clearSearchBtn;

	@FindBy(xpath = "//span[contains(text(),'Create New Bill To')]//ancestor::button")
	public WebElement createNewBillToBtn;

	@FindBy(xpath = "//span[contains(text(),'Create New Ship To')]//ancestor::button")
	public WebElement createNewShipToBtn;

	@FindBy(xpath = "//span[contains(text(),'Change Bill')]")
	public WebElement changeBillToBtn;

	@FindBy(xpath = "//span[contains(text(),'Change Ship')]//ancestor::button")
	public WebElement changeShipToBtn;

	@FindBy(xpath = "//span[contains(text(),'Email')]//ancestor::button")
	public WebElement emailPreviewBtn;

	@FindBy(xpath = "//ul[@class='a-Notification-list htmldbUlErr']//li")
	public WebElement errorPopupText;

	@FindBy(xpath = "//h2[@class='a-Notification-title aErrMsgTitle']")
	public WebElement errorPopup;

	@FindBy(xpath = "//span[contains(text(),'Cancel Order')]//ancestor::button")
	public WebElement cancelOrderBtn;

	@FindBy(xpath = "//iframe[@title]")
	public WebElement iframe;

	@FindBy(xpath = "//div[@class='t-Alert-content']")
	public WebElement cancelledOrderMessageDisplay;
	
	@FindBy(xpath = "//span[normalize-space()='REDO Order']//parent::button")
	public WebElement redoOrder;
	/*
	 * Cancel order frame
	 */

	@FindBy(xpath = "//select[contains(@id,'CHANGE_CATEGORY')]")
	public WebElement selectChangeCategory;
	
	@FindBy(xpath = "//select[contains(@id,'_CHANGE_REASON')]")
	public WebElement selectCancelReason;
	
	@FindBy(xpath = "//span[contains(text(),'Submit')]//ancestor::button")
	public WebElement submitBtn;

	/*
	 * 
	 * Header Details Of Order
	 */

	@FindBy(xpath = "//span[@id='P11_CREATED_BY']")
	public WebElement orderCreatedBy;

	@FindBy(xpath = "//span[@id='P11_ABC_ORDER_NUMBER_DISPLAY']")
	public WebElement orderNumberABC;

	@FindBy(xpath = "//span[@id='P11_M2_CUSTOMER_NUMBER_DISPLAY']")
	public WebElement orderCustomerNumber;

	@FindBy(xpath = "//span[@id='P11_M2_CUSTOMER_NAME_DISPLAY']")
	public WebElement customerName;

	@FindBy(xpath = "//span[@id='P11_ORACLE_ORDER_DISPLAY']")
	public WebElement oracleOrderNumber;

	@FindBy(xpath = "//span[@id='P11_MATRIX_ORDER_DISPLAY']")
	public WebElement matrixOrderNumber;

	@FindBy(xpath = "//span[@id='P11_M2_SALESREP_DISPLAY']")
	public WebElement salesRep;

	@FindBy(xpath = "//span[@id='P11_M2_DATE_CREATED_DISPLAY']")
	public WebElement dateCreated;

	@FindBy(xpath = "//span[@id='P11_M2_CURRENCY_CODE_DISPLAY']")
	public WebElement currencyCode;

	@FindBy(xpath = "//span[@id='P11_M2_ORDER_TYPE_DISPLAY']")
	public WebElement orderType;

	@FindBy(xpath = "//span[@id='P11_M2_SHIP_METHOD_DISPLAY']")
	public WebElement shipMethod;

	@FindBy(xpath = "//input[@id='P11_M2_REQ_DATE']")
	public WebElement requestDate;

	@FindBy(xpath = "//button[@title='Update Request Date']")
	public WebElement updateRequestDateBtn;

	@FindBy(xpath = "//h2[@class='t-Alert-title']")
	public WebElement alertMessage;

	@FindBy(xpath = "//span[@class='a-IRR-pagination-label']")
	public WebElement totalLine;
	
	@FindBy(xpath = "//span[contains(text(),'Pay')]//ancestor::button")
	public WebElement payButton;
	
	
	@FindBy(xpath = "//button[@data-clipboard-source]")
	public WebElement pctNumber;
	
	//imprintLines
	@FindBy(xpath = "//img[contains(@alt,'uploads')]")
	public WebElement logoImage;
		
	@FindBy(xpath = "//span[contains(@class,'close')]")
	public WebElement close;
	
	public int getTotalLine() {
		return Integer.parseInt(String.valueOf(StringUtils.substringAfter(totalLine.getAttribute("textContent"),"of").trim()));
	}
	
	@FindBy(xpath = "//span[contains(@id,'_LINEITEMSTATUS_DISPLAY')]")
	public WebElement abcStatusHeader;
	
	@FindBy(xpath = "//span[contains(@id,'_ORDER_STATUS_DISPLAY')]")
	public WebElement oracleOrderStatus;
	
	@FindBy(xpath = "(//img[@class='apex-edit-page'])[1]")
	public WebElement viewImpAndLogo;
	
	@FindBy(xpath = "//td[text()='Mainitem']//following-sibling::td")
	public List<WebElement> mainItem;
	
	@FindBy(xpath = "//h2[text()='Cancelled Successfully.']")
	public WebElement cancelLineMsg;
	
	@FindBy(xpath = "//table[contains(@aria-label,'Region = Line Details_ A, Report = Primary Default, View')]//td[position()=5]")
	public List<WebElement> lineQty;
	
	@FindBy(xpath = "//table[contains(@aria-label,'Region = Line Details_ A, Report = Primary Default, View')]//td[position()=6]")
	public List<WebElement> lineItemPrice;
	
	@FindBy(xpath = "//button[@title='Next']")
	public WebElement nextButton;
	
	@FindBy(xpath = "//div[@class='t-Alert-body']")
	public WebElement updatePremiumProductText;
	
	@FindBy(xpath = "//button[contains(@title,'Close')]")
	public WebElement closeUpdateOrderPopup;
	
	@FindBy(xpath = "//span[contains(text(),'Cancel')]//parent::div/button")
	public WebElement closeCancelOrderPopup;
	
	@FindBy(xpath = "//span[@class='t-Icon fa fa-tags']")
	public WebElement viewLineNotes;
	
	@FindBy(xpath = "//textarea[contains(@name,'HEAD_NOTES')]")
	public WebElement lineNotes;
	
	@FindBy(xpath = "//button[contains(@id,'B256360259109190147')]")
	public WebElement saveLineNotesChanges;
	
	/*
	 * Add Dependent Line
	 */
	@FindBy(xpath = "//div[contains(@id,'R1504')]//button[contains(@class,'t-Button')]")
	public WebElement saveBtn4;

	@FindBy(xpath = "//button[contains(@id,'P118_AC_ITEM_lov_btn')]")
	public WebElement addChargesItem;
	
	@FindBy(xpath = "//input[contains(@id,'AC_Q')]")
	public WebElement addChargesQTY;
	
	@FindBy(xpath = "//input[contains(@id,'AC_P')]")
	public WebElement addChargesPrice;
	
	@FindBy(xpath = "//iframe[@title='Add Dependent Line']")
	public WebElement depLineFrame;
	
	@FindBy(xpath = "//span[text()='Add Dependent Line']//following-sibling::*[@title='Close' and text()='Close']")
	public WebElement closeDepLine;
	
	@FindBy(xpath = "//span[@class='fa fa-plus-square-o fa-2x fam-information fam-is-success']")
	public WebElement addDepLine;
	
	@FindBy(xpath = "//span[contains(@id,'_UPSELL_TOTAL') and text()]")
	public WebElement upsellTotal;
	
	@FindBy(xpath = "//td[contains(text(),'Logo Charge')]//preceding-sibling::td//child::input")
	public WebElement logoChargeinput;
	
	@FindBy(xpath = "//td[contains(text(),'Before Charge')]//preceding-sibling::td//child::input")
	public WebElement logoAsBeforeChargeinput;
	
	@FindBy(xpath = "(//input[@name='f04'])[2]")
	public WebElement logoCharge;
	
	@FindBy(xpath = "(//input[@name='f04'])[1]")
	public WebElement logoAsBeforeCharge;
	
	public void selectDependentLineType(String lineType) {
		driver.findElement(By.xpath("//label[contains(text(),'"+lineType+"')]")).click();
	}
	
	public WebElement selectDiscountPercentage(String discount) {
		return driver.findElement(By.xpath("//label[text()='"+discount+"%' and contains(@for, '_DISCOUNT')]"));
	}
	
	public WebElement getAddDepedentLineQTY(String line){
		return driver.findElement(By.xpath("(//td[contains(text(),'"+line+"')]//following-sibling::td)[3]"));
	}
	public WebElement getAddDepedentLinePrice(String line){
		return driver.findElement(By.xpath("(//td[contains(text(),'"+line+"')]//following-sibling::td)[4]"));
	}
	public String getDependentLineMsg(String msg) {
		return driver.findElement(By.xpath("//div[normalize-space()='"+msg+"']")).getAttribute("textContent");
	}
	
	/**
	 * Add Accessories
	 */
	@FindBy(xpath = "//input[contains(@id,'_ITEM') and @role]")
	public WebElement accessoryItem;
	
	@FindBy(xpath = "//input[contains(@id,'_QTY') and @size]")
	public WebElement accessoryQTY;
	
	@FindBy(xpath = "//input[contains(@id,'_PRICE') and @size]")
	public WebElement accessoryPrice;
	
	@FindBy(xpath = "//input[contains(@id,'_ITEM')]//ancestor::div[@class='']//child::*[text()='Save']//ancestor::button")
	public WebElement saveBtn;
	
	@FindBy(xpath = "//h2[text()='Logo Charges']//ancestor::div[@class and @style]//child::*[text()='Save']//ancestor::button")
	public WebElement saveBtn2;
	
	@FindBy(xpath = "//h2[text()='Discount']//ancestor::div[@class and @style]//child::*[text()='Save']//ancestor::button")
	public WebElement saveBtn3;
	
	
	public void checkMsgDepLineAdded(String depLineType, String msg ) {
		BaseClass.getDriver().switchTo().defaultContent();
		BaseClass.exceptionHandle.visibilityOf(manageOrder.oracleOrderNumber);
		BaseClass.wait.until(ExpectedConditions.elementToBeClickable(manageOrder.addDepLine)).click();
		BaseClass.wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(manageOrder.depLineFrame));
		manageOrder.selectDependentLineType(depLineType);
		Assert.assertEquals(msg,BaseClass.manageOrder.getDependentLineMsg(msg).trim());
		BaseClass.getDriver().switchTo().defaultContent();
		BaseClass.js.click(manageOrder.closeDepLine);
	}
	
	@FindBy(xpath = "//table[contains(@aria-label,'Addresses, Report = Primary Default')]//td[position()=3]")
	public List<WebElement> newAddressLine1;
	
	@FindBy(xpath = "//table[contains(@aria-label,'Addresses, Report = Primary Default')]//tr[position()>1]")
	public List<WebElement> addresses;
	
	@FindBy(xpath = "//td[contains(text(), 'Free')]")
	public WebElement freeLine;
	
	@FindBy(xpath = "//td[contains(text(), 'Upsell')]")
	public WebElement upSellLine;
	
	@FindBy(xpath = "//td[contains(text(), 'Discount')]" )
	public WebElement discountLine;
	
/**
 * Update Line Item
 * @param lineType
 * @return
 */
	public WebElement getUpdateLine(String lineType) {
		return driver.findElement(By.xpath("//td[text()='"+lineType+"']//following-sibling::td[position()=9]//img"));
	}
	
	@FindBy(xpath = "//input[contains(@id,'_ORDERED_QTY')]")
	public WebElement updateQty;
	
	@FindBy(xpath = "//input[contains(@id,'_ORDERED_PRICE')]")
	public WebElement updatePrice;	
	
	@FindBy(xpath = "//input[contains(@id,'P112_SHIPMENT_PRIORITY_0')]")
	public WebElement shipmentPriorityStandard;
	
	@FindBy(xpath = "//input[contains(@id,'P112_SHIPMENT_PRIORITY_1')]")
	public WebElement shipmentPriorityExpress;
	
	@FindBy(xpath = "//span[contains(@id,'P112_SHIPMENT_CHANGE_MSG_DISPLAY')]")
	public WebElement shipmentMessage;
	
	
	/**
	 * Cancel Line Item
	 * @param lineType
	 * @return WebElement
	 */
	public WebElement getCancelLine(String lineType) {
		return driver.findElement(By.xpath("//td[text()='"+lineType+"']//following-sibling::td[position()=10]//img"));
	}
	
	public WebElement getlLineStatus(String lineType) {
		return driver.findElement(By.xpath("(//td[text()='"+lineType+"']//following-sibling::td)[4]"));
	}
	
	public WebElement getAbcLineStatus(String lineType) {
		return driver.findElement(By.xpath("(//td[text()='"+lineType+"']//following-sibling::td)[6]"));
	}
	
	public WebElement getLineItemQty(String lineType) {
		return driver.findElement(By.xpath("(//td[text()='"+lineType+"']//following-sibling::td)[1]"));
	}
	
	public WebElement getLineItemPrice(String lineType) {
		return driver.findElement(By.xpath("(//td[text()='"+lineType+"']//following-sibling::td)[2]"));
	}
	public List<WebElement> getAllLineItemNameWithoutCancelledstatus() {
		return driver.findElements(By.xpath("//table[contains(@id,'229202534016051411_orig')]//tr/td[8][not(contains(text(),'CANCELLED'))]//preceding-sibling::td[6]"));
		}
	
	public List<WebElement> getAllLineItemPriceWithoutCancelledstatus() {
		return driver.findElements(By.xpath("//table[contains(@id,'229202534016051411_orig')]//tr/td[8][not(contains(text(),'CANCELLED'))]//preceding-sibling::td[2]"));
		}
	
	public List<WebElement> getAllLineItemQuantityWithoutCancelledstatus() {
		return driver.findElements(By.xpath("//table[contains(@id,'229202534016051411_orig')]//tr/td[8][not(contains(text(),'CANCELLED'))]//preceding-sibling::td[3]"));
		}
	
	public List<WebElement> getAllLineTypeWithoutCancelledstatus() {
		return driver.findElements(By.xpath("//table[contains(@id,'229202534016051411_orig')]//tr/td[8][not(contains(text(),'CANCELLED'))]//preceding-sibling::td[4]"));
		}
	@FindBy(xpath = "//div[contains(@id,'P0_TIMER_CONTAINER')]")
	public WebElement timer;
	
	@FindBy(xpath = "//span[contains(text(),'Fax')]//ancestor::button")
	public WebElement faxPreviewBtn;
	
	@FindBy(xpath = "//span[contains(text(),'Save')]//ancestor::button")
	public WebElement saveBtnChangeBillToShipTo;

	@FindBy(xpath = "//span[contains(text(),'Cancel')]//ancestor::button")
	public WebElement cancelBtn;

	@FindBy(xpath = "//textarea[contains(@id,'COMMENTS')]")
	public WebElement cancellationComments;
	
	
	@FindBy(xpath = "//h2[normalize-space()='Cancelled Successfully.']")
	public WebElement cancelledSuccessMsg;

	/*
	 * Hold
	 */

	@FindBy(xpath = "//span[contains(@id,'APPLIED_HOLDS_DISPLAY')]")
	public WebElement orderHolds;
	
	@FindBy(xpath = "//span[contains(text(),'Manage Holds')]//ancestor::button")
	public WebElement manageHold;

	@FindBy(xpath = "//span[contains(@id,'_CURRENT_HOLDS_DISPLAY')]")
	public WebElement currentAppliedHold;

	// label[text()='CSR APPROVAL']
//	public WebElement selectRemoveHold(String holdname) {
//		return driver.findElement(By.xpath("//label[text()='" + holdname + "']"));
//	}

	@FindBy(xpath = "//textarea[contains(@id,'_APPLY_COMMENTS')]")
	public WebElement enterCommentForHold;

	@FindBy(xpath = "//textarea[contains(@id,'_RELEASE_COMMENTS')]")
	public WebElement releaseCommentForHold;

	@FindBy(xpath = "//span[contains(text(),'Remove Hold')]//ancestor::button")
	public WebElement removeHold;

	@FindBy(xpath = "//b[contains(text(),'Cancellation History')]//ancestor::button")
	public WebElement cancellationHistoryBtn;
	
	@FindBy(xpath = "//table[contains(@aria-label,'Region = Order Cancellation Details Report, Report')]")
	public WebElement cancellationHistoryTable;
	
	@FindBy(xpath = "//span[contains(text(),'Apply Hold')]//ancestor::button")
	public WebElement applyHoldBtn;

	@FindBy(xpath = "//*[text()='Applied']//ancestor::td//following-sibling::td[position()=1]")
	public List<WebElement> holdHistAppiedHoldName;



	@FindBy(xpath = "//b[contains(text(),'Holds Legend')]//ancestor::button")
	public WebElement holdsLegendBtn;

	@FindBy(xpath = "//table[contains(@aria-label,'Region = Holds Legend Data')]//td[position()=1]")
	public List<WebElement> holdsLegendHoldName;
	
	@FindBy(xpath = "//select[@id='P11_SHIP_CONTACT']")
	public WebElement selectShipContact;
	
	@FindBy(xpath = "//select[@id='P11_BILL_CONTACT']")
	public WebElement selectBillContact;
	
	@FindBy(xpath = "//i[text()='Bill To Contact']//ancestor::div[@class='t-Region-body a-Collapsible-content']")	
	public WebElement billToAddress;	
		
	@FindBy(xpath = "//div[@id='a_Collapsible2_shipTo_content']")	
	public WebElement shipToAddress;	
		
	@FindBy(xpath = "//td[normalize-space()='Shipping']//following-sibling::td[2]")	
	public WebElement shippingCharge;	
		
	@FindBy(xpath = "(//span[contains(@id,'P11_TOTAL')])[1]")	
	public WebElement total;
	
	@FindBy(xpath = "(//span[contains(@id,'P11_SUBTOTAL')])[1]")	
	public WebElement subTotal;
	
	@FindBy(xpath = "(//span[contains(@id,'TAX')])[1]")	
	public WebElement tax;
	
	@FindBy(xpath = "(//span[contains(@id,'_CHARGES_1')])[1]")	
	public WebElement orderCharges;
	
	/**
	 * Redo Order
	 */
	
	@FindBy(xpath = "//input[@type='text' and contains(@name,'IMPRINT_')]")
	public List<WebElement> imprintLines;
	
	@FindBy(xpath = "//span[normalize-space()='Redo Order']//parent::button")
	public WebElement placeredoOrder;
	
	/**
	 * View Imprint and logo Page
	 */
	
	@FindBy(xpath = "//span[contains(@id,'P147_NO_IMPRINT_MSG')]//ancestor::div[@class='t-Form-itemWrapper']")	
	public WebElement imp;
	
	@FindBy(xpath = "//input[@type='radio' and contains(@id,'P147_UPLOAD_LOGO_N')]")
	public WebElement logo;
	
	@FindBy(xpath = "//span[contains(text(),'Cancel')]//ancestor::button")
	public WebElement cancel;
}