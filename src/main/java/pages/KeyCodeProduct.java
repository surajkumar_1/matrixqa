package pages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.ListIterator;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import base.BaseClass;

public class KeyCodeProduct {

	WebDriver driver;
   
    public KeyCodeProduct(WebDriver driver) {

		PageFactory.initElements(driver, this);

		this.driver = driver;
	}

	@FindBy(xpath = "//span[normalize-space()='Choose Design Template']//parent::button")
	public WebElement chooseTemplate;

	@FindBy(xpath = "//input[@id='oj-inputsearch-input-P188_SEARCH_TEMPLATE_HIDDEN']")
	public WebElement templateName;

	@FindBy(xpath = "//button[@id='searchtemplate']")
	public WebElement searchTemplate;

	@FindBy(xpath = "//input[contains(@id,'_ROUTE_TO_LEGACY') and @type='checkbox']")
	public WebElement productAndPrintLegacy;

	@FindBy(xpath = "//span[@class='fa fa-hand-o-up']")
	public By temp;

	@FindBy(xpath = "//span[@class='fa fa-hand-o-up']")
	public WebElement selectTemplate;

	@FindBy(xpath = "//input[contains(@id,'_ANNIVERSARY') and @type='text']")
	public WebElement anniversary;

	@FindBy(xpath = "//span[contains(text(),'Clear Text')]/ancestor::button")
	public WebElement clearImprint;

	@FindBy(xpath = "//div[@id='SelectImprintColour']//child::div/span")
	public WebElement imprintColor;

	@FindBy(xpath = "//span[@class='t-Form-itemText t-Form-itemText--post']//span")
	public WebElement fullColor;

	@FindBy(xpath = "//label[contains(@for,'UPLOAD_LOGO_Y')]")
	public WebElement uploadLogoYes;

	@FindBy(xpath = "//input[contains(@id,'_UPLOAD_NEW_LOGO_0')]")
	public WebElement newLogo;

	@FindBy(xpath = "//input[contains(@id,'_LOGO_AS_BEFORE_0')]")
	public WebElement logoAsBefore;

	@FindBy(xpath = "//input[contains(@id,'_IMPRINT_LOGO') and @type='file']")
	public WebElement inputLogo;

	@FindBy(xpath = "//button[@id='uploadlogobtn']")
	public WebElement uploadLogo;

	@FindBy(xpath = "//input[contains(@id,'_QTY_SELECTION_1')]")
	public WebElement customQty;

	@FindBy(xpath = "//input[contains(@id,'_CUSTOM_QUANTITY')]")
	public WebElement inputCustQty;

	@FindBy(xpath = "//b[contains(text(),'Set Quantity')]/ancestor::button")
	public WebElement setQty;

	@FindBy(xpath = "//ul[@class='a-Notification-list htmldbUlErr']/li")
	public List<WebElement> errorList;

	@FindBy(xpath = "//button[@title='Close Notification']")
	public WebElement closeError;

	@FindBy(xpath = "//input[contains(@id,'discounted-setup-charge')]")
	public WebElement discountedSetupCharge;

	@FindBy(xpath = "//td[contains(text(),'Discounted Setup Charge')]/following-sibling::td/input")
	public WebElement discountedSetupPrice;

	@FindBy(xpath = "//input[contains(@id,'discounted-logo-charge')]")
	public WebElement discountedLogoCharge;

	@FindBy(xpath = "//td[contains(text(),'Discounted Logo Charge')]/following-sibling::td/input")
	public WebElement discountedLogoPrice;

	@FindBy(xpath = "//input[@id='IDCHKBX1']")
	public WebElement splPackagingCharge;

	@FindBy(xpath = "//input[@id='IDPUQTY1']")
	public WebElement splPkgChargeQty;

	@FindBy(xpath = "//span[contains(@id,'PER_UNIT_EXTRA_CHARGES')]")
	public WebElement perUnitExtraCharges;

	@FindBy(xpath = "//img[contains(@id,'PREMIUM_IMAGE')]")
	public WebElement premiumProduct;

	@FindBy(xpath = "//span[@id = 'P136_UPSELL_QTY']")
	public WebElement upsellQty;

	@FindBy(xpath="//td[@class= ' u-tL'][2]//a[@href]")
	public WebElement url_template;

	public void productOptions() {
		ListIterator<WebElement> options = driver.findElements(By.xpath("//select[contains(@id,'_ATTRIBUTE_')]"))
				.listIterator();
		int index = 1;
		while (options.hasNext()) {
			WebElement opt = options.next();
			BaseClass.wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(
					By.xpath("(//select[contains(@id,'_ATTRIBUTE_')])[" + index + "]/option"), 1));
			BaseClass.select.selectByIndex(opt, BaseClass.select.getOptions(opt).size() - 1);
			++index;
		}
	}

	public void imprintLines() throws InterruptedException {
		StringBuilder ch = new StringBuilder();
		int line = 0;
		int len = Integer.parseInt(BaseClass.CustProd.imprintLeng.getAttribute("textContent").replaceAll("[^0-9]", ""));
		for (int i = 0; i < len; i++) {
			ch.append("a");
		}
		BaseClass.js.click(clearImprint);
		List<WebElement> imprints = driver.findElements(By
				.xpath("//div[@class='t-Form-itemWrapper']//child::input[contains(@id,'IMPRINT_' ) and @type='text']"));
		ListIterator<WebElement> imprint = imprints.listIterator();
		if (!imprints.isEmpty()) {
			BaseClass.js.click(clearImprint);
			while (imprint.hasNext()) {
				WebElement impline = imprint.next();
				impline.sendKeys(ch);
				assertEquals(len, impline.getAttribute("value").length());
			}

			clearImprint.click();
			imprint = imprints.listIterator();
			while (imprint.hasNext()) {
				line += 1;
				imprint.next().sendKeys("Imprint Line " + line);
			}
		}

		imprintPrevieStyleValidation();
	}

	public void imprintPrevieStyleValidation() throws InterruptedException {
		List<WebElement> impPreview = driver.findElements(By.xpath("//div[contains(text(),'Imprint')]"));
		Thread.sleep(5000);
		for (int i = 0; i < boldImprint.size(); i++) {
			if (boldImprint.get(i).isEnabled()) {
				BaseClass.js.click(boldImprint.get(i));
				assertTrue("bold not working or line is missing",
						impPreview.get(i).getAttribute("style").contains("font-weight: bold;"));
			}
			if (italicImprint.get(i).isEnabled()) {
				BaseClass.js.click(italicImprint.get(i));
				assertTrue("italic not working or line is missing",
						impPreview.get(i).getAttribute("style").contains("font-style: italic;"));
			}
			if (underlineImprint.get(i).isEnabled()) {
				BaseClass.js.click(underlineImprint.get(i));
				assertTrue("underline not working or line is missing",
						impPreview.get(i).getAttribute("style").contains("text-decoration: underline;"));
			}
		}

	}

	@FindBy(xpath = "//label[contains(@id,'IMPRINT_1_LABEL')]//child::font[text()]")
	public WebElement imprintLeng;

	@FindBy(xpath = "//input[contains(@id,'_BOLD_LINE')]")
	public List<WebElement> boldImprint;

	@FindBy(xpath = "//input[contains(@id,'_ITALIC_LINE')]")
	public List<WebElement> italicImprint;

	@FindBy(xpath = "//input[contains(@id,'_UNDERLINE_LINE')]")
	public List<WebElement> underlineImprint;

	@FindBy(xpath = "//input[contains(@id,'PROOF_REQUIRED_Y')]")
	public WebElement proofReqiuredYes;

	/**
	 * Vendor Product
	 */

	@FindBy(xpath = "//input[contains(@id,'_QTY')]")
	public WebElement vendorProdQty;

	@FindBy(xpath = "//input[contains(@id,'_PRICE')]")
	public WebElement vendorProdPrice;

	@FindBy(xpath = "//input[contains(@id,'_QUOTE_ID')]")
	public WebElement vendorQutId;

	@FindBy(xpath = "//input[contains(@id,'_PROD_CODE')]")
	public WebElement vendorProdCode;

	@FindBy(xpath = "//input[contains(@id,'_PRINT_PROCESS')]")
	public WebElement vendorProdPrint;

	@FindBy(xpath = "//input[contains(@id,'_PROD_NAME')]")
	public WebElement vendorProdName;

	@FindBy(xpath = "//input[contains(@id,'_PRODUCT_COLOR')]")
	public WebElement vendorProdColor;

	@FindBy(xpath = "//input[contains(@id,'_LOGO_COLOR')]")
	public WebElement vendorProdLogoColor;

	@FindBy(xpath = "//input[contains(@id,'_IMPRINT_CLR')]")
	public WebElement vendorProdImpColor;

	@FindBy(xpath = "//input[contains(@id,'_IMPRINT_POSITION')]")
	public WebElement vendorProdImpPos;

	@FindBy(xpath = "//input[contains(@id,'_SHIPPING_CHARGE')]")
	public WebElement vendorProdShipChg;

	@FindBy(xpath = "//*[contains(text(),'Return To Product Search')]/ancestor::button")
	public WebElement returnToProductSearch;

	@FindBy(xpath = "//b[contains(text(),'Cancel Order')]/ancestor::button")
	public WebElement cancelOrderBtn;

	@FindBy(xpath = "//b[contains(text(),'Refresh Total ')]/ancestor::button")
	public WebElement refreshTotalBtn;

	@FindBy(xpath = "//span[contains(text(),'dd Template')]/ancestor::button")
	public WebElement addTemplInternalNotes;

	@FindBy(xpath = "//span[contains(text(),'Clear Template')]/ancestor::button")
	public WebElement clearTemplInternalNotes;

	@FindBy(xpath = "//td[contains(text(),'ACC')]//preceding-sibling::td[position()=2]")
	public WebElement selectAccessorie;

	public void selectAccessory(String accessory) {
		driver.findElement(
				By.xpath("//td[contains(text(),'" + accessory + "')]//preceding-sibling::td[position()=2]/input"))
				.click();
	}

	public void setAccesssoryQty(String accessory, String qty) {
		WebElement accQty = driver.findElement(By.xpath(
				"//td[contains(text(),'" + accessory + "')]//following::input[@class='clsAccQty' and position()=1]"));
		accQty.clear();
		accQty.sendKeys(qty);
	}

	public WebElement freeQty(String value) {
		return driver.findElement(By.xpath(
				"//div[contains(@id,'_FREE_CONTAINER')]//input[@value='" + value + "']/following-sibling::label"));
	}

	public WebElement discountPercent(String value) {

		return driver.findElement(
				By.xpath("//div[contains(@id,'_DISCOUNT')]//input[@value='" + value + "']/following-sibling::label"));
	}

	@FindBy(xpath = "//span[contains(@class,'is-selected')]//parent::th//following-sibling::td[1]")
	public WebElement selectedMainLineQty;

	public WebElement getAccessoryLineTotal(String accessory) {
		return driver.findElement(By.xpath("//table[contains(@id,'12_orig')]//td[contains(text(),'" + accessory
				+ "')]//following-sibling::td[4]/input"));
	}

	public WebElement getSelectedAccessoryQuantity(String accessory) {
		return driver.findElement(By.xpath("//table[contains(@id,'12_orig')]//td[contains(text(),'" + accessory
				+ "')]//following-sibling::td[2]/input"));
	}

	public WebElement getAccessoryProductName(String accessory) {
		return driver.findElement(By.xpath("//table[contains(@id,'12_orig')]//td[contains(text(),'" + accessory
				+ "')]//following-sibling::td[1]"));
	}

	public void selectAccessoryByName(String accessory) {
		driver.findElement(By.xpath("//td[text()='" + accessory + "']//preceding-sibling::td[position()=1]/input"))
				.click();
	}

	@FindBy(xpath = "//button[@id='np_checkout_button']")
	public WebElement checkOutBtn;

	@FindBy(xpath = "//button[normalize-space()='OK']")
	public WebElement okBtn;

	@FindBy(xpath = "//textarea[contains(@name,'PRODUCTION_LINE_NOTES')]")
	public WebElement productionNotes;

	@FindBy(xpath = "//textarea[contains(@name,'INTERNAL_NOTES')]")
	public WebElement internalNotes;

	@FindBy(xpath = "//td[contains(text(),'Logo Charge')]/preceding-sibling::td/input")
	public WebElement logoCharge;

	@FindBy(xpath = "//td[contains(text(),'Setup Charge')]/preceding-sibling::td/input")
	public WebElement setupCharge;

	@FindBy(xpath = "//td[contains(text(),'Logo as Before Charge')]/preceding-sibling::td/input")
	public WebElement logoAsBeforeCharge;

	@FindBy(xpath = "//label[contains(@for,'UPSELL_FLAG_1')]")
	public WebElement upsellYes;

	@FindBy(xpath = "//input[contains(@id,'_UPSELL_FLAG_0')]")
	public WebElement upsellNo;

	@FindBy(xpath = "//input[@class='clsAccQty']")
	public WebElement accessorieQty;

	@FindBy(xpath = "//span[contains(@id,'ACCESSORIES_SUM')]")
	public WebElement totalAccessorieCharges;

	@FindBy(xpath = "//label[contains(@for,'_HOLD')]")
	public List<WebElement> holds;

	/*
	 * PresetQTY
	 */
	@FindBy(xpath = "//tr[@data-rownum]")
	public List<WebElement> totalRows;

	@FindBy(xpath = "//tr[@data-rownum]/td[1]")
	public List<WebElement> qtycoloum;

	/*
	 * Charges Columns
	 */

	@FindBy(xpath = "//label[normalize-space()='Product Total']//parent::div//following-sibling::div//span[contains(@class,'display_only apex-item')]")
	public WebElement productTotal;

	@FindBy(xpath = "//span[contains(@id,'DISCOUNT_CHARGES')]")
	public WebElement discountCharges;

	@FindBy(xpath = "//span[contains(@id,'EXTRA_CHARGES')]")
	public WebElement extraCharges;

	@FindBy(xpath = "//textarea[contains(@id,'_SPECIAL_REQUEST')]")
	public WebElement specialInst;

	@FindBy(xpath = "//span[contains(@id,'_TOTAL_WITHOUT_TAX')]")
	public WebElement totalWithOutTax;

	@FindBy(xpath = "//button[normalize-space()='Cancel']")
	public WebElement orderCancel;

	@FindBy(xpath = "//span[contains(@id,'ACCESSORIES_CHARGES')]")
	public WebElement accessoriesCharges;

	@FindBy(xpath = "//span[contains(@id,'SHIPPING') and text()]")
	public WebElement shippingCharges;

	@FindBy(xpath = "//span[contains(@id,'HANDLING')]")
	public WebElement handlingCharges;

	@FindBy(xpath = "//label[normalize-space()='Product Total']//following::label[contains(@id,'SUBTOTAL')]//parent::div//following-sibling::div//span[contains(@class,'display_only apex-item')]")
	public WebElement subTotal;

	@FindBy(xpath = "(//span[contains(@class,'display_only apex-item-display-only') and contains(@id, '_TAX')])[1]")
	public WebElement tax;

	@FindBy(xpath = "//span[contains(@id,'TOTAL_WITH_TAX')]")
	public WebElement totalWithTax;

	@FindBy(xpath = "//span[contains(text(),'Processing')]")
	public WebElement processEle;

	@FindBy(xpath = "//span[@class='u-Processing-spinner']")
	public WebElement processSpinner;

	@FindBy(xpath = "//input[contains(@id,'RGB_COLOR_PICKER')]")
	public WebElement colorBox;

	@FindBy(xpath = "//label[contains(@for,'NO_IMPRINT_0')]")
	public WebElement checkButtonNoImprint;

	@FindBy(xpath = "//input[@id='rgb_color_picker' and @type = 'color']")
	public WebElement colorPick;
	// ***Logo scrap Order
	@FindBy(xpath = "//button[contains(@class, 't-Button t-Button--icon t-Button--warning t-Button--iconLeft')]")
	public WebElement sendToArtDeptBtn;

	@FindBy(xpath = "//button[contains(@class, 't-Button t-Button--icon t-Button--primary t-Button--iconRight')]")
	public WebElement saveLogobtn;

	@FindBy(xpath = "//div[contains(@id,'ui-id-')]")
	public WebElement scrapLogoPopup;

	public void selectFullColor() throws InterruptedException, AWTException {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		// Colour Value needs to be passed from the data sheet change later.
		js.executeScript("document.getElementById('rgb_color_picker').value='#971717'");
		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[contains(@id,'RGB_COLOR_PICKER')]")).sendKeys("#971717");
		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[contains(@id,'RGB_COLOR_PICKER')]")).sendKeys(Keys.TAB, Keys.TAB,
				Keys.ENTER);
		Robot robot = new Robot();
		Thread.sleep(1000);
		robot.keyPress(KeyEvent.VK_DOWN);
		Thread.sleep(1000);
		robot.keyRelease(KeyEvent.VK_DOWN);
	}

	@FindBy(xpath = "//span[contains(@id,'_CUSTOM_DISPLAY')]//tr")
	public List<WebElement> customQtyValues;

	// **Shipping Details NA

	@FindBy(xpath = "//select[contains(@id,'_SHIP')]")
	public WebElement selectShipMethod;

	@FindBy(xpath = "//input[contains(@id,'_SHIPPING_PRICE')]")
	public WebElement shipPrice;

	@FindBy(xpath = "//textarea[contains(@id,'_SHIPPING_INSTRUCT')]")
	public WebElement shipInstruct;

}
