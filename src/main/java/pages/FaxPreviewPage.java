package pages;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import base.BaseClass;

public class FaxPreviewPage {

	public FaxPreviewPage(WebDriver driver) {

		PageFactory.initElements(driver, this);

	}

	@FindBy(xpath = "//table//span")
	public List<WebElement> orderDetails;

	@FindBy(xpath = "//font[@color='white'][normalize-space()='総合計']//parent::td//following-sibling::td")
	public WebElement totalprice;

	@FindBy(xpath = "//font[@color='white'][normalize-space()='小計']//parent::td//following-sibling::td")
	public WebElement subtotalprice;

	@FindBy(xpath = "//font[@color='white'][normalize-space()='消費税']//parent::td//following-sibling::td")
	public WebElement tax;

	@FindBy(xpath = "//p[contains(text(),'Please click anywhere on the preview below to copy')]")
	public WebElement emailMsg;

	// Order details table

	@FindBy(xpath = "(//b[text()='ご注文内容']//ancestor::table//following-sibling::table)[1]")
	public WebElement orderDetailsTable;

	@FindBy(xpath = "(//b[text()='ご注文内容']//ancestor::table//following-sibling::table)[1]//tr[position()>2]//td[2]")
	public List<WebElement> productDescription;

	@FindBy(xpath = "(//b[text()='ご注文内容']//ancestor::table//following-sibling::table)[1]//tr[position()>2]//td[3]")
	public List<WebElement> productPrice;

	@FindBy(xpath = "(//b[text()='ご注文内容']//ancestor::table//following-sibling::table)[1]//tr[position()>2]//td[4]")
	public List<WebElement> productQuantity;

	@FindBy(xpath = "(//b[text()='ご注文内容']//ancestor::table//following-sibling::table)[1]//tr[position()>2]//td[5]")
	public List<WebElement> lineTotal;
	
	public WebElement getProductQty(String description) {	
	 	return BaseClass.getDriver().findElement(By.xpath(
				"(//b[text()='ご注文内容']//ancestor::table//following-sibling::table)[1]//tr[position()>2]//td[2][contains(text(),'"
						+ description + "')]//following-sibling::td[2]"));
		
	}

	public WebElement getProductPrice(String description) {
		return BaseClass.getDriver().findElement(By.xpath(
				"(//b[text()='ご注文内容']//ancestor::table//following-sibling::table)[1]//tr[position()>2]//td[2][contains(text(),'"
						+ description + "')]//following-sibling::td[1]"));
	}

	public WebElement getProductTotal(String description) {
		return BaseClass.getDriver().findElement(By.xpath(
				"(//b[text()='ご注文内容']//ancestor::table//following-sibling::table)[1]//tr[position()>2]//td[2][contains(text(),'"
						+ description + "')]//following-sibling::td[3]"));
	}

	// Fax Template

	@FindBy(xpath = "//span[contains(@id,'P85_CUSTOMER_NUMBER_DISPLAY')]")
	public WebElement customerNumber;

	@FindBy(xpath = "//span[contains(@id,'P85_ORDER_NUMBER')]")
	public WebElement orderNumber;


}
