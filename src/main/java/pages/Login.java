
/*
 * Author- Sumit.kumar@pens.com
 * 
 */
package pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Login {

	public Login(WebDriver driver) {

		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//input[contains(@id,'_USERNAME')]")
	public WebElement loginID;

	@FindBy(xpath = "//input[contains(@id,'_PASSWORD')]")
	public WebElement password;

	@FindBy(xpath = "//button[@class='t-Button t-Button--hot ']")
	public WebElement signinBtn;

	@FindBy(xpath = "//li[@class='t-NavigationBar-item has-username']//button")
	public WebElement loggedInUser;

	@FindBy(xpath = "//div[@class='t-Alert-body']//p")
	public WebElement errorMessage;

	@FindBy(xpath = "//span[@class='a-Menu-labelContainer']//a[normalize-space()='Sign Out']")
	public WebElement signOutBtn;
	
	@FindBy(xpath = "//div[@class='section_container flex_container flex_center']/a")
	public List <WebElement> trainingLinks;

}
