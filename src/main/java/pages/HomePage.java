
/*
 * Author- sumit.kumar@pens.com
 * 
 */
package pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {

	public HomePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}


	@FindBy(xpath = "//h2[text()='Manage Customers']")
	public WebElement manageCustomerBtn;

	@FindBy(xpath = "//div[@class='feature flex_feature_item feature3']/h2")
	public WebElement placeOrder;
	
	@FindBy(xpath = "//div[@class='feature flex_feature_item feature4']/h2")
	public WebElement manageOrder;
	
	@FindBy(xpath = "//div[contains(@id,'t_TreeNav')]/ul/li")
	public List<WebElement> options;

}
