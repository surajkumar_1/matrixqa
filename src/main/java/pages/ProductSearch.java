/*
 * Author- Suraj and Sumit
 */
package pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProductSearch {

	public ProductSearch(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//input[@id='P120_KEYCODE']")
	public WebElement keycode;
	
	@FindBy(xpath = "//input[@id='P120_KEYCODE_error']")
	public WebElement keycodeError;

	@FindBy(xpath = "//input[@id='P120_NAME_DESCRIPTION']")
	public WebElement nameDescription;

	@FindBy(xpath = "//input[@id='P120_PRODUCT_CODE']")
	public WebElement productCode;

	@FindBy(xpath = "//span[@class='t-Button-label']//b[text()='Add Accessories Product']")
	public WebElement addAccessories;

	@FindBy(xpath = "//b[text()='Add Non-Marketed Vendor Product']//ancestor::button")
	public WebElement addNonMarketedVenProd;

	@FindBy(xpath = "//span[normalize-space()='Clear']/parent::button")
	public WebElement clearBtn;

	@FindBy(xpath = "//span[normalize-space()='Filter']/parent::button")
	public WebElement filterBtn;

	@FindBy(xpath = "//select[@id='P120_PRODUCT_CATEGORY_LEVEL1']")
	public WebElement productCategoryLvl1;

	@FindBy(xpath = "//select[@id='P120_PRODUCT_CATEGORY_LEVEL2']")
	public WebElement productCategoryLvl2;

	@FindBy(xpath = "//table[contains(@aria-label,'Region = In-House Products')]//tr[position()=2]//td[position()=2]/a")
	public WebElement searchProductName;
	
	@FindBy(xpath = "//table[contains(@aria-label,'Region = In-House Products')]//tr[position()=2]//td[position()=4]")
	public WebElement searchProductFamily;
	
	@FindBy(xpath = "//table[contains(@aria-label,'Region = In-House Products')]//tr[position()=2]//td[position()=5]")
	public WebElement searchProductDecoration;
	
	@FindBy(xpath = "//table[contains(@aria-label,'Region = In-House Products')]//tr[position()=2]//td[position()=3]//span")
	public WebElement searchProductDescription;

	@FindBy(xpath = "//span[@class='a-IRR-noDataMsg-text']/font/h4")
	public WebElement noProductFound;

	@FindBy(xpath = "//h4")
	public WebElement errorMessage;

	@FindBy(xpath = "//ul[@class='a-Notification-list htmldbUlErr']/li")
	public WebElement errorMessageWrongProduct;

	@FindBy(xpath = "//select[contains(@id,'_DECO')]")
	public WebElement selectDecoration;

	/*
	 * Vendor product
	 */
	
	@FindBy(xpath = "//a[contains(text(),'PLACE SAMPLE ORDER')]")
	public WebElement sampleOrder;

	@FindBy(xpath = "//table[contains(@aria-label,' Products | Results Search, Report')]//tr[position()=2]//td[position()=2]/a")
	public WebElement searchProductNameVendor;

	@FindBy(xpath = "//table[contains(@aria-label,' Products | Results Search, Report')]//tr[position()=2]//td[position()=4]")
	public WebElement searchProductFamilyVendor;

	@FindBy(xpath = "//table[contains(@aria-label,' Products | Results Search, Report')]//tr[position()=2]//td[position()=5]")
	public WebElement searchProductDecoVendor;

	@FindBy(xpath = "//iframe[contains(@title,'Description Search')]")
	public WebElement frameDescriptionSearch;

	@FindBy(xpath = "//table[contains(@aria-label,' Vendor Products, Report')]//tr[position()=2]//td[position()=1]/a")
	public WebElement productCodeDescriptionSearch;

	@FindBy(xpath = "//table[contains(@aria-label,' Vendor Products, Report')]//tr[position()=2]//td[position()=2]")
	public List<WebElement> productNameDescriptionSearch;
	
	@FindBy(xpath = "//span[@class='a-Icon icon-irr-no-results']")
	public WebElement searchIcon;
}
