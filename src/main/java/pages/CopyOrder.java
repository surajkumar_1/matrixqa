package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CopyOrder {

	WebDriver driver;

	public CopyOrder(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;

	}
	
	@FindBy(xpath = "//span[contains(@id,'_COPY_ORDER_NUMBER_DISPLAY')]")
	public WebElement copyFrom;

	@FindBy(xpath = "//select[contains(@id,'_COPY_SALESREP_ID')]")
	public WebElement salesRep;

	@FindBy(xpath = "//input[contains(@id,'_SALESREP_EMAIL')]")
	public WebElement copySalesRepEmail;
	
	@FindBy(xpath = "//select[contains(@id,'_COPY_ORDER_TYPE')]")
	public WebElement orderType;
	
	@FindBy(xpath = "//span[@class='u-Processing-spinner']")
	public WebElement processEle;

	@FindBy(xpath = "//input[contains(@id,'_COPY_REQUEST_DATE|input')]")
	public WebElement requestDate;

	@FindBy(xpath = "//span[contains(text(),'Cancel')]//ancestor::button")
	public WebElement cancelOrder;

	@FindBy(xpath = "//span[contains(text(),'Copy Order')]//ancestor::button")
	public WebElement copyOrderBtn;

	@FindBy(xpath = "//button[normalize-space()='OK']")
	public WebElement okBtn;

	@FindBy(xpath = "//span[contains(text(),'Cancel')]//ancestor::button")
	public WebElement cancelBtn;

	@FindBy(xpath = "(//span[contains(@id,'_IMPRINT_') and text()])[2]")
	public WebElement copyOrderImp;
	
	@FindBy(xpath = "//img[contains(@id,'imprint_src')]")
	public WebElement copyOrderImg;
	
	@FindBy(xpath = "//div[@id='preview_line_1']")
	public WebElement copyImpPre;
	
	@FindBy(xpath = "//h2[normalize-space()='Order Copied Successfully.']")
	public WebElement copySuccessMsg;
	
	@FindBy(xpath = "//li[@class='a-Notification-item htmldbStdErr']")
	public WebElement errorPopup;
	
	@FindBy(xpath = "(//span[contains(text(),'of')])[1]")
	public WebElement lineItemsCount;
	

}
