package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AccessoriesOnly {

	WebDriver driver;

	public AccessoriesOnly(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}

//	@FindBy(xpath = "//span[contains(@id,'P177_TOTAL_WITH_TAX')]//following-sibling::span[@class='u-Processing u-Processing--inline']")
//	public WebElement processEle;

//	@FindBy(xpath = "//input[contains(@id,'_ACCESSORIES')]")
//	public WebElement accessories;
	/**
	 * Search Accessories
	 */
	@FindBy(xpath = "//button[contains(@id,'_ACCESSORIES_BUTTON')]")
	public WebElement serachAcc;

	@FindBy(xpath = "//input[contains(@id,'_ACCESSORIES_SEARCH')]")
	public WebElement accessoryName;

	@FindBy(xpath = "//input[contains(@id,'_MAIN_QUANTITY')]")
	public WebElement productQty;

	@FindBy(xpath = "//input[contains(@id,'_MAIN_ITEM_PRICE')]")
	public WebElement productPrice;

	@FindBy(xpath = "//input[contains(@id,'_SHIPPING_CHARGE')]")
	public WebElement shipCharge;

	public void selectAccessory(String accessory) {

		driver.findElement(By.xpath("//*[contains(text(),'" + accessory + "') and @class]")).click();

	}
	
	//**Shipping Details NA
	
	@FindBy(xpath = "//select[contains(@id,'_SHIP_METHOD')]")
	public WebElement selectShipMethod;
	
	@FindBy(xpath = "//input[contains(@id,'_SHIPPING_PRICE')]")
	public WebElement shipPrice;
	
	@FindBy(xpath = "//textarea[contains(@id,'_SHIPPING_INSTRUCT')]")
	public WebElement shipInstruct;

}
