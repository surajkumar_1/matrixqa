package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import base.BaseClass;

public class PreviewDatailsPage {

	
	public PreviewDatailsPage(WebDriver driver) {
		
		PageFactory.initElements(driver, this);
		
	}
	
	
	@FindBy(xpath = "//select[contains(@id,'LANGUAGE')]")
	public WebElement emailLang;
	
	@FindBy(xpath = "//input[contains(@id,'_SUBJECT')]")
	public WebElement subjectLine;
	
	@FindBy(xpath = "//table//span")
	public List<WebElement> orderDetails;
	
	@FindBy(xpath = "//p[contains(text(),'Please click anywhere on the preview below to copy')]")
	public WebElement emailMsg;
	
	@FindBy(xpath = "//font[normalize-space()='Your order will be Billed to']//parent::td")
	public WebElement billToAdd;
	
	@FindBy(xpath = "//font[normalize-space()='Your Order will be Shipped to']//parent::td")
	public WebElement shipToAdd;
	
	@FindBy(xpath = "(//table[@id='imprint_preview_div']//td)[1]")
	public WebElement imprintPre;
	
	@FindBy(xpath = "//table[@id='imprint_preview_div']//img")
	public WebElement productImg;
	
	@FindBy(xpath = "//font[@color='white'][normalize-space()='Total']//parent::td//following-sibling::td")
	public WebElement totalprice;
	
	@FindBy(xpath = "//i[contains(text(),'Please refer to the attached PDF which will show y')]")
	public WebElement onlyLogoInst;
	
	@FindBy(xpath = "//i[contains(text(),'This product will NOT have personalised imprint or')]")
	public WebElement noLogoImp;
	
	
	//sm
	
		@FindBy(xpath = "//font[@color='white'][normalize-space()='Subtotal']//parent::td//following-sibling::td")
		public WebElement subtotalprice;
		
		@FindBy(xpath = "//font[@color='white'][normalize-space()='Tax']//parent::td//following-sibling::td")
		public WebElement tax;
		

			//Order details table
			
			@FindBy(xpath = "(//b[text()='YOUR ORDER DETAILS']//ancestor::table//following-sibling::table)[1]//tr[position()>2]//td[2]")
			public List<WebElement> productDescription;
			
			@FindBy(xpath = "(//b[text()='YOUR ORDER DETAILS']//ancestor::table//following-sibling::table)[1]//tr[position()>2]//td[3]")
			public List<WebElement> productPrice;
			
			@FindBy(xpath = "(//b[text()='YOUR ORDER DETAILS']//ancestor::table//following-sibling::table)[1]//tr[position()>2]//td[4]")
			public List<WebElement> productQuantity;
			
			@FindBy(xpath = "(//b[text()='YOUR ORDER DETAILS']//ancestor::table//following-sibling::table)[1]//tr[position()>2]//td[5]")
			public List<WebElement> lineTotal;
			
			public WebElement getProductQty(String description) {
				return BaseClass.getDriver().findElement(By.xpath("(//b[text()='YOUR ORDER DETAILS']//ancestor::table//following-sibling::table)[1]//tr[position()>2]//td[2][contains(text(),'"+description+"')]//following-sibling::td[2]"));
			
				
			}
			
			public WebElement getProductPrice(String description) {
				return BaseClass.getDriver().findElement(By.xpath("(//b[text()='YOUR ORDER DETAILS']//ancestor::table//following-sibling::table)[1]//tr[position()>2]//td[2][contains(text(),'"+description+"')]//following-sibling::td[1]"));
			}
			
			public WebElement getProductTotal(String description) {
				return BaseClass.getDriver().findElement(By.xpath("(//b[text()='YOUR ORDER DETAILS']//ancestor::table//following-sibling::table)[1]//tr[position()>2]//td[2][contains(text(),'"+description+"')]//following-sibling::td[3]"));
			}

			//sm
			
	
	
	
	
	
	
}
