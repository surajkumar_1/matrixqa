package pages;
import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ManageServiceRequestsPage {

	public ManageServiceRequestsPage(WebDriver driver) {

		PageFactory.initElements(driver, this);

	}
	
	
	@FindBy(xpath = "//h2[contains(@id,'R373906876543699066_heading')]")
	public WebElement heading;
	
	@FindBy(xpath = "//input[contains(@id,'P60_ORDER_NUMBER')]")
	public WebElement orderNoField;
	
	@FindBy(xpath = "//input[contains(@id,'P60_CUSTOMER_NUMBER')]")
	public WebElement customerNoField;
	
	@FindBy(xpath = "//input[contains(@id,'P60_SR_NUMBER')]")
	public WebElement srNoField;
	
	@FindBy(xpath = "//input[contains(@id,'P60_ASSIGNEE')]")
	public WebElement actionAssigneeField;
	
	
	@FindBy(xpath = "//button[contains(@id,'B332777443362797720')]")
	public WebElement searchBtn;
	
	@FindBy(xpath = "//button[contains(@id,'B332777859845797721')]")
	public WebElement clear;
	
	@FindBy(xpath = "//div[contains(@id,'t_Alert_Notification')]//a")
	public WebElement errorMsg;
	
	
	
	// Search Results
	
	@FindBy(xpath = "//span[contains(@class,'fa fa-plus-square fa-lg')]")
	public WebElement createSR;
	
	
	@FindBy(xpath = "//table[contains(@id,'_orig')]")
	public WebElement searchResultTable;
	
	@FindBy(xpath = "//table[contains(@id,'_orig')]//td[4]")
	public WebElement customerNo;
	
	@FindBy(xpath = "//table[contains(@id,'_orig')]//td[1]")
	public WebElement orderNo;
	
	@FindBy(xpath = "//table[contains(@id,'_orig')]//td[10]")
	public WebElement srNo;
	
	@FindBy(xpath = "//table[contains(@id,'_orig')]//td[10]//a[contains(@href,'SR')]")
	public List<WebElement> srNumbers;
	
	@FindBy(xpath = "//table[contains(@id,'_orig')]//td[11]")
	public WebElement srType;
	
	@FindBy(xpath = "//table[contains(@id,'_orig')]//td[13]")
	public WebElement problemCode;
	
	@FindBy(xpath = "//button[contains(@id,'actions_button')]")
	public WebElement actionsBtn;
	
	@FindBy(xpath = "//span[contains(@class,'a-IRR-pagination-label')]")
	public WebElement actionPagination;
	
	
	
	
	
	
	
	



}
