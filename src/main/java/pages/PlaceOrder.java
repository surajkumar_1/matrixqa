package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PlaceOrder {

	public PlaceOrder(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//input[contains(@id,'_CUSTOMER_NUMBER')]")
	public WebElement customerNo;

	@FindBy(xpath = "//input[contains(@id,'_PCT_NUMBER')]")
	public WebElement pctNo;

	@FindBy(xpath = "//input[contains(@id,'_CUSTOMER_NAME')]")
	public WebElement customerName;

	@FindBy(xpath = "//input[contains(@id,'_PHONE_NUMBER')]")
	public WebElement phoneNo;

	@FindBy(xpath = "//input[contains(@id,'_POSTAL_CODE')]")
	public WebElement postalCode;

	@FindBy(xpath = "//button[@id='search']")
	public WebElement searchBtn;

	@FindBy(xpath = "//span[normalize-space()='Clear Search']/parent::button")
	public WebElement clearBtn;

	@FindBy(xpath = "//td[@headers='CUSTOMER_NUMBER']")
	public WebElement customerNoLink;

	@FindBy(xpath = "//td[text()='Prospect']//following-sibling::td[@headers='PLACE_ORDER']")
	public WebElement prospect;
	
	@FindBy(xpath = "//tr[@id='prim-selected']/td[@headers='PLACE_ORDER']")
	public WebElement primeAddPlaceOrder;

	@FindBy(xpath = "//span[text()='Continue']//parent::button")
	public WebElement contiNoEmail;

	@FindBy(xpath = "//span[normalize-space()='Cancel']/parent::button")
	public WebElement cancelBtn;

	@FindBy(xpath = "//span[normalize-space()='Click Here to Proceed with the Order']//parent::button")
	public WebElement proceedWithOrderBtn;

	@FindBy(xpath = "//select[contains(@id,'_REASON')]")
	public WebElement selectReason;

	@FindBy(xpath = "//span[normalize-space()='Proceed']/parent::button")
	public WebElement proceedBtn;
		

}
