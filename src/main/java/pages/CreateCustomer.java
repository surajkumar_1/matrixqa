/*
 * Author- Suraj.kumar@pens.com
 * 
 */
package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CreateCustomer {

	public CreateCustomer(WebDriver driver) {

		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//span[@id='P0_TIMER']")
	public WebElement timer;

	@FindBy(xpath = "//span[@class='u-Processing']")
	public WebElement smallprocessEle;
	
	@FindBy(xpath = "//span[@class='u-Processing-spinner']")
	public WebElement bigprocessEle;
	
	/*
	 * Add New Customer
	 */
	@FindBy(xpath = "//select[contains(@id,'_MASTER_COUNTRY')]")
	public WebElement selectCountry;

	@FindBy(xpath = "//input[contains(@id,'_CUSTOMER_NAME')]")
	public WebElement customerName;

	@FindBy(xpath = "//input[contains(@id,'_TAX_REG_NO')]")
	public WebElement taxRegistrationNum;

	@FindBy(xpath = "//input[contains(@id,'_TAX_REG_NO')]")
	public WebElement profileClassDisplay;

	@FindBy(xpath = "//select[contains(@id,'_TAX_CODE')]")
	public WebElement taxCode;

	/*
	 * Contact Details
	 * 
	 */

	@FindBy(xpath = "//select[contains(@id,'_CONTACT_TITLE')]")
	public WebElement contactTitle;

	@FindBy(xpath = "//input[contains(@id,'_CONTACT_FIRST_NAME')]")
	public WebElement contactFirstName;

	@FindBy(xpath = "//input[contains(@id,'_CUSTOMER_INVOICE_EMAIL')]")
	public WebElement customerEmailAddress;
	
    @FindBy(xpath = "//select[contains(@id,'_REASON_FOR_NO_EMAIL')]")
	public WebElement selectReasonNoEmail;
    
	@FindBy(xpath = "//input[contains(@id,'_CONTACT_LAST_NAME')]")
	public WebElement contactLastName;

	@FindBy(xpath = "//input[contains(@id,'_CONTACT_PHONE_NUM')]")
	public WebElement contactPhone;

	@FindBy(xpath = "//input[contains(@id,'_CONTACT_MOBILE')]")
	public WebElement contactMobile;

	@FindBy(xpath = "//label[contains(@id,'_FAX_LABEL')]//following::input[contains(@id,'_FAX')]")
	public WebElement contactFax;

	@FindBy(xpath = "//input[contains(@id,'_CONTACT_PHONE_PREF')]")
	public WebElement countryCodePhone;

	@FindBy(xpath = "//input[contains(@id,'_MOBILE_PHONE_PREF')]")
	public WebElement countryCodeMobile;

	@FindBy(xpath = "//input[contains(@id,'_FAX_PREF')]")
	public WebElement countryCodeFax;

	/*
	 * BillTo
	 */

	@FindBy(xpath = "//span[normalize-space()='Same For ShipTo']//parent::button")
	public WebElement sameForShipToBtn;

	@FindBy(xpath = "//span[normalize-space()='Clear']//parent::button")
	public WebElement billToClear;

	@FindBy(xpath = "//select[contains(@id,'_BILL_COUNTRY')]")
	public WebElement billToCountry;

	@FindBy(xpath = "//selectcontains(@id,'_BILL_INVOICE_LANG')]")
	public WebElement billToInvoice;

	@FindBy(xpath = "//input[contains(@id,'_BILL_LINE1')]")
	public WebElement billToALOne;

	@FindBy(xpath = "//input[contains(@id,'_BILL_LINE2')]")
	public WebElement billToALTwo;

	@FindBy(xpath = "//input[contains(@id,'_BILL_CITY')]")
	public WebElement billToCity;

	@FindBy(xpath = "//input[contains(@id,'_BILL_STATE')]")
	public WebElement billToState;
	
	@FindBy(xpath = "//select[contains(@id,'_BILL_STATE_USA')]")
	public WebElement selectBillToState;
	
	@FindBy(xpath = "//input[contains(@id,'_BILL_PROVINCE')]")
	public WebElement billToProvince;
	
	@FindBy(xpath = "//select[contains(@id,'_BILL_PROVINCE_1')]")
	public WebElement selectBillToProvince;
	
	@FindBy(xpath = "//input[contains(@id,'_BILL_COUNTY')]")
	public WebElement billToCounty;

	@FindBy(xpath = "//input[contains(@id,'_BILL_POSTALCODE')]")
	public WebElement billToPostalCode;

	/*
	 * ShipTo
	 */

	@FindBy(xpath = "(//span[normalize-space()='Clear']//parent::button)[2]")
	public WebElement shipToClear;

	@FindBy(xpath = "//select[contains(@id,'_SHIP_COUNTRY')]")
	public WebElement shipToCountry;

	@FindBy(xpath = "//select[contains(@id,'_SHIP_INVOICE_LANG')]")
	public WebElement shipToInvoice;

	@FindBy(xpath = "//input[contains(@id,'SHIP_LINE1')]")
	public WebElement shipToALOne;

	@FindBy(xpath = "//input[contains(@id,'_SHIP_LINE2')]")
	public WebElement shipToALTwo;

	@FindBy(xpath = "//input[contains(@id,'_SHIP_CITY')]")
	public WebElement shipToCity;

	@FindBy(xpath = "//input[contains(@id,'_SHIP_STATE')]")
	public WebElement shipToState;
	
	@FindBy(xpath = "//select[contains(@id,'_SHIP_STATE_US')]")
	public WebElement selectShipToState;

	@FindBy(xpath = "//input[contains(@id,'_SHIP_PROVINCE')]")
	public WebElement shipToProvince;

	@FindBy(xpath = "//select[contains(@id,'_SHIP_PROVINCE_1')]")
	public WebElement selectShipToProvince;
	
	@FindBy(xpath = "//input[contains(@id,'_SHIP_COUNTY')]")
	public WebElement shipToCounty;

	@FindBy(xpath = "//input[contains(@id,'_SHIP_POSTALCODE')]")
	public WebElement shipToPostalCode;

	@FindBy(xpath = "//button[@id='create_customer']")
	public WebElement createCustomerBtn;
	
	@FindBy(xpath = "//button[text()='OK']//parent::button")
	public WebElement okBtn;

}
