package base;


import java.io.IOException;
import java.time.Duration;

import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Function;

import helper.ActionHelper;
import helper.AlertHandle;
import helper.ExceptionHandler;
import helper.JavaScriptExecute;
import helper.SelectHelper;
import helper.WebAction;
import pages.AccessoriesOnly;
import pages.CopyOrder;
import pages.CreateCustomer;
import pages.FaxPreviewPage;
import pages.HomePage;
import pages.KeyCodeProduct;
import pages.Login;
import pages.ManageCustomer;
import pages.ManageOrder;
import pages.ManageServiceRequestsPage;
import pages.OrderConfirmation;
import pages.PlaceOrder;
import pages.PreviewDatailsPage;
import pages.ProductSearch;
import pages.RedoOrder;
import pages.ServiceRequestPage;
import utilities.GetDataExcel;
import utilities.ReadConfig;

public class BaseClass {

	public static WebAction webactions;
	private static WebDriver driver;
	public static AlertHandle alert;
	public static ReadConfig config;
	public static GetDataExcel excel;
	public static JavaScriptExecute js;
	public static SelectHelper select;
	public static ActionHelper action;
	public static WebDriverWait wait;
	@SuppressWarnings("rawtypes")
	public static FluentWait fwait;
	public static AccessoriesOnly accOnly;
	public static CreateCustomer createCustomer;
	public static HomePage homePage;
	public static KeyCodeProduct CustProd;
	public static Login login;
	public static ManageCustomer manageCustomer;
	public static ManageOrder manageOrder;
	public static OrderConfirmation orderconf;
	public static PlaceOrder placeOrder;
	public static ProductSearch productSearch;
	public static CopyOrder copyOrder;
	public static RedoOrder redoOrder;
	public static PreviewDatailsPage emailPre;
	public static FaxPreviewPage faxPre;
	public static ManageServiceRequestsPage manageSR;
	public static ServiceRequestPage sr;
    public static ExceptionHandler exceptionHandle;
	public static void initDriver() throws IOException {
		config = new ReadConfig();
		switch (config.getBrowser().toUpperCase()) {
		case "FIREFOX":
			System.setProperty("webdriver.gecko.driver", config.getFirefoxPath());
			driver = new FirefoxDriver();		
			break;
		case "CHROME":
			System.setProperty("webdriver.chrome.driver", config.getChromePath());
			driver = new ChromeDriver();
			break;
		default:
			throw new RuntimeException("Please check the value of browser entered by User in Config.Properties file");
		}
		select = new SelectHelper();
		alert = new AlertHandle();
		excel = new GetDataExcel(config.getExcelPath());
		login = new Login(BaseClass.getDriver());
		createCustomer = new CreateCustomer(BaseClass.getDriver());
		copyOrder = new CopyOrder(BaseClass.getDriver());
		manageCustomer = new ManageCustomer(BaseClass.getDriver());
		manageOrder = new ManageOrder(BaseClass.getDriver());
		action = new ActionHelper(BaseClass.getDriver());
		homePage = new HomePage(BaseClass.getDriver());
		accOnly = new AccessoriesOnly(BaseClass.getDriver());
		orderconf = new OrderConfirmation(BaseClass.getDriver());
		placeOrder = new PlaceOrder(BaseClass.getDriver());
		productSearch = new ProductSearch(BaseClass.getDriver());
		CustProd = new KeyCodeProduct(BaseClass.getDriver());
		emailPre = new PreviewDatailsPage(BaseClass.getDriver());
		redoOrder = new RedoOrder(driver);
		 faxPre = new FaxPreviewPage(driver);
		 manageSR=new ManageServiceRequestsPage(driver);
		 sr=new ServiceRequestPage(driver);
		webactions = new WebAction();
		exceptionHandle = new ExceptionHandler();
		js = new JavaScriptExecute(BaseClass.getDriver());
		wait = new WebDriverWait(driver, Duration.ofSeconds(60));
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(60));
		driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(120));
		fwait = new FluentWait<WebDriver>(BaseClass.getDriver()).withTimeout(Duration.ofSeconds(60))
				.pollingEvery(Duration.ofSeconds(20)).ignoring(StaleElementReferenceException.class);
		
		driver.manage().window().maximize();
	
		

	}

	public static WebElement fluentWaitForStaleElement(final WebElement element) {
		wait.until(new Function<WebDriver, WebElement>() {
			public WebElement apply(WebDriver driver) {
				if (element.isDisplayed()) {
					return element;
				} else {
					System.out.println("FluentWait Failed");
					return null;
				}
			}
		});

		return element;

	}

	public static WebDriver getDriver() {
		return driver;
	}

	public static void teardown() {
		driver.quit();
	}

	public static void pageRefresh() {
		driver.navigate().refresh();
	}
}